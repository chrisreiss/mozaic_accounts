<?php
	session_start();
	require_once("cDatabase.php");

	// This shouldn't happen - if so then a session variable hasn't saved
	if (!isset($_SESSION['challenge']))
	{
		print_r($_SESSION);
		die('Could not retrieve the challenge code (session lost)');
	}
	
	// Connect to the shared administative database
	Database::Connect("mozaic");
	$password = Database::QueryGetResult("SELECT password FROM users WHERE userid='".$_POST['userid']."';");
	if ($_POST['response']!=md5($_SESSION['challenge'].$password['password']))
	{
		header("Location:login.php?error=2");
	}
	else
	{
		
		// give the browser a cookie and session id
		$secret = "s7d9jgl3i4o";
		$sessionkey = md5($_POST['userid'].time().$secret); //session_id(); 

		$user = Database::QueryGetResult("SELECT lastlogin,displayname,accountid,email FROM users WHERE userid='".$_POST['userid']."' LIMIT 1;");

		print_r($user);

		$account = Database::QueryGetResult("SELECT * FROM accounts WHERE uid='".$user['accountid']."';");
		if ($account['status']!='S')
		{
			$lastlogin = $user['lastlogin'];
			
			// store the session id on the browser
//			setcookie("sessionkey",$sessionkey,time()+(60*30)); 

			$_SESSION['sessionkey'] 	= $sessionkey;			
			$_SESSION['displayname'] 	= $user['displayname'];
			$_SESSION['email'] 			= $user['email'];
			$_SESSION['accountid'] 		= $user['accountid'];
			$_SESSION['accountname']    = $account['name'];
			$_SESSION['database'] 		= $account['database'];
			$_SESSION['site'] 			= $account['site'];
			$_SESSION['packages']		= explode(',',$account['packages']);
			$_SESSION['userid'] 		= $_POST['userid'];
			$_SESSION['transactionid'] 	= 0;
			$_SESSION['currencydenomination'] = $account['currencydenomination'];
			$_SESSION['timeout']		= time()+3600;
			$_SESSION['defaultminpayout']	= $account['defaultminpayout'];
		//	session_write_close();

			if ($account['superuser']==$_POST['userid']) $_SESSION['superuser']=true; else $_SESSION['superuser']=false;
			
			// store session in database
			Database::Query("UPDATE users SET sessionkey='".$sessionkey."', lastlogin='".date('Y-m-d H:i:s')."', lastactivity='".date('Y-m-d H:i:s')."' WHERE userid='".$_POST['userid']."';");
		}

		$_POST['page']='home';
		$site = "v3"; //$_SESSION['site'];
		if ($site!='') $site.='/';
		if ($_SERVER['SERVER_NAME']=="local.mozaic")
			header("location:http://local.mozaic/".$site."index.php");
		else
			header("location:https://cyphon.com/".$site."index.php");
		
	}
?>
