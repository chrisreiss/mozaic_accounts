
<?php	

	header("location:http://www.mozaicaccounts.com/v3");

	session_start();
	
	ini_set("include_path", "./:./classes:./thirdparty:./localise");
//	ini_set('display_errors', 'On');
//	error_reporting(E_ALL);
	require_once("headerfooter.php");

	if ($_SERVER['SERVER_NAME']!='localhost')
		$rootpath = 'http://www.mozaicaccounts.com';
	else
		$rootpath = 'http://localhost:8888/MozaicAccountsLive/public_html';
	
//	header('Location:'.$rootpath.'/login?error=5');


	if (session_id()!=$_SESSION['sessionkey'])
		header('Location:'.$rootpath.'/login.php?error=5');

	if ($_SESSION['timeout']<time())
		header('Location:'.$rootpath.'/login.php?error=4');
	
	$_SESSION['timeout'] = time()+3600;
		
	$firsttime = false;
	if (empty($_POST['page']))
	{
		if (empty($_GET['page']))
		{
			$page="home";
			$firsttime = true;
		}
		else
			$page=$_GET['page'];
	}
	else
	{
		$page = $_POST['page'];
	}
	
/*	if (!empty($_SESSION))
	{
		if ($_SESSION['site']!="") 
		{
			if ($_SERVER['SCRIPT_FILENAME']!='/home/szirtest/public_html/'.$_SESSION['site'].'/index.php')
			{
	//			print $_SERVER['SCRIPT_FILENAME'];
				header("Location:http://www.mozaicaccounts.com/".$_SESSION['site']."/index.php");
				exit(0);
			}
		}
	}
*/	

	displayHeader();
	
	if (!empty($_SESSION))
	{

		displayMenuBar();
		displayStartDynamic();
		print "	<!DYNAMIC CONTENT OF PAGE ".$page." BEGINS HERE>\n";
		
		$fullpage = 'pages/'.$page.".php";
		if (file_exists($fullpage))
		{
			include $fullpage;
		}
		else
		{
			$fullpage = 'pages/'.$page.".htm";
			if (file_exists($fullpage))
				include $fullpage;
			else
			{
				print "Cannot find requested page $page";
			}
		}
		displayEndDynamic();
	}
	else
	{
		include "present.php";
	}
	displayFooter();
?>
