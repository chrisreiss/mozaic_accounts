<?php
	ini_set("include_path", "./:./classes:./thirdparty:./localise");
	
	require_once('cAuthorize.php');
	require_once('cDataTable.php');
	require_once('cTableInfo.php');

	session_start();
	if (!Authorize::IsAuthorized())
	{
		print "Not logged in";
		exit();
	}

	$handler = $_GET['handler'];
	$id = $_GET['id'];
	if ($id=="undefined") { exit(); }

	Database::Connect();
//	print_r($_SESSION);
	$response = "<table class='preview'>";
	
	switch($handler)
	{
		case "getartistinfo":
		{
			if ($id[0]=="U") 
				$artistid = substr($id,1);
			else
				$artistid = DataTable::GetFieldFromTable("bandartist",$id,"artist");

			$fields = TableInfo::GetFields("artist",$artistid,"realname,address1,address2,city,postcode,country");
			foreach ($fields as $key=>$value)
			{
				$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
			$laststatement = Database::QueryGetResult("SELECT enddate FROM artiststatements WHERE artist='$artistid' ORDER BY enddate DESC LIMIT 1;");
			if ($laststatement!==false)
			{
				$response .= "<tr><td>Accounted To:</td><td>".displaydate($laststatement['enddate'])."</td></tr>";
			}
			break;			
		}
		case "getdistributorinfo":
		{
			$fields = TableInfo::GetFields("partners",$id,"address1,address2,city,postcode,country,phone");
			if ($fields!=false)
			{
				foreach ($fields as $key=>$value)
				{
					$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
				}
			}
			break;			
		}
		case "getproductinfo":
		{
			$fields = TableInfo::GetFields("product",$id,"title,barcode,releasedate,format,comments");
			foreach ($fields as $key=>$value)
			{
				$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
			$result = Database::QueryGetResult("SELECT COUNT(track) FROM tracksproduct WHERE product='".$id."'");
			$response .= "<tr><td>Num. Tracks:</td><td>".$result['COUNT(track)']."</td></tr>";
			break;
		}
		case "getdealinfo":
		{
//			$response .= "<tr><td>UID:</td><td>$id</td></tr>";
			$crossdeal = Database::QueryGetResult("SELECT crossdealid FROM deals WHERE uid='$id';");
			$fields = TableInfo::GetFields("deals",$id,"product,band,startdate,enddate,dealtype,advance,reserve,packagingdeductions,minpayout,crossdealid");
			foreach ($fields as $key=>$value)
			{
				if ($key=="End Date" && $value=="00-00-0000")
					$response .= "<tr><td>".$key.":</td><td>[IN PERPETUITY]</td></tr>";
				else if ($key!="Cross Calculate With")
					$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
			$dealterm = Database::QueryGetResult("SELECT rate FROM dealterms WHERE dealid='".$crossdeal['crossdealid']."';");
			$response .= "<tr><td>Rate:</td><td>".$dealterm['rate']."%</td></tr>";
			$list = "NONE";
			$deal = Database::QueryGetResult("SELECT GROUP_CONCAT(product),COUNT(product) FROM deals WHERE crossdealid='".$fields['Cross Calculate With']."' GROUP BY crossdealid;");
			if ($deal!=false && $deal['COUNT(product)']>1)
			{
				$list = $deal['GROUP_CONCAT(product)'];
				$list = str_replace(",",", ",$list);
			}		
			$response .= "<tr><td>Cross Calculate With:</td><td>".$list."</td></tr>";
			break;
		}
		case "getexpenseinfo":
		{
			$fields = TableInfo::GetFields("expenses",$id,"product,invoicedate,type,description,invoice,amount,paiddate,comments");
			foreach ($fields as $key=>$value)
			{
				$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
			break;
		}
		case "gettrackinfo":
		{
			$fields = TableInfo::GetFields("tracks",$id,"uid,title,artist,version,remixer,publisher,tracklength,comments");
			foreach ($fields as $key=>$value)
			{
				$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
			$products = Database::QueryGetResults("SELECT DISTINCT product FROM tracksproduct WHERE track='".$id."';");
			if ($products!=false)
			{
				foreach ($products as $index=>$product)
				{
					if ($index==0)
						$response .= "<tr><td>Included in:</td>";
					else
						$response .= "<tr><td></td>";
						
					$response .= "<td>".$product['product']."</td></tr>";
				}
			}
			break;
		}
		case "getlicenceinfo":
		{
			$fields = TableInfo::GetFields("licences",$id,"description,licencetype,advance,advancepaiddate,royaltyrate,datestatement,comments");
			foreach ($fields as $key=>$value)
			{
				$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
//			$response .= "<tr><td>Next Statement:</td><td>".."</td></tr>";
			break;
		}
		case "getsalesinfo":
		{
			$fields = TableInfo::GetFields("sale",$id,"date,product,track,distributor,quantity,value,baseprice");
			foreach ($fields as $key=>$value)
			{
				$response .= "<tr><td>".$key.":</td><td>".$value."</td></tr>";
			}
//			$response .= "<tr><td>Next Statement:</td><td>".."</td></tr>";
			break;
		}
		default:
		{
			print "Error : Unknown handler";
			exit();
		}
	}
	
	$response .= "</table>";
	print $response;
?>