<?php

require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cStatement.php');
require_once( 'cValidatedForm.php');
require_once( 'dbexport.php');
require_once( 'cMail.php' );
require_once( 'cReports.php' );
require_once('cCurrency.php');
require_once('strings.php');
require_once('cEmail.php');
require_once('cWindow.php');

Database::Connect("szirtest_mozaic");

$aspect = new Aspect;
$aspect->Start("home","VIEW");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
	//		$win = new InfoWindow(400,400);
			
			$list = new TableList;
			$list->Begin("messageid","home");
			///print $_SESSION['userid'];
			$mails = MailManager::GetMails($_SESSION['userid']);
			if (!empty($mails))
			{
				foreach ($mails as $mail)
				{
					$list->AddItem($mail['group'],$mail['uid'],displaydate($mail['sentdate'])." : ".$mail['subject']);
				}
			}
			$list->AddAction("READ MAIL",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_HOME_READMAIL);		
			$list->AddAction("REPORTS",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_HOME_REPORTS);		
			if ($_SESSION['superuser']==true || $_SESSION['userid']=="tom") 
			{
				$list->AddAction("EDIT PROFILE",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_HOME_EDITPROFILE);
				$list->AddAction("MANAGE USERS",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_HOME_MANAGEUSERS);
			}
			if ($_SESSION['userid']=="tom" || $_SESSION['userid']=="robertt")
			{
				$list->AddAction("CREATE DEMO ACCOUNT",false);
			}

			$list->AddAction("CHANGE PASSWORD",false);
			$aspect->Attach($list);			
	//		$aspect->Attach($win);			
			$aspect->Present();
			break;
		}
		case "CREATE DEMO ACCOUNT":
		{
			$form = new ValidatedForm("users","displayname,userid,email");
			$form->AddAction("SUBMIT ACCOUNT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT ACCOUNT":
		{
			$aspect->SetNextAction("VIEW");
			// create password
			$password = md5($aspect->GetVar('userid'));
			$password = substr($password,0,8);
			// create acc
			$aspect->SetVar('password',$password);
			$aspect->SetVar('accountid',3);
			DataTable::InsertFromPost("users","displayname,userid,email,password,accountid",$aspect);
			// send mail
			$body  = "Dear ".$aspect->GetVar('displayname')."\n";		  
			$body .= "We have created a demo account for our label management and royalty accounting system MozaicAccounts.com\n";
			$body .= "This account will let you try out some of the features of our system. You will not be able to modify or save data using this login.\n";
			$body .= "If you have any questions regarding usage of the system please do get in contact.\n\n";
			$body .= "Your login details are below:\n";
			$body .= 'URL: http://www.mozaicaccounts.com/'."\n";
			$body .= 'User ID: '.$aspect->GetVar('userid')."\n";
			$body .= 'Password: '.$aspect->GetVar('password')."\n";
			Email::Send($aspect->GetVar('email'), $body, "Mozaic Accounts Demo", "support@mozaicaccounts.com", "Mozaic Accounts Support", false);
			
			$body = "This is an automated email to tell you that a demo account was created for ".$aspect->GetVar('displayname')." and a mail was sent to ".$aspect->GetVar('email')."\n";
			$body .= 'User ID: '.$aspect->GetVar('userid')."\n";
			$body .= 'Password: '.$aspect->GetVar('password')."\n";
			Email::Send($_SESSION['email'], $body, "Mozaic Accounts Demo", "support@mozaicaccounts.com", "Mozaic Accounts Support", false);
			Email::Send("support@mozaicaccounts.com", $body, "Mozaic Accounts Demo", "support@mozaicaccounts.com", "Mozaic Accounts Support", false);
	//		print_r($_SESSION);
			break;
		}
		case "REPORTS":
		{
			Reports::_SalesChart();
			$form = new Form;
			$form->Begin("home");
			$form->AddAction("BACK");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CHANGE PASSWORD":
		{
		  $form = new Form;
		  $form->Begin("home");
		  //	function AddField($descrip,$name,$template,$value)
		  $form->AddField("Old Password","oldpasswd","password","");
		  $form->AddField("New Password","passwd1","password","");
		  $form->AddField("Re-type Password","passwd2","password","");
		  $form->AddAction("SUBMIT PASSWORD");
		  $form->AddCancel();
		  $aspect->Attach($form);
		  $aspect->Present();
      break;
    }
    case "SUBMIT PASSWORD":
    {
      $aspect->SetNextAction("VIEW");
      
      Database::Connect("szirtest_mozaic");
      $password = Database::QueryGetResult("SELECT password FROM users WHERE userid='".$_SESSION['userid']."';");
//      $password = md5($password['password']);
      if ($password['password'] != $aspect->GetVar('oldpasswd'))
      {
	      $aspect->Error("Wrong password, please try again");
        $aspect->SetNextAction("CHANGE PASSWORD");
        break;
    	}
      if ($aspect->GetVar('passwd1')!=$aspect->GetVar('passwd2'))
      {
        $aspect->Error("New password did not match in both fields");
        $aspect->SetNextAction("CHANGE PASSWORD");
        break;
      }
      $passwd = $aspect->GetVar('passwd1');
      if (strlen($passwd)<8 || strlen($passwd)>16)
      {
        $aspect->Error("Please make sure password is between 8 and 16 characters in length");
        $aspect->SetNextAction("CHANGE PASSWORD");
        break;
      }
      if ($passwd==$aspect->GetVar('oldpasswd'))
      {
        $aspect->Error("Password did not change");
        $aspect->SetNextAction("CHANGE PASSWORD");
        break;
      }
      
      Database::Query("UPDATE users SET password='".$passwd."' WHERE userid='".$_SESSION['userid']."' LIMIT 1;");
      break;
    }
		case "READ MAIL":
		{
			$mail = MailManager::Get($aspect->GetVar('messageid'));
			print "<table class='mail'><tr><td>";
			print "<b>Subject : </b>".$mail->GetSubject()."<br>";
			print "<b>Date : </b>".displaydate($mail->GetDate())."<br>";
			print "<br>".$mail->GetBody()."<br>";
			if ($mail->GetAction()!==false)
			{
				print "<form method=post action=index.php><input type=hidden name=page value='".$mail->GetActionPage()."'><button class=mail type=submit name=submitvalue value='".$mail->GetAction()."'>GO</button></form>\n";
			}
			print "</td></tr></table>\n";
			$form = new Form;
			$form->Begin("home");
			$form->AddHidden("messageid",$aspect->GetVar('messageid'));
			$form->AddAction("BACK");
			if ($mail->CanDelete()) $form->AddAction("DELETE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
	 	case "DELETE":
		{
			MailManager::Delete($aspect->GetVar('messageid'));
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "EDIT PROFILE":
		{
			$form = new ValidatedForm();
			$form->AddTable("accounts","name,vatrate,currencydenomination");
			$form->AddTable("addresses","address1,address2,city,country,postcode,email,phone");
			$addressid = DataTable::GetFieldFromTable("accounts",$_SESSION['accountid'],"invoiceaddress");
			$form->SetValuesFromDB("accounts",$_SESSION['accountid']);
			$form->SetValuesFromDB("addresses",$addressid);
			$form->AddHidden("invoiceaddress",$addressid);
			// usual stuff
			$form->AddCancel();
			$form->AddAction("SUBMIT PROFILE",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT PROFILE":
		{
			$result = Database::QueryGetResult("SELECT invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."' LIMIT 1;");
			if ($result['invoiceaddress']==0)
			{
				$addressid = DataTable::InsertFromPost("addresses","address1,address2,city,country,postcode,email,phone",$aspect);
				$aspect->SetVar("invoiceaddress",$addressid);
			}
			else
			{
				DataTable::UpdateFromPost("addresses",$aspect->GetVar("invoiceaddress"),"address1,address2,city,country,postcode,email,phone",$aspect);
			}
			
			DataTable::UpdateFromPost("accounts",$_SESSION['accountid'],"name,vatrate,currencydenomination,invoiceaddress",$aspect);
			// update this now - otherwise need to log out and in again
			$_SESSION['currencydenomination'] = $aspect->GetVar('currencydenomination');
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "MANAGE USERS":
		{
			$list = new TableList;
			$list->Begin("userid","home");
			$list->AddTitle($_SESSION['accountname']." users");
			$list->SetUidField("userid");
			$users = Database::QueryGetResults("SELECT userid,displayname FROM szirtest_mozaic.users WHERE accountid='".$_SESSION['accountid']."';");
			foreach ($users as $user)
			{
				if ($user['userid']!="tom")
					$list->AddItem("",$user['userid'],$user['displayname']);
			}
			$list->AddAction("ADD USER",false);		
//			$list->AddAction("EDIT USER",true);
			$list->AddAction("REMOVE USER",true);
			$list->AddAction("MAIL PASSWORD",true);
			$list->SetConfirm("REMOVE USER","Are you sure?");
			$list->AddAction("BACK",false);
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		case "MAIL PASSWORD":
		{
		  $aspect->SetNextAction("MANAGE USERS");
		  $user = Database::QueryGetResult("SELECT email,password,displayname FROM users WHERE userid='".$aspect->GetVar('userid')."' AND accountid='".$_SESSION['accountid']."';");
      $body .= "Dear ".$user['displayname']."\n";		  
      $body .= "This email contains your login information for mozaicaccounts.com. Please keep it safe:\n\n";
      $body .= 'URL: http://www.mozaicaccounts.com/'."\n";
		  $body .= 'User ID: '.$aspect->GetVar('userid')."\n";
		  $body .= 'Password: '.$user['password']."\n";
      Email::Send($user['email'], $body, "Mozaic Account Userinfo", "support@mozaicaccounts.com", "Mozaic Accounts Support", false);

      
      break;
    }
		case "REMOVE USER":
		{
			Database::Query("DELETE FROM users WHERE userid='".$aspect->GetVar("userid")."' AND accountid='".$_SESSION['accountid']."';");
			$aspect->SetNextAction("MANAGE USERS");
			break;
		}
		case "ADD USER":
		{
			$form = new ValidatedForm();
			$form->AddTable("users","userid,password,displayname,email");
			$form->AddCancel("CANCEL USER EDIT");
			$form->AddHidden("accountid",$_SESSION['accountid']);
			$form->AddAction("SUBMIT NEW USER",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW USER":
		{
			DataTable::InsertFromPost("users","userid,password,displayname,email,accountid",$aspect);
			$aspect->SetNextAction("MANAGE USERS");
			break;
		}
		case "SUBMIT USER":
		{
			DataTable::UpdateFromPost("users",$aspect->GetVar('userid'),"userid,password,displayname,email",$aspect);
			$aspect->SetNextAction("MANAGE USERS");
			break;
		}
		case "EDIT USER":
		{
			$form = new ValidatedForm();
			$form->AddTable("users","userid,password,displayname,email");
			$user = DataTable::GetEntryFromIDUsingUID("users",$aspect->GetVar("userid"),"userid");
			$form->SetValues($user);
			$form->AddCancel("CANCEL USER EDIT");
			$form->AddAction("SUBMIT USER",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CANCEL USER EDIT":
		{
			$aspect->SetNextAction("MANAGE USERS");
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}
$aspect->End();
?>
