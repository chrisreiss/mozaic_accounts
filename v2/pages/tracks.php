<?php
require_once( 'util.php' );
require_once( 'cForm.php' );
require_once( 'cList.php' );
require_once( 'cAspect.php' );
require_once('cDatabase.php' );
require_once('cDataTable.php');
require_once('cValidatedForm.php');

Database::Init();

$aspect = new Aspect;
$aspect->Start("tracks","VIEW");

while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$list = new TableList();
			$list->Begin("trackid","tracks");
			$list->AddView("by band","tracks","%title% %version%","artist","title","%artist.bands:name%");
			$list->AddView("by irsc","tracks","%uid% : %artist.bands:name% - %title% (%version)","","uid");
			$list->SetInfoBoxHandler("gettrackinfo");
			$list->SetMultipleSelection(true);
			$list->AddAction("ADD TRACKS",false);
			$list->AddAction("REMOVE TRACKS",true,
							 ACTION_CONFIRM, STRING_DIALOG_CONFIRM_REMOVETRACKS);
			$list->AddAction("EDIT TRACK",true);
			$list->AddAction("ADD LICENCE",true,
							 ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_TRACKS_ADDLICENCE);
			$list->AddAction("SEARCH BY IRSC",false,
							ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_TRACKS_SEARCHBYIRSC);
			$list->Select($aspect->GetVar('trackid'));
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		case "SEARCH BY IRSC":
		{
			print "<p>Search for track by IRSC</p>";
			$form = new Form();
			$form->Begin("tracks");
			$form->AddField("Track IRSC","irsc","irsc","");
			$form->AddAction("SEARCH PROCESS");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SEARCH PROCESS":
		{
			$query = "SELECT * FROM tracks WHERE uid='".$_POST['irsc']."';";
			$result = Database::Query($query);
			if ($result->GetNum()==0)
			{
				print "No Match Found for ".$_POST['irsc'];
			}
			else
			{
				$assoc = $result->GetNext();
				print "<p>Match found for ".$assoc['uid']." : ";
				print $assoc['title']." by ".$assoc['artist']."</p>";
			}
			Database::FinishQuery($result);
			$form = new Form();
			$form->Begin("tracks");
			$form->AddCancel("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE TRACKS":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// tracksproduct table
			// trackid, productid, position
			// Reorder the positions so that they are shifted down
			foreach ($_POST['trackid'] as $trackid)
			{
				$result = Database::Query("SELECT product,position FROM tracksproduct WHERE track='".$trackid."';");
				// Update the product/track numbers
				if ($result!=false)
				{
					for ($i=0;$i<$result->GetNum();++$i)
					{
						$row = $result->GetNext();
						$query = "UPDATE tracksproduct SET position=position-1 WHERE product='".$row['product']."' AND position>".$row['position'].";";
						Database::Query($query);
						print $query."<br>";
					}
					Database::FinishQuery($result);
					
					Database::Query("DELETE FROM tracksproduct WHERE track='".$trackid."';");
				}
				Database::Query("DELETE FROM tracks WHERE uid='".$trackid."';");
			}
			
			break;
		}
		case "EDIT TRACK":
		{
			// Need to make sure that there are no references to this track
			$form = new ValidatedForm("tracks","uid,artist,title,version,remixer,publisher,tracklength,comments");
			$form->SetValuesFromDB("tracks",$_POST['trackid'][0]);
			$form->AddAction("SUBMIT EDIT",true);	// validate before sending
			$form->AddHidden("trackid",$_POST['trackid'][0]);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			$rows = Database::QueryGetResults("SELECT name FROM bands;");
			foreach ($rows as $row) $bands[]=$row['name'];
			print '<script language="javascript" type="text/javascript">'."\n";
			print "AutoComplete_Create('remixer', ['".implode("','",$bands)."'].sort(), 6);\n";
			print '</script>'."\n";

			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			if ($aspect->GetVar('trackid')!=$aspect->GetVar('uid'))
			{
				if (DataTable::GetIDExist("tracks",$aspect->GetVar('uid')))
				{
					Error("That catologue number is already in use. Operation aborted","tracks");
					$aspect->Present();
					break;
				}
				Database::Query("UPDATE tracksproduct SET track='".$aspect->GetVar("uid")."' WHERE track='".$aspect->GetVar("trackid")."';");
				Database::Query("UPDATE sale SET product='".$aspect->GetVar("uid")."' WHERE product='".$aspect->GetVar("trackid")."';");
				Database::Query("UPDATE licences SET licenceduid='".$aspect->GetVar("uid")."' WHERE licencetype='T' AND licenceduid='".$aspect->GetVar("trackid")."';");
			}
			
			DataTable::UpdateFromPost("tracks",$aspect->GetVar('trackid'),"uid,artist,title,version,remixer,publisher,tracklength,comments",$aspect);
			break;
		}
		case "ADD TRACKS":
		{
			if (!isset($_POST['addcount'])) $_POST['addcount']=1;
			print "Track ".$_POST['addcount']."<hr>";
			if ($_POST['addcount']==1)
			{
				$uidresult = Database::QueryGetResult("SELECT uid FROM tracks ORDER BY uid DESC LIMIT 1;");
				$uid = CreateNewCatNumber($uidresult['uid']);
			}
			else
			{
				$uid = CreateNewCatNumber($_POST['uid']);
			}
			$form = new ValidatedForm("tracks","uid,artist,title,version,remixer,publisher,tracklength,comments");
			if ($_POST['addcount']>1) 
				$form->SetValuesFromPost($aspect,"artist,publisher");
			$form->AddHidden('addcount',$_POST['addcount']+1);
//			$form->SetValuesFromPost($aspect);
			$form->SetValue("uid",$uid);
			$form->AddAction("SAVE",true);	// validate before sending
			$form->AddCancel("FINISHED");
			$aspect->Attach($form);
			$aspect->Present();
			
			$rows = Database::QueryGetResults("SELECT name FROM bands;");
			foreach ($rows as $row) $bands[]=$row['name'];
			print '<script language="javascript" type="text/javascript">'."\n";
			print "AutoComplete_Create('remixer', ['".implode("','",$bands)."'].sort(), 6);\n";
			print '</script>'."\n";

			break;
		}
		case "SAVE":
		{
			$aspect->SetNextAction("ADD TRACKS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$result = DataTable::InsertFromPost("tracks","uid,artist,title,version,remixer,publisher,tracklength,comments",$aspect);
			$aspect->SetVar('trackid',$aspect->GetVar('uid'));
			break;
		}
		case "ADD LICENCE":
		{
		
			$form = new ValidatedForm("licences","licenceduid,licencetype,licensor,track,description,advance,royaltyrate,packagingdeductions,reserverforreturns,datestatement,royaltyinterval","licenceduid,licencetype");
			$trackid = $aspect->GetVar('trackid');
			$form->SetValue("licenceduid",$trackid[0]);
			$form->SetValue("licencetype","T");
			$form->AddAction("SUBMIT LICENCE",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT LICENCE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("licences","licensor,licencetype,licenceduid,advance,royaltyrate,packagingdeductions,reserveforreturns,royaltyinterval,description,datestatement",$aspect);
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}

$aspect->End();

?>
