<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cListTable.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cImport.php');
require_once( 'cValidatedForm.php' );
require_once( 'cCurrency.php' );

Database::Init();

$aspect = new Aspect;
$aspect->Start("sales","VIEW");

while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			if (isset($_POST['saleid2']))
				$_POST['selectedgroup']=$_POST['saleid2'];

	//		print_r($_POST);
			
			// Do filter
			$dispfilter = '';
			if (!empty($_POST['filter']))
			{
				$filter = $dispfilter = $_POST['filter'];
				if (strpos($filter,'[')!=FALSE)
					$filter = substr($filter,strpos($filter,'[')+1,strpos($filter,']')-strpos($filter,'[')-1);
			}
			else
				$filter = '';
			if (!empty($_POST['startdate']))
				$startdate = date('Y-m-d',strtotime($_POST['startdate']));
			else
				$startdate = date('Y-01-01',time());
			if (!empty($_POST['enddate']))
				$enddate = date('Y-m-d',strtotime($_POST['enddate']));
			else
				$enddate = '';
			if (!empty($_POST['filterbatchid']))
				$filterbatchid = $_POST['filterbatchid'];
			else
				$filterbatchid = '';

			print "<form method='post' action='index.php'><input type=hidden name='page' value='sales'>PRODUCT <input type=text name='filter' id='filterfield' size=45 value='$dispfilter'> BETWEEN <input type=text name='startdate' size=10 value='$startdate'> AND <input type=text name='enddate' size=10 value='$enddate'> <input type=submit value='FIND'></form>\n";
			$list = new ListTable;
			$list->Begin("saleid","sales");
			if ($filter=='')
				$where = "1";
			else
				$where = "(product='$filter' OR track='$filter')";
			if ($startdate!="") $where .= " AND date>='$startdate'";
			if ($enddate!="") $where .= " AND date<='$enddate'";
			if ($filterbatchid!='') $where .= " AND batchid='$filterbatchid'";
			$list->AddView("by date","sale",$where,"date,product,track,distributor,shop,quantity,value,territory","date");
			$list->SetMultipleSelection(true);
			$list->AddAction("ADD SALES",false);
			$list->AddAction("REMOVE SALES",true);
			$list->AddAction("EDIT SALE",true);
			$list->AddAction("REMOVE MULTIPLE SALES",false);
			$list->AddAction("MANAGE SALES BATCH",false);
	//		$list->AddAction("IMPORT SALES",false);
	//		$list->SetInfoBoxHandler("getsalesinfo");
			// Add total sales
		//	if (isset($_POST['selectedgroup']))
			{
				$totalsales = 0;
				$totalquantity = 0;
				$totalnegquantity = 0;
				$result = Database::QueryGetResults("SELECT quantity,value FROM sale WHERE $where;");
				if (!empty($result))
				{
					foreach($result as $row)
					{
						$totalsales += Currency::GETRAWVALUE($row['value']);
						if ($row['quantity']<0)
							$totalnegquantity += $row['quantity'];
						else
							$totalquantity += $row['quantity'];
					}
				}
				$list->AddFooter(count($result)." rows making ".$totalquantity." sales (".-$totalnegquantity." returns) generating ".Currency::DISPLAY($totalsales));
		
			}
			$aspect->Attach($list);
			
			$aspect->Present();

			
			$tracks = Database::QueryGetResults("SELECT uid,title FROM tracks ORDER BY title");
			if (!empty($tracks)) foreach($tracks as $track) $options[] = str_replace("'","",$track['title'])." [".$track['uid']."]";
			$tracks = Database::QueryGetResults("SELECT uid,title FROM product ORDER BY title");
			if (!empty($tracks)) foreach($tracks as $track) $options[] = str_replace("'","",$track['title'])." [".$track['uid']."]";

			print '<script language="javascript" type="text/javascript">'."\n";
//			print '<!--'."\n";
			print "AutoComplete_Create('filterfield', ['".implode("','",$options)."'].sort(), 6);\n";
//			print '\\-->'."\n";
			print '</script>'."\n";

			break;
		}
		case "MANAGE SALES BATCH":
		{
			$form = new Form();

			$form->AddField("Batch Id","batchid","batchid","");
			$form->AddAction("VIEW BATCH SALES",false);
			$form->AddAction("REMOVE BATCH",false);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE BATCH":
		{
			$aspect->SetNextAction("VIEW");
			if ($aspect->GetVar('batchid')!="")
			{
				Database::Query("DELETE FROM sale WHERE batchid='".$aspect->GetVar('batchid')."';");
			}
			break;
		}
		case "VIEW BATCH SALES":
		{
			$aspect->SetVar('filterbatchid',$aspect->GetVar('batchid'));
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "SCAN AGAIN":
		case "SCAN FOR SALES":
		case "REMOVE MULTIPLE SALES":
		{
			$form = new ValidatedForm();
			$form->AddField("Distributor/Shop","source","creditor",false);
			$form->AddField("Start Date","startdate","date",false);
			$form->AddField("End Date","enddate","date",false);

			// If these don't exist then should just be null
			$form->SetValue("source",$aspect->GetVar("source"));
			$form->SetValue("startdate",$aspect->GetVar("startdate"));
			$form->SetValue("enddate",$aspect->GetVar("enddate"));


			if ($aspect->IsVarSet("source"))
			{
				$sales = Database::QueryGetResults("SELECT * FROM sale WHERE distributor='".$aspect->GetVar("source")."' AND date BETWEEN '".$aspect->GetVar("startdate")."' AND '".$aspect->GetVar("enddate")."';"); 
				$distname = TableInfo::DisplayFromType("creditor",$aspect->GetVar('source'));

				$form->AddAction("SCAN AGAIN",true);
				if (!empty($sales))
				{
					$form->AddAction("DELETE SALES",true);
				}
				$form->AddCancel();
				$aspect->Attach($form);
				$aspect->Present();

				if (!empty($sales))
				{
					print "<p>Found Results:</p><table width=100%>\n";
					print "<tr><td><b>Date</b></td><td><b>Product</b></td><td><b>Track</b></td><td><b>Distributor/Shop</b></td><td><b>Quantity</b></td><td><b>Value</b></td><td><b>Type</b></td></tr>\n";
					foreach($sales as $sale)
					{
						print "<tr><td>".displaydate($sale['date'])."</td><td>".$sale['product']."</td><td>".$sale['track']."</td><td>".$distname."</td><td>".$sale['quantity']."</td><td>".Currency::DISPLAY($sale['value'])."</td><td>".$sale['saletype']."</td></tr>\n";
					}
					print "</table>";
				}
				else
				{
					print "<p>No results found</p>";
				}
			}
			else
			{
				$form->AddAction("SCAN FOR SALES",true);
				$form->AddCancel();
				$aspect->Attach($form);
				$aspect->Present();
			}
			break;
		}
/*		case "SCAN FOR SALES":
		{

			$form = new Form();
			$form->AddHidden("source",$aspect->GetVar("source"));
			$form->AddHidden("startdate",$aspect->GetVar("startdate"));
			$form->AddHidden("enddate",$aspect->GetVar("enddate"));
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}*/
		case "DELETE SALES":
		{
			Database::Query("DELETE FROM sale WHERE distributor='".$aspect->GetVar("source")."' AND date BETWEEN '".$aspect->GetVar("startdate")."' AND '".$aspect->GetVar("enddate")."';"); 
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "ADD SALES":
		{
			if (!isset($_POST['addcount'])) $_POST['addcount']=1;
			print "Add Sales ".$_POST['addcount']."<hr>";
			$form = new ValidatedForm("sale","product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey");
			if (isset($_POST['saleid2'])) $form->SetValue("product",$_POST['saleid2']);

			if ($_POST['addcount']>1) 
				$form->SetValuesFromPost($aspect,"distributor,saletype,date,termkey");
			$form->AddHidden('addcount',$_POST['addcount']);

			$form->SetFilter("product","track");
			$form->SetEventCode("track","onChange=\"ChangeSaleType(this)\"");
			$form->AddAction("SAVE",true);	// validate before sending
			$form->AddCancel("FINISHED");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "EDIT SALE":
		{
		//	print_r($_POST);
			$saleid = $_POST['saleid'][0];
			$form = new ValidatedForm("sale","product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey,batchid");
			$form->SetValuesFromDB("sale",$saleid);
			$form->AddHidden("saleid",$saleid);
			$form->AddHidden("saleid2",$saleid);
			$form->AddAction("SUBMIT SALE",true);	// validate before sending
			$form->SetFilter("product","track");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SAVE":
		{
			$aspect->SetNextAction("ADD SALES");
			// Demo safety check
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			
			DataTable::InsertFromPost("sale","product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey",$aspect);
			$_POST['addcount']+=1;
			break;
		}
		case "SUBMIT SALE":
		{
			$aspect->SetNextAction("VIEW");

			// Demo safety check
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::UpdateFromPost("sale",$aspect->GetVar('saleid'),"product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey,batchid",$aspect);
			break;
		}
		case "REMOVE SALES":
		{
			$aspect->SetNextAction("VIEW");
			// Demo safety check
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			foreach ($_POST['saleid'] as $value)
			{
				DataTable::Delete("sale",$value);
			}
			break;
		}
		case "IMPORT SALES":
		{
			$aspect->SetNextAction("VIEW");
			$form = new Form();
			$form->AddField("Batch Name","batchid","text16","");
			$form->AddField("Spreadsheet File","importfile","file","");
			$salesimporters = Import::EnumerateImports();
			if (!empty($salesimporters))
			{
				$scriptlist = "";
				foreach ($salesimporters as $script)
				{
					$scriptlist .= $script.".imp=".$script.",";
				}
				$form->AddSelectionList("Format","format",$scriptlist,"");
				$form->AddAction("IMPORT",true);	// validate before sending
			}
			else
			{
				$form->AddFixed("Format","format","text16","No Scripts Available");
			}
			$form->AddAction("EDIT IMPORT TEMPLATES",false);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();

			break;
		}
		case "EDIT IMPORT TEMPLATES":
		{
		
			break;
		}
		case "IMPORT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$import = new Import;
			$import->Load($_POST['format']);
			$importfile = "accounts/".$_SESSION['accountid']."/".$_FILES['importfile']['name'];  //import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);
			if (file_exists($importfile))
			{
				unlink($importfile);
			}
			if (move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile)==FALSE)
			{
				die("Unable to upload file. Please inform support.");
      		}
			$form = new Form;
			$fixedvalue = $import->GetFixedValues();
			$_SESSION['query'] = $import->ProcessFile($importfile,$fixedvalue);
			$form->Begin("import");
			$form->AddCancel();
			$form->AddAction("COMMIT VALID DATA");
			$aspect->Attach($form);
			$aspect->Present();
			break;

		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}

$aspect->End();
?>