<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cValidatedForm.php');
require_once( 'strings.php' );
require_once( 'cEmail.php' );

require_once( 'cStatement.php');

// test code
/*require_once('cAccounting.php');

$artiststatement = new ArtistStatement(1,array(1),'2011-01-01','2011-06-30');
die(1);

*/

Database::Init();

$aspect = new Aspect;
$aspect->Start("artists","VIEW");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$list = new TableList;
			$list->Begin("bandartistid","artists");
			$list->AddView("by band","bandartist","%artist.artist:realname%","band","artist","%band.bands:name%");
			$list->AddView("by artist","bandartist","%band.bands:name%","artist","band","%artist.artist:realname%");
			$unassigned = Database::QueryGetResults("SELECT uid,realname FROM artist WHERE uid NOT IN (SELECT DISTINCT artist FROM bandartist);");
			$unassignedignore="~";
			if ($unassigned!=false)
			{
				foreach ($unassigned as $artist)
				{
					// append "U" to front of ID to differentiate these from the assigned artists
					$unassigneduids[] = "U".$artist['uid'];
					$list->AddItem("!NO BAND ASSIGNED!","U".$artist['uid'],$artist['realname']);
				}
				$unassignedignore .= implode(",",$unassigneduids);	// negate these
			}
			$list->SetInfoBoxHandler("getartistinfo");
			$list->AddAction("QUICK ADD ARTISTBAND",false,ACTION_TOOLTIP,"Adds a single producer to the database");
			$list->AddAction("ADD ARTIST",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_NEWARTIST);
			$list->AddAction("ADD BAND",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_NEWBAND);
			$list->AddAction("REMOVE ARTIST",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_REMOVEARTIST);
			$list->SetConfirm("REMOVE ARTIST","This removes the artist completely from the system and all bands that they are a member of. If you just want to remove the artist from the band select EDIT BAND option.");
			$list->AddAction("REMOVE BAND",true,ACTION_ENABLELIST,$unassignedignore,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_REMOVEBAND);
			$list->SetConfirm("REMOVE BAND","Are you sure you want to remove the band? The individual artist(s) will remain on the system for later assignment.");
			$list->AddAction("EDIT ARTIST",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_EDITARTIST);
			$list->AddAction("EDIT BAND",true,ACTION_ENABLELIST,$unassignedignore,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_NEWBAND);
			$list->AddAction("MANAGE STATEMENTS",true,ACTION_ENABLELIST,$unassignedignore,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_MANAGESTATEMENTS);
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		case "QUICK ADD ARTISTBAND":
		{
			print 'This quick form lets you create a producer artist (i.e. one member)<br>';
			$form = new ValidatedForm("artist","realname,email");
			$form->AddField_FromTable("bands","name");
			$form->AddAction("SUBMIT ARTISTBAND",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		} 
		case "SUBMIT ARTISTBAND":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$artistuid = DataTable::InsertFromPost("artist","realname,email",$aspect);
			$banduid = DataTable::InsertFromPost("bands","name",$aspect);
			DataTable::Insert("bandartist","band,artist,split",$banduid,$artistuid,100);
			break;
		}
		case "REMOVE ARTIST":
		{
			$aspect->SetNextAction("VIEW");
							
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// We need to go from bandartist uid to artist uid
			$id = $aspect->GetVar('bandartistid');
			if ($id[0]=="U") 
				$artistid = substr($id,1);
			else
				$artistid = DataTable::GetFieldFromTable("bandartist",$id,"artist");
			DataTable::DeleteMultiple("bandartist","artist='$artistid'");
			DataTable::Delete("artist",$artistid);
			break;
		}
		case "REMOVE BAND":
		{
			$aspect->SetNextAction("VIEW");				

			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$bandid = DataTable::GetFieldFromTable("bandartist",$aspect->GetVar("bandartistid"),"band");
			DataTable::DeleteMultiple("bandartist","band='$bandid'");
			DataTable::Delete("bands",$bandid);
			break;
		}
		case "ADD ARTIST":
		{
			$form = new ValidatedForm("artist","realname,address1,address2,city,postcode,country,email,phone");
			$form->AddAction("SUBMIT NEW ARTIST",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW ARTIST":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("artist","realname,address1,address2,city,postcode,country,email,phone",$aspect);
			break;
		}
		case "ADD BAND":
		{
			$form = new ValidatedForm();
			$form->AddField_FromTable("bands","name");
			$form->StartGroup(0,9,true);
				$form->AddField("Band Member","member","artist",true);
			$form->EndGroup();
			$form->AddAction("SUBMIT NEW BAND",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW BAND":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			$i = 0;
			// check if any band members
			if (ValidatedForm::GetGroupFieldFromPost("member",$i)==false)
			{
				$aspect->Error("Cannot add a band with no members");
				break;
			}

			DataTable::InsertFromPost("bands","name",$aspect);
			$bandid = DataTable::GetID("bands","name",$aspect->GetVar('name'));
			for ($i=0;$i<999;++$i)
			{
				$value = ValidatedForm::GetGroupFieldFromPost("member",$i);
				if ($value==false)
					break;
				$members[] = $value;
			}
			if ($i>0)
			{
				$split = 100 / $i;
				for ($j=0;$j<$i;++$j)
				{
					DataTable::Insert("bandartist","band,artist,split",$bandid,$members[$j],$split);
				}
			}
			break;
		}
		case "EDIT BAND":
		{
			// Bit more complex from usual - because bandartist table references two other tables - band and artist!
			$form = new ValidatedForm();

			// Need to get the band from the bandartist entry the user clicked on
			$bandid = DataTable::GetFieldFromTable("bandartist",$aspect->GetVar('bandartistid'),"band");
			$form->AddField_FromTable("bands","name");
			
			// Get the band name from band table
			$bandname = DataTable::GetFieldFromTable("bands",$bandid,"name");
			$form->SetValue("name",$bandname);
			
			// Then fill out the band members - declare the first then fill the values
			$form->StartGroup(0,9,true);
				$form->AddField("Split","split","percentage",true);
				$form->AddField("Band Member","member","artist",true);
			$form->EndGroup();
		
			// Fill in the band members from the database
			$result = Database::Query("SELECT artist,split FROM bandartist WHERE band='$bandid';");
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				// Set group value will cause these to appear! (I hope)
				$form->SetGroupValue($i,"member",$row['artist']);
				$form->SetGroupValue($i,"split",$row['split']);
			}
			Database::FinishQuery($result);
			
			// usual stuff
			$form->AddAction("SUBMIT EDIT BAND",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddHidden("bandartistid",$aspect->GetVar('artistid'));
			$form->AddHidden("bandid",$bandid);
			$form->AddCancel();
			$form->AddCustomScript("if (!CheckSplit()) {alert('Artist splits do not add up to 100%'); return false;}");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT EDIT BAND":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$bandid = $aspect->GetVar("bandid");
			DataTable::UpdateFromPost("bands",$bandid,"name",$aspect);
			// Wipe all band members from the database
			DataTable::DeleteMultiple("bandartist","band='$bandid'");
			for ($i=0;$i<999;++$i)
			{
				$value = ValidatedForm::GetGroupFieldFromPost("member",$i);
				if ($value==false)
					break;
				$split = ValidatedForm::GetGroupFieldFromPost("split",$i);
				DataTable::Insert("bandartist","band,artist,split",$bandid,$value,$split);
			}
			break;
		}
		case "SUBMIT EDIT ARTIST":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::UpdateFromPost("artist",$aspect->GetVar('artistid'),"realname,address1,address2,city,postcode,country,email,phone",$aspect);
			break;
		}
		case "EDIT ARTIST":
		{
			// We need to go from bandartist uid to artist uid
			$id = $aspect->GetVar('bandartistid');
			if ($id[0]=="U") 
				$artistid = substr($id,1);
			else
				$artistid = DataTable::GetFieldFromTable("bandartist",$id,"artist");
				
			$form = new ValidatedForm("artist","realname,address1,address2,city,postcode,country,email,phone");
			$form->SetValuesFromDB("artist",$artistid);
			$form->AddAction("SUBMIT EDIT ARTIST",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddHidden("artistid",$artistid);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVESTATEMENT":
		{
			$pdfhash = $aspect->GetActionData();
			$query = "DELETE FROM artiststatements WHERE pdfhash='".$pdfhash."';";

			$aspect->SetNextAction("MANAGE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			Database::Query($query);
			unlink(Statement::MakeStatementFilename2($pdfhash));
			break;
		}
		case "MANAGE STATEMENTS":
		{
			$artistid = DataTable::GetFieldFromTable("bandartist",$aspect->GetVar("bandartistid"),"artist");
			$bandid = DataTable::GetFieldFromTable("bandartist",$aspect->GetVar("bandartistid"),"band");
//			$statements = Database::QueryGetResults("SELECT * FROM artiststatements WHERE artist='".$bandid."' AND crossdealid NOT LIKE 'C%' ORDER BY startdate,pdfhash");
			$statements = Database::QueryGetResults("SELECT * FROM artiststatements WHERE artist='".$bandid."' ORDER BY startdate,pdfhash");
	//		PrintTable($statements);

			print "<form method=post action=index.php><input type=hidden name=page value=artists>";
			print "<input type=hidden name=artistid value='".$artistid."'>";
			print "<input type=hidden name=bandartistid value='".$aspect->GetVar('bandartistid')."'>";
			if (date("m",time())>6)
			{
				$startdate  = date("Y-m-d",mktime(0, 0, 0, 1, 1, date("Y",time())));
				$enddate  = date("Y-m-d",mktime(0, 0, 0, 6, 30, date("Y",time())));
			}
			else
			{
				$startdate  = date("Y-m-d",mktime(0, 0, 0, 7, 1, date("Y",time())-1));
				$enddate  = date("Y-m-d",mktime(0, 0, 0, 12, 31, date("Y",time())-1));
			}

			$reserve = 0;
			if (!empty($statements))
			{
				print "Past Statements:<br>";
				print "<table width=100%>";
				print "<tr><th>Period</th><th>Amount Paid</th><th>Balance</th><th>Actions</th></tr>";
				$laststartdate = "";
				$lastenddate = "";
				$lastpdfhash = "";
				$total_amount = 0;
				$maxi = count($statements)-1;
				foreach ($statements as $i=>$statement)
				{
					if (($i!=0 && $laststatement['pdfhash']!=$statement['pdfhash'] )) 
					{
						print '<tr>';
						print '<td>';
						print date("d-m-Y",strtotime($laststatement['startdate']))." to ".date("d-m-Y",strtotime($laststatement['enddate']));
// add band 
//						print "<td>".TableInfo::DisplayFromType("band",$laststatement['artist'])."</td>";
						print "<td>".Currency::DISPLAY($total_amount)."</td><td>".Currency::DATABASETODISPLAY($laststatement['reserve'])."</td>";					
						print "<td><button type=submit name=submitvalue class=submitsmall value='REMOVESTATEMENT:".$laststatement['pdfhash']."'>REMOVE</button></td>\n";
						
						$total_amount = 0;
						$outfilename = "statement_".$laststatement['pdfhash'].".pdf";
						print "<td><a href='download.php?file=".$outfilename."'>pdf link</a></td>\n";
						$laststartdate = $laststatement['startdate'];
						$lastenddate = $laststatement['enddate'];
						
						print '</tr>'; 
					}
					// if not a compilation - then tot it up
					if ($statement['crossdealid'][0]!='C') 
						$total_amount += Currency::GETRAWVALUE($statement['amount']);
					
					$laststatement = $statement;
					
					// Update our next statement dates
					$nextdate = strtotime($statement['enddate']);
					$startdateraw = mktime(0, 0, 0, date("m",$nextdate),  date("d",$nextdate)+1,  date("Y",$nextdate));
					$startdate  = date("Y-m-d",$startdateraw);
					$enddate  = date("Y-m-d",mktime(0, 0, 0, date("m",$startdateraw)+6,  date("d",$startdateraw),  date("Y",$startdateraw))-1);
					$reserve = $statement['reserve'];

				}
				
				// last time
				print "<tr>";
				print "<td>";
				print date("d-m-Y",strtotime($laststatement['startdate']))." to ".date("d-m-Y",strtotime($laststatement['enddate']));
//				print "<td>".TableInfo::DisplayFromType("band",$laststatement['artist'])."</td>";
				print "<td>".Currency::DATABASETODISPLAY($total_amount)."</td><td>".Currency::DATABASETODISPLAY($laststatement['reserve'])."</td>";
			
				print "<td><button type=submit name=submitvalue class=submitsmall value='REMOVESTATEMENT:".$laststatement['pdfhash']."'>REMOVE</button></td>\n";
				$outfilename = "statement_".$laststatement['pdfhash'].".pdf";
				print "<td><a href='download.php?file=".$outfilename."'>pdf link</a></td>\n";
				$laststartdate = $laststatement['startdate'];
				$lastenddate = $laststatement['enddate'];
				
				print "</tr>"; 
				
				// Update our next statement dates
				$nextdate = strtotime($statement['enddate']);
				$startdateraw = mktime(0, 0, 0, date("m",$nextdate),  date("d",$nextdate)+1,  date("Y",$nextdate));
				$startdate  = date("Y-m-d",$startdateraw);
				$enddate  = date("Y-m-d",mktime(0, 0, 0, date("m",$startdateraw)+6,  date("d",$startdateraw),  date("Y",$startdateraw))-1);
				$reserve = $statement['reserve'];

				// end repeat
				
				print "</table><hr><br>";
			}
			else
			{
				$reserve = false;
				print "No Statement History<hr><br>";
			}

			print "</form>\n";

			print "<b>Generate New Statement</b><br>";
			$form = new ValidatedForm("artiststatements","artist,startdate,enddate","artist");
			$form->AddField("Treat in-house compilations as third party","treatasthirdpartycomp","checkbox",true);
			$form->AddField("Compilation losses are transferred to artist account","transferloss","checkbox",true);
			$form->AddField("Include compilation calculations in artist copy","includecomps","checkbox",true);
			$form->AddField("Include bands in statement","bands","multiselect",true);
			// Find bands the artist is a member of
			$result = Database::Query("SELECT DISTINCT band FROM bandartist WHERE artist='$artistid';"); //artist=$artistid;");
			$bandlist = "";
			for ($i=0;$i<$result->GetNum();$i++)
			{
				if ($i>0) $bandlist .= ",";
				$row = $result->GetNext();
				$result2 = Database::Query("SELECT name FROM bands WHERE uid='".$row['band']."';");
				$namerow = $result2->GetNext();
				if ($row['band']==$bandid) $bandlist .= "*";
				$bandlist .= $row['band']."=".$namerow['name'];
				Database::FinishQuery($result2);
			}
			Database::FinishQuery($result);
			$form->SetValue("bands",$bandlist);
		
			$form->SetValue("artist",$artistid);
			$form->SetValue("startdate",$startdate);
			$form->SetValue("enddate",$enddate);
			$form->AddAction("CALCULATE STATEMENT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CALCULATE STATEMENT":
		{
			$bandlist = "";
			for ($i=0; $i<count($_POST['bands']);$i++) {
				if ($i>0) $bandlist .= ",";
				$bandlist .= $_POST['bands'][$i];
			}
			
			$startdate = $aspect->GetVar('startdate');
			$enddate = $aspect->GetVar('enddate');
			$statement = new Statement;
			$includecomps = $aspect->GetVar('includecomps');
			$statement->Generate(true,$aspect->GetVar("artist"),$bandlist,$startdate,$enddate,true,$aspect->GetVar('treatasthirdpartycomp'),$aspect->GetVar('transferloss'));
			$statement->Display();


//			$outfilename = Statement::MakeStatementFilename($aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
			$pdfhash = Statement::MakeStatementHash($aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
			$outfilename = Statement::MakeStatementFilename2($pdfhash);
			if ($statement->IsComplete())
			{
				// Store this for later			
				$_SESSION['results'] = $statement->GetResults();
				// If we've been doing this at wrong detail level	
				$statement->Generate($includecomps,$aspect->GetVar("artist"),$bandlist,$startdate,$enddate,true,$aspect->GetVar('treatasthirdpartycomp'),$aspect->GetVar('transferloss'));

				$statement->SaveAsPDF($outfilename);
//				print "<a href='$outfilename' title='right or alt click to download'>PDF file</a><br>";
			}
			
			$form = new Form;
			$form->Begin("artists");
			$form->AddHidden("artistid",$aspect->GetVar("artist"));
			$form->AddHidden("startdate",$startdate);
			$form->AddHidden("enddate",$enddate);
			$form->AddHidden("pdfhash",$pdfhash);
			if ($statement->IsComplete())
			{
				$email = Database::QueryGetValue("email","artist","uid='".$aspect->GetVar("artist")."'");
				$form->AddHidden("email",$email);
				$form->AddHidden("filename",$outfilename);
				if ($email!="")
				{	
					$form->AddAction("MAIL STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
				}
				else
				{
					print "<p>There is no email address registered for this artist. If you fill in this detail we can send it directly to them</p>";
				}
				$form->AddAction("SAVE STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
			}
			else
				print "<p>This statement is not complete and so can not be saved. It may have no data in it, or require entry of balance information</p>";
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "MAIL STATEMENT":
		{
			$result = Database::QueryGetResult("SELECT name,invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."';");
			$address = DataTable::GetEntryFromID("addresses",$result['invoiceaddress']);
			$attachment[] = array("file"=>$aspect->GetVar("filename"),"content_type"=>"pdf");
			Email::Send($aspect->GetVar('email'), "Please find attached your royalty statement.", "Royalty Statement", "tom@discerningear.co.uk", $_SESSION['accountname'], $attachment);
		}
		case "SAVE STATEMENT":
		{

			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// Get session
			foreach ($_SESSION['results'] as $key=>$result)
			{
				$query = "INSERT INTO artiststatements SET artist='".$result['band']."',crossdealid='".$key."',amount='".$result['payable']."',reserve='".$result['balance']."',startdate='".$_POST['startdate']."',enddate='".$_POST['enddate']."',pdfhash='".$_POST['pdfhash']."';";
		//		print $query.'<br>';
				Database::Query($query);
			}
			// don't need now
			unset($_SESSION['results']);			
			$aspect->SetNextAction("VIEW");
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}
$aspect->End();		
?>


