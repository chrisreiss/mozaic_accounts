<?php

require_once('util.php');
require_once('cForm.php');
require_once('cDataTable.php');
require_once('cList.php');
require_once('cAspect.php');
require_once('cDatabase.php');
require_once('cStatement.php');
require_once('cValidatedForm.php');
require_once('dbexport.php');
require_once('cMail.php');
require_once('cReports.php');
require_once('cCurrency.php');
require_once('strings.php');
require_once('cFile.php');

Database::Connect("szirtest_mozaic");

if ($_SESSION['userid']!="tom")
	die("not authorised");

$aspect = new Aspect;
$aspect->Start("admin","MANAGE ACCOUNTS");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		case "MANAGE ACCOUNTS":
		{
			Database::Connect("szirtest_mozaic");
			$list = new TableList;
			$list->Begin("accountid","admin");
			$results = Database::QueryGetResults("SELECT uid,name FROM accounts;");
			foreach ($results as $result)
			{
				if ($result['uid']==$_SESSION['accountid'])
					$list->AddItem("",$result['uid'],$result['name']." (active)");
				else
					$list->AddItem("",$result['uid'],$result['name']);
			}
			$list->AddAction("ADD ACCOUNT",false);
			$list->AddAction("SET ACTIVE",true);		
//			$list->AddAction("EDIT ACCOUNT",false);		
//			$list->AddAction("REMOVE ACCOUNT",true);
			$list->AddAction("EDIT ACCOUNT",true);
			$list->AddAction("BACKUP DATABASE",false);
			$list->AddAction("RESTORE DATABASE",false);
			$list->AddAction("IMPORT ARTISTS",false);
			$list->AddAction("IMPORT CATALOGUE",false);
			$list->AddAction("AUTO GEN DEALS",false);
			$list->AddAction("ACTIVITY",false);
			$list->SetConfirm("REMOVE ACCOUNT","Are you sure? This deletes EVERYTHING!");
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		
		case "ACTIVITY REFRESH":
		case "ACTIVITY":
		{
			$users = Database::QueryGetResults("SELECT displayname, lastlogin, lastactivity, lastlogout, accountid FROM szirtest_mozaic.users ORDER BY lastlogin DESC;");
			print "<table width=100%>";
			print "<tr><td><b>Last Login</b></td><td><b>Display Name</b></td><td><b>Account</b></td><td><b>Status</b></td></tr>\n";
			foreach ($users as $user)
			{
				if ($user['lastlogin']=="0000-00-00 00:00:00")
				{
					$status = "INACTIVE";
				}
				else
				{
					$timeout = 30*60; 
					if ($user['lastlogout']!="0000-00-00 00:00:00" && strtotime($user['lastlogout'])>=strtotime($user['lastactivity']))
					{	
						$status="LOGGED OUT"; 
					}
					else if (strtotime($user['lastactivity'])+$timeout<time()) 
					{
						$status = "TIMED OUT";
					}
					else 
						$status="ACTIVE";
				}
				
				print "<tr><td>".$user['lastlogin']."</td><td>".$user['displayname']."</td><td>".$user['accountid']."</td><td>".$status."</td></tr>\n";
			}
			print "</table>";
			$form = new Form;
			$form->AddAction("ACTIVITY REFRESH");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SET ACTIVE":
		{
			Database::Query("UPDATE users SET accountid='".$aspect->GetVar("accountid")."' WHERE userid='tom';");
			$_SESSION['accountid'] = $aspect->GetVar("accountid");
			
			Database::Connect("szirtest_mozaic");
			$accountdata = Database::QueryGetResult('SELECT `database`,`site`,`packages` FROM accounts WHERE uid=\''.$_SESSION['accountid'].'\';');
			$_SESSION['database'] = $accountdata['database'];
			$_SESSION['site'] = $accountdata['site'];
			$_SESSION['packages'] = explode(',',$accountdata['packages']);

			Database::Disconnect();
			
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			break;
		}
		case "ADD ACCOUNT":
		{
			$form = new ValidatedForm();
			$form->AddTable("accounts","name,superuser,startdate");
			$form->AddTable("users","password,displayname,email");
			$form->AddField("Trial Account","trial","checkbox","true");
			$form->AddField("Modules","packages","multiselect",true);
			$form->SetValue("packages","*licensing=Licensing,promo=Promo");
			$form->AddCancel("CANCEL");
			$form->AddAction("SUBMIT NEW ACCOUNT",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW ACCOUNT":
		{
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			$aspect->SetVar("userid",$aspect->GetVar('superuser')); // copy value from name to user id because we inserting to two tables
			if ($aspect->GetVar("trial"))
				$aspect->SetVar("status","T");
			else
				$aspect->SetVar("status","A");
				
			Database::Connect("szirtest_mozaic");
			
			// create an empty address
//			$invoiceaddresid = DataTable::Insert("addresses","address1","Please fill out your address");	
//			$aspect->SetVar("invoiceaddress",$invoiceaddresid); 
			$aspect->SetVar('site','v2');
			$aspect->SetVar('packages',implode($_POST['packages'],','));
			$accountid = DataTable::InsertFromPost("accounts","name,superuser,status,packages,site",$aspect);
			Database::Query("UPDATE accounts SET database='".$accountid."' WHERE uid='".$accountid."';");
			$aspect->SetVar("accountid",$accountid); 
			DataTable::InsertFromPost("users","userid,password,displayname,email,accountid",$aspect);
			if (!file_exists(".//accounts//"))
			{
				mkdir ("..//accounts");
			}
			print "accountid = $accountid";
			mkdir("..//accounts//".$accountid);
			mkdir("..//accounts//".$accountid."//scripts",0755);
			
/*			// Send a welcome mail to them
			$mail = new Mail;
			$mail->SetMail("Welcome","This is your welcome message. Please take the time to fill out your invoice address from the home page edit profile option");
			MailManager::Send($mail,MAIL_TARGET_USER,$aspect->GetVar('superuser'));*/
			break;
		}
		case "EDIT ACCOUNT":
		{
			$form = new ValidatedForm();
			$form->AddTable("accounts","name,superuser,startdate,status");
			$form->AddHidden("accountid",$aspect->GetVar('accountid'));
			$form->AddCancel("CANCEL");
			$form->AddAction("UPDATE ACCOUNT",true);	// validate before sending
			$form->SetValuesFromDB("accounts",$aspect->GetVar('accountid'));
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "UPDATE ACCOUNT":
		{
			DataTable::UpdateFromPost("accounts",$aspect->GetVar('accountid'),"name,superuser,startdate,status",$aspect);
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			break;
		}
		case "REMOVE ACCOUNT":
		{
			$accountid = $aspect->GetVar('accountid');		

			// Delete all user mails and user accounts
			$users = Database::QueryGetResults("SELECT userid FROM users WHERE accountid='$accountid';");
			foreach ($users as $user)
				Database::Query("DELETE FROM mails WHERE distribution='user' AND destination='".$user['userid']."';"); 
			Database::Query("DELETE FROM mails WHERE distribution='account' AND destination='$accountid';");
			Database::Query("DELETE FROM users WHERE accountid='$accountid';"); 
			Database::Query("DELETE FROM accounts WHERE uid='$accountid';");
			// ALL GONE!!!

			$sql = 'DROP DATABASE my_db';
			$result = Database::Query("DROP DATABASE szirtest_".$accountid.";");
			if ($result==false)
				die("Could not drop database: ".mysql_error());
			
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			break;
		}
		case "BACKUP DATABASE":
		{
			Database::Connect();
			$filename = "temp/backup_".date("Y-d-m").".sql";
			$db_backup = new db_backup(Database::GetLinkID(),$filename);
			print "<p>Save your database by right clicking <a href=\"$filename\">here</a> and save to your computer.</p>";
			Success("admin");
			$aspect->Present();
			break;
		}
		case "RESTORE DATABASE":
		{
			print "<p><b>Warning restoring will reset all data in the database. If in doubt backup first!</b><br>A restore may take some time, please be patient.</p>";
			print "<p>Please select the database sql file: ";
			$form = new Form;
			$form->Begin("admin");
			$form->AddField("Database File","sqlfile","file","");
			$form->AddHidden("MAX_FILE_SIZE","30000000");
			$form->AddAction("CONFIRM RESTORE");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CONFIRM RESTORE":
		{
			Database::Connect();
			if (is_uploaded_file($_FILES['sqlfile']['tmp_name']))
			{
				print "Restoring from file ".$_FILES['sqlfile']['tmp_name']."<br>";
				$db_restore = new db_restore(Database::GetLinkID(),$_FILES['sqlfile']['tmp_name']);
				if ($db_restore->was_errors())
				{
					print "<p>Errors detected:<br>";
					print $db_restore->get_errors();
					print "</p>";
				}
				print "<p>Database Updated</p>";
				Success("admin");
				
			}
			else
			{
				Error("Cannot restore file ".$_FILES['sqlfile']['name'],"admin");
			}
			$aspect->Present();
			break;
		}
		case "IMPORT ARTISTS":
		{
			print "Format : BAND, ARTIST, SPLIT<br><br>";
			$form = new Form;
			$form->Begin("admin");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddField("Filename","importfile","file","");
			$form->AddCancel();
			$form->AddAction("PROCESS ARTISTS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "PROCESS ARTISTS":
		{
			$aspect->SetNextAction("VIEW");
			Database::Connect();

			$importfile = "../accounts/".$_SESSION['accountid']."/import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);

			if (!move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile))
			{
				$aspect->Error("Cannot move uploaded file ".$_FILES['importfile']['tmp_name']." to ".$importfile);
				break;
			}
			$worksheet = Worksheet::Load($importfile);
			$numrow = count($worksheet);
			for($i=2;$i<$numrow;++$i)
			{
				$artist = addslashes($worksheet[$i][2]);
				$band = addslashes($worksheet[$i][1]);
				$split = addslashes($worksheet[$i][3]);
				
				$result = Database::QueryGetResult("SELECT uid FROM artist WHERE realname='".$artist."';");	
				if ($result==false)
					$artist_uid = DataTable::Insert("artist","realname",$artist);
				else
					$artist_uid = $result['uid'];
					
				$result = Database::QueryGetResult("SELECT uid FROM bands WHERE name='".$band."';");	
				if ($result==false)
					$band_uid = DataTable::Insert("bands","name",$band);
				else
					$band_uid = $result['uid'];
						
				DataTable::Insert("bandartist","band,artist,split",$band_uid,$artist_uid,$split);
				print $i." : ".$band_uid." = ".$band." includes ".$artist_uid." = ".$artist." (split $split)<br>";
			}
			break;
		}
		case "IMPORT CATALOGUE":
		{
			print "Format : PRODUCT_NAME, PRODUCT_CODE, BAND_NAME, TRACK_POS, TRACK_NAME, REMIX_NAME, ISRC, BARCODE, RELEASE DATE, PUBLISHER, TRACKLENGTH<br><br>";
			$form = new Form;
			$form->Begin("admin");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddField("Filename","importfile","file","");
			$form->AddCancel();
			$form->AddAction("PROCESS CATALOGUE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "AUTO GEN DEALS":
		{
			$aspect->SetNextAction("VIEW");
			Database::Connect();

			// Now autocreate deals - assume 50:50 profit splits
			// one deal created per product
			// one dealterm created per artist (so all deals cross calculated)
			
			// get all products grouped by band
			$products = Database::QueryGetResults("SELECT uid,artist FROM product WHERE artist!=0 ORDER BY artist;");

			foreach($products as $i=>$product)
			{
				print_r($product); print'<br>';
				// if artist has changed then reset the crossdealid
				if ($lastband!=$product['artist']) 
				{
					$lastband=$product['artist'];
					$crossdealid = 0;
				}
				
				// insert a deal with the current crossdealid - will be 0 for the first entry per artist
				$query = "INSERT INTO deals (dealtype,band,product,crossdealid) VALUES ('P','".$product['artist']."','".$product['uid']."','".$crossdealid."');";
				print $query.'<br>';
				Database::Query($query);
				$lastdealid = mysql_insert_id();
								
				// once for each artist create a dealterm that references the first deal			
				if ($crossdealid == 0) 
				{
					// create a single dealterm for the artist
					$dealid = DataTable::InsertFromArray('dealterms', array('rate'=>50,'descrip'=>'Autogen 50/50','base'=>'salevalue','dealid'=>$lastdealid));
					$crossdealid=$lastdealid;
					
				}
			}
			Database::Query("UPDATE deals SET crossdealid=uid WHERE crossdealid=0;");

			break;
		}
		case "PROCESS CATALOGUE":
		{
			Database::Connect();
			$aspect->SetNextAction("VIEW");
			$importfile = File::GetPath("import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1));
			//"./accounts/".$_SESSION['accountid']."/import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);
			print $_FILES['importfile']['name']." mm " . $_FILES['importfile']['tmp_name'] . ' to '. $importfile. '<br>';
			if (!move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile))
			{
				$aspect->Error("Cannot move uploaded file ".$_FILES['importfile']['tmp_name']." to ".$importfile);
				break;
			}

//			1 PRODUCT_NAME, 2 PRODUCT_CODE, 3 BAND_NAME, 4 TRACK_POS, 5 TRACK_NAME, 6 REMIX_NAME, 7 ISRC, 8 BARCODE, 9 RELEASE DATE, 
// 10 PUBLISHER, 11 TRACKLENGTH;


			$worksheet = Worksheet::Load($importfile);
			$numrow = count($worksheet);
			$stamp = time();

			$product_number_last = "";
			$barcode_last = "";
            $track_pos_auto = 1;
			$band_name_last = "";
			
			for($i=2;$i<=$numrow;++$i)
			{
				print $i." : "; print_r($worksheet[$i]); print"<br>";
				$product_name = addslashes($worksheet[$i][1]);				
                $product_number = addslashes($worksheet[$i][2]);
                if ($product_number!=$product_number_last)
				{
					$track_pos_auto=1;
				}
				else
				{
					$track_pos_auto += 1;

				}
				if ($product_number=="") 
				{
					$product_number=$product_number_last;
				}	
				$band_name = addslashes($worksheet[$i][3]);
				if ($band_name=="")
				{
					$band_name = $band_name_last;
				}
				$track_pos = addslashes($worksheet[$i][4]);
				if ($track_pos=="")
				{
					$track_pos = $track_pos_auto;
				}
				$track_name = addslashes($worksheet[$i][5]);
				$remix_name = addslashes($worksheet[$i][6]);
				$isrc = addslashes($worksheet[$i][7]);
				$isrc = str_replace(" ","",$isrc);
				$isrc = str_replace("-","",$isrc);
				if ($isrc=="")
				{
					$isrc = $worksheet[$i][10];
			//		print "$i : No ISRC - skipped line<br>";
			//		continue;	// can't process this
				}
				$barcode = addslashes($worksheet[$i][8]);
				$barcode = str_replace(" ","",$barcode);
				$barcode = str_replace("-","",$barcode);
				if ($barcode=="")
				{
					$barcode = $barcode_last;
				}
				$release_date = addslashes($worksheet[$i][9]);
				$publisher = addslashes($worksheet[$i][10]);
				$tracklength = addslashes($worksheet[$i][11]);
				
                $product_number_last = $product_number;
				$barcode_last = $barcode;
				$band_name_last = $band_name;
                
                // Check if band exists                
				$result = Database::QueryGetResult("SELECT uid FROM bands WHERE name='".$band_name."';");	
				if ($result===false)
				{
					$band_uid = DataTable::Insert("bands","name",$band_name);
					$artist_uid = DataTable::Insert("artist","realname",$band_name);
					DataTable::Insert("bandartist","band,artist,split",$band_uid,$artist_uid,100);
				}
				else
				{
					$band_uid = $result['uid'];
				}
				
                // Check that track exists - if not add it
				$result = Database::QueryGetResult("SELECT uid FROM tracks WHERE uid='".$isrc."';");
				if ($result==false)
					DataTable::Insert("tracks","uid,artist,title,version,publisher,tracklength,comments",$isrc,$band_uid,$track_name,$remix_name,$publisher,$tracklength,$stamp);

                // check that the product exists - if not add it
				$result = Database::QueryGetResult("SELECT uid FROM product WHERE uid='".$product_number."';");
				if ($result==false)
					DataTable::Insert("product","uid,artist,title,barcode,releasedate,comments",$product_number,$band_uid,$product_name,$barcode,$release_date,$stamp);

                // add the track to the product
				DataTable::Insert("tracksproduct","track,product,position",$isrc,$product_number,$track_pos);
				print "$i : $isrc, $product_number, $track_pos<br>";
				
			}
			
			break;
			
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}
$aspect->End();
?>