<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cCurrency.php' );
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cImport.php');
require_once( 'cExport.php');
require_once( 'cValidatedForm.php' );
require_once( 'cStatement.php' );
require_once( 'strings.php');
require_once( 'cStatementManager.php');

Database::Init();

$aspect = new Aspect;
$aspect->Start("accounting","VIEW");

//print_r($_POST);
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$list = new TableList;
			$list->Begin("statementid","statements");

			$statements = StatementManager::GetStatements(); 

			$lastartistid = -1;
			foreach ($statements as $statement)
			{
				if ($lastartistid!=$statement['artist'])
				{
					$artistname = TableInfo::DisplayFromType('artist',$statement['artist']);
				}
				$list->AddItem( $artistname,
								$statement['pdfhash'],
								$statement['period']." (".$statement['amounttotal'].")"
								//$statement['crossdealid'],$statement['startdate']." - ".$statement['enddate']
								);
				$lastartistid = $statement['artist'];
			}
			
			$list->AddAction("MAKE STATEMENT",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_RUN);
			$list->AddAction("DELETE STATEMENT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_RUN);
			$list->SetConfirm("DELETE STATEMENT",STRING_DIALOG_CONFIRM);
			$list->AddAction("VIEW STATEMENT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_RUN);
			$list->AddAction("MAIL STATEMENT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_RUN);
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		case "MAKE STATEMENT":
		{
			if ($aspect->IsVarSet('statementid'))
			{
				$result = Database::QueryGetResult("SELECT artist FROM artiststatements WHERE pdfhash='".$aspect->GetVar('statementid')."' LIMIT 1;");
				$artistid = $result['artist'];
			}
			
			print "<b>Generate New Statement</b><br>";
			$form = new ValidatedForm("artiststatements","artist,startdate,enddate");
			$form->AddField("Artist Detail Level","detaillevel","statementdetaillevel",true);
			
			if ($aspect->IsVarSet('artist')) $artistid = $aspect->GetVar('artist');
			
			if (isset($artistid))
			{
				$form->SetValue("artist",$artistid);
				$statement = Database::QueryGetResult("SELECT artist FROM artiststatements WHERE crossdealid='".$aspect->GetVar('statementid')."';");
				$daterange = StatementManager::GetNextPeriodForArtist($artistid);
				$form->SetValue("startdate",$daterange['start']);
				$form->SetValue("enddate",$daterange['end']);
				$bands = Database::QueryGetResults("SELECT DISTINCT band FROM bandartist WHERE artist='$artistid';");
				$bandlist = "";
				foreach ($bands as $i=>$band)
				{
					if ($i>0) $bandlist .= ",";
					$bandlist .= $band['band']."=".TableInfo::DisplayFromType("band",$band['band']);
				}
				$form->AddField("Include Bands","bands","multiselect",true);
				$form->SetValue("bands",$bandlist);

			}
			$form->SetFilter("artist","*");
		
			$form->AddAction("CALCULATE STATEMENT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);

			$aspect->Present();
			break;
		}
		case "CALCULATE STATEMENT":
		{
			$bandlist = "";
			for ($i=0; $i<count($_POST['bands']);$i++) {
				if ($i>0) $bandlist .= ",";
				$bandlist .= $_POST['bands'][$i];
			}
			$startdate = $aspect->GetVar('startdate');
			$enddate = $aspect->GetVar('enddate');
			$statement = new Statement;
			$detaillevel = $aspect->GetVar('detaillevel');
			// slight hack you have to display comps in order to enter the right balances
			if ($aspect->GetVar('detaillevel')==0) $detaillevel = 1;
			// but when we do the pdf we will leave them out
			$statement->Generate($detaillevel,$aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
			$statement->Display();

			$pdfhash = Statement::MakeStatementHash($aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
			$outfilename = Statement::MakeStatementFilename2($pdfhash);
			if ($statement->IsComplete())
			{
				// Store this for later			
				$_SESSION['results'] = $statement->GetResults();
				
				// If we've been doing this at wrong detail level
				if ($detaillevel!=$aspect->GetVar('detaillevel'))
				{
					// need to recalculate for pdf
					unset($statement);
					$statement = new Statement;
					$statement->Generate($aspect->GetVar('detaillevel'),$aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
				}
	
				$statement->SaveAsPDF($outfilename);
			}
			
			$form = new Form;
			$form->Begin("artists");
			$form->AddHidden("artistid",$aspect->GetVar("artist"));
			$form->AddHidden("startdate",$startdate);
			$form->AddHidden("enddate",$enddate);
			$form->AddHidden("pdfhash",$pdfhash);
			if ($statement->IsComplete())
			{
				$email = Database::QueryGetValue("email","artist","uid='".$aspect->GetVar("artist")."'");
				$form->AddHidden("email",$email);
				$form->AddHidden("filename",$outfilename);
				if ($email!="")
				{	
					$form->AddAction("MAIL STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
				}
				else
				{
					print "<p>There is no email address registered for this artist. If you fill in this detail we can send it directly to them</p>";
				}
				$form->AddAction("SAVE STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
			}
			else
				print "<p>This statement is not complete and so can not be saved. It may have no data in it, or require entry of balance information</p>";
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;

		}
		case "MAIL STATEMENT":
		{
			$result = Database::QueryGetResult("SELECT name,invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."';");
			$address = DataTable::GetEntryFromID("addresses",$result['invoiceaddress']);
			$attachment[] = array("file"=>$aspect->GetVar("filename"),"content_type"=>"pdf");
			Email::Send($aspect->GetVar('email'), "Please find attached your royalty statement.", "Royalty Statement", "tom@discerningear.co.uk", $_SESSION['accountname'], $attachment);
		}
		case "SAVE STATEMENT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// Get session
			foreach ($_SESSION['results'] as $key=>$result)
			{
				Database::Query("INSERT INTO artiststatements SET artist='".$_POST['artistid']."',crossdealid='".$key."',amount='".$result['payable']."',reserve='".$result['balance']."',startdate='".$_POST['startdate']."',enddate='".$_POST['enddate']."',pdfhash='".$_POST['pdfhash']."';");
			}
			// don't need now
			unset($_SESSION['results']);			
			$aspect->SetNextAction("VIEW");
			break;
		}

		case "DELETE STATEMENT":
		{
			$result = Database::QueryGetResult("SELECT artist,pdfhash FROM artiststatements WHERE pdfhash='".$aspect->GetVar('statementid')."';");
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$query = "DELETE FROM artiststatements WHERE artist='".$result['artist']."' AND pdfhash='".$result['pdfhash']."';";
			Database::Query($query);
			unlink(Statement::MakeStatementFilename2($result['pdfhash']));
			break;

			break;
		}
		case "VIEW STATEMENT":
		{
			$aspect->SetNextAction("VIEW");
			$outfilename = "statement_".$aspect->GetVar('statementid').".pdf";
			print "<td><a href='download.php?file=".$outfilename."'>pdf link</a></td>\n";
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}

$aspect->End();
?>
