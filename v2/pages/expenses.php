<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cValidatedForm.php');

Database::Init();

$aspect = new Aspect;
$aspect->Start("expenses","VIEW");
 
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			if (isset($_POST['expenseid2']))
				$_POST['selectedgroup']=$_POST['expenseid2'];

			$list = new TableList;
			$list->Begin("expenseid","expenses");
			$list->SetMultipleSelection(true);
			$list->SetSplitMode(true);
			$list->SetExtraConditions("type!='Advance'");
			$list->AddView("by product","expenses","%invoicedate% : %description% (%amount%)","product","invoicedate","%product% (%product.product:title%)");
			$list->AddView("by type","expenses","%invoicedate% : %product% - %description% (%amount%)","type","invoicedate","%type%");
			$list->AddAction("ADD EXPENSES",false);
			$list->AddAction("REMOVE EXPENSES",true, ACTION_CONFIRM, STRING_DIALOG_CONFIRM_REMOVEEXPENSES);
			$list->AddAction("EDIT EXPENSE",true);
			$list->SetInfoBoxHandler("getexpenseinfo");
			//			$list->AddAction("ADD MULTIPLE",false,ACTION_TOOLTIP,"Add multiple expenses at a time. Alternatively you can create a spreadsheet and use the IN/OUT functionality.");
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		case "REMOVE EXPENSES":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			foreach ($_POST['expenseid'] as $value)
			{
				DataTable::Delete("expenses",$value);
			}
			break;
		}
		case "EDIT EXPENSE":
		{
			$form = new ValidatedForm("expenses","product,invoicedate,type,description,invoice,amount,paiddate,comments");
			$form->SetValuesFromDB("expenses",$_POST['expenseid'][0]);
			$form->AddHidden("expenseid",$_POST['expenseid'][0]);
			$form->AddHidden("expenseid2",$_POST['expenseid'][0]);
			$form->AddAction("SUBMIT EDIT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "NEW REPEAT":
		{
		}
		case "ADD EXPENSES":
		{
			if (!isset($_POST['addcount'])) $_POST['addcount']=1;
			print "Expense ".$_POST['addcount']."<hr>";
			$form = new ValidatedForm("expenses","product,invoicedate,type,description,invoice,amount,paiddate,comments");
			if ($_POST['addcount']>1) 
				$form->SetValuesFromPost($aspect,"product,invoicedate,type");
			else
				$form->SetValue("product",$_POST['expenseid2']);
			$form->AddAction("SAVE",true);	// validate before sending
			$form->AddCancel("FINISHED");	// validate before sending
			$form->AddHidden('addcount',$_POST['addcount']+1);
			$aspect->Attach($form);
			$aspect->PreserveVar('expenseid2');
			$aspect->Present();
			break;
		}
		case "SAVE":
		{
			$aspect->SetNextAction("NEW REPEAT");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("expenses","product,invoicedate,type,description,invoice,amount,paiddate,comments",$aspect);
			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			
			DataTable::UpdateFromPost("expenses",$aspect->GetVar('expenseid'),"product,invoicedate,type,description,invoice,amount,paiddate,comments",$aspect);
			break;
		}
		case "ADD MULTIPLE":
		{
			$form = new Form;
			$form->Begin("expenses");
			$form->StartGroupSet(1,15); //"Description","Product","Invoice #","Amount","Type","Invoice Date","Paid Date");
				$form->AddField("Description","description","text","");
				$form->AddField("Product","product","product","");
				$form->AddField("Invoice #","invoice","invoice","");
				$form->AddNewLine();
				$form->AddField("Amount","amount","money","");
				$form->AddField("Type","type","expensetype","");
				$form->AddField("Invoice Date","invoicedate","date","");
				$form->AddField("Paid Date","paiddate","date","");
			$form->EndGroupSet();
			$form->AddAction("SUBMIT MULTIPLE");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT MULTIPLE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			for ($i=1;$i<=15;++$i)
			{
				if ($_POST["product".$i]!="")
				{
					DataTable::Insert("expenses","invoice,product,invoicedate,type,description,amount,paiddate",
									$_POST["invoice_".$i],$_POST["product_".$i],getpostdate2("invoicedate_".$i),$_POST["type_".$i],$_POST["description_".$i],
									storemoney($_POST["amount_".$i]),getpostdate2("paiddate_".$i));
				}			
			}
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}

	}
}

$aspect->End();

?>