<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cValidatedForm.php');
require_once( 'strings.php' );

function PopulateLicenceStatements(&$list,&$licence,$displayname,$ignorepaid)
{
	// will fill this out with a comma seperated list of values
	$expected = "";
	$paid = "";
	$awaitingpayment = "";

	if ($displayname)
		$prefixtext = $licence['description']." : ";
	else	
		$prefixtext = "";

	// get any licence statements already in the system
	$result = Database::Query("SELECT * FROM licencestatements WHERE licenceuid='".$licence['uid']."' ORDER BY statementdate ;");
	for ($i=0;$i<$result->GetNum();++$i)
	{
		$licencestatement = $result->GetNext();
		$ddate = displaydate($licencestatement['statementdate']);
		$optionvalue = $licence['uid']."_".$ddate;
		if ($licencestatement['paiddate']=="0000-00-00")
		{
			$list->AddItem( "Statements Awaiting Payment",
							$optionvalue,
							$prefixtext.$ddate." (".Currency::DATABASETODISPLAY($licencestatement['amount']).")",$licencestatement['statementdate']);
			$awaitingpayment = $awaitingpayment.$optionvalue.",";
			$useddates[] = $ddate;
		}
		else
		{
			if (!$ignorepaid)
			{
				$list->AddItem("Statements Paid",
								$optionvalue,
								$prefixtext.$ddate." (".Currency::DATABASETODISPLAY($licencestatement['amount']).")",$licencestatement['statementdate']);
				$paid = $paid.$optionvalue.",";
			}
			$useddates[] = $ddate;
		}
	}

	$date = strtotime($licence['datestatement']);
	$nextdate = $date;
	$iteration = 0;
	while ($nextdate<time() && 
	       ($licence['laststatementdate']=='0000-00-00' || $nextdate<=strtotime($licence['laststatementdate'])) )
	{
		$ddate = date("d-m-Y",$nextdate);
		$optionvalue = $licence['uid']."_".$ddate;
		// NB - note that array_search after PHP 4.20 will return false not NULL
		if (!empty($useddates))
		{
			$arrayresult = array_search($ddate,$useddates);
		}
		else
			$arrayresult = false;
		
					
		if ($arrayresult===NULL || $arrayresult===false)
		{
			$list->AddItem( "Statements Due",
							$optionvalue,
							$prefixtext.$ddate,$nextdate);
			$expected .= $optionvalue.",";
		}
		++$iteration;
		
		if ($licence['royaltyinterval']==0) break;
		
		$nextdate  = mktime(0, 0, 0, date("m",$date)+($licence['royaltyinterval']*$iteration),  date("d",$date),  date("Y",$date));
	}

	
	Database::FinishQuery($result);
	$results['expected'] = $expected;
	$results['paid'] = $paid;
	$results['awaitingpayment'] = $awaitingpayment;
	return $results;
}

Database::Init();
$aspect = new Aspect;
$aspect->Start("licensors","VIEW");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$list = new TableList;
			$list->Begin("licenceuid","licensors");
			$list->AddView("by licensor","licences","%description%: %licenceduid.tracks|product:artist.bands:name% - %licenceduid.tracks|product:title%","licensor","uid","%licensor.partners:name%");
			$list->AddView("by licenced","licences","%licenceduid.tracks|product:title% - %description% ","licenceduid","uid","%licenceduid.tracks|product:artist.bands:name%");
//			$list->AddView("by artist","licences","%description% (%licensor.creditor:name%) : %licenceduid.tracks|product:title%","licensor","uid","%licenceduid.tracks|product:artist%");
			$list->AddAction("NEW LICENCE",false);
			$list->AddAction("REMOVE LICENCE",true);
			$list->SetInfoBoxHandler("getlicenceinfo");
			$list->SetConfirm("REMOVE LICENCE","This will remove the licence. Are you sure?");
			$list->AddAction("EDIT LICENCE",true);
			$list->AddAction("LICENCE STATEMENTS",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_LICENCESTATEMENTS);
			$list->AddAction("OUTSTANDING STATEMENTS",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_OUTSTANDINGSTATEMENTS);
			$list->AddAction("OUTSTANDING ADVANCES",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_OUTSTANDINGADVANCES);
			$list->End();			
			$aspect->Present();
			break;
       }
	   case "REMOVE LICENCE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::Delete("licences",$aspect->GetVar('licenceuid'));
			break;
		}
		case "STOCK LEVELS":
		{
			$stock = CalculateStockLevels();
			print $_POST['creditor']." Stock<br>";
			print '<table>';
			foreach ($stock as $key => $value)
			{
				print "<tr><td>".$key."</td><td>".$stock[$key]." units</td></tr>";
			}
			print '</table>';
			BackButton();
			$aspect->Present();
			break;
		}

		case "NEW LICENCE":
		{
			$form = new ValidatedForm("licences","licenceduid,licencetype,licensor,advance,royaltyrate,laststatementdate,packagingdeductions,reserveforreturns,datestatement,royaltyinterval,description,advancepaiddate,comments");
			$form->AddAction("SUBMIT NEW",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("licences","licenceduid,licencetype,licensor,advance,royaltyrate,laststatementdate,packagingdeductions,reserveforreturns,datestatement,royaltyinterval,description,advancepaiddate,comments",$aspect);
			break;
		}
		case "EDIT LICENCE":
		{
			$licenceuid = $aspect->GetVar('licenceuid');
			if ($aspect->IsVarSet('statementid'))
			{
				$licenceuid = $aspect->GetVar('statementid');
				if (($trimpos = strpos($licenceuid,"_",0))!==false)
					$licenceuid = substr($licenceuid,0,$trimpos);
				$_POST['licenceuid'] = $licenceuid;
			}
			$form = new ValidatedForm("licences","licenceduid,licencetype,licensor,advance,royaltyrate,laststatementdate,packagingdeductions,reserveforreturns,datestatement,royaltyinterval,description,advancepaiddate,comments");
			$form->SetValuesFromDB("licences",$licenceuid);
			$_POST['licencetype'] = $form->GetValue("licencetype");

			$form->AddAction("SUBMIT EDIT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->PreserveVar('licenceuid');
			$aspect->Present();
			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::UpdateFromPost("licences",$aspect->GetVar('licenceuid'),"licenceduid,licencetype,laststatementdate,licensor,advance,royaltyrate,packagingdeductions,reserveforreturns,datestatement,royaltyinterval,description,advancepaiddate,comments",$aspect);
			break;
		}
		case "CHECK OVERDUE":
		{
			$result = Database::Query("SELECT * FROM licencestatements WHERE amount>0 AND paiddate=0000-00-00");
			print "<table><tr><td>Licensor</td><td>Statement Date</td><td>Owed</td><td></td><td></td></tr>";
			$totalamount = 0.0;
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				$result2 = Database::Query("SELECT licensor FROM licences WHERE uid=".$row['licenceuid']);
				$licensor = $result2->GetNext();
				print "<tr>";
				print "<td>".$licensor['licensor']."</td><td>".$row['statementdate']."</td><td>".$row['amount']."</td>";
				print '<td><form method="post" action="index.php"><input type="hidden" name="page" value="licensors"><input type="submit" class="submitsmall" name="submit" value="INVOICE" disabled></form></td>';
				print '<td><form method="post" action="index.php"><input type="hidden" name="page" value="processlicensors"><input type=hidden name=licence value='.$row['licenceuid'].'><button type="submit" class="submitsmall" name="submit" value="LOG ROYALTIES">EDIT</button></form></td>';
				print "</tr>";
				$totalamount += $row['amount'];
				Database::FinishQuery($result2);
			}
			print "</table><br>";
			if ($result->GetNum()==0)
			{
				print "<p>No overdue royalty payments detected</p>";
			}
			else
			{
				print "Owed ".$totalamount." over ".$num_result." licences<br>";
			}
			$aspect->Present();
			break;
		}
		case "OUTSTANDING STATEMENTS":
		{
			$list = new TableList;
			$list->Begin("statementid","licensors");
			// Get the licence we are going find the statement for
			$list->AddTitle("All outstanding statements");

			$results['awaitingpayment'] = "";
			$results['expected'] = "";
			$result = Database::Query("SELECT * FROM licences;");
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$licence = $result->GetNext();
				$tempresults = PopulateLicenceStatements($list,$licence,true,true);
				$results['awaitingpayment'] .= $tempresults['awaitingpayment'];
				$results['expected'] .= $tempresults['expected'];
			}
			$list->AddAction("REGISTER STATEMENT",true,ACTION_ENABLELIST,$results['expected']);
			$list->AddAction("MARK PAID",true,ACTION_ENABLELIST,$results['awaitingpayment'],ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_MARKPAID);
			$list->AddAction("INVOICE",true,ACTION_ENABLELIST,$results['awaitingpayment'],ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_INVOICE);
			$list->AddAction("REMIND",true,ACTION_ENABLELIST,$results['expected'],ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_REMIND);
			$list->AddAction("EDIT LICENCE",false);
			$list->AddAction("BACK TO LICENCES",false);
			$list->SetInfoBoxHandler("getlicenceinfo");
//			$list->();			
			$aspect->Attach($list);
			$aspect->Present();
			break;
		}
		case "OUTSTANDING ADVANCES":
		{
			$list = new TableList;
			$list->Begin("licenceuid","licensors");
			// Get the licence we are going find the statement for
			$list->AddTitle("Licences Awaiting Advance Payment");

			$results['awaitingpayment'] = "";
			$results['expected'] = "";
			$result = Database::Query("SELECT uid,description,licenceduid,advance FROM licences WHERE advance!='' AND advancepaiddate='0000-00-00';");
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$item = $result->GetNext();
				$list->AddItem( "",	$item['uid'],
							$item['description']." (".Currency::DATABASETODISPLAY($item['advance']).")");

			}
			$list->AddAction("REMIND LICENSOR",true);
			$list->AddAction("EDIT LICENCE",true);
			$list->AddAction("BACK TO LICENCES",false);
			$list->SetInfoBoxHandler("getlicenceinfo");

			$aspect->Attach($list);
			$aspect->Present();
			break;
		}
		case "REMIND LICENSOR":
		{
			$licence = DataTable::GetEntryFromID("licences",$aspect->GetVar('licenceuid'));
			$licensor = DataTable::GetEntryFromID("partners",$licence['licensor']);
			if ($licensor['email']=="")
			{
				print "No email address registered for this company";
				$form = new ValidatedForm;
				$form->Begin("licensors");
				$form->AddField_FromTable("addresses","email");
				$form->AddAction("REGISTER EMAIL ADDRESS",false);
				$form->AddCancel();
				$aspect->Attach($form);
			}
			else
			{
				$result = Database::QueryGetResult("SELECT name,invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."';");
				$address = DataTable::GetEntryFromID("addresses",$result['invoiceaddress']);

				$form = new Form;
				$form->Begin("licensors");
				
				$subject = "Reminder: Advance Payment Due";
				$body = "This is an email from ".$_SESSION['displayname']." at ".$_SESSION['accountname'];
				$body.= ".\n";
				$body.= "According to our records an advance of ".Currency::DATABASETODISPLAY($licence['advance'])." for '".$licence['description']."' is due.\n";
				$body.= "\n";
				$body.= "Many thanks\n";
				$body.= "-----------------------------------\n";
				$body.= str_replace("<br>","\n",DisplayAddress($result['name'],$address));
//				print "<table class=spreadsheet width=100%><tr><td>".str_replace("\n","<br>",$body)."</td></tr></table>";			
				$form->AddField("To","to","text",$licensor['email']);
				$form->AddField("From","from","text",$address['email']);
				$form->AddField("Subject","subject","text",$subject);
				$form->AddField("Body","body","email",$body);
				$form->AddAction("POST");
				$form->AddCancel();
				$aspect->Attach($form);
//				$body.= "\n* This email is generated by music.counts record label management system.\n";
			}
			$aspect->Present();
			break;
		}
		case "REGISTER EMAIL ADDRESS":
		{
			$query ="UPDATE partners SET email='".$aspect->GetVar("email")."' WHERE uid='".$aspect->GetVar("licensor")."';";
			Database::Query($query);
			// Let fall through to remind again
		}
		case "REMIND":
		{
			$statementdata = explode("_",$aspect->GetVar('statementid'));
			$licence = DataTable::GetEntryFromID("licences",$statementdata[0]);
			$licensor = DataTable::GetEntryFromID("partners",$licence['licensor']);
			if ($licensor['email']=="")
			{
				print "No email address registered for this company";
				$form = new ValidatedForm;
				$form->AddField_FromTable("addresses","email");
				$form->AddAction("REGISTER EMAIL ADDRESS",true);
				$form->AddHidden("statementid",$aspect->GetVar("statementid"));
				$form->AddHidden("licensor",$licence['licensor']);
				$form->AddCancel();
				$aspect->Attach($form);
			}
			else
			{
				$result = Database::QueryGetResult("SELECT name,invoiceaddress FROM szirtest_mozaic.accounts WHERE uid='".$_SESSION['accountid']."';");
				$address = DataTable::GetEntryFromID("addresses",$result['invoiceaddress']);

				$form = new Form;
				$form->Begin("licensors");
				
				$subject = "Reminder: Statement Due";
				$body = "This is an email from ".$_SESSION['displayname']." at ".$_SESSION['accountname'];
				$body.= ".\n";
				$body.= "According to our records a royalty statement for the period ending ".$statementdata[1]." for '".$licence['description']."' is now due.\n";
				$body.= "\n";
				$body.= "Many thanks in advance\n";
				$body.= "-----------------------------------\n";
				$body.= str_replace("<br>","\n",DisplayAddress($result['name'],$address));
//				print "<table class=spreadsheet width=100%><tr><td>".str_replace("\n","<br>",$body)."</td></tr></table>";			
				$form->AddField("To","to","text",$licensor['email']);
				$form->AddField("From","from","text",$address['email']);
				$form->AddField("Subject","subject","text",$subject);
				$form->AddField("Body","body","email",$body);
				$form->AddAction("POST");
				$form->AddCancel();
				$aspect->Attach($form);
//				$body.= "\n* This email is generated by music.counts record label management system.\n";
		}
			$aspect->Present();
			break;
		}
		case "POST":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

		
			$headers = 'From: '.$aspect->GetVar('from'). "\r\n" .
				   'Reply-To: '.$aspect->GetVar('from') . "\r\n" .
  						'X-Mailer: PHP/' . phpversion();
							
			$body = $aspect->GetVar('body')."\n* This email is generated by music.counts record label management system.\n";
			if (!mail($_POST['to'], $_POST['subject'], $body, $headers)) 
			{
				print "<p>Your message could not be delivered.</p><p>The most likely reason is that either the email address is incorrect or that you don't have your mail settings properly configured. Your SMTP needs to be set up in php.ini.</p>";
			}
			break;
		}
		case "INVOICE":
		{
			$statementdata = explode("_",$aspect->GetVar('statementid'));
			$licence = DataTable::GetEntryFromID("licences",$statementdata[0]);
			$licensor = DataTable::GetEntryFromID("partners",$licence['licensor']);
			$licencestatement = Database::QueryGetResult("SELECT * FROM licencestatements WHERE licenceuid='".$statementdata[0]."' AND statementdate='".MakeDate($statementdata[1])."';");
			
			$output = "";
			$output.= "<center><font size=3><b>INVOICE</b></font></center><br>";
			$output.= "<center><font size=1>generated by music.counts</font></center><br>";
			$output.= "<br>";
			$results = Database::QueryGetResults("SELECT name,invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."';");
			$output.= "<b>From:</b><br>";
			$output.= DisplayAddressFromID($results[0]['name'],$results[0]['invoiceaddress']);
			$output.= "<br>";
			
			$results = Database::QueryGetResults("SELECT name,invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."';");
			$output.= "<b>To:</b><br>";
			$output.= DisplayAddress($licensor['name'],$licensor);
			$output.= "<br>";
	
			// DATE
			$output.= "<b>Invoice Date : ".date("d-m-Y",time())."</b><br>";
			$output.= "<b>Reference No. : </b><br><br>";
			
			$output.= "<table width=100% border=1>";
//			print "<tr><td>Item</td><td>Amount</td></tr>";
			$output.= "<tr><td>";
			$output.= "Royalties due for period ending ".$statementdata[1]." for '".$licence['description']."'<br><br>";
			$output.= "</td><td>";
			$output.= Currency::DATABASETODISPLAY($licencestatement['amount']);
			$output.= "</td></tr>";
			$vat = 0;
			// VAT
			$vatrateresult = Database::QueryGetResult("SELECT vatrate FROM accounts WHERE uid='".$_SESSION['accountid']."';");
			if ($vatrateresult['vatrate']!=0)
			{
				$output.= "<tr><td>";
				$vat = Currency::PRODUCTOF($licencestatement['amount'],($vatrateresult['vatrate']/100));
				$output.= "VAT (".$vatrateresult['vatrate']."%)</td><td>".Currency::DATABASETODISPLAY($vat);
				$output.= "</td></tr>";
			}
			// TOTAL
			$output.= "<tr><td>";
			$output.= "<b>TOTAL DUE</b></td><td><b>".Currency::DATABASETODISPLAY(Currency::SUMOF($licencestatement['amount'],$vat))."</b>";
			$output.= "</td></tr>";			
			$output.= "</table><br><br>";
			print "<table width=100% class=spreadsheet><tr><td>".$output."</td></tr></table>";;
			$fd = fopen("invoice.html","wb");
			fwrite($fd,$output,strlen($output));
			fclose($fd);
			print "<a href='invoice.html' target=_blank>print version</a>";

			$form = new Form;
			$form->Begin("licensors");
			$form->AddCancel("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;

		}
		case "LICENCE STATEMENTS":
		{
			$list = new TableList;
			$list->Begin("statementid","licensors");
			// Get the licence we are going find the statement for
			$licence = DataTable::GetEntryFromID("licences",$_POST['licenceuid']);
			$list->AddTitle("Statements for ".$licence['description']);
			$results = PopulateLicenceStatements($list,$licence,false,false);
//			$list->AddHidden("licenceuid",$_POST['licenceuid']);
			$list->AddAction("REGISTER STATEMENT",true,ACTION_ENABLELIST,$results['expected']);
			$list->AddAction("MARK PAID",true,ACTION_ENABLELIST,$results['awaitingpayment'],ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_MARKPAID);
			$list->AddAction("EDIT STATEMENT",true,ACTION_ENABLELIST,$results['awaitingpayment'].$results['paid'],ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_EDITSTATEMENT);
			$list->AddAction("SET LAST STATEMENT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_LASTSTATEMENT);
			$list->AddAction("EDIT LICENCE",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_LICENCING_EDITLICENCEFROMSTATEMENT);
			$list->AddAction("BACK TO LICENCES",false);
			$aspect->Attach($list);
			$aspect->PreserveVar('licenceuid');
			$aspect->Present();
			break;
		}
		case "SET LAST STATEMENT":
		{
			$aspect->SetNextAction("LICENCE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$statementdata = explode("_",$aspect->GetVar('statementid'));
			$query = "UPDATE licences SET laststatementdate='".MakeDate($statementdata[1])."' WHERE uid='".$_POST['licenceuid']."';";
			Database::Query($query);
			break;

		}
		case "BACK TO LICENCES":
		{
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "REGISTER STATEMENT":
		{
			$statementdata = explode("_",$aspect->GetVar('statementid'));
			$form = new ValidatedForm("licencestatements","statementdate,units,balance,amount,paiddate","statementdate");
			$form->SetValue("statementdate",$statementdata[1]);
			$form->AddHidden('licenceuid',$statementdata[0]);
			$form->AddAction("SUBMIT ROYALTY",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddAction("LICENCE STATEMENTS",false, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_CANCEL);
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT ROYALTY":
		{
/*			$statementdate = MakeDate($_POST['statementdate']);
			Database::Query("INSERT INTO licencestatements (licenceuid,statementdate,paiddate,units,amount) VALUES ('".$_POST['licenceuid']."','".$statementdate."','".getpostdate2("paiddate")."','".$_POST["units"]."','".$_POST["amount"]."');");*/
			$aspect->SetNextAction("LICENCE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("licencestatements","licenceuid,statementdate,paiddate,units,balance,amount",$aspect);
			break;
		}
		case "EDIT STATEMENT":
		{
			$statementdata = explode("_",$aspect->GetVar('statementid'));
			$statementdate = MakeDate($statementdata[1]);
			$licenceuid = $statementdata[0];
			$result = Database::Query("SELECT * FROM licencestatements WHERE licenceuid='".$licenceuid."' AND statementdate='".$statementdate."';");
			$statement = $result->GetNext();
			$form = new ValidatedForm("licencestatements","statementdate,units,amount,paiddate,balance","statementdate");
			$form->SetValues($statement);
			$form->AddHidden('licenceuid',$statementdata[0]);
			$form->AddAction("SAVE STATEMENT",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel("LICENCE STATEMENTS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SAVE STATEMENT":
		{
			$aspect->SetNextAction("LICENCE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$statementdate = MakeDate($_POST['statementdate']);
			$query = "UPDATE licencestatements SET paiddate='".getpostdate2("paiddate")."', units='".$_POST['units']."', amount='".storemoney($_POST['amount'])."' WHERE licenceuid='".$_POST['licenceuid']."' AND statementdate='".$statementdate."';";
			Database::Query($query);
			break;
		}
		case "MARK PAID":
		{
			$form = new Form;
			$form->Begin("licensors");
			$statementdata = explode("_",$aspect->GetVar('statementid'));
			$form->AddHidden("licenceuid",$statementdata[0]);
			$form->AddFixed("Statement Date","statementdate","date",$statementdata[1]);
			$form->AddField("Date Paid","paiddate","date","today");
			$form->AddAction("SUBMIT PAID DATE", ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_CONFIRM);
			$form->AddCancel("LICENCE STATEMENTS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT PAID DATE":
		{
			$aspect->SetNextAction("LICENCE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$statementdate = MakeDate($_POST['statementdate']);
			Database::Query("UPDATE licencestatements SET paiddate='".getpostdate2("paiddate")."' WHERE licenceuid='".$_POST['licenceuid']."' AND statementdate='".$statementdate."';");
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}

$aspect->End();
?>


