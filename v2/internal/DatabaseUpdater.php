<?php
require_once("cDatabase.php");
require_once("cAuthorize.php");

// Put your queries here - you will need to be logged on
/*$queries[] = "ALTER TABLE `sale` ADD `baseprice` VARCHAR( 16 ) NOT NULL AFTER `quantity` ;";
$queries[] = "ALTER TABLE `deals` ADD `packagingdeductions` FLOAT NOT NULL DEFAULT '0' AFTER `reserve` ;";
	*/

if (get_auth_level()>0 && ($_SESSION['userid']=="tom") )
{
	Database::ConnectAdmin("szirtest_mozaic");
	$accounts = Database::QueryGetResults("SELECT DISTINCT uid FROM accounts;");
	Database::Disconnect();
	
	print "<html><body>Updating database...<br><br>\n";
	foreach($accounts as $account)
	{
		print "Connecting to szirtest_".$account['uid']."<br>\n";
		Database::ConnectAdmin("szirtest_".$account['uid']);
		foreach ($queries as $query)
		{
			print "   Query: ".$query."<br>\n";
			Database::Query($query);
		}
		Database::Disconnect();
		print "Done.<br><br>\n";
	}
	print "<br></body></html>\n";
}
else
{
	die("Not authorised");
}
?>