<?php

	include "util.php";
	$linkID = get_database();

	$query = "CREATE TABLE creditor ( name varchar(64), address1 varchar(64), address2 varchar(64), city varchar(16), postcode varchar(8), country varchar(32), phone varchar(32), email varchar(64), description varchar(64) );";
	mysql_query($query,$linkID);
	
	$query = "CREATE TABLE expenses ( invoicedate date, amount int(11), invoice varchar(32), paiddate date, type varchar(32), product varchar(64), description varchar(64));";
	mysql_query($query,$linkID);
	
	$query = "CREATE TABLE product ( product varchar(16), artist varchar(32), title varchar(32), tracks text);";
	mysql_query($query,$linkID);
	
	$query = "CREATE TABLE stock ( date date, order varchar(64), netamount int(11), quantity int(11), product varchar(64), source varchar(64), destination varchar(64), description text);";
	mysql_query($query,$linkID);
	
	$query = "CREATE TABLE tracks ( id varchar(16), artist varchar(64), title varchar(64) );";
	mysql_query($query,$linkID);

?>