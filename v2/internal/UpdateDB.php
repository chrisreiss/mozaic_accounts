<?php
// Very important file - this will update the database to various revisions.
// Need to be logged in as administrator


ini_set("include_path", "../:../classes:../thirdparty:../localise");
ini_set('display_errors', 'On');
error_reporting(E_ALL);

include_once("classes/cDatabase.php");
include_once("classes/cAuthorize.php");
include_once("classes/cCurrency.php");

// Put your queries here - you will need to be logged on
/*$queries[] = "ALTER TABLE `sale` ADD `baseprice` VARCHAR( 16 ) NOT NULL AFTER `quantity` ;";
$queries[] = "ALTER TABLE `deals` ADD `packagingdeductions` FLOAT NOT NULL DEFAULT '0' AFTER `reserve` ;";
	*/

$accountid = $_GET['accountid'];
global $runquery;

$runquery = true;

function Query($query)
{
	global $runquery;
	print "Running query: ".$query."<br>";
	
	if ($runquery) Database::Query($query);
}

function FixCrossdealids()
{
		// Do processing
		// This fixes the artist statement compilations
		// Go through each item, get the deal id - if its a compilation set it to C+ID, if not then set it to crossdealid
		
		// first get all the deals - we want UID, CROSSDEALID
		// then add field COMPILATION
	
		Query("DELETE FROM artiststatements WHERE artist=0");
		
		print 'Finding comps<br>';
		$products = Database::QueryGetResults("SELECT uid,case WHEN artist='0' THEN 1 ELSE 0 END AS iscomp FROM product;");
		foreach($products as $product)
		{
			$iscomp[$product['uid']] = $product['iscomp'];
			if ($product['iscomp']) print $product['uid'].",";
		}
		
		
		print '<br>Artist Statements<br>';
		$artiststatements = Database::QueryGetResults("SELECT uid,artist,crossdealid,startdate FROM artiststatements ORDER BY uid");
		foreach ($artiststatements as $statement)
		{
			print 'Processing artiststatement '.$statement['uid'].' (artist = '.$statement['artist'].', crossdealid = '.$statement['crossdealid'].', startdate = '.$statement['startdate'].')<br>';
//			if ($statement['artist']==168) print'HELLO!!!';
			$crossdealid = $statement['crossdealid'];
			if ($crossdealid[0]=='C')
			{
				$dealid = substr($crossdealid,1);
				$deal = Database::QueryGetResult("SELECT uid,product,crossdealid FROM deals WHERE uid='".$dealid."';");
//				print 'Product = '.$deal['product'].' ('.$iscomp[$deal['product']].') was set as comp<br>';
				
				// this is regsitered as a comp, but it might not be
				// so see if this dealid is a crossdealid for other deals. If it is then it means its a comp which is also the head deal of a crossdealid
				$q = "SELECT COUNT(uid) FROM deals WHERE crossdealid='".$dealid."';";
				$numdeals = Database::QueryGetResult($q);
				if ($numdeals['COUNT(uid)']>1) print 'Incorrect set as comp<br>';
				
				if (!$iscomp[$deal['product']] || $numdeals['COUNT(uid)']>1)
					Query("UPDATE artiststatements SET crossdealid='".$deal['crossdealid']."' WHERE uid='".$statement['uid']."';");
			}
			else
			{
				$dealid = $crossdealid;
				$deal = Database::QueryGetResult("SELECT uid,product,crossdealid FROM deals WHERE uid='".$dealid."';");
				if (empty($deal))
					print 'No deal found to match<br>';
					
//				print 'Product = '.$deal['product'].' ('.$iscomp[$deal['product']].')<br>';
				else if ($iscomp[$deal['product']])
					Query("UPDATE artiststatements SET crossdealid='C".$deal['uid']."' WHERE uid='".$statement['uid']."';");
			
			}
		}
		
		/*
		$deals = Database::QueryGetResults('SELECT uid,crossdealid,product FROM deals WHERE band=5');
		foreach($deals as $deal)
		{
			print 'Deal '.$deal['uid']." for ".$deal['product']." (crossdealid ".$deal['crossdealid'].") is ";
			if ($iscomp[$deal['product']]==1)
			{
				print 'compilation<br>';
				Query("UPDATE artiststatements SET crossdealid='C".$deal['crossdealid']."' WHERE crossdealid='".$deal['uid']."'");
			}
			else
			{
				print 'artist product<br>';
				Query("UPDATE artiststatements SET crossdealid='".$deal['uid']."' WHERE crossdealid='C".$deal['uid']."'");
			}
		}*/
}


function FindCompCrossdeals()
{
//		Query("DELETE FROM artiststatements WHERE artist=0");
		
		
		print '<br>Checking comps as crossdeals<br>';
		$artiststatements = Database::QueryGetResults("SELECT uid,artist,crossdealid,startdate FROM artiststatements ORDER BY uid");
		foreach ($artiststatements as $statement)
		{
			$crossdealid = $statement['crossdealid'];
			if ($crossdealid[0]=='C')
			{
//				$dealid = substr($crossdealid,1);
//				$deal = Database::QueryGetResult("SELECT uid,product,crossdealid FROM deals WHERE uid='".$dealid."';");
//				$q = "SELECT COUNT(uid) FROM deals WHERE crossdealid='".$crossdealid."';";
//				$numdeals = Database::QueryGetResult($q);
//				if ($numdeals['COUNT(uid)']>1) print 'Incorrect set as comp<br>';
				$res  = Database::QueryGetResults("SELECT uid,crossdealid FROM deals WHERE crossdealid='".substr($crossdealid,1)."';");
				if (!empty($res) && count($res)>1)
				{	print 'Processing artiststatement '.$statement['uid'].' (artist = '.$statement['artist'].', crossdealid = '.$crossdealid.', startdate = '.$statement['startdate'].')<br>';
				print_r($res);
				print '<br>';
				}
//				if (!$iscomp[$deal['product']] || $numdeals['COUNT(uid)']>1)
//					Query("UPDATE artiststatements SET crossdealid='".$deal['crossdealid']."' WHERE uid='".$statement['uid']."';");
			}

		}

}


// The artiststatements were linked to artists, they should have been to the band
// so grab crossdeal that each line links to and find out the band for that deal (all crossdeals must be for same band)
function FixArtistStatementsArtistToBand()
{
	$artiststatements = Database::QueryGetResults("SELECT uid,crossdealid FROM artiststatements;");
	foreach($artiststatements as $statement)
	{
		print 'Processing artiststatement uid '.$statement['uid'].' has crossdealid='.$statement['crossdealid'];
		if ($statement['crossdealid'][0]=='C')
			$crossdealid = substr($statement['crossdealid'],1);
		else
			$crossdealid = $statement['crossdealid'];
		
		$query = "SELECT band FROM deals WHERE uid='".$crossdealid."' LIMIT 1";
		$band = Database::QueryGetResult($query);
		print ' changing artist to band '.$band['band'].'<br>';
		//print_r($band); print'<br>';
		
		Query("UPDATE artiststatements SET artist='".$band['band']."' WHERE uid='".$statement['uid']."'");
	}
}

/*so go through artiststataement get the crossdealid, if its a 'C' then check if the deal is a comp (i.e. product artist = 0)
If its not just a comp, then find all other artist statements with this crossdealid. If there is only one then 'remove' the C.
If there are multiple then the last one (order by uid) is the normal one so just remove 'C' from that one.
Or order them by UID, if date or artist changes reset state, make a note of each crossdealid, if duplicate found then rename duplicate.
*/

function CheckDuplicateArtistStatements()
{
	print 'Check Duplicate Artist Statements<br>';
	$artiststatements = Database::QueryGetResults("SELECT uid,artist,startdate,crossdealid,amount FROM artiststatements ORDER BY uid");
	
	$crossdeals = array();
	foreach($artiststatements as $i=>$statement)
	{
		if ($i>0)
		{
			if ($statement['startdate']!=$laststatement['startdate'] || 
				$statement['artist']!=$laststatement['artist'])
			{
				$crossdeals = array();
				print 'reset<br>';
			}
		}
		print $i.' : uid '.$statement['uid'].',startdate '.$statement['startdate'].',artist '.$statement['artist'].',crossdealid '.$statement['crossdealid'].',amount '.$statement['amount'];
//		if ($statement['artist']==168) {print $statement['crossdealid']; print ' ?= '; print_r($crossdeals); }
		if (array_search($statement['crossdealid'],$crossdeals)!==FALSE)
		{
			
			// found duplicate
			print ' this is a duplicate ';
			$deal = Database::QueryGetResult("SELECT product FROM deals WHERE uid='".substr($statement['crossdealid'],1)."'");
			print ' for '.$deal['product'];
			$product = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$deal['product']."'");
			if ($product['artist']==0)
			{
				print ' is comp';	
				// check if a crossdeal comp
				$q = "SELECT COUNT(uid) AS cnt FROM deals WHERE crossdealid='".substr($statement['crossdealid'],1)."' ORDER BY crossdealid";
			//	print $q.'<br>';
				$res = Database::QueryGetResult($q);
				if ($res['cnt']>1)
				{
					print ' to rename to '.substr($statement['crossdealid'],1);
					Query("UPDATE artiststatements SET crossdealid='".substr($statement['crossdealid'],1)."' WHERE uid='".$statement['uid']."'");
				}
			}
		}
		else
		{
			$crossdeals[] = $statement['crossdealid'];
		}
		
		$laststatement = $statement;
		print '<br>';
			
	}

}


function SplitToFloat()
{
	Query("ALTER TABLE  `bandartist` CHANGE  `split`  `split` FLOAT( 4 ) NOT NULL DEFAULT  '100'");
}

function ConvertCurrencyFormat()
{
	function convert($in)
	{
		return Currency::GETRAWVALUE($in)/100;
	}

	$sales = Database::QueryGetResults("SELECT uid,baseprice,value,mechanicals FROM sale");
	print "ConvertCurrencyFormat (".count($sales)." records)";
	foreach($sales as $sale)
	{
//		Query("UPDATE sale SET baseprice='".convert($sale['baseprice'])."',value='".convert($sale['value'])."',mechanicals='".convert($sale['mechanicals'])."' WHERE uid='".$sale['uid']."'");
	}
	
//	Query("ALTER TABLE  `sale` CHANGE  `value`  `value` FLOAT( 16 ) NOT NULL DEFAULT  '0'");
	Query("ALTER TABLE  `sale` CHANGE  `baseprice`  `baseprice` FLOAT( 16 ) NOT NULL DEFAULT  '0'");
	Query("ALTER TABLE  `sale` CHANGE  `mechanicals`  `mechanicals` FLOAT( 16 ) NOT NULL DEFAULT  '0'");

}

if (Authorize::IsAuthorized() && ($_SESSION['userid']=="tom") )
{
	Database::Connect("szirtest_mozaic");
	if ($accountid=='*')
		$accounts = Database::QueryGetResults("SELECT `uid`,`name`,`database` FROM accounts;");
	else
		$accounts = Database::QueryGetResults("SELECT `uid`,`name`,`database` FROM accounts WHERE uid=$accountid LIMIT 1;");
	
	if (!$runquery) print "Dummy run - no transactions will occur<br><br>";

	foreach ($accounts as $account)
	{
		$accountdb = $account['database'];
		$accountid = $account['uid'];
		$accountname = $account['name'];
		
		print "Processing account '$accountname' ($accountid) on database 'szirtest_$accountdb'<br>";
		Database::Connect("szirtest_".$accountdb);
	
	//	Query("ALTER TABLE  `tracks` ADD  `version` VARCHAR( 64 ) NOT NULL AFTER  `title` , ADD  `remixer` VARCHAR( 64 ) NOT NULL AFTER  `version`");	
	//	Query("ALTER TABLE  `deals` CHANGE  `dealtype`  `dealtype` ENUM(  'P',  'R',  'L',  'S' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  'P'");
		
	//	FixCrossdealids();		
//		SplitToFloat();

//		FixCrossdealids();		
	
	
		FixArtistStatementsArtistToBand();
		CheckDuplicateArtistStatements();
		
	//	CheckForComps();
	//	CheckDuplicateArtistStatements();
//	FindCompCrossdeals();

	//	ConvertCurrencyFormat();
	}

}
else
{
	die("Not authorised to run this script");
}	

?>