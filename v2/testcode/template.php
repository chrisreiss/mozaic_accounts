<?php
require_once('cAuthorize.php');
require_once('cTabNav.php');



class Template {
// This should be the first thing you call
function Authenticate()
{
	if (Authorize::IsAuthorized())
	{
		Authorize::ResetTimeout();
	}
	else
	{
		if ($_SERVER['SERVER_NAME']!='localhost')
			header('Location:https://www.mozaicaccounts.com/login?error=4');
		else
			header('Location:http://localhost/~Tom/MozaicAccountsLive/public_html/login?error=4');
	}
}



function doHTMLHeader()
{
	print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head>'."\n";
	print '<title>Mozaic Accounts - Royalty Accounting Solutions (TEST)</title>'."\n";
	print '<META NAME="keywords" CONTENT="royalties,accounting,music,record label,application,statements,licencing,calculating">';
	print '<META NAME="description" CONTENT="Affordable On-line Royalty Accounting Solutions for the Music Industry">';
	print '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'."\n";
	print '<link href="./main2.css" rel="stylesheet" type="text/css">'."\n";
    print '<link rel="stylesheet" href="AutoComplete.css" media="screen" type="text/css">'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/md5.js"></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="javascript/macc.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="javascript/ajax.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/DynamicOptionList.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/autocomplete.js" ></SCRIPT>'."\n";
	print '</head>'."\n\n";
}

function doBanner($login)
{

	if ($login==1)
	{
		if (isset($_SESSION) && isset($_SESSION['accountid']))
		{
			print "<p align=right>You are logged in as <b>".$_SESSION['displayname']."</p>";
			$logofilename = "../logos/".$_SESSION['accountid'].".jpg";
			if (file_exists($logofilename))
			{
				$size = getimagesize($logofilename);
				if ($size[0]>320) { $size[1] *= (320/$size[0]); $size[0] = 320; }
				if ($size[1]>55) { $size[0] *= (55/$size[1]); $size[1] = 55; }
				print "<p align=right><img src='$logofilename' width='".$size[0]."' height='".$size[1]."'></p>";
			}
			print "<a href='mailto:support@mozaicaccounts.com'>Support</a> | <a href='logout.php'>Logout</a>";
		}
		else
		{
		}
	}

	
	print "</td></tr></table>";
	print "</div>\n";
	print '<!-- main content -->'."\n";
}

function	Top()
{
	Template::doHTMLHeader();

	print '<body onLoad="initDynamicOptionLists();" bgcolor=white>'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/wz_tooltip.js" ></SCRIPT>'."\n";
	print "<div id='container'>\n";
	print "<div id='header'>\n";
	print "<table border=0 padding=0 cell-spacing=0 width=100%><tr><td>";
	print "</td><td align=right valign=top>";
	print "<p align=right>You are logged in as <b>".$_SESSION['displayname']."</p>";
	$logofilename = "../logos/".$_SESSION['accountid'].".jpg";
	if (file_exists($logofilename))
	{
		$size = getimagesize($logofilename);
		if ($size[0]>320) { $size[1] *= (320/$size[0]); $size[0] = 320; }
		if ($size[1]>55) { $size[0] *= (55/$size[1]); $size[1] = 55; }
		print "<p align=right><img src='$logofilename' width='".$size[0]."' height='".$size[1]."'></p>";
	}
	print '<a href="mailto:support@mozaicaccounts.com">Support</a> | <a href="logout.php">Logout</a>';
	print '</td></tr></table>';
	print '</div>';

}

function TopLevelMenuBar($activepage)
{
	print "<div id='menu'>\n";
	print '<!-- menu bar -->'."\n";
	$tabBar = new TabNav;
	if ($_SESSION['userid']=="tom") 
		$tabBar->AddTab("Admin","page","admin",STRING_TOOLTIP_BUTTON_HOME);
	
	$tabBar->AddTab("Home","page","home",STRING_TOOLTIP_BUTTON_HOME);
	$tabBar->AddTab("Bands","page","artists",STRING_TOOLTIP_BUTTON_BANDS);
	$tabBar->AddTab("Partners","page","partners",STRING_TOOLTIP_BUTTON_PARTNERS);
	$tabBar->AddTab("Tracks","page","tracks",STRING_TOOLTIP_BUTTON_TRACKS);
	$tabBar->AddTab("Product","page","products",STRING_TOOLTIP_BUTTON_PRODUCT);
	$tabBar->AddTab("Deals","page","deals",STRING_TOOLTIP_BUTTON_DEALS);
	$tabBar->AddTab("Sales","page","sales",STRING_TOOLTIP_BUTTON_SALES);
	if (array_search('licensing',$_SESSION['packages'])!==FALSE) 
	{
		$tabBar->AddTab("Licensing","page","licensors",STRING_TOOLTIP_BUTTON_LICENCING);
	}
	$tabBar->AddTab("Expenses","page","expenses",STRING_TOOLTIP_BUTTON_EXPENDITURE);

	$tabBar->AddTab("In/Out","page","import",STRING_TOOLTIP_BUTTON_INOUT);
	if (array_search('promo',$_SESSION['packages'])!==FALSE) 
	{
		$tabBar->AddTab("Promos","page","promos",STRING_TOOLTIP_BUTTON_HOME);
	}
	
	$tabBar->SetActiveTab($activepage);
	$tabBar->Display();
	print '</div>';
}



function Bottom()
{
//	print '</table></td></tr></table></td></tr></table></center>
	
	print "<div id='footer'>\n";
	print "<center>".$_SERVER['SERVER_NAME']."</center>";
	print "</div>";
	print "</div>";
//	print $_SESSION['querylog'];
//	$_SESSION['querylog'] = "";
	print "</body></html>";
}
}

?>