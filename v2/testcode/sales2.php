<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect2.php');
require_once( 'cDatabase.php');
require_once( 'cImport.php');
require_once( 'cValidatedForm.php' );

Database::Init();

// Defined commands
define('ACTION_VIEW',0);
define('ACTION_ADD_SALES',1);
define('ACTION_EDIT_SALE',2);
define('ACTION_SAVE',3);
define('ACTION_SUBMIT_SALE',4);
define('ACTION_REMOVE_SALES',5);



class	SalesAspect extends Aspect
{
	function SalesAspect()
	{
		$this->m_name = "sales2";
	}

	function ProcessAction($action,$data)
	{
		// By default go back to view
		$this->SetNextAction(ACTION_VIEW);
		
		switch($action)
		{
			case ACTION_VIEW:
			{
				if (isset($data['saleid2']))
					$data['selectedgroup']=$data['saleid2'];
	
				$list = new TableList;
				$list->Begin("saleid","sales");
				$list->SetSplitMode(true);
				$list->AddView("by product","sale","%date% : %distributor.creditor:name% (%value%)","product","date","%product% %product.product|tracks:artist.bands:name% - %product.product|tracks:title%");
				$list->AddView("by distributor","sale","%date% : %product.tracks|product:title% (%quantity% units)","distributor","date","%distributor.creditor:name%");
				$list->SetMultipleSelection(true);
				$list->AddAction(ACTION_ADD_SALES,false);
				$list->AddAction(ACTION_REMOVE_SALES,true);
				$list->AddAction(ACTION_EDIT_SALE,true);
				$list->SetInfoBoxHandler("getsalesinfo");
				// Add total sales
				if (isset($data['selectedgroup']))
				{
					$result = Database::QueryGetResult("SELECT SUM(quantity) FROM sale WHERE product='".$data['selectedgroup']."';");
					$list->AddFooter($result['SUM(quantity)']." total sales.");
				}
				$this->Attach($list);
				$this->Present();
				break;
			}
			case ACTION_ADD_SALES:
			{
				if (!isset($data['addcount'])) $data['addcount']=1;
					
				print "Add Sales ".$data['addcount']."<hr>";
				$form = new ValidatedForm("sale","product,track,distributor,quantity,value,saletype,date");
				if (isset($data['saleid2'])) $form->SetValue("product",$data['saleid2']);
				if ($data['addcount']>1)  $form->SetValuesFromPost($this,"distributor,saletype,date");
	
				$form->SetFilter("product","track");
				$form->SetEventCode("track","onChange=\"ChangeSaleType(this)\"");
				$form->AddAction("SAVE",true);	// validate before sending
				$form->AddCancel("FINISHED");
				$this->Attach($form);
				$this->Present();
				break;
			}
			case ACTION_EDIT_SALE:
			{
				$saleid = $data['saleid'][0];
				$form = new ValidatedForm("sale","product,track,distributor,quantity,value,saletype,date");
				$form->SetValuesFromDB("sale",$saleid);

				$form->AddHidden("saleid",$saleid);
				$form->AddHidden("saleid2",$saleid);

				$form->AddAction("SUBMIT SALE",true);	// validate before sending
				$form->SetFilter("product","track");
				$form->AddCancel();
				$this->Attach($form);
				break;
			}
			case ACTION_SAVE:
			{
				$this->SetNextAction(ACTION_ADD_SALES);
				// Demo safety check
				if (IsDemoAccount()) { $this->Error(STRING_DEMO); break; }
				
				DataTable::InsertFromPost("sale","product,track,distributor,quantity,value,saletype,date",$this);
				$data['addcount']+=1;
				break;
			}
			case ACTION_SUBMIT_SALE:
			{
				// Demo safety check
				if (IsDemoAccount()) { $this->Error(STRING_DEMO); break; }
	
				DataTable::UpdateFromPost("sale",$data['saleid'],"product,track,distributor,quantity,value,saletype,date",$this);
				break;
			}
			case ACTION_REMOVE_SALES:
			{
				// Demo safety check
				if (IsDemoAccount()) { $this->Error(STRING_DEMO); break; }
	
				foreach ($data['expenseid'] as $value)
				{
					DataTable::Delete("sale",$data['saleid']);
				}
				break;
			}
			default:
			{
			}
		}
		return false;
	}
}

$aspect = new SalesAspect;
$aspect->Process();
	
?>