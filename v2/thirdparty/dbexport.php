<?php
    /*
    Created by : Yudhi Jatnika(yudi_jatnika@worxcode.com)(yudhi.jatnika@gmail.com)
    *********************WORXCODE********************
    ********************01-25-2006*******************
    */

require_once("cDatabase.php");

class db_restore{

    var $link_id;
    var $db_sel;
    var $file_name;
    var $file_handle;
    var $read_file;
    var $contents;
	var $error;

    function db_restore($linkID,$file_name)
	{
		set_time_limit(1200);
		$this->file_name = $file_name;
        print "Restoring from ".$this->file_name."<br>";
        $this->file_handle = fopen($this->file_name, "r");
        $this->contents = fread($this->file_handle, filesize($this->file_name));
        $exploe = explode("<query>", $this->contents);
        $nExploe = sizeof($exploe);
        for($n=1;$n<$nExploe;$n++)
		{
            print "Processing: $exploe[$n]<br>";
			$query = Database::Query($exploe[$n]);
        }
    }

	function was_errors()
	{
		return !($this->error=="");
	}
	
	function get_errors()
	{
		return $this->error;
	}
}
    /*
    Created by : Yudhi Jatnika(yudi_jatnika@worxcode.com)(yudhi.jatnika@gmail.com)
    *********************WORXCODE********************
    ********************01-25-2006*******************
    */
//configurator
class db_backup{

    var $link_id;
	var $filename;
	
    function db_backup($linkid,$filename)
	{
		$this->link_id = $linkid;
    	$this->filename = $filename;
		 
        $whatToWrite = $this->_getRetrive();
        $this->_saveToFile($whatToWrite);
    }
    
    function _query($query){
        return $result = mysql_query($query);
    }
    
    function _getTable(){
    
        $getTable = array();
        
        $result = $this->_query("SHOW TABLES");
        while($row = mysql_fetch_row($result)){
            $getTable[] = $row[0];
        }
        if(!sizeof($getTable)){
            return "No tables found in database $dbname";
        }elseif(sizeof($getTable)){
            return $getTable;
        }
    }
    
    function _getDumpTable($table){
    
        $getDumpTable = "";
        
        $q_getDumpTable = $this->_query("LOCK TABLES".$table."WRITE");
        
        $getDumpTable .= "<query>DROP TABLE IF EXISTS ".$table."\r\n";
        
        $result = $this->_query("SHOW CREATE TABLE ".$table);
        $row = mysql_fetch_assoc($result);
        
        $getDumpTable .= "<query>".str_replace("\n", "\r\n", $row['Create Table'])."\r\n";
        $getDumpTable .= $this->_getInsertsTable($table);
        
        $this->_query("UNLOCK TABLES");
        
        return $getDumpTable;
    }
    
    function _getInsertsTable($table){
    
        $getInsertsTable = "";
        
        $result = $this->_query("SELECT * FROM ".$table);
        
        while($row = mysql_fetch_row($result)){
            $getInsertsTables = "";
            foreach($row as $data){
				$data = str_replace("'","\'",$data);
                $getInsertsTables .="'".$data."', ";
            }
            $getInsertsTables = substr($getInsertsTables, 0, -2);
            $getInsertsTable .= '<query>INSERT INTO '.$table.' VALUES ('.$getInsertsTables.')'."\r\n";
        }
        
        return $getInsertsTable;
        
    }
    
    function _getRetrive(){
        
        $getRetrive = "";
        
        $g_table = $this->_getTable();
        foreach($g_table as $table){
            $getRetrive .= $this->_getDumpTable($table);
        }
        return $getRetrive;
    }
    
    function _saveToFile($whatToWrite){
        $f_open = fopen($this->filename, "w");
        $f_write = fwrite($f_open, $whatToWrite, strlen($whatToWrite));
        fclose($f_open);
    }
}
?>