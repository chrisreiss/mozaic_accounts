// Javascript document



function Ajax() {
	this.req = null;
	this.url = null;
	this.method = 'GET';
	this.async = true;
	this.status = null;
	this.statusText = '';
	this.postData = null;
	this.readyState = null;
	this.responseText = null;
	this.responseXML = null;
	
	this.handleResp = null;
	this.responseFormat = 'text';
	this.mimeType = null;
	

	this.init = function () 
	{
		if (!this.req)
		{
			try {
				this.req = new XMLHttpRequest();
			}
			catch (e)
			{ 
				try {
					this.req = new ActiveXObject('Microsoft.XMLHTTP');
				}
				catch (e1) 
				{ 
					try {
						this.req = new ActiveXObject("Msxml2.XMLHTTP");
					} 
					catch (e2) 
					{
						// both methods failed 
						return false;
					}
				} 
			}
		}
		return this.req;
	};
	
	this.doReq = function() {
		if (!this.init()) {
			alert('Could not create XMLHttpRequest object.');
			return;
		}
		this.req.open(this.method, this.url, this.async);
		var self = this;
		this.req.onreadystatechange = function()
		{
			if (self.req.readyState == 4)
			{
				switch (self.responseFormat) {
					case 'text':
						resp = self.req.responseText;
						break;
					case 'xml':
						resp = self.req.responseXML;
						break;
					case 'object':
						resp = req;
						break;
				}
				if (self.req.status >= 200 && self.req.status <= 299)
				{
					self.handleResp(resp);
				}
				else
				{
					self.handleErr(resp);
				}
			}
		}
		this.req.send(this.postData);
	}

	this.handleErr = function() 
	{
		var errorWin;
		try {
			errorWin = window.open('','errorWin');
			errorWin.document.body.innerHTML = this.responseText;
		}
		catch(e)
		{
			alert ('An error occured and could not open error window\n' +
					'Status Code: '+this.req.status +'\n' +
					'Status Description: '+this.req.statusText);
		}
	}
	
	this.abort = function()
	{
		if (this.req) {
			this.req.onreadystatechange = function() {};
			this.req.abort();
			this.req = null;
		}
	}
	
	this.doGet = function(url, hand, format) 
	{
		this.url = url;
		this.handleResp = hand;
		this.responseFormat = format || 'text';
		this.doReq();
	}	
}

