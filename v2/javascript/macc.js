// JavaScript Document

function DVClickRow(obj,row,multiple)
{
	try {
		var evtobj=window.event? event : obj;
		if (!multiple && !evtobj.shiftKey)
		{
		 	var i = 0;
			// clear previous selections
			for (trchild in obj.parentNode.parentNode.childNodes)
			{
				var node = obj.parentNode.parentNode.childNodes[trchild];
				if (trchild%2==0)
					node.className='';
				else
					node.className='odd';

			//	node.firstChild.firstChild.checked = false;
				//if (node.type=="tableRow") node.firstChild.firstChild.checked = false;
			}
		}
		
		{
			if (obj.parentNode.className!='selected')
			{
				obj.parentNode.firstChild.firstChild.checked = true;
				obj.parentNode.className='selected';
			}
			else
			{
				obj.parentNode.firstChild.firstChild.checked = false;
				if (row%2==0)
					obj.parentNode.className='';
				else
					obj.parentNode.className='odd';
			}
		}

	}
	catch(ex){
		alert(ex);
	}
}

function AddTrack(obj,name) 
{
	try {
		objcopy = obj.parentNode.cloneNode(true);
		obj.parentNode.parentNode.insertBefore(objcopy, obj.parentNode.nextSibling);
		RenumberTracks(name);
	}
	catch(ex) {
		alert(ex);
	}
}

function substr(subject,start,length)
{
	var substring='';
	for (var i=start; i<start+length; i++)
	{
		substring += subject[i];
	}
	return substring;
}


function RenumberTracks(name)
{
	try {
		var objs = document.getElementsByTagName("SELECT");
		var trackcount = 0;
		for (var i=0; i<objs.length; i++) {
			if (substr(objs[i].name,0,name.length)==name) {
				objs[i].name = name+'_'+trackcount;
				trackcount++;
			}
		}
	}
	catch(ex) {
		alert(ex);
	}
}
			
function RemoveTrack(obj,name) 
{ 
  try 
  {
    var objs = document.getElementsByTagName("SELECT");
	var trackcount = 0;
	for (var i=0; i<objs.length; i++) 
	{ 
		if (objs[i].parentNode == obj.parentNode) 
		{
 			obj.parentNode.parentNode.removeChild(obj.parentNode);
			RenumberTracks(name);
			break;
		}
	}
  } 
  catch (ex) 
  { 
    alert (ex); 
  }
}

function AddCheckboxToList(obj,name)
{
  try 
  {
    var newvalue = obj.previousSibling.value;
  	but = document.createElement('INPUT');
	but.type = 'checkbox';
	but.name = name+'[]';
	but.checked = true;
	but.value = newvalue;
	
	br = document.createElement('BR');
	text = document.createTextNode(' '+newvalue);
	obj.parentElement.insertBefore(br,obj.previousSibling.previousSibling);
	obj.parentElement.insertBefore(text,br);
	obj.parentElement.insertBefore(but,text);
	obj.previousSibling.value = '';	// clear
  }
  catch(ex)
  {
  	alert(ex);
  }
  return false;
}

function CheckSplit(form)
{
	var fieldname = "split";
	var i=0;
	var total = Number(0);
	while(true)
	{
		var groupfieldname = fieldname + "_" + i;
		var field = document.getElementById(groupfieldname);
		if (field==null) break;
		total += Number(field.value);
		++i;
	}
	if (total>=99 && total<=101)
		return true;	
	
	return false;
}

function checkemptydate(form,fieldname)
{
	if (document.getElementById(fieldname)!=null)
	{
		if (form.elements[fieldname+"_day"].value=="" ||
			form.elements[fieldname+"_month"].value=="" ||
			form.elements[fieldname+"_year"].value=="")
		{
			form.elements[fieldname+"_day"].focus();
			return false;
		}
		return true; 
	}
	// check all group elements too
	var i=0;
	while(true)
	{
		var groupfieldname = fieldname + "_" + i;
		if (document.getElementById(groupfieldname)==null) break;
		if (form.elements[groupfieldname+"_day"].value=="" ||
			form.elements[groupfieldname+"_month"].value=="" ||
			form.elements[groupfieldname+"_year"].value=="")
		{
			form.elements[groupfieldname+"_day"].focus();
			return false;
		}
		++i;
	}
	return true;	
}


function _validatedate2(form,fieldname)
{
	//This function is used to validate date in the format dd/mm/yyyy (U.K. style)
   var value = form.elements[fieldname].value;
   var validformat=/^\d{1,2}\/\d{1,2}\/\d{4}$/ //Basic check for format validity
   if (!validformat.test(value))
		return false;

	//Detailed check for valid date ranges
	var dayfield=value.split("/")[0];
	var monthfield=value.split("/")[1];
	var yearfield=value.split("/")[2];
   
	var dayobj = new Date(yearfield, monthfield-1, dayfield);
	if ((dayobj.getMonth()+1!=monthfield)||
		(dayobj.getDate()!=dayfield)||
		(dayobj.getFullYear()!=yearfield))
	{
		//alert('Invalid Day, Month, or Year range detected. Please correct.')
		return false;
	}

	return true;
} 


function _validatedate(form,fieldname)
{
	var day =   form.elements[fieldname+"_day"].value;
	var month = form.elements[fieldname+"_month"].value;
	var year =  form.elements[fieldname+"_year"].value;
	
	var d = new Date(month + "/" + day + "/" + year);
	
	if (day!="" || month!="" || year!="") 
	{
		if (d.getMonth()+1!=month || d.getDate()!=day || d.getFullYear()!=year) 
		{	
			form.elements[fieldname+"_day"].focus(); 
			return false;  
		}
		else
		{ 
			form.elements[fieldname].value = year+"-"+month+"-"+day; 
		} 
	}
	return true;
}

// Validates all dates with the given fieldname
function ValidateDate(form,fieldname)
{
	try {
	if (document.getElementById(fieldname)!=null)
	{
		return _validatedate(form,fieldname);
	}
	// check all group elements too
	var i=0;
	while(true)
	{
		var groupfieldname = fieldname + "_" + i;
		if (document.getElementById(groupfieldname)==null) break;
		if (!_validatedate(form,groupfieldname))
		{ 
			return false; 
		}
		++i;
	}
	return true;
	}
	catch (ex)
	{
		alert(ex);
	}
}


function _validatePercentage(form,fieldname)
{
	if (form.elements[fieldname].value<0 || 
		form.elements[fieldname].value>100 ||
		!IsNumeric(form.elements[fieldname].value) ) 
	{ 
		form.elements[fieldname].focus(); 
		return false; 
	}
	return true;
} 

// Validates all dates with the given fieldname
function ValidatePercentage(form,fieldname)
{
	if (document.getElementById(fieldname)!=null)
	{
		return _validatePercentage(form,fieldname);
	}
	// check all group elements too
	var i=0;
	while(true)
	{
		var groupfieldname = fieldname + "_" + i;
		if (document.getElementById(groupfieldname)==null) break;
		if (!_validatePercentage(form,groupfieldname))
		{ 
			return false; 
		}
		++i;
	}
	return true;
}

function ChangeSaleType(obj)
{
	var saletypeobj = document.getElementById("saletype");
	if (obj.value==0)
	{
		saletypeobj.value = 'P';
//		saletypeobj.disabled = false;
	}
	else
	{
		saletypeobj.value = 'V';
//		saletypeobj.disabled = true;
	}
}



function ValidateCurrency(form,fieldname)
{
	if (document.getElementById(fieldname)!=null)
	{
		if (!IsCurrency(form.elements[fieldname].value)) 
		{ 
			form.elements[fieldname].focus();
			return false; 
		}
		return true; 
	}
	// check all group elements too
	var i=0;
	while(true)
	{
		var groupfieldname = fieldname + "_" + i;
		if (document.getElementById(groupfieldname)==null) break;
		if (!IsCurrency(form.elements[groupfieldname].value)) 
		{ 
			form.elements[groupfieldname].focus();
			return false; 
		}
		++i;
	}
	return true;	
}	



function	CheckEmpty(form,fieldname)
{
	if (document.getElementById(fieldname)!=null)
	{
		if (form.elements[fieldname].value=="")
		{
			form.elements[fieldname].focus();
			return false;
		}
		return true; 
	}
	// check all group elements too
	var i=0;
	while(true)
	{
		var groupfieldname = fieldname + "_" + i;
		if (document.getElementById(groupfieldname)==null) break;
		if (form.elements[groupfieldname].value=="")
		{
			form.elements[groupfieldname].focus();
			return false;
		}
		++i;
	}
	return true;	
}



// This function goes through each field in a form that belongs to a "group"
// and renames it sequentially by name_<id> where <id> is an ascending integer
function SetInputNames(name)
{
	var form = document.getElementById("validateform");
	var idcount = 0;
	var rows = document.getElementsByTagName("TR");
	for (var j=0; j<rows.length; j++)
	{
		matchname = rows[j].id.split("_");
		if (matchname[0]==name) 
		{
			rows[j].id = matchname[0] + "_" + idcount;
			for (var i=0; i<form.length; i++)
			{
				if (form.elements[i].parentNode.parentNode == rows[j])
				{
					// belongs to same group
					inputname = form.elements[i].name.split("_");
					form.elements[i].name = inputname[0] + "_" + idcount;
					form.elements[i].id = inputname[0] + "_" + idcount;
				}
			}
			++idcount;
		}
	}
}

// Duplicate a group - add it to the next position and rename it
//function AddGroup(obj) 

			
function RemoveGroup(obj) 
{ 
	name = obj.parentNode.parentNode.id.split("_");
	var objs = document.getElementsByTagName("TR");
	var count = 0;
	for (var i=0; i<objs.length; i++) 
	{ 
		matchname = objs[i].id.split("_");
		if (matchname[0]==name[0]) 
		{ 
			count++; 
		}
	}
	if (count>1)
	{
		obj.parentNode.parentNode.parentNode.removeChild(obj.parentNode.parentNode); 
	}
	SetInputNames(name[0]);
}
			
function updateDualPanelist(ob)
{
	selected = new Array();
	for (var i = 0; i < ob.options.length; i++) 
	{
		if (ob.options[ i ].selected) 
		{
			selected.push(ob.options[ i ].value);
		}
	}
	myform = document.getElementById('viewform');
	var newinput = document.createElement('input');
	newinput.setAttribute('type','hidden');
	newinput.setAttribute('name','selectedgroup');
	newinput.setAttribute('value',selected[0]);
	myform.appendChild(newinput);
	myform.submit();
}

function IsNumeric(strString) 
{ 
	var strValidChars = '0123456789.-'; 
	var strChar; 
	for (i = 0; i < strString.length; i++) 
	{
		strChar = strString.charAt(i); 
		if (strValidChars.indexOf(strChar) == -1) 
		{ 
			return false; 
		} 
	} 
	return true; 
}
	
function IsCurrency(val)
{ 
	cur  = /^([�,$])?-?\d{1,6}(,\d{3})*(\.\d{1,2})?(GBP)?(EUR)?(USD)?$/;
	return cur.test(val); 
}


function AddGroup(obj)
{
	objcopy = obj.parentNode.parentNode.cloneNode(true);
	name = objcopy.id.split("_");
	groupid = parseInt(name[1])+1;
	objcopy.id = name[0] + "_" + groupid;
	// now go through each input and rename
	obj.parentNode.parentNode.parentNode.insertBefore(objcopy,obj.parentNode.parentNode.nextSibling);
	SetInputNames(name[0]);
}


function	RepostForm(obj,action)
{
	var form = document.getElementById('validateform');
	inputs = form.getElementsByTagName('input');
	inputs += form.getElementsByTagName('select');
	var but;
	var numfields = inputs.length;
/*	for(var i=0;i<numfields;i++)
	{
		if (inputs[i].name!='submitvalue')
		{
			but = document.createElement('INPUT');
			but.type = 'hidden';	
			but.name = inputs[i].name;
			but.value = inputs[i].value;
//			alert(i+' '+but.name+' '+but.value);
			form.appendChild(but);
		}
	}*/
	but = document.createElement('INPUT');
	but.type = 'hidden';
	but.name = 'submitvalue';
	but.value = action;
	form.appendChild(but);
	var value;
	for (var j = 0; j < obj.options.length; j++) 
	{
		if (obj.options[j].selected) 
		{ 
			value=obj.options[j].value; 
			break;
		}
	}
	but = document.createElement('INPUT');
	but.type = 'hidden';
	but.name = 'refreshedpost';
	but.value = 1;
	form.appendChild(but);
	form.submit();  
}

function	licencetypechanged(obj,action)
{
	var form = document.getElementById('validateform');
	inputs = form.getElementsByTagName('input');
	var but;
	var numfields = inputs.length;
	for(var i=0;i<numfields;i++)
	{
		if (inputs[i].name!='submitvalue')
		{
			but = document.createElement('INPUT');
			but.type = 'hidden';	
			but.name = inputs[i].name;
			but.value = inputs[i].value;
			form.appendChild(but);
		}
	}
	but = document.createElement('INPUT');
	but.type = 'hidden';
	but.name = 'submitvalue';
	but.value = action;
	form.appendChild(but);
	var value;
	for (var j = 0; j < obj.options.length; j++) 
	{
		if (obj.options[j].selected) 
		{ 
			value=obj.options[j].value; 
			break;
		}
	}
	but = document.createElement('INPUT');
	but.type = 'hidden';
	but.name = 'licencetype';
	but.value = value;
	form.appendChild(but);
	but = document.createElement('INPUT');
	but.type = 'hidden';
	but.name = 'licencetypechanged';
	but.value = 1;
	form.appendChild(but);
	form.submit();  
}

