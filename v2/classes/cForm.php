<?php
require_once('cEditField.php');

class Form
{
	var $m_group;
	var $m_tableformat;
	var $m_target;
	var $m_fields;
	var $m_title;
	var $m_parent;
	var $m_insertedhtml;
	
	function SetTarget($target)
	{
		$this->m_target = $target;
	}

	function Begin($target)
	{
		$this->m_target = $target;
		$this->m_group = "";
		$this->m_title ="";
		$this->m_tableformat = "horizontal";
	}

	function	InsertHTML($html)
	{
		$this->m_insertedhtml = $html;
	}
	
	function AddTitle($title)
	{
		$this->m_title = $title;
	}

	function AddNewLine()
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'newline','name'=>'','value'=>'','description'=>'','type'=>'');
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'newline','name'=>'','value'=>'','description'=>'','type'=>'');
	}
	
	function AddField($descrip,$name,$template,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'field','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'field','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
	}
	
	function AddSelectionList($descrip,$name,$list,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'list','name'=>$name,'value'=>$value,'description'=>$descrip,'list'=>$list);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'list','name'=>$name,'value'=>$value,'description'=>$descrip,'list'=>explode(",",$list));
	}

	function AddFixed($descrip,$name,$template,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'fixed','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'fixed','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
	}
	
	function AddHidden($name,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'hidden','name'=>$name,'value'=>$value);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'hidden','name'=>$name,'value'=>$value);
	}

	// fields
	function StartGroupSet($startid,$endid)
	{
		$this->m_fields[] = array('property'=>'groupset','name'=>'groupset1','startid'=>$startid,'endid'=>$endid);
		$this->m_group = 'groupset1';
	}
	
	
	function EndGroupSet()
	{
		$this->m_group = '';
	}
	
	function AddCancel()
	{
		$args = func_get_args();
		if (!empty($args[0]))
			$value = $args[0];
		else
			$value = "CANCEL";
			
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'button','name'=>'submitvalue','value'=>$value,'description'=>$value);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'button','name'=>'submitvalue','value'=>$value,'description'=>$value);
	}
	
	function AddAction($submit)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'button','name'=>'submitvalue','value'=>$submit);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'button','name'=>'submitvalue','value'=>$submit);
	}
	
	function End()
	{
		die("Depreciated end function in form");
	}

	function	_presentField(&$field,$id)
	{
		switch($field['property'])
		{
		case 'newline' :
			print "</tr><tr>";
			break;
		case 'button':
			print "<td><button class='submit' type='submit' name='".$field['name'].$id."' value='".$field['value']."'>".$field['value']."</button></td>";
			break;
		case 'field':
			if ($this->m_group=="" || $this->m_tableformat!="horizontal") 
				print "<td>".$field['description']."</td>";
			print "<td>";
			EditField($field['name'].$id,$field['type'],$field['value']);
			print "</td>";
			break;
		case 'list':
			if ($this->m_group=="" || $this->m_tableformat!="horizontal") 
				print "<td>".$field['description']."</td>";
			print "<td>";
//			print_r($field); print'<br>';
			DropListFromString($field['list'],$field['name'],$field['value'],"");
			print "</td>";
			break;
		case 'fixed':
			if ($this->m_group=="" || $this->m_tableformat!="horizontal") 
				print "<td>".$field['description']."</td>";
			print "<td>";
			print $field['value'];
			print "</td>";
			// let fall through to hidden!!!
		case 'hidden':
			print "<input type=hidden name='".$field['name'].$id."' value='".$field['value']."'>";
			break;
		default:
			die("Unknown form field property");
		}
	}
	
	function SetAspect(&$aspect)
	{
		$this->m_parent = $aspect;
	}
	
	function Present()
	{
		print "<p>";
//		print_r($this->m_groupfields);
		print "</p>";

		// Start form
		print "<form name='form1' method='post' action='index.php' enctype='multipart/form-data'>\n
			   <input type='hidden' name='page' value='".$this->m_target."'>\n";

		print "<table>\n";
		if ($this->m_title!="")
			print "<tr><td>".$this->m_title."</td></tr>\n";
		
		if (!empty($this->m_insertedhtml))
			print $this->m_insertedhtml;
			
		foreach ($this->m_fields as $field)
		{
			if ($field['property']!='groupset')
			{
				print "<tr>";
				$this->_presentField($field,"");
				print "</tr>\n";
			}
			else
			{
				// is horizontal
				$this->m_group = $field['name'];
				if ($this->m_tableformat=="horizontal")
				{
					print "<tr>";
					foreach ($this->m_groupfields[$field['name']] as $field2)
					{
						print "<td>";
						if (!empty($field2['description']))
							print $field2['description'];
						print "</td>";
					}
					print "</tr>\n";
				} 
				for ($id = $field['startid'];$id <= $field['endid'];++$id)
				{
					print "<tr>";
					if (isset($this->m_groupfields[$field['name']]))
					{
						foreach ($this->m_groupfields[$field['name']] as $field2)
						{
							$this->_presentField($field2,$id);
						}
					}
					else
					{
						print "<td>Undefined groupset ".$field['name']."</td>";
					}
					print "</tr>\n";
				}
				$this->m_group = "";
			}
		}

		print "</table>\n";
		print "</form>\n";
	}

}

?>
