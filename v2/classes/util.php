<?php

function	IsDemoAccount()
{
	if ($_SESSION['accountid']==3 && $_SESSION['userid']!='tom') return true;
}

function	JavaScript_Begin()
{
	print '<script type="text/javascript">'."\n"; //<!--'."\n";
}

function	JavaScript_Write($script)
{
	print $script."\n";
}

function	JavaScript_WriteNB($script)
{
	print $script;
}

function	JavaScript_End()
{
	print "</script>\n";
}

function	IsUsingExplorer()
{
	if (isset($_SERVER['HTTP_USER_AGENT']) && 
		(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
		return true;
	else
		return false;
}

 function csv_explode($str, $delim = ',', $qual = "\"")
   {
       $skipchars = array( $qual, "\\" );
       $len = strlen($str);
       $inside = false;
       $word = '';
       for ($i = 0; $i < $len; ++$i) {
           if ($str[$i]==$delim && !$inside) {
               $out[] = $word;
               $word = '';
           } else if ($inside && in_array($str[$i], $skipchars) && ($i<$len && $str[$i+1]==$qual)) {
               $word .= $qual;
               ++$i;
           } else if ($str[$i] == $qual) {
               $inside = !$inside;
           } else {
               $word .= $str[$i];
           }
       }
       $out[] = $word;
       return $out;
   }

function 	CreateNewCatNumber($lastuid)
{
	$i=0; 
	$len = strlen($lastuid);
	while (!is_numeric($lastuid[$i]) && $len>$i) ++$i;
	$numlen = strlen(substr($lastuid,$i));
	$trail = "";
	if ($len!=$i)
	{
		$codenum = substr($lastuid,$i);
		$numlen2 = strlen($codenum+1);
		if (strlen($codenum + 1)<$numlen) 
		{
			while ($numlen2<$numlen)
			{
				$trail = "0".$trail;
				$numlen2++;
			}
		}
		
		$uid = substr($lastuid,0,$i).$trail.($codenum+1);
	}
	return $uid;
}

function SortResultsByCol(&$array,$col)
{	
	$data = $array;
	
	foreach ($data as $key => $row) {
		$sortme[$key]  = $row[$col];
	}
	array_multisort($sortme, SORT_DESC, $data);
	return $data;
}

/*
// ---- Takes internal representation and money and converts to user display
function displaymoney($money)
{
	if ($money==0)
	{
		return "";
	}
	$pounds = $money/100;
	$pounds = round($pounds,2);
	$moneystring = (string)$pounds;
	$decimalplace = strpos($moneystring,".");
	if ($decimalplace===FALSE)
	{
		$moneystring = $moneystring.".00";
	}
	else if ($decimalplace==(strlen($moneystring)-2))
	{
		$moneystring = $moneystring."0";
	}
	
	if ($money>0)
		return "�".$moneystring;
	else
		return "(�".$moneystring.")";
}
*/
// ---- Takes user input and converts it to internal representation of currency
function storemoney($text)
{
	// check if valid money format
/*	if (!preg_match("/^([$|�]?)\d+(?:\.\d{0,2})?$/",$text))
	{
		return "Invalid ".$text;
}*/

	
	// remove any extra stuff
	$text = str_replace(" ","",$text);	// remove spaces
	$text = str_replace(",","",$text);	// remove commas

	$format = "GBP";
	$negate = false;

	// get first character
	if (strpos($text,"$")!==false) $format = "USD";
	if (strpos($text,"�")!==false) $format = "GBP";
	if (strpos($text,"-",0)!==false) $negate = true;
	
	$i = 0;
	$len = strlen($text);
	$digits = "";
	// find first numeral
	while($i<$len)
	{
		if ($text[$i]>="0" && $text[$i]<="9")
			break;
		++$i;
	}
	while($i<$len)
	{
		if (($text[$i]<"0" || $text[$i]>"9") && $text[$i]!=".")
			break;
		$digits .= $text[$i];
		++$i;
	}

	$value = round($digits*100);
	if ($negate==true) 
		$value = -$value;
//	print $text." >> ".$value.$format;
	return $value.$format;
}

function reverse_date_order($date)
{
	$dates = explode('/',$date);
	return $dates[1].'/'.$dates[0].'/'.$dates[2];
}

function todate($day,$month,$year)
{
	return $year."-".$month."-".$day;
}

function getpostdate()
{
	return getpostdate2("date");
}

function getpostdate2($stem)
{
	$date = "";
	if (!empty($_POST[$stem.'_year']) && !empty($_POST[$stem.'_month']) && !empty($_POST[$stem.'_day']))
	{
		$date = $_POST[$stem.'_year']."-".$_POST[$stem.'_month']."-".$_POST[$stem.'_day'];
		if ($date=="--") return "";
	}
	return $date;
}

function MakeDate($datestring)
{
	$daymonthyear = explode("-",$datestring);
	if (strlen($daymonthyear[2])>2)
		return $daymonthyear[2]."-".$daymonthyear[1]."-".$daymonthyear[0];
	else
		return $datestring;
}

function displaydate($datestring)
{
	$daymonthyear = explode("-",$datestring);
	if (strlen($daymonthyear[0])>2)
		return $daymonthyear[2]."-".$daymonthyear[1]."-".$daymonthyear[0];
	else
		return $datestring;
}

function ts_assert($bool,$message)
{
	if (!$bool)
		die ($message);
}

function button_go_page($page,$text)
{
	print '<form action="index.php" method="post"><input type="hidden" name="page" value="'.$page.'"><input type="submit" class="submit" name="submit" value="'.$text.'"></form>';
}

function getartisttitle($id,$linkID)
{
	// check if track or album from id
	$result = mysql_query("SELECT artist,title FROM tracks WHERE id='".$id."';",$linkID);
	if ($result==false || mysql_num_rows($result)==0)
	{
		// check album
		$result = mysql_query("SELECT artist,title FROM product WHERE product='".$id."';",$linkID);
		if ($result==false || mysql_num_rows($result)==0)
		{
			// can find
			return "Unknown ".$id;
		}
	}

	$row = mysql_fetch_row($result);
	return ($row[0]." - ".$row[1]." ($id)");
}

function GetEnumDescription($field,$value)
{
	if ($field=="type")
	{
		switch($value)
		{
			case "D":
				return "Distributor";	
			case "M":
				return "Manufacturer";
			case "V":
				return "Download Service";
			case "L":
				return "Licensor";
			case "S":
				return "Retail Outlet";
		}
	}
	return $value;	
}

function Error($error,$returnpage)
{
	print "<p>$error</p>\n";
	button_go_page($returnpage,"ACKNOWLEDGE");
}

function Success($returnpage)
{
	button_go_page($returnpage,"RETURN");
}

function Cancel($returnpage)
{
	button_go_page($returnpage,"CANCEL");
}

function DisplayAddressFromID($name,$addressid)
{
	$address = DataTable::GetEntryFromID("addresses",$addressid);
	return DisplayAddress($name,$address);
}

function DisplayAddress($name,&$address)
{
	$display =  $name."<br>";
	$display .= $address['address1']."<br>";
	if (!empty($address['address2'])) 
		$display .= $address['address2']."<br>";
	$display .= $address['city']."<br>";
	$display .= $address['country']."<br>";
	$display .= $address['postcode']."<br>";
	return $display;
}

function Debug($text)
{
	if ($_SESSION['userid']!='tom') return;
	
   	print $text;
}

function Debug_r($text)
{
	if ($_SESSION['userid']!='tom') return;
	
   	print_r($text);
}


function PrintTable($array, $title="")
{
    if ($_SESSION['userid']!='tom')
         return;
         
	if (empty($array) || !is_array($array))
	{	
		print "empty table $title<br>";
		return;
	}
	
	
	print '<table class="debug" border=1>';
	if (!empty($title)) print "<tr><td>$title</td></tr>";
//	print_r($array);
	print '<tr><td><b>Key</b></td>';
	if (is_array(current($array)))
	{
		foreach(current($array) as $key=>$value)
			print '<td><b>'.$key.'<b></td>';
	}
	else { print '<td></td>'; }
	print '</tr>';
	reset($array);
	foreach($array as $index=>$row)
	{
		print '<tr><td>'.$index.'</td>';
		if (is_array($row))
		{
			foreach($row as $cell)
			{
				print '<td>';
				if (is_array($cell))
					PrintTable($cell);//recurse
				else
					print $cell;
				print '</td>';
			}
		}
		else
		{
			print "<td>$row</td>";
		}
		print '</tr>';
	}
	print '</table>';
}


?>