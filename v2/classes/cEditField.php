<?php
require_once('cCurrency.php');

function GetSizeFromType($type)
{
	if ($type=="productcode") return 16;
	if ($type=="quantity") return 8;
	if ($type=="integer") return 8;
	if ($type=="money" || $type=="currency") return 12;
	if ($type=="invoice") return 16;
	if ($type=="months") return 2;
	if ($type=="percentage") return 4;

	return 64;
}
	
	
function DropList($query,$name,$value,$addcode)
{
	echo "<select id='".$name."' name='".$name."' $addcode>";
	$result = Database::Query($query);
	$num_result = $result->GetNum();
	$found = false;
	for ($j=0;$j<$num_result;++$j)
	{
		$row = $result->GetNextRow();
		if (empty($row[1])) $display = $row[0];
		else $display = $row[0]." : ".$row[1];
		if ($row[0]==$value)
		{
			$found = true;
			echo "<option value='".$row[0]."' selected>".$display."</option>";
		}
		else
			echo "<option value='".$row[0]."'>".$display."</option>";
	}
	if ($found==false)
	{
		echo "<option selected value=$value >".$value."</option>";
	}
	echo "</select>";
	Database::FinishQuery($result);
}	
	
	
	
// take a comma seperated list and do a drop list
function DropListFromKey($list,$name,$value,$addcode)
{
//	$items = explode(',',$list);
//	print_r($items);
	echo "<select id='".$name."' name='".$name."' $addcode>";
	$found = false;
	foreach($list as $key=>$item)
	{
		if ($key==$value)
		{
			$found = true;
			echo "<option value='".$key."' selected>".$item."</option>";
		}
		else
		{
			echo "<option value='".$key."'>".$item."</option>";
		}
	}
	if ($found==false)
	{
		echo "<option selected value=$value >".$value."</option>";
	}
	echo "</select>";
}	

	
		
// take a comma seperated list and do a drop list
function DropListFromDBResult($list,$name,$value,$addcode)
{
//	print_r($list);
//	$items = explode(',',$list);
//	print_r($items);
	echo "<select id='".$name."' name='".$name."' $addcode>";
	$found = false;
	foreach($list as $item)
	{
		$keys = array_keys($item);
		if ($item[$keys[0]]==$value)
		{
			$found = true;
			echo "<option value='".$item[$keys[0]]."' selected>".$item[$keys[1]]."</option>";
		}
		else
		{
			echo "<option value='".$item[$keys[0]]."'>".$item[$keys[1]]."</option>";
		}
	}
	if ($found==false)
	{
		echo "<option selected value=$value >".$value."</option>";
	}
	echo "</select>";
}	

	
// take a comma seperated list and do a drop list
function DropListSimple($list,$name,$value,$addcode)
{
//	$items = explode(',',$list);
//	print_r($items);
	echo "<select id='".$name."' name='".$name."' $addcode>";
	$found = false;
	foreach($list as $item)
	{
		if ($item==$value)
		{
			$found = true;
			echo "<option selected>".$item."</option>";
		}
		else
		{
			echo "<option>".$item."</option>";
		}
	}
	if ($found==false)
	{
		echo "<option selected value=$value >".$value."</option>";
	}
	echo "</select>";
}	


// take a comma seperated list and do a drop list
function DropListFromString($list,$name,$value,$addcode)
{
	$items = explode(',',$list);
//	print_r($items);
	echo "<select id='".$name."' name='".$name."' $addcode>";
	$found = false;
	foreach($items as $item)
	{
		$keyvalue = explode("=",$item);
		print "<option value=".$keyvalue[0];
		
		if ($keyvalue[0]==$value)
		{
			$found = true;
			print " selected";
		}

		print " >".$keyvalue[1]."</option>";

	}
	if ($found==false)
	{
		echo "<option selected value=$value >".$value."</option>";
	}
	echo "</select>";
}	

function RadioEnumerated($list,$name,$value,$addcode)
{
	$found = false;
	while ($list!="")
	{
		$commapos = strpos($list,',',0);
		if ($commapos==false)
		{
			$row = substr($list,0,strlen($list));
			$list = "";
		}
		else
		{
			$row = substr($list,0,$commapos);
			$list = substr($list,$commapos+1,strlen($list)-$commapos-1);
		}
		$eqpos = strpos($row,'=');
		$enum = substr($row,0,$eqpos);
		$enumname = substr($row,$eqpos+1,strlen($row)-$eqpos-1);
		if (strpos($value,$enum)!==false)
		{
			$found = true;
			print "<input type=\"checkbox\" name=\"".$name."[]\" group=\"".$name."\" value='".$enum."' checked>".$enumname."</input><br>\n";
		}
		else
			print "<input type=\"checkbox\" name=\"".$name."[]\" group=\"".$name."\" value='".$enum."'>".$enumname."</input><br>\n";
	}
	if (!$found && $value!="")
    print "<b>ERROR: not found value '$value'</b>";
}


// take a comma seperated list and do a drop list
function DropListEnumerated($list,$name,$value,$addcode)
{
	print "\n<select id='".$name."' name='".$name."' $addcode>\n";
	$found = false;
	
//	$values = explode(",",$list);
	foreach($values as $v)
	{
		if ($v=="")
		{
			print '<option disabled="disabled">---------</option>';
			//print '<option>sepeartor</option>';
		}
		else
		{
			$parts = explode("=",$v);
			$enum = $parts[0];
			$enumname = $parts[1];		
			if ($enum==$value)
			{
				$found = true;
				print "<option selected value='".$enum."'>".$enumname."</option>\n";
			}
			else
				print "<option value='".$enum."'>".$enumname."</option>\n";
		}
	}
	if ($found==false)
	{
		print "<option selected value='".$value."' >".$value."</option>\n";
	}
	print "</select>\n";
}	
	
function AddCustomField_Select($name,$list,$value)
{
	$args = func_get_args();
	if (empty($args[3]))	$addcode=""; else $addcode = $args[3];
	
	DropListEnumerated($list,$name,$value,$addcode);
}

	
// 	FilterCombo($name,$value,"artist:artistname:artist::","uid:title:tracks:artist:");

function FilterCombo($name,$value,$stringA,$stringB)
{
	// Break up the argument strings into an array using colon
	$argsA = preg_split( "/:/", $stringA );
	$argsB = preg_split( "/:/", $stringB );

	// name args for readability
	$selectnameA = $argsA[0];
	$displayA = $argsA[1];
	$tableA = $argsA[2];
	$filterA = $argsA[3];

	$valueB = $argsB[0];
	$displayB = $argsB[1];
	$tableB = $argsB[2];
	$matchcolB = $argsB[3];

	$result = Database::Query("SELECT $matchcolB FROM $tableB WHERE uid='".$value."';");
	$row = $result->GetNext();
	$valueA = $row[$matchcolB]; 
	Database::FinishQuery($result);

	$queryA = "SELECT $displayA,uid FROM $tableA ";
	if (empty($filterA))
		$queryA = $queryA." ORDER BY $displayA;";
	else
		$queryA = $queryA." WHERE $filterA ORDER BY $displayA;";
		
	$resultA = Database::Query($queryA);
	if ($resultA==false)
	{
		print "No Results for $queryA";
		return;
	}

   // start javascript
	JavaScript_Begin();
	$optionlistname = $name."combo";
//	JavaScript_Write('try {');
	JavaScript_Write('var '.$optionlistname.' = new DynamicOptionList("'.$name.$selectnameA.'","'.$name.'");');

	for ($i=0;$i<$resultA->GetNum();++$i)
	{
		$rowA = $resultA->GetNextRow();
		JavaScript_WriteNB($optionlistname.".forValue(\"".$rowA[1]."\").addOptionsTextValue(");

		$needleA = $rowA[1];
		$queryB = "SELECT $valueB,$displayB FROM $tableB WHERE $matchcolB='".$needleA."';";
		$resultB = Database::Query($queryB);	
//		print $queryB."<br>";
		$numresultB = $resultB->GetNum();
		if ($resultB !== false)
		{
			for ($j=0;$j<$numresultB;++$j)
			{
				$rowB = $resultB->GetNextRow();
//				$rowB[0] = str_replace("'","",$rowB[0]);
				JavaScript_WriteNB("'".addslashes($rowB[1])."','".addslashes($rowB[0])."'");
				if ($j!=$numresultB-1)  JavaScript_WriteNB(","); else JavaScript_Write(");");
			}
		}
		if ($numresultB==0)
		{
			JavaScript_Write(");");
		}

		Database::FinishQuery($resultB);
		$keyA[$i] = $rowA[1];
		$selectdisplayA[$i] = $rowA[0];
	}

//	JavaScript_Write($optionlistname.'.forField("'.$name.$selectnameA.'").setValues("'.$valueA.'");');
	JavaScript_Write($optionlistname.'.forField("'.$name.'").setValues("'.$value.'");');

//	JavaScript_Write('} catch (ex) { alert(ex); }');
	JavaScript_End();
	
	print "<SELECT NAME=\"".$name.$selectnameA."\">";
	for ($i=0;$i<$resultA->GetNum();++$i)
	{
		print "<OPTION value='".$keyA[$i]."' ";
		if ($keyA[$i]==$valueA) print " selected ";
		print ">".$selectdisplayA[$i]."</OPTION>";
	}
	print "</SELECT>\n";
	print "<SELECT NAME=\"".$name."\">";
	print "<SCRIPT>try {".$optionlistname.".printOptions(\"".$name."\")} catch(ex) { alert(ex); }</SCRIPT>";
	print "</SELECT>\n";
}
	
	
function _CreateSelectListFromResult($result)
{
	$num_result = $result->GetNum();
	$list = "";
	for ($j=0;$j<$num_result;++$j)
	{
		$creditor = $result->GetNextRow();
		if ($j<$num_result-1)
			$list = $list.$creditor[0]."=".$creditor[1].",";
		else
			$list = $list.$creditor[0]."=".$creditor[1];
		//echo "<option>".$creditor[0]."</option>";
	}
	return $list;
}


function _TrackOptionsFromResults($name,$value,&$results,$showartist)
{
	
	$found = false;
	foreach ($results as $result)
	{
		echo "<option value='".$result['uid']."'";
		if ($result['uid']==$value && $value!="")
		{
			echo " selected ";
			$found = true;
		}
		echo ">";
		if ($result['version']=="") 
			$version = "";
		else
			$version = " - ".$result['version'];
		
		if ($showartist)
			echo substr(TableInfo::Display("tracks","artist",$result['artist'])." - ".$result['title'].$version,0,90);
		else
			echo substr($result['title'].$version,0,90);
	
		echo "</option>";
	}
		
	if ($found==false)
		echo "<option value='' selected> </option>";
	else
		echo "<option value=''> </option>";
}

function _PresentTrackSelect($name,$value,$filter,$addcode)
{
	// Multiple tracks
	echo "<select name='$name' id='$name' $addcode >";
	$query = "";
	if (strpos($filter,"product")!==false)
	{
		if (strpos($filter,"band")!==false)
		{
		// filter by both
			$productid = substr($filter,strlen("product"),stripos($filter,",")-strlen("product"));
			$artistid = substr($filter,stripos($filter,"band")+strlen("band"));
			$query = "SELECT artist, title, version, uid FROM tracks WHERE artist='".$artistid."' AND uid IN ( SELECT track FROM tracksproduct WHERE product='".$productid."') ORDER BY uid";
			//print "sql".$query;
		}
		else
		{
			$filtervalue = substr($filter,strlen("product"));
			if ($filtervalue!="")
				$query = "SELECT artist, title, version, uid FROM tracks WHERE uid IN ( SELECT track FROM tracksproduct WHERE product='".$filtervalue."') ORDER BY uid";
		}
	}
	else if (strpos($filter,"artist")!==false)
	{
		$filtervalue = substr($filter,strlen("artist"));
		if ($filtervalue!=0)
			$query = "SELECT artist, title, version, uid FROM tracks WHERE artist='".$filtervalue."' ORDER BY artist, uid";
	}

	if ($query=="")	
	{
		$query = "SELECT artist, title, version, uid FROM tracks ORDER BY artist,uid;";
		$filter = "";
	}

	$result = Database::QueryGetResults($query);
	_TrackOptionsFromResults($name,$value,$result,($filter==""));			
	echo "</select>";
}

$licencetype = "";

function EditField($name,$type,$value)
{

	global $licencetype;
	$args = func_get_args();
	if (empty($args[3]))	$addcode = ""; else $addcode = $args[3];

	// Licence is special case since it will depend on the value of licencetype to decide which one to use
	if ($type=="licence")
	{
		if (isset($_POST['licencetype']))
			$licencetype = $_POST['licencetype'];

		if ($licencetype=="T")
		{
			$type = "artisttrack";
		}
		else if ($licencetype=="P")
		{
			$type = "product";
		}
		else
		{	
			$type = "band";
		}
	}
	
	$typeparameters = explode(":",$type);
	$type = $typeparameters[0];
	if (!isset($typeparameters[1])) $typeparameters[1]="";
	
	switch ($type)
	{
		case "product":
		case "productartist":
		{
			if ($typeparameters[1]!="")
			{
				$band = substr($typeparameters[1],4);
				DropList("SELECT uid,title,releasedate FROM product WHERE (artist='$band' OR artist='') ORDER BY uid",$name,$value,$addcode);
			}
			else
			{
				DropList("select uid,title,releasedate from product ORDER BY uid",$name,$value,$addcode);
			}
			break;
		}
		case "batchid":
		{
			$batches = Database::QueryGetResults("SELECT DISTINCT batchid FROM sale;");
			if (!empty($batches))
			{
				foreach($batches as $batch)
				{
					if ($batch['batchid']!="") 
					{
						$fixedlist[] = $batch['batchid'];
					}
				}
				DropListSimple($fixedlist,$name,$value,$addcode);
			}
			else
			{
				DropListSimple(array(),$name,$value,$addcode);
			}
			break;
		}
		case "currencydenomination":
		{
			DropListSimple(array('GBP','EUR','USD'),$name,$value,$addcode);
			break;
		}
		case "artisttrack":
		{
			print '<script type="text/javascript">
					var '.$name.'combo = new DynamicOptionList("'.$name.'uid","'.$name.'");'."\n";

			$bands = Database::QueryGetResults("SELECT uid,name FROM bands");
			foreach ($bands as $band) {
				$tracks = Database::QueryGetResults("SELECT uid,CONCAT(title,' ',version) as title FROM tracks WHERE artist='".$band['uid']."';");
				print 'licenceduidcombo.forValue("'.$band['uid'].'").addOptionsTextValue(';
				if (!empty($tracks)) foreach ($tracks as $index=>$track) {
					if ($index>0) print ',';
					print "'".addslashes($track['title'])."','".$track['uid']."'";
				}
				print ");\n";
			}
			
			print $name.'combo.forField("'.$name.'").setValues("'.$value.'");'."\n";
			
			print "</script>\n";
			
			if ($value!="") {
				$bandvalue = Database::QueryGetResults("SELECT artist FROM tracks WHERE uid='".$value."'");
				$bandvalue = $bandvalue[0]['artist'];
			}
	
			print '<select name="'.$name.'uid">';
			foreach ($bands as $band) {
				print "<option value='".$band['uid']."' ";
				if ($bandvalue == $band['uid']) print "selected ";
				print ">".$band['name']."</option>";
			}
			print "</select>\n";
			
			print '<select name="'.$name.'"><script>try {'.$name.'combo.printOptions("'.$name."')} catch(ex) { alert(ex); }</script></select>";
			
			break;
		}
		case "licencetype":
		{
			$licencetype = $value;
			DropListFromKey(array('T'=>'Track','P'=>'Product','A'=>'Artist'),$name,$value,$addcode);
			break;
		}
		case "saletype":
		{
			if ($value=="") $value='P';
			DropListFromKey(array('P'=>'Physical','V'=>'Download','S'=>'Stream'),$name,$value,$addcode);
			break;
		}
		case "accountstatus":
		{
			if ($value=="") $value='A';
			DropListFromKey(array('A'=>'Active','S'=>'Suspended','T'=>'Trial'),$name,$value,$addcode);
			break;
		}
		case "importscript":
		{
			DropListSimple(array('None','Finetunes Format'),$name,$value,$addcode);
			break;
		}
		case "keyfield":
		{					
			DropListFromKey(array('distributor'=>'Distributor','territory'=>'Territory','shop'=>'Shop','key'=>'Sale Key'),$name,$value,$addcode);
			break;
		}
		case "file":
		{
			print "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"30000000\">";
			print "<input type=file name=\"$name\" id=\"$name\" value=\"$value\">";
			break;
		}
		case "artist":
		{
			$result = Database::QueryGetResults("select uid,realname from artist order by realname");
			
		//	$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "band":
		{
			$result = Database::QueryGetResults("select uid,name from bands order by name");
			$result[] = array('uid'=>0,'name'=>'Various Artists');
//			$list = _CreateSelectListFromResult($result);
//			$list .= ",0=Various Artists";
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "bandauto":
		{
			$bands = Database::QueryGetResults("SELECT uid,name FROM bands ORDER BY name");
			$bandslist="";
			foreach($bands as $band)
			{
				if ($bandslist!="") $bandslist .= ',';
				$bandslist .= '\''.str_replace("'","",$band['name']).' ['.$band['uid'].']\'';
			}
			
			$script =  '<script language="javascript" type="text/javascript">'."\n";
			$script .= "AutoComplete_Create('$name', [".$bandslist."].sort(), 6); } catch(ex)\n";
			$script .= '</script>'."\n";
			print $script;
			print "<input type=text name='myname' id='$name' size=45>";
			return $script;
			break;
		}
		case "creditor":
		{
			$result = Database::QueryGetResults("select uid,name from partners order by name");
//			$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "creditordest":
		{
			$result = Database::QueryGetResults("select uid,name from partners order by name");
			$result[] = array('uid'=>'0','name'=>'SALE');
//			$list = _CreateSelectListFromResult($result);
//			$list = $list.",0=SALE";
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "distributor":
		{
			$result = Database::QueryGetResults("select uid,name from partners where LIKE='%D%'");
//			$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "statementdetaillevel":
		{
			DropListFromKey(array('0'=>'Collated','1'=>'Collated + Compilations','2'=>'Full Detail'),$name,$value,$addcode);
			break;
		}
		case "dealtype":
		{
			DropListFromKey(array('P'=>'Profit Share','R'=>'Royalty','L'=>'Licence','S'=>'Remix Split'),$name,$value,$addcode);
	//		DropListEnumerated("P=Profit Share,R=Royalty,L=Licence",$name,$value,$addcode);
			break;
		}
		case "createdeal":
		{
			DropListFromKey(array('0'=>'Automatically create linked deal for me','1'=>'I will create one manually later'),$name,$value,$addcode);
			break;
		}
		case "supplier":
		{
			$result = Database::QueryGetResults("select uid,name from partners where type LIKE '%M%'");
//			$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "licensor":
		{
			$result = Database::QueryGetResults("select uid,name from partners where type LIKE '%L%'");
	//		$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "licensee":
		{
			$result = Database::QueryGetResults("select uid,name from partners where type LIKE '%C%'");
	//		$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "distributorshop":
		{
			$result = Database::QueryGetResults("select uid,name from partners where type LIKE '%D%' OR type LIKE '%S%'");
//			$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "supplychain":
		{
			$result = Database::QueryGetResults("select uid,name from partners where (type LIKE '%D%' OR type LIKE '%S%' OR type LIKE '%M%')");
//			$list = _CreateSelectListFromResult($result);
			DropListFromDBResult($result,$name,$value,$addcode);
			break;
		}
		case "category":
		{	
			echo "<select name='$name' id='$name' $addcode>";
			echo "<option>add</option><option>custom</option><option>sale</option><option>move</option><option>licence</option>";
			echo "</select>";
			break;
		}
		case "expensetype":
		{
			if ($value=="") $value="Misc";
			DropListSimple(array('Manufacturing','Promotion','Remix Fees','Mechanical Royalties','Misc'),$name,$value,$addcode);
			break;
		}
		case "basetype":
		{
			DropListFromKey(array('salevalue'=>'Sale Value','baseprice'=>'Base Price'),$name,$value,$addcode);
			break;
	    }
		case "format":
		{
			DropListSimple(array('CD Album','2 CD Album','3 CD Album','4 CD Album','CD Single,7" Vinyl','10" Vinyl EP','12" Vinyl LP','2 x 12" Vinyl LP','3 x 12" Vinyl LP','MP3 Single','MP3 Album','DVD','12" Vinyl Single','Digital Mix','Digital Track Only','Digital Album Only'),$name,$value,$addcode);
			break;
		}
		case "crossdealid":
		{
			$band = substr($typeparameters[1],4);
			//$list = "0=NONE";
			$list[] = array('uid'=>'0','description'=>'NONE');
			$deals = Database::QueryGetResults("SELECT GROUP_CONCAT(product),dealtype,crossdealid FROM deals WHERE band='".$band."' AND dealtype!='S' GROUP BY crossdealid;");
			if (!empty($deals))
			{
				if (count($deals)==1)
				{
					//$list.= ",".$deals[0]['crossdealid']."=ALL OTHER DEALS";
					$list[] = array('uid'=>$deals[0]['crossdealid'],'description'=>'ALL OTHER DEALS');
				}
				else
				{
					foreach ($deals as $deal)
					{
					//	$list.=",".$deal['crossdealid']."=";
						$text = $deal['GROUP_CONCAT(product)'];
						$text = str_replace(",",".",$text);
						if (strlen($text)>20)
							$text = substr($text,0,18)."...";
						
						if ($deal['dealtype']=="P") 
							$list[] = array('uid'=>$deal['crossdealid'],'description'=>'PROFIT SHARE ('.$text.')');
						else if ($deal['dealtype']=="S")
							$list[] = array('uid'=>$deal['crossdealid'],'description'=>'REMIX SPLIT ('.$text.')');
						else						
							$list[] = array('uid'=>$deal['crossdealid'],'description'=>'ROYALTY ('.$text.')');
					}
				}
			}
			DropListFromDBResult($list,$name,$value,$addcode);
			break;
		}
		case "tracks":
		{

			$tracks = explode(",",$value);
			foreach ($tracks as $index=>$track)
			{
				print "<div>";
				_PresentTrackSelect($name."_".$index,$track,$typeparameters[1],$addcode);
				print "\n";
				print '<a href="#" onClick="AddTrack(this,\''.$name.'\');" ><img border=0 src="images/plussign.png"></a>';
				print '<a href="#" onClick="RemoveTrack(this,\''.$name.'\');" ><img border=0 src="images/minussign.png"></a><br>';
		        print '</div>';
			}
			print "<input type=hidden id='".$name."' name='".$name."'>";	
			break;
		}
		case "track":
		{
			_PresentTrackSelect($name,$value,$typeparameters[1],$addcode);
			break;
		}
		case "mailgroups":
		{
			
			$currentgroups = explode(",",$value);
			$allgroups = Database::QueryGetSingleResults("SELECT DISTINCT mailgroup FROM recipientgroups");
			//print_r($allgroups);
			if (!empty($allgroups))
			{
				foreach($allgroups as $group)
				{
					print '<input type="checkbox" name="'.$name.'[]" value="'.$group.'"';
					if (array_search($group,$currentgroups)!==FALSE)
						print ' checked ';
					print '> '.$group.'<br>';	
				}
			}
			// do the new group thing here
			print "new group <input name='newmailgroupname' type='text' size=32><input type='submit' name='ADD' value='ADD' onClick='return AddCheckboxToList(this,\"".$name."\");'>";
			break;
		}
		case "supplychaintype":
		{
			RadioEnumerated('D=Distributor,M=Manufacturer,L=Licensor,S=Shop,C=Licensee',$name,$value,$addcode);
			break;
		}
		case "date":
		{
			// by default use todays address
			if ($value=="" || $value=="0000-00-00")
			{
				$value = "//";
			}
			else if ($value=="today")
			{
		//		$date = date('d-m-Y');
				$value = date('d/m/Y');
			}
			else
			{
				$value = date('d/m/Y',strtotime($value));
			}

//			print "<input type='text' name='".$name."' id='".$name."' size=10 maxlength=10 value='".$value."'>";


			$splitvalue = explode("/",$value);
			$day = $splitvalue[0];
			$month = $splitvalue[1];
			$year = $splitvalue[2];

			print "<input type='text' name='".$name."_day' id='".$name."_day' size=2 maxlength=2 value='".$day."'>";
			print "<input type='text' name='".$name."_month' id='".$name."_month' size=2 maxlength=2 value='".$month."'>";
			print "<input type='text' name='".$name."_year' id='".$name."_year' size=4 maxlength=4 value='".$year."'>";
			echo "<input type=hidden name='".$name."' id='".$name."'>";

//			echo "<input type='text' name='".$name."' size=10 maxlength=10 value='".$date."'>";
			break;
		}
		case "float":
		{
			echo "<input type=text name='".$name."' id='".$name."' size=4 value='$value' $addcode>";
			break;
		}
		case "password":
		{
 			echo "<input type=password name='".$name."' id='".$name."' size=16 value='".$value."' $addcode>";
			break;
		}
		case "currency":
		{
			echo "<input type=text name='".$name."' id='".$name."' size=8 value='".Currency::DATABASETODISPLAY($value)."' $addcode>";
			break;
		}
		case "months":
		{
/*			print "<select name=$name id=$name $addcode>";
			for ($i=3;$i<=18;)
			{
				if ($i==$value) 
					print "<option value=$i selected>$i months</option>";
				else
					print "<option value=$i>$i months</option>";
				$i+=3;
			}
			print "</select>";*/
			print "<input type=text size=3 maxlength=3 name='".$name."' id='".$name."' value='".$value."'> months";
			break;
		}
		case "checkbox":
		{
			print "<input type=checkbox name='".$name."' id='".$name."' ";
			if ($value==true) print "checked ";
			print "</input><br>\n";
			break;
		}	
		case "multiselect":
		{
			// value format = "<value>=<descrip>,*<value>=<descrip>" * = selected
			$values = explode(",",$value);
			foreach ($values as $value)
			{
				$valuenamepair = explode("=",$value);
				print "<input type=checkbox name='".$name."[]' id='".$name."' ";
				if ($valuenamepair[0][0]=="*") 
					print "value='".substr($valuenamepair[0],1)."' checked>";
				else
					print "value='".$valuenamepair[0]."'>";
				print $valuenamepair[1]."</input><br>\n";
			}
			break;
		}
		case "multitext":
		{
			print "<textarea name=$name id=$name rows=3 cols=32>$value</textarea>";
			break;
		}
		case "email":
		{
			print "<textarea name=$name id=$name rows=20 cols=80>$value</textarea>";
			break;
		}
		case "select":
		{
			// Do a special selection
			print "<select name=$name id=$name>";
			$options = explode(",",$typeparameters[1]);
			for ($i=0;$i<count($options);++$i)
			{
				if ($options[$i]!="")
				{
					$option = explode("=",$options[$i]);
					if (isset($option[1])==true)
					{
						print "<option value='".$option[0]."' ";
						if ($value==$option[0]) print "selected";
						print ">".str_replace("'","",$option[1])."</option>\n";
					}
					else
					{
					
						print "<option value=-1></option>\n";
					}
					
				}
			}
			print "</select>";
			break;
		}
		default:
		{
		  $length = 0;
      if (strncmp($type,"text",4)==0)
      {
        $length = (int)substr($type,4);
      }
      if ($length==0)
			{
        echo "<input type=\"text\" name=\"".$name."\" id=\"".$name."\" maxlength=\"".GetSizeFromType($type)."\" size=\"".GetSizeFromType($type)."\" value=\"".htmlentities($value)."\" ".$addcode.">";
      }
      else
      {
        echo "<input type=\"text\" name=\"".$name."\" id=\"".$name."\" maxlength=\"".$length."\" size=\"".$length."\" value=\"".htmlentities($value)."\" ".$addcode.">";
      }
			if ($type=="percentage") print "%";
		}
	}
}
?>