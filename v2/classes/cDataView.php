<?php

// A Data view

class DataView {
	private $rows;
	private $columns;
	private $sortcolumn;
	private $sortdirection;
	private $name;
	private $views;
	private $defaultview;
	private $hidden;
	private $indexcol;

// items - these can be autoadded from a database, or added manually
// should be selectable - single and multiple (optional)
// generate events on selection
// should be resortable

	public function DataView($tablename)
	{
		$this->name = $tablename;
		$this->indexcol = 0;
	}


	public function AddViewDropDown($views,$defaultview)
	{
		$this->views = $views;
		$this->defaultview = $defaultview;
	}
	
	public function GetCurrentView()
	{
		if (empty($_POST[$this->name.'viewselect']))
			return $this->defaultview;
			
		return $_POST[$this->name.'viewselect'];
	}


 public function AddRowFromArray($array)
 {
 	$this->rows[] = $array;
 }

 public function SetFromDatabaseResults($results,$indexcol=0)
 {
 	
	foreach(current($results) as $key=>$value)
		$cols[] = $key;
	$this->SetColumns($cols);
	$this->indexcol = $indexcol;
	
	reset($results);
	foreach($results as $index=>$row)
	{
		unset($newrow);
		foreach($row as $cell)
			$newrow[] = $cell;
		$this->AddRowFromArray($newrow);	
	}

 }

 
 public function SetColumns($columns)
 {
 	$this->columns = $columns;
 }


 public function AddAction($action,$requiresselection)
 {
 	$this->action[] = array('action'=>$action,'requiresselection'=>$requiresselection);
 }
 
 private function DisplayActions()
 {
 	print '<div id="actions">';
 	print '<table>';
 	foreach($this->action as $action)
 	{
 		print '<tr><td>';
 		print '<button type=submit name="submitvalue" value="'.$action['action'].'" class="submit">'.$action['action'].'</button';
 		print '</td></tr>';
 	}
 	print '</table>';
 	print '</div>';
 }

 private function ViewSelect()
 {
 	print '<div class="dataviewheader">';
		if (!empty($this->views))
		{
			print "<script type=\"text/javascript\">
					function refreshview()
					{
						try {
						myform = document.getElementById('viewform');
						myform.submit();
						} catch(ex)
						{
							alert(ex);
						}
					}
					</script>";

			print "<form name=\"viewform\" id=\"viewform\" action=\"index.php\" method=\"post\">
				   <input type=hidden name=\"page\" value=\"".$_POST['page']."\">";
			if (!empty($_POST['submitvalue']))
				   print "<input type=hidden name=\"submitvalue\" value=\"".$_POST['submitvalue']."\">";
				   
			print "View ";
		 	print '<select name="'.$this->name.'viewselect" onchange="javascript:refreshview();">';
	 		foreach($this->views as $viewkey=>$viewdescription)
 			{
 				print '<option value="'.$viewkey.'" ';
 				if ($this->GetCurrentView()==$viewkey) print 'selected';
 				print ' >'.$viewdescription.'</option>';
 			}
 			print '</select>';
			print "</form>";
		}
 	print '</div>';
 }

 public function SetVar($key,$value)
 {
 	$this->hidden[] = array('key'=>$key,'value'=>$value);
 }

 public function Present()
 {
 	print '<div class="dataview">';
 
 	$this->ViewSelect();
 	
 	print '<form method=post action="index.php">';
 	print '<input type=hidden name=page value="promos">';
	
	// display the header - set up javascript for resort events
	// display the cells
	print "<div class='scrolltable'>";
		
 	print '<table class=spreadsheet2>';
 	print '<thead><tr><th></th>';
 	foreach($this->columns as $colindex=>$col)
 	{
 		if ($colindex!=$this->indexcol)
 			print '<th>'.$col.'</th>';
 	}
 	print '</tr></thead>';
 	
 	
 	//SortRows($this->sortcolumn,$this->sortdirection); - shall we do this on the client side??
 	
 	
 	print '<tbody>';
 	
 	foreach($this->rows as $rowindex=>$row)
 	{
 //		print_r($row);print"<br>";
 		if ($rowindex%2==0)
		 print "<tr>";
		else
		 print "<tr class='odd'>";
		
		print '<td>'; // onClick="DVClickRow(this,'.$rowindex.',false)">';
		print '<input type=checkbox id=checkbox name='.$this->name.'[] value='.$row[$this->indexcol].' ></td>';
		print '</td>';
		
		foreach ($row as $colindex=>$cell)
 		{
 			if ($colindex!=$this->indexcol)
 			{ 
 				print '<td onClick="DVClickRow(this,'.$rowindex.',false)">';
				print $cell;
 				print '</td>';
 			}
 		}
 		print '</tr>';
 	}
 	
 	print '</tbody>';
 	
 	print '</table>';
 	print '</div>';
 	
 	$this->DisplayActions();
 	
 	print '</div>';
 	
 	// add hidden vars
 	if (!empty($this->hidden))
 	{
 		foreach($this->hidden as $hidden)
 		{
 			print '<input type="hidden" name="'.$hidden['key'].'" value="'.$hidden['value'].'">'."\n";
 		}
 	}
 	
 	print '</form>';
 }


};



?>