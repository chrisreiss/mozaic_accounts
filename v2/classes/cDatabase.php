<?php

require_once('cDebug.php');

// change these for the different hosts
if ($_SERVER['SERVER_NAME'] == "www.mbryonik.co.uk")
{
	define("HOST","localhost");
	define("DATABASE","mbryonik");
	define("USER","mbryonik");
	define("PASSWORD","jUJUGYVOO");
}
else if ($_SERVER['SERVER_NAME'] == "www.mozaicaccounts.com" || $_SERVER['SERVER_NAME'] == "mozaicaccounts.com")
{
	define("HOST","localhost");
	define("DATABASE","szirtest_mozaic");
	define("USER","szirtest_www");
	define("PASSWORD","Nx5g7(8w19.D");

define("ADMINUSER","szirtest_admin");
define("ADMINPASSWORD","headhunt123");		
}
else
{
	define("HOST","localhost");
	define("DATABASE","szirtest_mozaic");
	define("USER","supportal_user");
	define("PASSWORD","supportal_pass");

	define("ADMINUSER","supportal_user");
	define("ADMINPASSWORD","supportal_pass");
}






// PHP4 doesn't have visibility - bah!

class DatabaseResult
{
	var $m_result;
	
	function DatabaseResult($result)
	{
		$this->m_result = $result;
	}
	
	function GetNum()
	{
		if ($this->m_result==false)
		{
			return 0;
		}
		
		return mysql_num_rows($this->m_result);
	}
	
	function GetNext()
	{
		return mysql_fetch_assoc($this->m_result);
	}

	function GetNextRow()
	{
		return mysql_fetch_row($this->m_result);
	}
	
	function Get($index)
	{
		mysql_data_seek($this->m_result,$index);
		return mysql_fetch_assoc($this->result);		
	}
};



$db_linkID = false;
$db_dbSelected = false;
$db_results = false;
$db_numopenqueries = 0;

class Database
{

	
	function Init()
	{
		global $db_numopenqueries;
		Database::Connect();
		$db_numopenqueries = 0;
	}
	
	function Connect()
	{
		$args = func_get_args();
		if (empty($args[0])) 
			$database = "szirtest_".$_SESSION['database'];
		else
			$database = $args[0];
		
		global $db_linkID;
		global $db_dbSelected;

		$db_linkID = mysql_connect(HOST, USER, PASSWORD) or die("Cannot connect to MySQL: ".mysql_error());	
		$db_dbSelected = mysql_select_db($database, $db_linkID) or die("Cannot select the database: ".mysql_error());
	}


	function ConnectAdmin()
	{
		$args = func_get_args();
		if (empty($args[0])) 
			$database = "szirtest_".$_SESSION['database'];
		else
			$database = $args[0];
		
		global $db_linkID;
		global $db_dbSelected;

		$db_linkID = mysql_connect(HOST, ADMINUSER, ADMINPASSWORD) or die("Cannot connect to MySQL: ".mysql_error());	
		$db_dbSelected = mysql_select_db($database, $db_linkID) or die("Cannot select the database: ".mysql_error());
	}

	function CheckConnect()
	{
		global $db_linkID;
		// connect if not connected
		if (!isset($db_linkID) || $db_linkID==false)
		{
			Database::Connect();
		}
		return true;
	}

	function QueryParam($query)
	{
		//$query = func_get_arg(0);
		for ($i=1;$i<func_num_args();++$i)
		{
			$count = 0;
			$varl = "'".(string)func_get_arg($i)."'";
			$query = str_replace("%".strval($i),$varl,$query,$count);
			if ($count==0) break;
		}
	//	print $query."<br>";
		return Database::QueryGetResults($query);
	}

	// This is not slash safe!
	function Query($query)
	{
//		if (!isset($_SESSION['querycount'])) $_SESSION['querycount'] = 1; else $_SESSION['querycount'] +=1;
//		if (!isset($_SESSION['querylog'])) $_SESSION['querylog'] = $query."<br>"; else $_SESSION['querylog'] .= $query."<br>";

		Database::CheckConnect();
		// check there is only one semi colon - to avoid hacking
		if (substr_count($query,";")>1)
		{
			$query = substr($query,0,strpos($query,";"));
			Trace(1,"Removed multiple query");
		}
//		print "QUERY : ".$query."<br>";
		$result = mysql_query($query);
		if ($result==false)
		{
			Trace(1,mysql_error());
			Trace(1,$query);
			return new DatabaseResult(false);
		}
		return (new DatabaseResult($result));
	}
	
	// Return the first value
	function QueryGetValue($field,$table,$condition)
	{
		$result = Database::QueryGetResult("SELECT $field FROM $table WHERE ".$condition);
		return $result[$field];
	}
		// This is not slash safe!
	function QueryGetResults($query)
	{
		Database::CheckConnect();
		// check there is only one semi colon - to avoid hacking
		if (substr_count($query,";")>1)
		{
			$query = substr($query,0,strpos($query,";"));
			Trace(1,"Removed multiple query");
		}
//		print "QUERY : ".$query."<br>";
		$result = mysql_query($query);
		if ($result==false)
		{
			Trace(1,mysql_error());
			Trace(1,$query);
			return false;
		}
		$numresult = mysql_num_rows($result);
		for ($i=0;$i<$numresult;++$i)
		{
			$results[] = mysql_fetch_assoc($result);
		}
		mysql_free_result($result);
		if ($numresult!=false)
			return ($results);
	}

	function QueryGetSingleResults($query)
	{
		$results = Database::QueryGetResults($query);
		if (!empty($results))
		{
			foreach($results as $row)
			{
				$arr[] = $row[key($row)];
			}
			return $arr;
		}
	}
	
	function QueryGetResult($query)
	{
		$results = Database::QueryGetResults($query);
		if ($results==false)
			return false;
		return $results[0];
	}

	
	function FindUID($table,$searchcol,$searchvalue)
	{
		$result = Database::Query("SELECT uid FROM ".$table." WHERE ".$searchcol." = '".$searchvalue."';");
		if ($result->GetNum()==0)
		{
			return false;
		}
		$row = $result->GetNext();
		Database::FinishQuery($result);
		return $row['uid'];
	}
	
	function GetLinkID()
	{
		global $db_linkID;
		// connect if not connected
		if (!isset($db_linkID) || $db_linkID==false)
		{
			return false;
		}
		return $db_linkID;
	}
	
	function FinishQuery(&$result)
	{
		mysql_free_result($result->m_result);
		unset ($result);
	}
	
	function Validate($string)
	{
		global $db_link;

		if ($string=="") return $string;
		
        if(get_magic_quotes_gpc()) 
		{
            $string = stripslashes($string);
  		}
		
		$string = mysql_real_escape_string($string);	
		return $string;
	}
	
	
	function Disconnect()
	{
		global $db_linkID;
		if (isset($db_linkID))
		{
			mysql_close($db_linkID);
			$db_linkID = false;
		}
	}
	
	function	CreateDatabase($accountid)
	{
		$linkID = mysql_connect(HOST, USER, PASSWORD) or die("Cannot connect to MySQL : ".mysql_error());
		mysql_create_db($accountid,$link);
	}
}
?>
