<?php

require_once("util.php");
require_once("cTableInfo.php");


// Some PHP5 bizness

function _html($text)
{
	print $text; print "\n";
}


class FlexField {
	public      $key;
	public		$currentvalue;
	public		$validationcode;
	public		$description;
	public		$comment;

	function Present($parent) { _html("Bad call from $parent"); }
	

	public function Description($text)
	{
		$this->description = $text;
		return $this;
	}

	public function Set($value)
	{
		$this->currentvalue = $value;
		return $this;
	}
	
	public function Comment($text)
	{
		$this->comment = $text;
		return $this;
	}
}

class TextFlexField extends FlexField {
	function __construct($key)
	{
		$this->key = $key;
		return $this;
	}
	
	public function Create($key)
	{
		return new TextFlexField($key);
	}

	
	public function Present($parent)
	{
		_html('<input name="'.$this->key.'" type=text maxsize=16 value="'.$this->currentvalue.'">');	
	}
}


class CurrencyFlexField extends FlexField {
	function __construct($key)
	{
		$this->key = $key;
		return $this;
	}
	
	public function Create($key)
	{
		return new TextFlexField($key);
	}
	
	public function Present($parent)
	{
		$money = Currency::DATABASETODISPLAY($this->currentvalue);
//		print $this->currentvalue." - ".$money."<br>";
		_html('<input name="'.$this->key.'" type=text maxsize=16 value="'.$money.'">');	
	}
}


class PercentageFlexField extends FlexField {
	function __construct($key)
	{
		$this->key = $key;
		return $this;
	}
	
	public function Create($key)
	{
		return new TextFlexField($key);
	}
	
	public function Present($parent)
	{
		_html('<input name="'.$this->key.'" type=text size=3 maxsize=3 value="'.$this->currentvalue.'">%');	
	}
}

class DateFlexField extends FlexField {
	function __construct($key)
	{
		$this->key = $key;
		$this->Comment('(dd/mm/yyyy)');
		return $this;
	}
	
	public function Create($key)
	{
		$obj = new DateFlexField($key);
		$obj->Comment('(dd/mm/yyyy)');
		return $obj ;
	}
	
	public function Present($parent)
	{
		if (isset($this->currentvalue))
			$value = $this->currentvalue;
		else
			$value = "";
			
		// by default use todays address
		if ($value=="" || $value=="0000-00-00")
		{
			$value = "";
		}
		else if ($value=="today")
		{
			$value = date('d/m/Y');
		}
		else
		{
			$value = date('d/m/Y',strtotime($value));
		}


/*		$splitvalue = explode("/",$value);
		$day = $splitvalue[0];
		$month = $splitvalue[1];
		$year = $splitvalue[2];

		_html("<input type='text' name='".$this->key."_day' id='".$this->key."_day' size=2 maxlength=2 value='".$day."'>");
		_html("<input type='text' name='".$this->key."_month' id='".$this->key."_month' size=2 maxlength=2 value='".$month."'>");
		_html("<input type='text' name='".$this->key."_year' id='".$this->key."_year' size=4 maxlength=4 value='".$year."'>");
		_html("<input type=hidden name='".$this->key."' id='".$this->key."'>");
*/

	_html("<input type='text' name='".$this->key."' id='".$this->key."' size=10 maxlength=10 value='".$value."'>");
	}
}



class DropdownFlexField extends FlexField {
	private $options;
	private $refresh = false;

	public static function CreateKeyedUIDArray($inarray,$keycol,$valuecol)
	{
		if (empty($inarray)) return;
		
		foreach($inarray as $row)
		{
			$keyarray[$row[$keycol]] = $row[$valuecol];
		}
		return $keyarray;
	}

	public function Set($value)
	{
		if (isset($this->options))
		{
			// check in options
			if (array_key_exists($value,$this->options))
			{
				$this->currentvalue = $value;
			}
			else
			{
				reset($this->options);
				$this->currentvalue = key($this->options);
			}	
		}
		return $this;
	}

	public function AddEmptyOption($descrip)
	{
		return $this->AddOption("",$descrip);
	}

	function __construct($key)
	{
		$this->key = $key;
		return $this;
	}
	
	public function Create($key)
	{
		return new DropdownFlexField($key);
	}

	public function OnChangeRefresh()
	{
		$this->refresh = true;
		return $this;
	}

	public function SetOptions($list)
	{
		$this->options = $list;
		return $this;
	}
	
	public function AddOption($value,$description)
	{
		$this->options[$value] = $description;
	//	print_r($this->options);
		return $this;
	}

	public function Present($parent)
	{
		//print_r($this);
		$addcode = "";
		if ($this->refresh)
		{
			$addcode = "onChange='RepostForm(this,\"".$parent->aspect->GetAction()."\")'";
			//$addcode = "onChange='alert(1)'";
		}
	
		_html('<select name="'.$this->key.'" id="'.$this->key.'" '.$addcode.' >');
		if (!empty($this->options))
		{
			foreach($this->options as $value=>$description)
			{
				if ($value=='_') $value="";
				_html('<option value="'.$value.'"');
				if (isset($this->currentvalue) && $value==$this->currentvalue)
					_html(' selected '.$value);
				_html('>'.$description.'</option>');
			}
		}
		_html('</select>');
	}
}




require_once('cDatabase.php');


class DBObject 
{
	public $uid = -1;		// uninit
	public $table;
	public $fields;
	
	public function SetTable($table)
	{
		$this->table = $table;
//		$result = Database::QueryGetResult("SELECT * FROM $table WHERE 1 LIMIT 1;");
		$result = Database::QueryGetResults("DESCRIBE $table;");
		foreach ($result as $row)
		{
			$this->fields[] = $row['Field'];
		}
	}
	
	public function LoadFromDB($uid)
	{
	// use some reflection here? ok, for now dont otherwise I'm going to get complete lost in rewrites
		$results = Database::QueryGetResult("SELECT * FROM ".$this->table." WHERE uid='$uid';");
		if ($results===false)
			return false;
		
		$this->uid = $uid;
		foreach($results as $key=>$value)
		{
			$this->$key = $value;
		}
	}
	
	public function LoadFromPost($post)
	{
		foreach($this->fields as $field)
		{
			if (isset($post[$field]))
				$this->$field = TableInfo::Validate($this->table,$field,$post[$field]);
			else
				$this->$field = NULL;
		}	
	}
	
	// if uid is not null then update else insert
	public function SaveToDB($uid)
	{
		$q = "";
		foreach($this->fields as $key=>$field)
		{
			if ($key!=0) $q .= ", ";
			$q .= "$field='".$this->$field."'";
		}
		if ($uid==NULL)
		{
			$query = "INSERT INTO ".$this->table." SET $q;";
			//print $query."<br>"; //Database::Query($query);
			Database::Query($query);
			$this->uid = mysql_insert_id();
		}
		else
		{
			$query = "UPDATE ".$this->table." SET $q WHERE uid='".$uid."';";
			//print $query."<br>";
			Database::Query($query);
		}
	}
}


class CDeal extends DBObject {

	function __construct()
	{
		$this->SetTable("deals");
	}
	
	function LoadDealTerms()
	{
		$res = Database::QueryGetResult("SELECT * FROM dealterms WHERE dealid='".$this->crossdealid."'");
		$this->rate = $res['rate'];
		$this->base = $res['base'];
	}
	
	public function LoadFromPost($post)
	{
		parent::LoadFromPost($post);
		if (isset($post['rate']))
			$this->rate = $post['rate'];
		if (isset($post['base']))
			$this->base = $post['base'];
	}
	
	/*
	Deals:
	2 different systems
		deals --->* dealterms  (licenses)
		crossdealid means that if one of a bunch of deal changes they all reflect the same
	*/
	public function SaveToDB($uid)
	{
		if ($uid==NULL)
		{
			// new entry
			parent::SaveToDB($uid);
			if ($this->crossdealid==0)
				Database::Query("UPDATE deals SET crossdealid='".$this->uid."' WHERE uid='".$this->uid."'");
			if ($this->dealtype!='L')
			{
	        	DataTable::Insert("dealterms","dealid,keyfield,key,rate,base,descrip",$this->uid,"","",$this->rate,$this->base,"default");			
			}

		}
		else
		{
			// need to check if the crossdealid has changed
			// if so check if this was the master record
			if ($this->crossdealid==0)
			{	
				$this->crossdealid = $uid;	
			}
	
		    // Get any other deals that might be linked to this
		    $linkeddeals = Database::QueryGetResults("SELECT uid FROM deals WHERE crossdealid='".$this->crossdealid."' AND uid!='$uid';");
		    // If there are some change their crossdealid so its the first deal in the list
		    if ($linkeddeals!=false)
	        {
	          $dealconditions = Database::QueryGetResult("SELECT reserve,packagingdeductions,minpayout FROM deals WHERE uid='$uid';");
	          Database::Query("UPDATE deals SET reserve='".$dealconditions['reserve']."',packagingdeductions='".$dealconditions['packagingdeductions']."',minpayout='".$dealconditions['minpayout']."',crossdealid='".$this->crossdealid."' WHERE crossdealid='$uid' AND uid!='$uid';");
	        }
	
	        Database::Query("DELETE FROM dealterms WHERE dealid='".$this->crossdealid."';");
			if ($this->dealtype!='L')
			{
	        	DataTable::Insert("dealterms","dealid,keyfield,key,rate,base,descrip",$this->crossdealid,"","",$this->rate,$this->base,"default");			
			}
		
			
			// then update the other
			// else change
			parent::SaveToDB($uid);
			// need to save some extra stuff to dealterms
		}
	}
}

class FieldFactory
{
	public static function CreateDealType($key)
	{
		return DropdownFlexField::Create($key)->SetOptions(array('P'=>'Artist Profit Split Deal','R'=>'Artist Royalty Deal','L'=>'License Deal','S'=>'Remixer Split Deal'));
	}

	public static function CreateBand($key)
	{
		$results = Database::QueryGetResults("SELECT uid, name FROM bands ORDER BY name");
		$instance = new DropdownFlexField($key);
		$instance->SetOptions(DropdownFlexField::CreateKeyedUIDArray($results,'uid','name'));
		$instance->AddEmptyOption("--select a band--");
		return $instance;
	}
	
	public static function CreateAdvance($key)
	{
		return new CurrencyFlexField($key);
	}

	public static function CreatePercentage($key)
	{
		return new PercentageFlexField($key);
	}

	
	public static function CreateDate($key)
	{
		return new DateFlexField($key);
	}

	public static function CreateCurrency($key)
	{
		return new CurrencyFlexField($key);
	}

	public static function CreateBaseRate($key)
	{
		return DropdownFlexField::Create($key)->SetOptions(array('salevalue'=>'Sale Value')); //,'baseprice'=>'Base Price'));
	}


	public static function CreateTitleListFromQuery($key,$query)
	{
		$results = Database::QueryGetResults($query);
		$instance = new DropdownFlexField($key);
		$instance->SetOptions(DropdownFlexField::CreateKeyedUIDArray($results,'uid','title'));
		return $instance;
	}

	// These all call the above
	public static function CreateProductListForRemixer($key,$band)
	{
		// get the remixer name from bands
		$res = Database::QueryGetResults("SELECT name FROM bands WHERE uid='$band';");
		$bandid = $res[0]['name'];
		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, CONCAT(uid,': ',title) as title FROM product WHERE uid IN ( SELECT DISTINCT product FROM tracksproduct WHERE track IN ( SELECT uid FROM tracks WHERE remixer='$bandid') ) ORDER BY uid;");
	//	return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, title FROM product WHERE uid IN ( SELECT DISTINCT product FROM tracksproduct WHERE track IN ( SELECT uid FROM tracks WHERE remixer='$bandid') ) ORDER BY uid;");
	}
	
	public static function CreateProductListForBand($key,$band)
	{
		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, CONCAT(uid,': ',title) as title FROM product WHERE uid IN (SELECT DISTINCT product FROM tracksproduct WHERE track IN (SELECT uid FROM tracks WHERE artist='$band')) ORDER BY uid");
//		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, title FROM product WHERE uid IN (SELECT DISTINCT product FROM tracksproduct WHERE track IN (SELECT uid FROM tracks WHERE artist='$band')) ORDER BY uid");
	}

	public static function CreateTrackListForProductAndRemixer($key,$product,$remixer)
	{
		$res = Database::QueryGetResults("SELECT name FROM bands WHERE uid='$remixer';");
		$bandid = $res[0]['name'];
		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, title FROM tracks WHERE uid IN ( SELECT track FROM tracksproduct WHERE product='$product') AND (remixer='$bandid') ORDER BY uid");
	}
	
	public static function CreateForBand($key,$band)
	{
		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, title FROM tracks WHERE artist='$band' ORDER BY artist, uid");
	}
	
	public static function CreateForProduct($key,$product)
	{
		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, title FROM tracks WHERE uid IN ( SELECT track FROM tracksproduct WHERE product='$product') ORDER by uid;");
	}
	
	public static function CreateTrackListForProductAndBand($key,$product,$band)
	{
		$res = Database::QueryGetResults("SELECT name FROM bands WHERE uid='$band';");
		$bandid = $res[0]['name'];
		return FieldFactory::CreateTitleListFromQuery($key,"SELECT uid, title FROM tracks WHERE uid IN ( SELECT track FROM tracksproduct WHERE product='$product') AND (artist='$band' OR remixer='$bandid') ORDER BY uid");
	}
	
	public static function CreateLicensees($key)
	{
		$results = Database::QueryGetResults("SELECT uid,name FROM partners WHERE type LIKE '%L%'");
		$instance = new DropdownFlexField($key);
		$instance->SetOptions(DropdownFlexField::CreateKeyedUIDArray($results,'uid','name'));
		return $instance;
	}
	
	public static function CreateCrossDeal($key,$band)
	{
		$options[0] = "NONE";
		$defaultoption = 0;
		$deals = Database::QueryGetResults("SELECT GROUP_CONCAT(product),dealtype,crossdealid FROM deals WHERE band='".$band."' AND dealtype!='S' GROUP BY crossdealid;");
		if (!empty($deals))
		{
			if (count($deals)==1)
			{
				$options[$deals[0]['crossdealid']]="ALL OTHER DEALS";
				$defaultoption = $deals[0]['crossdealid'];
			}
			else
			{
				foreach ($deals as $deal)
				{			
					$text = $deal['GROUP_CONCAT(product)'];
					$text = str_replace(",",".",$text);
					if (strlen($text)>20)
						$text = substr($text,0,18)."...";
					
					if ($deal['dealtype']=="P") 
						$options[$deal['crossdealid']] = "PROFIT SHARE (".$text.")";
					else
						$options[$deal['crossdealid']] = "ROYALTY (".$text.")";
				}
			}
		}

		$instance = new DropdownFlexField($key);
		$instance->SetOptions($options);
		$instance->Set($defaultoption);
	//	print_r($instance);
		return $instance;
	}
}





class Action {
	public $key;
	public $validate;
	
	public function __construct()
	{
		$this->validate = false;
		// empty
	}
	
	public function SetValidate()
	{
		$this->validate = true;
		return $this;
	}
	
	public static function Create($key)
	{
		$instance = new Action();
		$instance->key = $key;
		return $instance;
	}
	
	protected function _submit($name,$onevent)
	{
		_html("<button type=submit class=submit name=submitvalue value='$name'");
		if ($onevent!="")
			print " onClick='$onevent' ";
		_html(">$name</button>");
	}

	public function Present($parent,$onevent)
	{
		$addcode = "";
//		$addcode = "javascript:if (bChanged) return confirm(\"You have made changes to this form that will be lost on performing this action. Do you wish to continue?\");";
		// If the action requires a validate then call our java-script on Submit.
//		if ($action['validate'])
//			$addcode = "return ".$this->_getscriptname()."(this.form);";
		$this->_submit($this->key,$onevent);
	}
}



class FlexForm {
	public $fields;
	public $actions;
	public $hidden;
	private $validationcode;

	public function AddField($field)
	{
		if ($field instanceof FlexField)
			$this->fields[] = $field;
	}	
	
	
	public function AddAction($action)
	{
		if ($action instanceof Action)
			$this->actions[] = $action;
	}
	
	public function GetValidationCode()
	{
		print_r($this->validationcode);
		if (!isset($this->validationcode))
			return "";
			
		$code = "function FormValidate(form)\n{ try {\n";
		foreach ($this->validationcode as $codeline)
		{
			$code .= $codeline."\n";
		}
		$code .= " return true; } catch (ex) { alert(ex); return false; }\n}\n";

		return $code;
	}
	
	public function Present()
	{
		_html('<form name="validatedform" id="validateform" method="post" action="index.php" enctype="multipart/form-data">'."\n".
			  '<input type="hidden" name="page" value="'.$this->target.'">'."\n");

		_html('<table>');
		foreach($this->fields as $index=>$field)
		{
			_html('<tr>');
			
			if (isset($field->description))
				$description = $field->description;
			else
				$description = $field->key;
			
			_html('<td>'.$description.'</td><td>');
			$field->Present($this);
			
			// get any validation code
			if (isset($field->validationcode))
				$this->validationcode[] = $field->validationcode;
			
			if (isset($field->comment))
				_html('<i> '.$field->comment.'</i>');
			_html('</td>');
			_html('</tr>');
		}
		
		
//		JavaScript_Begin();
//		JavaScript_Write($this->GetValidationCode());
//		JavaScript_End();
		
		foreach($this->actions as $index=>$action)
		{
			_html('<tr><td>');
			$onevent = "";
//			if ($action->validate)
//				$onevent = "return FormValidate(this.form)";
			$action->Present($this,$onevent);	
			_html('</td></tr>');
		}
		_html('</table>');
		if (!empty($this->hidden))
		{
			foreach($this->hidden as $key=>$value)
			{
				_html("<input type=hidden name=$key value=$value>");
			}
		}
		_html('</form>');
	}
	
	
	// Just to make aspect compatible
	
	public $target;
	public $aspect;
	public $transactionid;
	
	public function SetTarget($target)
	{
		$this->target = $target;
	}
	
	public function SetAspect($aspect)
	{
		$this->aspect = $aspect;
	}	
	
	public function SetHidden($key,$value)
	{
		$this->hidden[$key] = $value; 
	// not implemented
	}
}


?>