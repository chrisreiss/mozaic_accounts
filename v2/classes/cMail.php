<?php

require_once('cDatabase.php');


define('MAIL_TARGET_USER','user');
define('MAIL_TARGET_ACCOUNT','account');
define('MAIL_TARGET_ALL','all');

// This is a class that handles mails

class Mail 
{

	var $m_uid;
	var $m_subject;
	var $m_body;
	var $m_action;
	var $m_distribution;
	var $m_sentdate;

	function MailFromDB(&$assoc)
	{
		$this->m_uid = $assoc['uid'];
		$this->m_subject = $assoc['subject'];
		$this->m_body = $assoc['body'];
		$this->m_action = $assoc['action'];
		$this->m_sentdate = $assoc['sentdate'];
		$this->m_distribution = $assoc['distribution'];
	}
	
	function SetMail($subject,$body)
	{
		$this->m_subject = $subject;
		$this->m_body = $body;
	}
	
	function SetAction($page,$action)
	{
		$this->m_action = $page.":".$action;
	}
	
	function GetActionPage()
	{
		if (!empty($this->m_action))
		{
			$action = explode(":",$this->m_action);
			return $action[0];
		}
		return false;
	}

	function GetAction()
	{
		if (!empty($this->m_action))
		{
			$action = explode(":",$this->m_action);
			return $action[1];
		}
		return false;
	}
	
	function GetBody()
	{
		return $this->m_body;
	}

	function GetSubject()
	{
		return $this->m_subject;
	}

	function GetDate()
	{
		return $this->m_sentdate;
	}
	
	function GetDistribution()
	{
		return $this->m_distribution;
	}
	
	function CanDelete()
	{
		return ($this->m_distribution != MAIL_TARGET_ALL);
	}
	

}



class MailManager 
{
	// returns a list of mail ids and subjects
	function GetMails($userid)
	{
		$userdata = Database::QueryGetResult("SELECT accountid FROM users WHERE userid='".$userid."';");

		// Get mails to user / accountid / public		
		$result = Database::Query("SELECT uid,distribution,subject,sentdate FROM szirtest_mozaic.mails WHERE (distribution='".MAIL_TARGET_ALL."') OR (distribution='".MAIL_TARGET_ACCOUNT."' AND destination='".$userdata['accountid']."') OR (distribution='".MAIL_TARGET_USER."' AND destination='".$userid."') ORDER BY sentdate DESC;");
		for ($i=0;$i<$result->GetNum();++$i)
		{
			$row = $result->GetNext();

			if ($row['distribution']==MAIL_TARGET_ALL) $group = "Announcements";
			if ($row['distribution']==MAIL_TARGET_USER) $group = "Personal Mail";
			if ($row['distribution']==MAIL_TARGET_ACCOUNT) $group = "Actions";
					

			$mails[] = array('group'=>$group,'uid'=>$row['uid'],'subject'=>$row['subject'],'sentdate'=>$row['sentdate']);
		}
		Database::FinishQuery($result);
		if (isset($mails))
			return $mails;
		
		// no results return false;
		return false;
	}
	
	// Retreives a mail object
	function Get($mailid)
	{
		$dbmail = Database::QueryGetResult("SELECT * FROM szirtest_mozaic.mails WHERE uid='".$mailid."';");
		$mail = new Mail;
		$mail->MailFromDB($dbmail);
		return $mail;
	}
	
	// input - mail object, returns mailid
	function Send(&$mail,$distribution,$to)
	{
		Database::Query("INSERT INTO szirtest_mozaic.mails SET subject='".$mail->m_subject."',body='".$mail->m_body."',destination='".$to."',action='".$mail->m_action."',sentdate='".date("Y-m-d",time())."',distribution='".$distribution."';");  		
	}
	
	function Delete($mailid)
	{
		Database::Query("DELETE FROM mails WHERE uid='".$mailid."';");
	}
}


?>
