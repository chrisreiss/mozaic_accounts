/*

 Calculate per deal. Deal consists of a number of tracks and how income is accounted.
 
 Potential Track income sources are digital track sales, digital bundle sales, streaming
 
 Deal:
 	Track collection
 	Split for bundles
 	Split for tracks
 	Split for streams
 	
 
 
 Steps 
 1: Grab all unique bundles (products) that contain the tracks in the deal scheme
 2: Get all sales for those bundles
 	a) bundle sales only - i.e. no track specified in sale item
 	b) track sales from bundle (optional - set in deal)
 	c) streams from bundle (optional - set in deal)
 	d) bundled licensing
 	e) track licensing (optional)
 	
 3: Calc bundle profit = (SUM bundle sales - SUM bundle expenses) * prorata
 
 4. Get all sales for single tracks
 	a) track/stream sales with no product in them
 	b) track/stream sales from bundle (optional - set in deal - negate)
 	c) track licensing (optional)
 	
 5: Calc track profit = (SUM track sales - SUM track expenses)
 
 6: 

*/