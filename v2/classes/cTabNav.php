<?php

require_once('strings.php');


class TabNav
{
	var $m_tabs;
	var $m_numtabs = 0;
	var $m_activetab = -1;

// construtor
	function TabNav()
	{
		$m_numtabs = 0;
		$m_activetab = -1;
	}

	function AddTab($text,$var,$value,$tooltip)
	{
		$this->m_tabs[$this->m_numtabs] = array('text' => $text,'var' => $var, 'value' => $value, 'tooltip' => $tooltip);
		++$this->m_numtabs;
		return $this->m_numtabs;
	}
	
	function SetActiveTab($value)
	{
		for ($i=0;$i<$this->m_numtabs;++$i)
		{
			if ($this->m_tabs[$i]['value']==$value)
			{
				$this->m_activetab = $i;
				break;
			}
		} 
	}


	function Display()
	{
		$width = 100 / $this->m_numtabs;
		print "<table width=100% cellpadding=0 cellspacing=0><tr>\n";
		for ($i=0;$i<$this->m_numtabs;++$i)
		{
			if ($i==$this->m_activetab)
			{
				print "<td width=".$width."% ><form method=post action='index.php' name='tabby".$i."'><input type=hidden name=page value='".$this->m_tabs[$i]['value']."'>";
				print "<button disabled name='submitbutton' class='tabnav' value='".$this->m_tabs[$i]['text']."' >".$this->m_tabs[$i]['text']."</button></form></td>\n";
			}
			else
			{
				print "<td width=".$width."% ><form name='tabby".$i."' method=post action='index.php'><input type=hidden name=page value='".$this->m_tabs[$i]['value']."'>";
				print "<button name='submitbutton' class='tabnav' value='".$this->m_tabs[$i]['text']."' onmouseover=\"Tip('".$this->m_tabs[$i]['tooltip']."',DELAY,1000,WIDTH,300);\" onclick=\"document.tabby".$i.".submit()\">".$this->m_tabs[$i]['text']."</button></form></td>\n";
			}
		}
		print "</tr></table>\n";
	}

}

	
?>
