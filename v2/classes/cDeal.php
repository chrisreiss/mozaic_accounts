<?php

/*
require_once("cDatabase.php");
//Deal::CreateDealsFromProducts();
Deal::CompilationNoMaster();
exit(1);
*/

class Deal {
/*
	Deal Table

	UID			:	Unique Identifier for this deal
	Type		:	Profit Share / Royalty
	Percentage	:	Percentage of share / royalty
	Reserve		:	Reserve percentage
	StartDate	:	Start date of deal
	EndDate		:	End date of deal
	Advance		:	Advance for part of this deal
	Band		:	Artist who this deal is for
	
	DealProduct	Table
	
	Product		:	UID of Product
	Deal		:	UID of Deal	
*/

	function CompilationNoMaster()
	{
		// Get all compilations that are masters of crossdeals
		$results = Database::QueryGetResults("SELECT uid FROM product WHERE artist='' AND uid IN (SELECT product FROM deals WHERE crossdealid=uid);");
/*		print_r($results);
		print "<hr>";*/
		foreach ($results as $result)
		{
/*			print"result=";
			print_r($result);
			print "<br>";*/
			$deals = Database::QueryGetResults("SELECT uid,crossdealid FROM deals WHERE product='".$result['uid']."' AND crossdealid=uid;");
			foreach ($deals as $deal)
			{
/*				print "deal(".$deal['uid'].")=";
				print_r($deal);
				print "<br>";*/

//				$others = Database::Query("SELECT uid FROM deals WHERE product IN (SELECT uid FROM product WHERE artist!='' AND uid IN (SELECT product FROM deals WHERE crossdealid='".$deal['uid']."')) AND crossdealid='".$deal['uid']."' LIMIT 1;");
				$others = Database::QueryGetResults("SELECT uid FROM product WHERE artist!='' AND uid IN (SELECT product FROM deals WHERE crossdealid='".$deal['uid']."');");
				if (!empty($others))
				{
					$filtereddeals = Database::QueryGetResults("SELECT uid FROM deals WHERE product='".$others[0]['uid']."' AND crossdealid='".$deal['uid']."';");
//		Database::Query("UPDATE deals SET crossdealid='".$others[0]['uid']."' WHERE crossdealid='".$deal['uid']."';");
					print "UPDATE deals SET crossdealid='".$filtereddeals[0]['uid']."' WHERE crossdealid='".$deal['uid']."';<br>";
//					Database::Query("UPDATE deals SET crossdealid='".$filtereddeals[0]['uid']."' WHERE crossdealid='".$deal['uid']."';");
				}
				else
				{
					print "can't change ".$deal['uid']."<br>";
				}
			}
		}
	}


	function CreateDealsFromProduct($catno,$silent)
	{
		$defaults = Deal::GetDefaultValues();
		//die ('here');
		$product = Database::QueryGetResult("SELECT * FROM product WHERE uid='".$catno."' LIMIT 1;");
	//	print_r($product);
		if (!$silent) print "Processing ".$product['uid']."<br>";
		
		// Comp??		
		if ($product['artist']=='0')
		{
			// compilations are slightly different - need one deal per artist
			$tracks = Database::QueryGetResults("SELECT artist FROM tracks WHERE uid IN (SELECT track FROM tracksproduct WHERE product='".$product['uid']."') GROUP BY artist;");
			print "no tracks = ".count($tracks)."<br>";
			if (!empty($tracks))
			{
				foreach ($tracks as $track)
				{
					$artistcrossdeal = Database::QueryGetResult("SELECT crossdealid FROM deals WHERE band='".$track['artist']."' LIMIT 1;");
					if (empty($artistcrossdeal))
						$artistcrossdeal = -1;
					else
						$artistcrossdeal = $artistcrossdeal['crossdealid'];
					
					
					Database::Query("INSERT INTO deals SET product='".$product['uid']."',band='".$track['artist']."',dealtype='".$defaults['dealtype']."',reserve='".$defaults['reserve']."',startdate='".$product['releasedate']."',crossdealid='".$artistcrossdeal."';");
					$uid = mysql_insert_id();

					if ($artistcrossdeal==-1)
						Database::Query("UPDATE deals SET crossdealid='".$uid."' WHERE uid='".$uid."';");
						
					Database::Query("INSERT INTO dealterms SET dealid='".$uid."',rate='".$defaults['rate']."',base='salevalue'");
						
					if (!$silent) print "Created deal $uid for artist ".$track['artist']." crossdeal ($artistcrossdeal)<br>";
				}
			}
		}
		else
		{
			//die('here');
			// check if artist already has crossdeals
			$artistcrossdeal = Database::QueryGetResult("SELECT crossdealid FROM deals WHERE band='".$product['artist']."' LIMIT 1;");
			if (empty($artistcrossdeal))
				$artistcrossdeal = -1;
			else
				$artistcrossdeal = $artistcrossdeal['crossdealid'];

			$advance = 0;
/*				// Get the advance if there
			$advance = Database::QueryGetResult("SELECT amount FROM expenses WHERE product='".$product['uid']."' AND type='Advance';");
			if (empty($advance))
				$advance = 0;
			else
				$advance = $advance['amount'];
*/			
			// Insert this
			Database::Query("INSERT INTO deals SET product='".$product['uid']."',band='".$product['artist']."',dealtype='".$defaults['dealtype']."',reserve='".$defaults['reserve']."',startdate='".$product['releasedate']."',crossdealid='".$artistcrossdeal."';");

			
			$uid = mysql_insert_id();
			
			if ($artistcrossdeal==-1)
			{
				$artistcrossdeal = $uid;
				Database::Query("UPDATE deals SET crossdealid='".$artistcrossdeal."' WHERE uid='".$uid."';");
				Database::Query("INSERT INTO dealterms SET dealid='".$artistcrossdeal."',rate='".$defaults['rate']."',base='salevalue'");
					
			}
		}
		
	}

	function GetDefaultValues()
	{
		Database::ConnectAdmin("szirtest_mozaic");
		$defaults = Database::QueryGetResult("SELECT defaultdealtype,defaultdealrate,defaultdealreserve,defaultpackagingdeductions,defaultdealterm FROM accounts WHERE uid='".$_SESSION['accountid']."';");
		Database::Connect();
		return array('packagingdeductions'=>$defaults['defaultpackagingdeductions'],'dealtype'=>$defaults['defaultdealtype'],'rate'=>$defaults['defaultdealrate'],'reserve'=>$defaults['defaultdealreserve'],'term'=>$defaults['defaultdealterm']);
	}
	
	function Validate(&$fields,$doesexist)
	{
		// Some validation code
		if ($fields['band']==0)
			return "Cannot have a deal assigned to Various Artists.";
		
		if (!empty($fields['crossdealid']))
		{
			$result = Database::QueryGetResult("SELECT dealtype FROM deals WHERE uid='".$fields['crossdealid']."';");
			if ($result['dealtype']!=$fields['dealtype'])
				return "Cannot cross link royalty and profit share agreements";
		}
		
		if (!$doesexist)
		{
			$result = Database::QueryGetResult("SELECT uid FROM deals WHERE band='".$fields['band']."' AND dealtype!='L' AND product='".$fields['product']."' AND (enddate='0000-00-00' OR enddate>='".$fields['startdate']."');");
			if (!empty($result))
				return "Duplicate or overlapping deal - please correct";
		}
	}
	
	function Insert(&$fields)
	{
	}
	
	function Update($uid,&$fields)
	{
	}
	
	function Remove($uid)
	{
	}
	
	function AddProduct($uid,$productid)
	{
	}
	
}

?>