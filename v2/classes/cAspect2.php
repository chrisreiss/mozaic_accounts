<?php

define('STATE_PROCESS',0);
define('STATE_PRESENT',1);
define('STATE_ERROR',-1);

class Aspect {

	var $m_objects;
	var $m_state;
	var $m_name;
	var $m_action;		// next action to perform

	function	PrcessAction()
	{
		$error_state = "No Action defined";
		return STATE_ERROR;
	}

	function	Present()
	{
		if (!empty($this->m_objects))
		{
			foreach ($this->m_objects as $object)
			{
				$object->Present();
			}
		}
		$this->m_state = STATE_PRESENT;
	}
	
	
	function _isForm($object)
	{
		return (get_class($object)=="form" || get_class($object)=="validatedform" ||
				get_class($object)=="Form" || get_class($object)=="ValidatedForm" ||
				get_class($object)=="tablelist" || get_class($object)=="TableList");
	}
	
	
	function Attach(&$object)
	{
		$index = count($this->m_objects);
		$this->m_objects[$index] = $object;
		if ($this->_isForm($object))
		{
			$this->m_objects[$index]->SetTarget($this->m_name);
			$this->m_objects[$index]->SetAspect($this);
			$this->m_objects[$index]->AddHidden("transactionid",$_SESSION['transactionid']);
		}
				
	}
	
	function	SetNextAction($action)
	{
		$this->m_action = $action;
	}
	
	function	GetAction()
	{
		if (!isset($_POST['submitvalue']))
			$action = "VIEW";
		else
			$action = $_POST['submitvalue'];
			
		return $action;
	}
	
	function	GetData()
	{
		return $_POST;
	}
	
	
	function	Process()
	{
		$this->m_state = STATE_PROCESS;
		while($this->m_state==STATE_PROCESS)
		{
			$action = $this->GetAction();
			$data = $this->GetData();
			$this->ProcessAction($action,$data);
		}	

		switch ($this->m_state)
		{
			case STATE_PRESENT:		// present the results to user - terminate this script
			{
				break;
			}
			case STATE_ERROR:		// terminate this script in an error
			{
				print $error_state;
				exit(1);
			}
		}
	}
}
?>