<?php
require_once('cDatabase.php');

class Authorize
{
	function IsAuthorized()
	{
//		session_start();
		if (isset($_SESSION['sessionkey']))
		{
//			$timeout = 30*60; //30*60;		// in seconds = 2 hours
	
			if ($_SESSION['timeout']>time())
			{
				$_SESSION['timeout'] = time()+(60*120);
				return true;
			}
			return true;
		}
		return false;
	}
	
	function Logout()
	{
		$userid = $_SESSION['userid'];
		Database::ConnectAdmin("mozaic_mozaic");
		Database::Query("UPDATE mozaic_mozaic.users SET lastlogout='".date('Y-m-d H:i:s')."' WHERE userid='".$userid."';");

//		setcookie ("TestCookie", "", time() - 3600);
		session_start();
		session_destroy();
		session_unset();

	}
	
}
?>