<?php

require_once("cWorkSheet.php");
require_once("cTableInfo.php");
require_once("cCurrency.php");

define("TRANSACTION_SALE","0");
define("TRANSACTION_EXPENSE","1");
define("TRANSACTION_DUMMY","4");

// UPDATE TO TABLE
// ALTER TABLE `sale` ADD `baseprice` VARCHAR( 16 ) NOT NULL AFTER `quantity` ;
// ALTER TABLE `deals` ADD `packagingdeductions` FLOAT NOT NULL DEFAULT '0' AFTER `reserve` ;
// RENAME TABLE  `szirtest_11`.`creditor` TO  `szirtest_11`.`partners` ;

// override util 

function displaydate2($complexdatestring)
{
	$datestring = explode(" to ",$complexdatestring);
	if (empty($datestring[1]))
		return displaydate($datestring[0]);

	return displaydate($datestring[0])." - ".displaydate($datestring[1]);
}



class Statement2
{
	var $m_display;
	var $m_errormessage;
	var $m_worksheet;
	var $m_complete;
	var $m_amount;
	var $m_reserve;
	
	function Statement2()
	{
		$this->m_display = "";
		$this->m_errormessage = "";
		$this->m_artistid = -1;
		$this->m_complete = true;
	}
	
	function _print($string)
	{
		$this->m_display = $this->m_display.$string;
	}
	
	function Display()
	{
		$this->m_worksheet->DisplayAsTable();
	}
	
	function IsValid()
	{
		return ($this->m_artistid!=-1);
	}

	function	SortResults($assoc, $sortfield)
	{
		foreach ($assoc as $key=>$row)
		{
			$sort[$key] = $row[$sortfield];
		}
		array_multisort($sort,SORT_ASC,$assoc);
		return $assoc;
	}

	function	CollateResults(&$assoc, $sortfieldscsv )
	{
		if (empty($assoc)) return;
		
		// Sort
		$sortfields = explode(",",$sortfieldscsv);

		// clear the current vars
		foreach ($sortfields as $sortfield)
		{
			$collaterow[$sortfield] = "";
		}
		
		// Go through each row
		foreach ($assoc as $row)
		{
			$changerow = false;
			foreach ($sortfields as $sortfield)
			{
				if ($row[$sortfield] != $collaterow[$sortfield])
				{
					if ($collaterow[$sortfield]!="") 
					{
						$outrow[] = $collaterow;			// flush the previous row
						$changerow = true;
					}
					$collaterow[$sortfield] = $row[$sortfield];
				}
			}	
			// and each value in each row
			foreach ($row as $key=>$value)
			{
				// is this cell a sortfield?
				if (array_search($key,$sortfields)===false)
				{
					if ($changerow)
						$collaterow[$key] = 0;
						
					if (empty($collaterow[$key]))
						$collaterow[$key] = $value;
					else
					{
						// decide what to do
						if ($key=="quantity")
							$collaterow[$key] += $value;
						else
							if ($key=="value" || $key=="amount")
							{	
								$collaterow[$key] = Currency::SUMOF($collaterow[$key],$value);
							}
						else if ($key=="date")
						{
							if ($collaterow[$key]=="") 
								$collaterow[$key]=$value;
							else
							{
								$split = explode(" to ",$collaterow[$key]);
								if (empty($split[1]))
								{
									if ($split[0]<$value) 
										$collaterow[$key] = $split[0]." to ".$value;
									else if ($split[0]>$value)
										$collaterow[$key] = $value." to ".$split[0];
								}
								else
								{
									if ($split[0]>$value && $split[1]>$value)	$split[0] = $value;
									if ($split[1]<$value && $split[0]<$value)	$split[1] = $value;
									$collaterow[$key] = $split[0]." to ".$split[1];
								}
									
							}
							
						}
						else
							$collaterow[$key] = $value;
//							$collaterow[$key] .= ",".$value;
					}
				}
			}
		}
		$outrow[] = $collaterow;
		

		return $outrow;
	}

	function GetSales(&$deal)
	{
		$sales = Database::QueryGetResults("SELECT date,distributor,quantity,product,value,baseprice FROM sale WHERE track='' AND saletype='P' AND product='".$deal['product']."' AND date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' ORDER BY distributor;");
		return $sales;
	}

	function GetDigitalAlbumSales(&$deal)
	{
		$sales = Database::QueryGetResults("SELECT date,distributor,quantity,product,value,baseprice FROM sale WHERE track='' AND saletype='V' AND product='".$deal['product']."' AND date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' ORDER BY distributor;");
		return $sales;
	}


	function GetDigitalSales(&$deal)
	{
		$query = "SELECT date,distributor,quantity,track,value,baseprice FROM sale WHERE saletype='V' AND product='".$deal['product']."' AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."') AND date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."';";
		$sales = Database::QueryGetResults($query);

		return $sales;
	}

	function GetExpenses(&$deal)
	{
		$expenses = Database::QueryGetResults("SELECT invoicedate,type,product,amount FROM expenses WHERE product='".$deal['product']."' AND invoicedate BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."';");
		return $expenses;
	}
	
	// Get all licences relating to band
	function GetDealLicences($deal,$compilationdeal)
	{
		$band = $deal['band'];
		
		$cataloguelist = "'".$deal['product']."'";

		// Don't include any licencing for a tracks on compilations these should be included in the main statement
		if ($compilationdeal)
		{
			$excludelist = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product IN (SELECT uid FROM product WHERE artist='$band');");
			$excludecsv="";
			if (!empty($excludelist))
			{	
				foreach ($excludelist as $i=>$exclude)
				{
					if ($i>0) $excludecsv .= ",";
					$excludecsv .= "'".$exclude['track']."'";
				}
				$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
														(SELECT track FROM tracksproduct WHERE product='".$deal['product']."')
														AND uid NOT IN ($excludecsv);");
			}
			else
			{
				$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
														(SELECT track FROM tracksproduct WHERE product='".$deal['product']."');");
			}
		}
		else
		{
			$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
													(SELECT track FROM tracksproduct WHERE product='".$deal['product']."');");
		}

		if (!empty($catalogue))
		{
			foreach ($catalogue as $index=>$item)
			{
				$cataloguelist .= ",'".$item['uid']."'";
			}
			$tracklicences = Database::QueryGetResults("SELECT uid FROM licences WHERE ((licencetype='T') AND licenceduid IN ($cataloguelist));");
		}
		
		if ($compilationdeal && !empty($tracklicences))
		{
			// These are compilation only track licences - however we can't include in here so add them to a magic list!
			foreach ($tracklicences as $licence)
			{
				if (empty($this->m_additionallicences) || array_search($licence['uid'],$this->m_additionallicences)===false)
					$this->m_additionallicences[] = $licence['uid'];
			}
			unset($tracklicences);
		}

		if ($compilationdeal)
		{
			$productlicences = Database::QueryGetResults("SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';");
		}
		else
		{
			$artist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$deal['product']."';");
			if ($artist['artist']!='')
				$productlicences = Database::QueryGetResults("SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';");
		}
	  
	  	// merge these together
	  	if (empty($tracklicences) && !empty($productlicences)) $licences = $productlicences;
	  	else if (!empty($tracklicences) && empty($productlicences)) $licences = $tracklicences;
	  	else if (!empty($tracklicences) && !empty($productlicences)) $licences = array_merge($tracklicences,$productlicences);

		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				$licenceuids[] = $licence['uid'];
			}
		}
		if (!empty($licenceuids))
			return $licenceuids;
	}
	
	
	function GetLicenceMonies(&$licenceuids,$startdate,$enddate)
	{
		if (empty($licenceuids)) return;
		$licenceuids_csv = implode(",",$licenceuids);
		// Get all advances that are applicable
		$licences = Database::QueryGetResults("SELECT uid,description,advance,advancepaiddate,licenceduid,licencetype FROM licences WHERE uid IN ($licenceuids_csv);");
		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				// Any advances?
				if ($licence['advancepaiddate']>=$startdate && $licence['advancepaiddate']<=$enddate)
					$item[] = array('date'=>$licence['advancepaiddate'],'description'=>"Advance ".$licence['description'],'catno'=>$licence['licenceduid'],'value'=>$licence['advance'],'licencetype'=>$licence['licencetype']);

				
				// Any statements
				$licencestatements = Database::QueryGetResults("SELECT paiddate,amount,licenceuid FROM licencestatements WHERE licenceuid='".$licence['uid']."' AND paiddate BETWEEN '$startdate' AND '$enddate';");
	
				if (!empty($licencestatements))
				{
					foreach ($licencestatements as $statement)
					{
						$item[] = array('date'=>$statement['paiddate'],'description'=>"Royalties ".$licence['description'],'catno'=>$licence['licenceduid'],'value'=>$statement['amount'],'licencetype'=>$licence['licencetype']);
					}
				}
			}
		}

		if (!empty($item))
			return $item;		

	}
	
	function ProcessLicencing(&$deal,&$worksheet,&$splits,$compilationdeal)
	{	
	
		// Go through each product in the deal set and get the licences related to each of them
		// A licence is include that is for any product or track in the deal
		// This means that licences can appear multiple times when a track is in two products (eg. a main release and a comp)
		// Get Artist licences	
		$licences = $this->GetDealLicences($deal,$compilationdeal);
		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				if (empty($licencelist) || array_search($licence,$licencelist)==false)
				{
					$licencelist[] = $licence;
				}
			}
		}

		// $licencelist now should contain all the licences for this dealset but removed duplicates

		// Given a licence - get all monies e.g. advances and royalties eligable in this period
		$licencing = $this->GetLicenceMonies($licencelist,$this->m_startdate,$this->m_enddate);	
		$licencing_total = 0;
		if (!empty($licencing))
		{
			foreach ($licencing as $item)
			{
				$value = 0;
				$value = Currency::GETRAWVALUE($item['value']);
				$worksheet->AddRow(displaydate2($item['date']),$item['description'],$item['catno'],"",Currency::DISPLAY($value));
				$licencing_total += $value;
			}
		}
		return $licencing_total;
	}


	function	MakeStatementHash($artist,$bandlist,$startdate,$enddate)
	{
		return md5($artist."_".$bandlist."_".$startdate."_".$enddate);
	}

	function	MakeStatementFilename2($pdfhash)
	{
		return "../accounts/".$_SESSION['accountid']."/statement_".$pdfhash.".pdf";
	}

	function	MakeStatementFilename($artist,$bandlist,$startdate,$enddate)
	{
		return "../accounts/".$_SESSION['accountid']."/statement_".$artist."_".$bandlist."_".$startdate."_".$enddate.".pdf";
	}

	// This is a private function - it creates the html that we insert into the worksheet
	// this gives us the form "CARRYOVER BALANCE".
	function CreateBalanceHTML($crossdealid)
	{
		$this->m_complete = false;
		$html = "<form method=post action=index.php><input type=hidden name=page value=artists>";
		$html .="<input size=6 type=text name='carryover".$crossdealid."' value=''>";
		foreach($_POST as $key=>$value)
		{
			if ($key=='bands')
				$html .="<input type=hidden name='bands[]' value='".implode(",",$_POST['bands'])."'>";
			else
				$html .= "<input type=hidden name='".$key."' value='".$value."'>";
		}
		$html .="<input type=submit class=submitsmall name=submit value='SET'>";
		$html .="<input type=hidden name=transactionid value='".$_SESSION['transactionid']."'>";
		$html .="</form>";
		return $html;
	}

	function ProcessAdvances(&$deals,&$worksheet)
	{
		$advance_total = 0;
		foreach ($deals as $deal)
		{
			if ($deal['advancepaiddate']>=$this->m_startdate && $deal['advancepaiddate']<=$this->m_enddate)
			{
			
				$advance = Currency::GETRAWVALUE($deal['advance']);
				// Subtract advances
				if ($advance!=0)
				{
					$worksheet->AddHeaderRow("","ADVANCE FOR ".$deal['product'],"","",Currency::DISPLAY(-$advance));
					$advance_total += $advance;
				}
			}
		}
		return $advance_total;
	}
	
	
	function FindDealTerm(&$sale,&$dealterms)
	{
			foreach($dealterms as $dealterm)
		{
			if ($dealterm['keyfield']=="") $dealterm['keyfield']='termkey';
			if ($sale[$dealterm['keyfield']]==$dealterm['key'])
				return $dealterm;
		}
	}
	
	function DoSales(&$worksheet,&$sales,&$dealterms,$packagingdeductions,$split,$typedescrip,$printsplit)
	{
		$receipts_total = 0;
		if (!empty($sales))
		{
			$quantity_total = 0;
			// depending on detail level
//			if ($this->m_collate)
//				$sales = $this->CollateResults($sales,"distributor");
			
			foreach ($sales as $sale)
			{
//				print_r($sale);print"<br>";

				// find if matches any dealterms
				$dealterm = Statement2::FindDealTerm($sale,$dealterms);
//				print "dealterm = "; print_r($dealterm); print "<br>";
				
				$isempty = false;
				if ($dealterm['base']=="baseprice")
				{
					$value = Currency::GETRAWVALUE($sale['baseprice']) * $sale['quantity'];
				}else
				{
					$value = Currency::GETRAWVALUE($sale['value']);
				}
				
				if ($dealterm['key']=="")
					$descrip = $typedescrip." : ".TableInfo::DisplayFromType("partner",$sale['distributor']);
				else
					$descrip = $typedescrip." : ".TableInfo::DisplayFromType("partner",$sale['distributor'])." (".$dealterm[$sale['termkey']]['descrip'].")";
				
				//* ((100-$deal['packagingdeductions'])/100))
				$net = ($value - ($value*$packagingdeductions)) * ($dealterm['rate']/100) * $split;
				$quantity_total += $sale['quantity'];
				$receipts_total += $net;	
				$worksheet->AddRow(displaydate2($sale['date']),
									$descrip,
									$sale['quantity'],
									Currency::DISPLAY($value),
									$dealterm['rate']."%",
									floor($packagingdeductions*100)."%",
									$printsplit, //round($split,3),
									Currency::DISPLAY($net));
			}
//			$worksheet->AddHeaderRow("","TOTAL",$quantity_total,"","","","",Currency::DISPLAY($receipts_total));
		}
		return round($receipts_total);
	}
	
	
	// This process a set of deals as one "account"
	function ProcessDeal(&$worksheet,$deal,$carryover)
	{
		// RECEIPTS FIRST
	//	$worksheet->AddTitleRow("RECEIPTS");
		$worksheet->AddTitleRow(TableInfo::DisplayFromType("product",$deal['product']),TableInfo::DisplayFromType("track",$deal['track']));
		$worksheet->AddHeaderRow("DATE","DESCRIPTION","SALES","VALUE","ROYAL","PACK","PRORATA","DUE");

		$bandname = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$deal['band']."';");
		$bandname = strtoupper($bandname['name']);
	
		$dealterms = Database::QueryGetResults("SELECT * FROM dealterms WHERE dealid='".$deal['crossdealid']."';");
//    print "dealterms =  "; print_r($dealterms); print"<br>";

/*		foreach ($dealtermstemp as $d)
		{
			$dealterm[$d['keyfield'].$d['key']] = Array('keyfield'=>$d['keyfield'],'rate'=>$d['rate'],'descrip'=>$d['descrip'],'base'=>$d['base']);
		}
*/		
		$res = Database::QueryGetResult("SELECT COUNT(track) FROM tracksproduct WHERE product='".$deal['product']."';");
	//	$split = (1/$res['COUNT(track)']);
		$bandtracks = Database::QueryGetResult("SELECT COUNT(track) FROM tracksproduct WHERE product='".$deal['product']."' AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."');");
		// Split is the number of bands tracks divided by the number of tracks
		$printsplit = $bandtracks['COUNT(track)']."/".$res['COUNT(track)'];
		$split = $bandtracks['COUNT(track)'] / $res['COUNT(track)'];			

	
		// DO SALES	
		$balance = 0;
		$receipts_total = 0;
	
		$query = "SELECT * FROM sale WHERE product='".$deal['product']."' AND saletype='P' AND date>='".$this->m_startdate."' AND date<='".$this->m_enddate."';";
		$sales = Database::QueryGetResults($query);
		if (!empty($sales))
		{
			$receipts_total = $this->DoSales($worksheet,$sales,$dealterms,$deal['packagingdeductions']/100,$split,"Physical Sales",$printsplit);
			$balance += $receipts_total;
		}
		
		$query = "SELECT * FROM sale WHERE product='".$deal['product']."' AND track='' AND saletype='V' AND date>='".$this->m_startdate."' AND date<='".$this->m_enddate."';";
		$sales = Database::QueryGetResults($query);
		if (!empty($sales))
		{
			$receipts_total = $this->DoSales($worksheet,$sales,$dealterms,0,$split,"Digital Album Sales",$printsplit);
			$balance += $receipts_total;
		}
		
		$query = "SELECT * FROM sale WHERE product='".$deal['product']."' AND track='".$deal['track']."' AND date>='".$this->m_startdate."' AND date<='".$this->m_enddate."';";
		$sales = Database::QueryGetResults($query);
		if (!empty($sales))
		{
			$receipts_total = $this->DoSales($worksheet,$sales,$dealterms,0,1,"Digital Track Sales","1");
			$balance += $receipts_total;
		}
		$worksheet->AddHeaderRow("","SUB TOTAL","","","","","",Currency::DISPLAY($balance));

		return $balance;
	}
	
	
	function GetCarryover(&$deal)
	{
		// is this the master one of the dealset?
		$laststatement = Database::QueryGetResult("SELECT reserve FROM artiststatements WHERE crossdealid='".$deal['crossdealid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC LIMIT 1;");
//		print_r($laststatement);
		if (!empty($laststatement))
		{
			return $laststatement['reserve'];
		}
		else
		{
			$postid = 'carryover'.$deal['crossdealid'];
			if (isset($_POST[$postid]))
			{
				return Currency::DISPLAYTODATABASE($_POST[$postid]);
			}
		}
		return false;
	}

	
	// CALL THIS FUNCTION TO GENERATE A NEW STATEMENT
	function Generate($detaillevel,					// high detail or low detail report
					  $licenceid,					// UID of the artist this statement is for
					  $startdate,$enddate			// start and end dates in SQL format
					  )
	{
		// Create a new worksheet
		$worksheet = new WorkSheet;

		// Store the parameters in this object
		$this->m_artistid = $licenceid;
		$this->m_artistname = TableInfo::DisplayFromType("partner",$licenceid);
		$this->m_startdate = $startdate;
		$this->m_enddate = $enddate;
		
		if ($detaillevel==0)
			$this->m_includecomps = false;
		else	
			$this->m_includecomps = true;

	//	if ($detaillevel==2)
			$this->m_collate = false;
	//	else
	//		$this->m_collate = true;
			
		// Title and period
		$worksheet->AddTitleRow("LICENCING STATEMENT");
		$worksheet->AddTitleRow($this->m_artistname);
		$worksheet->AddHeaderRow("PERIOD",displaydate2($startdate)." TO ".displaydate2($enddate));
		$balance = 0;
		$carryover = 0;
		$deals = Database::QueryGetResults("SELECT * FROM deals WHERE licensee='".$licenceid."';");
		foreach ($deals as $deal)
		{
			$carryover = $this->GetCarryOver($deal);
			$balance += $this->ProcessDeal($worksheet,$deal,$carryover);
		}
		
		
		//--------------------------------------------------------------
		// TOTALS
		//--------------------------------------------------------------
		$worksheet->AddTitleRow("TOTALS");
		$worksheet->AddHeaderRow("","TOTAL ROYALTIES","","","","","",Currency::DISPLAY($balance));

		// Check if the balance is set for this deal - if not display it at the top and set the value to 0
		// If set then save this amount
		if ($carryover!==false)
		{
			$balance += Currency::GETRAWVALUE($carryover);
			$worksheet->AddHeaderRow("","PREVIOUS BALANCE","","","","","",Currency::DATABASETODISPLAY($carryover));
		}
		else
		{
			$worksheet->AddHeaderRow("","PREVIOUS BALANCE","","","","","",$this->CreateBalanceHTML($deal['crossdealid']));
			$carryovercalc = 0;
		}
		
		foreach ($deals as $deal)
		{
			if ($deal['advancepaiddate']>=$this->m_startdate)
			{
				$worksheet->AddHeaderRow("","ADVANCE PAID (".$deal['product'].")","","","","","",Currency::DATABASETODISPLAY($deal['advance']));
				$balance -= Currency::GETRAWVALUE($deal['advance']);
			}
		}
		
		$reserve = 0;
		if ($balance>0)
		{
			$reserve = floor($balance * ($deal['reserve'] / 100));
			$worksheet->AddHeaderRow("",$deal['reserve']."% RESERVE","","","","","",Currency::DISPLAY($reserve));
			$final = floor($balance-$reserve);
		}
		else
		{
			$reserve = floor($balance);
			$worksheet->AddHeaderRow("","BALANCE TO CARRYOVER","","","","","",Currency::DISPLAY($reserve));
			$final = 0;
		}
			
		if ($final<0) $final=0;
		
		if ($final<Currency::GETRAWVALUE($deal['minpayout']))
		{
			$newcarryover = $final;
			$final = 0;
			$worksheet->AddHeaderRow("","AMOUNT DUE (BELOW ".Currency::DISPLAY($deal['minpayout']).")","","","","","",Currency::DISPLAY($final));
		}
		else
		{
			$worksheet->AddHeaderRow("","AMOUNT DUE","","","","","",Currency::DISPLAY($final));
		}
			
			
		$this->m_totals[] = Array('id'=>$deals[0]['crossdealid'],'carryover'=>$_SESSION['currencydenomination'].$reserve,'amountdue'=>$_SESSION['currencydenomination'].$final);

		// Store this worksheet		
		$this->m_worksheet = $worksheet;
//		print_r($this->m_totals);
	}
	
	function GetTotals()
	{
		if (!empty($this->m_totals))
			return $this->m_totals;
	}
	
	//--- Accesssor functions
	function GetStartDate()
	{
		return $this->m_startdate;
	}
	
	function GetEndDate()
	{
		return $this->m_enddate;
	}
	
	function GetAmountDue()
	{
		return $this->m_amountdue;
	}
	
	function GetReserve()
	{
		return $this->m_reserve;
	}
	
	function IsComplete()
	{
		return $this->m_complete;
	}
	
	function SaveAsCSV($filename)
	{
		$this->m_worksheet->SaveAsCSV($filename);
	}

	function SaveAsPDF($filename)
	{
//		print_r($this->m_worksheet);
		$this->m_worksheet->SaveAsPDF($filename);
	}

}

?>