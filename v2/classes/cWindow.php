<?php

class Window {

	var $m_width;
	var $m_height;
	var $m_contents;

	function Window($width,$height)
	{
		$this->m_width = $width;
		$this->m_height = $height;
		$this->m_contents = "";
	}

	function AddLine($content)
	{
		$this->m_contents .= $content."<br>";
	}

	function Present()
	{
//		var_dump($this);
//
		print '<div  style="background-color: #758e98; overflow:auto; height:'.$this->m_height.'px; width:'.$this->m_width.'px;">';
		print $this->m_contents;
		print '</div>';
	}
}


require_once('cMail.php');

class InfoWindow extends Window {
	
	var $m_items;
	
	function Populate()
	{
		$mails = MailManager::GetMails($_SESSION['userid']);
		foreach ($mails as $mail)
		{
			$this->AddItem($mail['sentdate'],"Message: ".$mail['subject'],"READMAIL:".$mail['uid']);
		}
	}
	
	function AddItem($date,$subject,$action)
	{
		$this->m_items[] = Array('date'=>$date,'subject'=>$subject,'action'=>$action);
	}

	function Present()
	{
		$this->Populate();
		$sorteditems = SortResultsByCol($this->m_items,'date');
		foreach ($sorteditems as $item)
		{
			$this->m_contents .= "<p>".displaydate($item['date'])." : ".$item['subject']."</p>\n";
		}
		Window::Present();
	}
	
}
?>