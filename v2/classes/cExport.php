<?php
require_once('cDatabase.php');
require_once('util.php');
require_once('cEditField.php');
require_once('cForm.php');
require_once('cWorkSheet.php');
require_once('cTableInfo.php');

class Export {
	var $m_name;
	var $m_table;
	var $m_fieldmap;
	var $m_currentworksheet;
	var $m_columns;
	

	function	_isFieldUsed($field)
	{
		return ($this->m_fieldmap[$field]!="U");
	}
	
	function	GetUsingColumns($field)
	{
		$cols = $this->m_fieldmap[$field];
		if ($cols == "F" || $cols=="U") return false;
		return explode(",",$cols);
	}

	function Export()
	{
		$this->m_table = "sales";
	}

	function SetScript($script)
	{
		$this->m_name = $script;
	}	
	
	function 	FindFile($filename)
	{
		$directory = ".//importscripts//public//";
		$path = $directory.$filename;
		if (!file_exists($path))
		{
			$directory = "accounts//".$_SESSION['accountid']."//scripts//";
			$path = $directory.$filename;
		}	
		return $path;
	}

	function	Remove($filename)
	{
		$path = Import::FindFile($filename);
		if (file_exists($path))
		{
			unlink($path);
		}
	}
	
	function	Load($filename)
	{
		$path = $this->FindFile($filename);
		if ($path!="")
		{
			$vars = unserialize(file_get_contents($path));
			foreach($vars as $key=>$val)
			{           
				eval("$"."this->$key = $"."vars['"."$key'];");
			}
		}
		
	}
	
	function 	Save($filename)
	{
		$filename = ".\\accounts\\".$_SESSION['accountid']."\\scripts\\".$filename.".exp";
		$file = @fopen($filename,"w");
		if($file)
        {
            if(@fwrite($file,serialize(get_object_vars($this))))
            {
                @fclose($file);
				chmod($filename,755);
            }
            else die("Could not write to file ".$filename);
        }
        else die("Could not open file ".$filename);
	}

	


	function	GetScripts($directory)
	{
		$i = 0;
		// Scan directory - first public ones
		$directory = "accountdata//".$directory."//scripts";
		if (is_dir($directory))
		{
			if ($dh = opendir($directory)) 
			{
				while (($file = readdir($dh)) !== false) 
				{
					if (strpos($file,".exp")!=false)
					{
						$scripts[$i]['display'] = substr($file,0,strpos($file,".exp")); 
						$scripts[$i++]['path'] = $file;
					}
						
				}
				closedir($dh);
			}
		}
		if (!empty($scripts))
			return $scripts;
	}

	function	LoadFromPost()
	{
		$this->m_table = $_POST['table'];
		$this->m_columns = $_POST['columns'];		
		$this->m_name = $_POST['exportid'];
		
		unset($this->m_fieldmap);
		$tableinfo = TableInfo::GetTable($this->m_table);

		foreach ($tableinfo as $field => $info)
		{
			$this->m_fieldmap[$field] = $_POST[$field."_column"];
		}
	}
	


	function GetFixedValues()
	{
		$tableinfo = TableInfo::GetTable($this->m_table);
		foreach ($tableinfo as $field => $info)
		{
			if ($this->m_fieldmap[$field]=="F")
			{
				if ($info['type']=="date") 
				{
					$fixed[$field] = getpostdate2($field);
				}
				else
				{
					$fixed[$field] = $_POST[$field];
				}
			}
		}
		if (!empty($fixed)) return $fixed;
	}
	
	function ProcessFile($file)
	{
		$file = fopen($file,"w+");
		fwrite ($file,"Music.counts ".$this->m_table." export.\n\n");
		// Output columns
		$result = Database::Query("SELECT * FROM ".$this->m_table.";");
		$numresult = $result->GetNum();
		for ($i=0;$i<$numresult;++$i)
		{
			$row = $result->GetNext();
			foreach ($this->m_fieldmap as $field=>$value)
			{
				$header[$value] = $field;
				if ($value!="U")
					$outrow[$value] = TableInfo::Display($this->m_table,$field,$row[$field]);
			}

			$lineout = "";
			if ($i==0)
			{
				// First time - so output column heads
				for ($j=0;$j<10;++$j)
				{
					if ($j>0) $lineout .= ",";
					if (!empty($header[$j])) $lineout .= TableInfo::GetDescription($this->m_table,$header[$j]);
				}
				$lineout.="\n";
				fwrite($file,$lineout);
			}
			
			$lineout = "";
			$maxcol = count($outrow);
			for ($j=0;$j<10;++$j)
			{
				if ($j>0) $lineout .= ",";
				if (!empty($outrow[$j])) $lineout .= $outrow[$j];
			}
			$lineout.="\n";
			fwrite($file,$lineout);
		}	
		fclose($file);
	}

	function	SetTable($table)
	{
		$this->m_table = $table;
	}
	
	function	PresentFixedForm(&$form)
	{
		$tableinfo = TableInfo::GetTable($this->m_table);
		foreach ($tableinfo as $field => $info)
		{
			if ($this->m_fieldmap[$field]=="F")
			{
				$form->AddField($info['description'],$field,$info['type'],"");
			}
		}
	}
	
	function	PresentEditForm(&$form)
	{
//		$form = new Form;
		$form->AddField("Export Script Name","exportid","text",$this->m_name);
		$form->AddHidden("table",$this->m_table);
		$tableinfo = TableInfo::GetTable($this->m_table);				
		if (empty($this->m_columns))
		{
			$columnslist = "";
			for ($i=0;$i<10;++$i)
			{
				$columnslist = $columnslist."$i=Column ".chr(ord('A')+($i)).",";
			}
			$columnslist = $columnslist."U=Unused";
		}
		else
		{
			$columnslist = $this->m_columns;
		}

		$form->AddHidden("columns",$this->m_columns);
				
		print "<tr><td><b>Data Type</b></td><td><b>Column Assignment</b></td><td></td></tr>";
		$i=0;
		foreach ($tableinfo as $field => $info)
		{
			$col = "U";
			if (isset($this->m_fieldmap))
			{
				$col = $this->m_fieldmap[$field];
			}
			else
			{
				$col = $i;
			}
			++$i;
			$description = $info['description'];
			
			$form->AddSelectionList($description,$field."_column",$columnslist,$col);
		}
		print "<tr><td><font size=1><i>* mandatory fields</i></font></td></tr>";
		// Get the fields 
		$form->AddHidden("exportscript",$this->m_name);
	}

	function GetScriptName()
	{
		return $this->m_name;
	}
	
	function	Finalise()
	{
	}


};


?>