
<?php
// Class Table

include "util.php";
include "cEditField.php";

function DisplayEdit($table,$columns,$firstrow,$lastrow,$filter)
{
	echo "<table>";
	$linkID = get_database();
	$query = "SELECT $columns FROM $table";
	if ($filter!="")
	{
		$query = $query . " WHERE $filter";
	}
	print $query."<br>"; 
	$result = mysql_query($query,$linkID);
	
	if ($result==false)
	{
		print "No such table";
		return;
	}
	
	// Table header
	echo "<tr><td>#</td>";
	$cols = preg_split("/,/",$columns);
	foreach ($cols as $val)
	{
		echo "<td>".$val."</td>";
	}
	echo "</tr>";
	mysql_data_seek ( $result, $firstrow );
	if ($lastrow>mysql_num_rows($result))
		$lastrow = mysql_num_rows($result) - 1;
	for ($i=$firstrow;$i<=$lastrow;++$i)
	{
		echo "<tr><td>$i</td>";
		
		$row = mysql_fetch_row($result);
		$j=0;
		foreach($cols as $val)
		{
			echo "<td>";
			EditField($val,$val,$row[$j]);
			echo "</td>";
			++$j;
		}
		echo "</tr>";
	}
	
	echo "</tr>";
	echo "</table>";

	mysql_free_result($result);
}


function DisplayAdd($table,$columns,$firstrow,$lastrow,$filter)
{
	echo "<table>";

	// Table header
	echo "<tr><td>#</td>";
	$cols = preg_split("/,/",$columns);
	foreach ($cols as $val)
	{
		echo "<td>".$val."</td>";
	}
	echo "</tr>";

	for ($i=$firstrow;$i<=$lastrow;++$i)
	{
		echo "<tr><td>$i</td>";
		
		$j=0;
		foreach($cols as $val)
		{
			$name = $val.$i;
			echo "<td>";
			EditField($name,$val,"");
			echo "\n</td>";
			++$j;
		}
		echo "</tr>";
	}
	
	echo "</tr>";
	echo "</table>";
}

?>