<?php
require_once('util.php');
require_once('cCurrency.php');

$_tableinfo = array(
		"promos" => array(
					"product"=>		array("description"=>"Product","type"=>"product","optional"=>false,"reference"=>true,"helptext"=>""),
					"blurb"=>	array("description"=>"Promo Text","type"=>"multitext","optional"=>false,"reference"=>false,"helptext"=>""),
					"startdate"=>	array("description"=>"Start Date","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>""),
					"expiredate"=>	array("description"=>"Expire Date","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>"")
				),
		"recipients" => array(
					"name"=>		array("description"=>"Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"email"=>		array("description"=>"Email Address","type"=>"text64","optional"=>false,"reference"=>false,"helptext"=>""),
					"affiliations"=>array("description"=>"Affiliations","type"=>"text64","optional"=>true,"reference"=>false,"helptext"=>"Name of any labels/companies the recipient is affiliated with.")
				),
		"deals" => array(
					"dealtype"=>	array("description"=>"Deal Type","type"=>"dealtype","optional"=>false,"reference"=>false,"helptext"=>"Choose between a profit share (or net receipts split) and a royalty rate based deal. In profit share the artist is paid a share of products income after expenses have been deducted. In royalty deals the artist is paid on net sales regardless of expenses."),
					"band"=>		array("description"=>"Band","type"=>"band","optional"=>false,"reference"=>true,"helptext"=>"This is the band that this deal is for"),
					"product"=>		array("description"=>"Product","type"=>"product","optional"=>false,"reference"=>true,"helptext"=>"The product that the deal is for. If this is a compilation then the net receipts will be divided based on how many tracks there are on the product and how many of those tracks are the bands."),
					"track"=>		array("description"=>"Track","type"=>"track","optional"=>true,"reference"=>true,"helptext"=>"The track that the deal is for. If you leave this blank it will include all tracks on that product that belong to the artist."),
					"licensee"=>	array("description"=>"Licensee","type"=>"licensee","optional"=>true,"reference"=>true,"helptext"=>"The licensee that you will be accounting to. This is for licensing only."),
					"crossdealid"=> array("description"=>"Cross Calculate With","type"=>"crossdealid","optional"=>true,"reference"=>false,"helptext"=>"You can select which other deals to cross calculate this one against. This means thats all receipts/expenses in statement calculations will be pooled together so profits for one deal can be offset against losses in another. All these deals share one single rate and reserve percentage."),
//					"rate"=>		array("description"=>"Split/Royalty Rate","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>"Enter a percentage that reflects the split or royalty rate the artist will receive."),
					"reserve"=>		array("description"=>"Reserve","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>"Enter a percentage. This is how much you the label will keep back of the artists receipts for each statement to be carried over to the next."),
					"packagingdeductions"=>array("description"=>"Packaging Deductions","type"=>"percentage","optional"=>true,"reference"=>false,"helptext"=>"Enter a percentage. For royalty deals this is deducted from the receipts."),
					"startdate"=>	array("description"=>"Start Date","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"Date this deal starts from - typically this will be the release date (leave blank). If this is a renewal then set this to proceed the original end date."),
					"enddate"=>		array("description"=>"End Date","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"Date the deal will end. For example if you to five year deals then this will be the start date plus five years."),
					"advance"=>		array("description"=>"Advance","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>"Amount that the band has been advanced for this deal."),
					"advancepaiddate"=>array("description"=>"Advance Paid Date","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"Date the advance was paid, enter this so that advance will be included in the correct statement."),
					"minpayout"=>	array("description"=>"Minimum Balance Payout","type"=>"currency","optional"=>true,"reference"=>false,"helptext"=>"This is the minimum amount that will be paid out for any statement period, if the balance is less than this figure then it will be carried over.")
				),
				//dealid,key,rate,base,descrip
		"dealterms" => array(
					"dealid"=>  array("description"=>"Deal ID","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"keyfield"=>array("description"=>"Key Field","type"=>"keyfield","optional"=>false,"reference"=>false,"helptext"=>""),
					"key"=>		array("description"=>"Sale Key","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"rate"=>		array("description"=>"Royalty Rate","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>""),
					"base"=>	array("description"=>"Unit Base","type"=>"basetype","optional"=>false,"reference"=>false,"helptext"=>""),
					"descrip"=>	array("description"=>"Licensee","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"")
					),				
		"accounts" => array(
					"name"=>		array("description"=>"Company Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"Enter the name of your company/record label here"),
					"superuser"=>	array("description"=>"Administrator Userid","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"invoiceaddress"=>	array("description"=>"Invoice Address","type"=>"hidden","optional"=>false,"reference"=>true,"helptext"=>""),
					"vatrate"=>		array("description"=>"VAT Rate","type"=>"percentage","optional"=>true,"reference"=>false,"helptext"=>"If you are VAT registered enter the vat rate you are liable for, otherwise leave this blank or 0"),
					"currencydenomination"=>	array("description"=>"Default Currency","type"=>"currencydenomination","optional"=>false,"reference"=>false,"helptext"=>"Choose which your default currency is GBP,EUR or USD - if you omit to specify the denomination when entering currency it will use this one."),
					"defaultdealtype" => array("description"=>"Standard Deal Type","type"=>"dealtype","optional"=>false,"reference"=>false,"helptext"=>""),
					"defaultdealrate" => array("description"=>"Standard Deal Rate/Split","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>""),
					"defaultdealreserve" => array("description"=>"Standard Deal Reserve","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>""),
					"defaultpackagingdeductions" => array("description"=>"Standard Packaging Deduction","type"=>"percentage","optional"=>true,"reference"=>false,"helptext"=>""),
					"defaultdealterm" => array("description"=>"Standard Deal Term","type"=>"months","optional"=>false,"reference"=>false,"helptext"=>""),
					"defaultminpayout" => array("description"=>"Standard Minimum Payout","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>""),
					"startdate" =>	array("description"=>"First Invoice Date","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>""),
					"status" => 	array("description"=>"Status","type"=>"accountstatus","optional"=>true,"reference"=>false,"helptext"=>"")
				),
		"users" => array(
					"displayname"=>	array("description"=>"Full Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"This should be users full display name e.g. Bob Smith"),
					"userid"=>		array("description"=>"User Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"Enter the login id of the new user - for example bsmith"),
					"password"=>	array("description"=>"Password","type"=>"password","optional"=>false,"reference"=>false,"helptext"=>"Enter a password for the new user."),
					"email"=>		array("description"=>"Email Address","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"Enter the new users email address.")
				),
		"sale"=> array(
					"product"=>		array("description"=>"BarCode / Cat No.","type"=>"product","optional"=>false,"reference"=>true,"helptext"=>"Enter the product that this sale will be logged to when calcualting statements"),
					"track"=>		array("description"=>"Track ISRC","type"=>"track","optional"=>true,"reference"=>true,"helptext"=>"If the sale is a single track download sale then enter the track from the product that was sold."),
					"date"=>		array("description"=>"Date of Sale","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>"Enter the date of the sale. This will decide which statement period to assign this sale to."),
					"distributor"=>	array("description"=>"Distributor","type"=>"distributorshop","optional"=>false,"reference"=>true,"helptext"=>"The distributor/shop/download site that made this sale."),
					"quantity"=>	array("description"=>"Quantity","type"=>"integer","optional"=>false,"reference"=>false,"helptext"=>"The number of units sold in this sale log."),
					"value"=>		array("description"=>"Value of Sale","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>"The montary value of this sale. Can be in USD, GBP or EUR's."),
					"baseprice"=>	array("description"=>"Base Price","type"=>"currency","optional"=>true,"reference"=>false,"helptext"=>"For royalty calcuations - the base price."),
					"saletype"=>	array("description"=>"Sale Type","type"=>"saletype","optional"=>false,"reference"=>false,"helptext"=>"This marks if the sale is of a physical product, a download or a stream."),
					"territory"=>	array("description"=>"Territory","type"=>"text16","optional"=>true,"reference"=>false,"helptext"=>"Country sold in"),
					"shop"=>		array("description"=>"Shop","type"=>"text16","optional"=>true,"reference"=>false,"helptext"=>"If data from aggregator/distributor then this field will hold the shop"),
					"mechanicals"=>	array("description"=>"Mechanicals","type"=>"currency","optional"=>true,"reference"=>false,"helptext"=>"Mechanicals paid by distributor"),
					"distfee"=>		array("description"=>"Distribution Fee","type"=>"percentage","optional"=>true,"reference"=>false,"helptext"=>"Percentage distributor took"),
					"termkey"=>		array("description"=>"Sale Key","type"=>"text16","optional"=>true,"reference"=>false,"helptext"=>"This is a key that it matched to the deal term keys in order to choose which type of calculation to use on this sale when generating statements. This is primarily used for generating licencing statements for other labels. If in doubt leave blank for default terms."),
					"batchid"=>		array("description"=>"Batch ID","type"=>"text16","optional"=>true,"reference"=>false,"helptext"=>"This is an identifier for batch imports so that you can identify which statement they come from if you wish to ammend them later to correct errors.")
				),
		"expenses"=> array(
					"invoice"=> 	array("description"=>"Expense/Invoice Number","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"A reference number to help you keep track of this expense."),
					"product"=> 	array("description"=>"Product Assigned To","type"=>"productartist","optional"=>false,"reference"=>true,"helptext"=>"Set the product that this expense will be assigned to. When the system calculates the artist statement it will use this to deduct expense from the products sales in order to calculate the correct profit."),
					"invoicedate"=> array("description"=>"Date of Invoice","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>"This is the date on the invoice. This date will be used to decide which statement to include this expense in."),
					"amount"=> 		array("description"=>"Value of Expenses","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>"The amount of the expense. This can be in GBP, EUR or USD denominations."),
					"paiddate"=> 	array("description"=>"Date Invoice Paid","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"The date you paid the invoice. If you've not paid the invoice keep this blank - then you can check to see if you have any outstanding payments."),
					"type"=> 		array("description"=>"Category","type"=>"expensetype","optional"=>false,"reference"=>false,"helptext"=>"Select a category to file this expense under."),
					"description"=> array("description"=>"Description","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"A description of what this expense was for."),
					"comments"=> 	array("description"=>"Additional Comments","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"Anything else you might like to add.")
				),
		"tracks"=> array(
					"uid"=> 		array("description"=>"ISRC","type"=>"irsc","optional"=>false,"reference"=>false,"helptext"=>"Enter the ISRC code that identifies this track."),
					"artist"=> 		array("description"=>"Band","type"=>"band","optional"=>false,"reference"=>true,"helptext"=>"Select the band that made the track. If it doesn't appear go to the BANDS tab and add the band there first."),
					"title"=> 		array("description"=>"Title","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"The title of this track."),
					"version"=> 	array("description"=>"Version","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"The version of the track."),
					"remixer"=> 	array("description"=>"Remixer","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"The remixer of this track."),
					"publisher"=> 	array("description"=>"Publisher","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"The publisher of this track. Currently this is not used by the system and so optional but maybe added as a feature in a later version."),
					"tracklength"=> array("description"=>"Track Length","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"The length of the track in minutes:seconds"),
					"comments"=> 	array("description"=>"Comments","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"Any additional comments you may wish to enter.")
				),
		"product"=> array(
					"uid"=> 		array("description"=>"Catologue Number","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"This is your own catalogue number that you've assigned your product. We recommend you create a new catalogue number for each skew of the product."),
					"title"=> 		array("description"=>"Title","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"The title of the product"),
					"subtitle1"=>	array("description"=>"Disc 1 Subtitle","type"=>"text48","optional"=>true,"reference"=>false,"helptext"=>"Sub Title for Disc 1. Can leave this blank for single disc packs"),
					"subtitle2"=>	array("description"=>"Disc 2 Subtitle","type"=>"text48","optional"=>true,"reference"=>false,"helptext"=>"Sub Title for Disc 2. Can leave this blank for single disc packs"),
					"subtitle3"=>	array("description"=>"Disc 3 Subtitle","type"=>"text48","optional"=>true,"reference"=>false,"helptext"=>"Sub Title for Disc 3. Can leave this blank for single disc packs"),
					"subtitle4"=>	array("description"=>"Disc 4 Subtitle","type"=>"text48","optional"=>true,"reference"=>false,"helptext"=>"Sub Title for Disc 4. Can leave this blank for single disc packs"),
					"format"=> 		array("description"=>"Format","type"=>"format","optional"=>true,"reference"=>false,"helptext"=>"Enter the format/skew of this product."),
					"artist"=> 		array("description"=>"Band","type"=>"band","optional"=>true,"reference"=>true,"helptext"=>"Select the band who made this product. If it is a compilation or contains tracks by more than one band then select VARIOUS ARTISTS here."),
					"tracks"=> 		array("description"=>"Tracks","type"=>"tracks","optional"=>true,"reference"=>true,"helptext"=>"A product is a collection of tracks, use the plus and minus signs to add and remove tracks"),
					"barcode"=> 	array("description"=>"UPC/EAN Number","type"=>"barcode","optional"=>false,"reference"=>false,"helptext"=>"Enter the barcode or UPC/EAN number for this product. Most sales logs will use this to identify the product."),
					"releasedate"=> array("description"=>"Release Date","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"The release date of the track. This effects the ordering of the views and also the system will check that sales are not logged for products before they are released."),
					"comments"=> 	array("description"=>"Comments","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"Any additional comments you wish to enter")
				),
		"licences"=> array(
					"description"=> 		array("description"=>"Comp/Description","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"A descriptive name for this licence. If this is a licence of a track for a compilation we recommend you enter the name of the Compilation here, otherwise use a short description that makes sense to you."),
					"licencetype"=> 		array("description"=>"Type of Licence","type"=>"licencetype","optional"=>false,"reference"=>false,"helptext"=>"The licence can either be for an individual track, a product or an artist. Changing this will reset the form."),
					"licenceduid"=> 		array("description"=>"Licenced Item","type"=>"licence","optional"=>false,"reference"=>true,"helptext"=>"The track/product/artist that you are licencing."),
					"licensor"=> 			array("description"=>"Licensor","type"=>"licensor","optional"=>false,"reference"=>true,"helptext"=>"The licensor you have licenced to. If you can't find it in this list, please go to the PARTNERS tab and add them as a licensor to your list."),
					"advance"=> 			array("description"=>"Advance","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>"The value of the advance the licence has offered."),
					"royaltyrate"=> 		array("description"=>"Royalty Rate","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>"The percentage of the royalties granted. This is for reference only and not used in any calculations."),
					"packagingdeductions"=> array("description"=>"Packaging Deductions","type"=>"percentage","optional"=>true,"reference"=>false,"helptext"=>"The percentage allowed in the agreement for packaging deductions. This is for reference only and not used in any calculations."),
					"reserveforreturns"=> 	array("description"=>"Return Reserve","type"=>"percentage","optional"=>true,"reference"=>false,"helptext"=>"The percentage agreed for the reserve. This is for reference only and not used in any calculations."),
					"datepaid"=> 			array("description"=>"Date Statement Paid","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"??"),
					"datestatement"=>		array("description"=>"Date of First Statement","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"Enter the date that you expecting the first royalty statement from the licensor. The system will remind you when this is due."),
					"laststatementdate"=>	array("description"=>"Date of Last Statement","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"Enter the date of the last statement you will receive. Leave this blank until you no longer wish to be reminded about royalty statements."),
					"royaltyinterval"=>		array("description"=>"Interval between Statements","type"=>"months","optional"=>true,"reference"=>false,"helptext"=>"The interval between royalty statements in months."),
					"advancepaiddate"=>		array("description"=>"Date Advance Paid","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>"Enter the date the advance was paid, leave blank otherwise."),
					"comments"=> 			array("description"=>"Comments","type"=>"multitext","optional"=>true,"reference"=>false,"helptext"=>"Any additional comments you would like to enter.")
				),
		"stock"=> array(
					"product"=> 	array("description"=>"Product","type"=>"product","optional"=>false,"reference"=>true,"helptext"=>"Enter the product who's stock was moved."),
					"date"=> 		array("description"=>"Date of Movement","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>"Enter the date the stock was moved"),
//					"netamount"=> 	array("description"=>"Value of Transaction","type"=>"currency","optional"=>true,"reference"=>false,"helptext"=>""),
					"quantity"=> 	array("description"=>"Number Units","type"=>"integer","optional"=>false,"reference"=>false,"helptext"=>"Enter the number of units that was moved"),
					"source"=> 		array("description"=>"Stock Source","type"=>"supplychain","optional"=>true,"reference"=>true,"helptext"=>"The manufacturer/distributor/shop from which the stock was moved from"),
					"destination"=> array("description"=>"Stock Destination","type"=>"supplychain","optional"=>false,"reference"=>true,"helptext"=>"The manufacturer/distributor/shop from which the stock was moved to"),
					"description"=> array("description"=>"Description","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"Enter a description to help you remember the reason for this movement.")
				),
		"artist"=> array(
					"realname"=> 	array("description"=>"Real Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"The name of the artist. Not a psuedo name."),
					"address1"=> 	array("description"=>"Address","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"address2"=> 	array("description"=>"Address","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"city"=> 		array("description"=>"City","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"postcode"=>	array("description"=>"Postcode","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"country"=> 	array("description"=>"Country","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"email"=> 		array("description"=>"Email Address","type"=>"emailaddress","optional"=>true,"reference"=>false,"helptext"=>"If you enter this we can send emails directly to them from the system."),
					"phone"=> 		array("description"=>"Phone Number","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>"")
				),
		"bands"=> array(
					"name"=> 	array("description"=>"Band Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"The name of the band.")
				),
		"bandartist" => array(
					"band"=> 	array("description"=>"Band","type"=>"band","optional"=>false,"reference"=>true,"helptext"=>""),
					"artist"=> 	array("description"=>"Artist","type"=>"artist","optional"=>false,"reference"=>true,"helptext"=>""),
					"split"=> 	array("description"=>"Split","type"=>"percentage","optional"=>false,"reference"=>false,"helptext"=>"Enter the split for each member of the band if not equal share")
				),
		"tracksproduct" => array(
					"track"=> 	array("description"=>"Track","type"=>"track","optional"=>false,"reference"=>true,"helptext"=>""),
					"product"=> 	array("description"=>"Product","type"=>"product","optional"=>false,"reference"=>true,"helptext"=>""),
					"position"=> 	array("description"=>"Track Position","type"=>"integer","optional"=>false,"reference"=>false,"helptext"=>"")
				),
		"partners"=> array(
					"name"=> 		array("description"=>"Name","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>"The name of the company you are adding to the system."),
					"address1"=> 	array("description"=>"Address","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"address2"=> 	array("description"=>"Address","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"city"=> 		array("description"=>"City","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"postcode"=>	array("description"=>"Postcode","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"country"=> 	array("description"=>"Country","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"phone"=> 		array("description"=>"Phone Number","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"email"=> 		array("description"=>"Email Address","type"=>"emailaddress","optional"=>true,"reference"=>false,"helptext"=>"If you enter this information we can send invoices/emails directly to them via the system."),
					"type"=> 		array("description"=>"Business Type","type"=>"supplychaintype","optional"=>false,"reference"=>false,"helptext"=>"Enter the type of business the partner is. If they have multiple interests we suggest you enter them several times with appropiate descriptions into the system.")
				),
		"licencestatements"=> array(
					"licenceuid"=> 		array("description"=>"Licence","type"=>"text","optional"=>true,"reference"=>true,"helptext"=>""),
					"statementdate"=> 	array("description"=>"Date of Statement","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>""),
					"paiddate"=> 		array("description"=>"Date Paid","type"=>"date","optional"=>true,"reference"=>false,"helptext"=>""),
					"units"=> 			array("description"=>"Units Sold","type"=>"integer","optional"=>true,"reference"=>false,"helptext"=>""),
					"balance"=> 		array("description"=>"Balance","type"=>"currency","optional"=>true,"reference"=>false,"helptext"=>""),
					"amount"=> 			array("description"=>"Amount Payable","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>"")
				),
		"artiststatements"=> array(
					"type"=>		array("description"=>"Type","type"=>"statementtype","optional"=>false,"reference"=>true,"helptext"=>""),
					"artist"=> 	array("description"=>"Artist","type"=>"artist","optional"=>false,"reference"=>true,"helptext"=>""),
					"licensee"=> 	array("description"=>"Licensee","type"=>"licensee","optional"=>false,"reference"=>true,"helptext"=>""),
					"crossdealid"=>	array("description"=>"Cross Deal","type"=>"integer","optional"=>false,"reference"=>false,"helptext"=>""),
					"startdate"=>	array("description"=>"Period Start Date","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>""),
					"enddate"=> 	array("description"=>"Period End Date","type"=>"date","optional"=>false,"reference"=>false,"helptext"=>""),
					"amount"=> 		array("description"=>"Amount","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>""),
					"reserve"=> 	array("description"=>"Reserve","type"=>"currency","optional"=>false,"reference"=>false,"helptext"=>"")
				),
		"addresses"=> array(
					"address1"=> array("description"=>"House/Street","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"address2"=> array("description"=>"Street/Area","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"city"=> 	 array("description"=>"City/County","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"country"=>  array("description"=>"Country","type"=>"text","optional"=>false,"reference"=>false,"helptext"=>""),
					"postcode"=> array("description"=>"Postcode","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
					"email"=> 	 array("description"=>"Email Address","type"=>"emailaddress","optional"=>true,"reference"=>false,"helptext"=>""),
					"phone"=>    array("description"=>"Phone Number","type"=>"text","optional"=>true,"reference"=>false,"helptext"=>""),
				)
		);
	
$validationerror = "";

class TableInfo {

	function	GetValidationError()
	{
		global	$validationerror;
		return $validationerror;
	}
	
	function	_setvalidationerror($string)
	{
		global	$validationerror;
		$validationerror = $string;
	}

	function	_errorDuplicate()
	{
		TableInfo::_setvalidationerror("Duplicate entry exists");
		return false;
	}
	
	function	_errorNotFound($text)
	{
		TableInfo::_setvalidationerror("Value not found ($text)");
		return false;
	}
	
	function	_errorGarbage()
	{
		TableInfo::_setvalidationerror("Bad Value");
		return false;
	}
	
	function	_errorEmpty()
	{
		TableInfo::_setvalidationerror("Empty Field");
		return false;
	}

	function GetTable($table)
	{
		global $_tableinfo;
		return $_tableinfo[$table];
	}
	
	function IsOptional($table,$field)
	{
		global $_tableinfo;
		return $_tableinfo[$table][$field]['optional'];
	}
		
	function GetField($table,$field)
	{
		global $_tableinfo;
		return $_tableinfo[$table][$field];
	}

	function GetDescription($table,$field)
	{
		global $_tableinfo;
		return $_tableinfo[$table][$field]['description'];
	}
	
	function RequireAccountID($table)
	{
		if ($table=="accounts"||
			$table=="addresses") return false;
				
		return true;
	}

	function GetFields($table,$uid,$fieldcsv)
	{
		$result = Database::QueryGetResult("SELECT * FROM $table WHERE uid='".$uid."';");
		$fields = explode(",",$fieldcsv);
		foreach($fields as $field)
		{
			$fieldvalue = TableInfo::Display($table,$field,$result[$field]);
			if ($fieldvalue!="")
				$values[TableInfo::GetDescription($table,$field)] = $fieldvalue;
		}
		if (isset($values))
			return $values;
		else 
			return false;
	}
	
	function EpslionToString($value)
	{
		$eppos = stripos($value,"e");
		if ($eppos===false) return $value;
		
		$ep = substr($value,$eppos+1);
		$preep = substr($value,0,$eppos);
		$postep = str_repeat('0',intval($ep-strlen($preep))+3);
		return str_replace(".","",$preep.$postep);
	}
	
	// Validate - checks the field and converts any text to ids
	// will return false on failure
	
	function Validate($table,$field,$value)
	{
		// Ignore accountids
		if ($field=="accountid" || $field=="uid")
		{
			return $value;
		}

		$fieldinfo = TableInfo::GetField($table,$field);
		$reference = $fieldinfo['reference'];
		if ($value=="")
		{
			if ($fieldinfo['optional']==false)
				return TableInfo::_errorEmpty();
			else
			{
				if ($fieldinfo['type']=="date") $value="0000-00-00";
				return $value;
			}
		}
		
		
		switch($fieldinfo['type'])
		{
			case "date":
			{
				$time = strtotime($value);
				if ($time==false)
				{
					// try swapping dates to uk format
					if (preg_match('/^\s*(\d\d?)[^\w](\d\d?)[^\w](\d{1,4}\s*$)/', $value, $match))  
  							$value2 = $match[2] . '/' . $match[1] . '/' . $match[3]; 
					
					$time = strtotime($value2);
					
					if ($time==false)
						return TableInfo::_errorNotFound($value);
				}
				// dates may come in many formats
				$value = date("Y-m-d",$time);
				break;
			}
			case "saletype":
			{
				if ($value=='S' || strncasecmp("match",$value,5)==0 || strncasecmp("stream",$value,6)==0 || strncasecmp("playcounts",$value,10)==0) 
				{
					$value='S';
					break;
				}
				if ($value=='P' || strncasecmp("physical",$value,8)==0) 
				{
					$value='P';
					break;
				}
				$value='V';
				
				break;
			}
			case "licence":
			{
				$found = false;
				$result = Database::FindUID("tracks","uid",$value);
				if ($result==false)
				{
					$result = Database::FindUID("product","uid",$value);
					if ($result!=false)
					{
						$found = true;
					}
					else
					{
						$result = Database::FindUID("bands","uid",$value);
						if ($result!=false)
						{
							$found = true;
						}
					}
				}
				else
				{
					$found = true;
				}
				if ($reference)
				{
					if (!$found)
						return TableInfo::_errorNotFound($value);
				}
				else
				{
					if ($found)
						return TableInfo::_errorDuplicate();
				}
				break;
			}
			case "currency":
			{
			   if ($value=="") $value=0;
//				$value = storemoney($value);
				$value = Currency::DISPLAYTODATABASE($value);
				if ($value=="")
					return TableInfo::_errorGarbage();
				break;
			}
			case "irsc":
			{
				// get rid of any dashes or spaces
				$value = str_replace("-","",$value);
				$value = str_replace(" ","",$value);
				// fall through
			}
			case "track":
			{
					
				//$id = Database::FindUID("tracks","uid",$value);
				$res = Database::QueryGetResult("SELECT uid FROM tracks WHERE uid='$value' OR title='$value' LIMIT 1");
				$id = $res['uid'];
				if ($reference)
				{
					if ($id==false) 
						return TableInfo::_errorNotFound($value);
					$value = $id;
				}
				else
				{
					if ($id!=false)
					{
						return TableInfo::_errorDuplicate();
					}
				}
				break;
			}
			case "productartist":
			case "product":
			{
				$value = preg_replace("[ `\t]",'',$value);
				$value = addslashes($value);
				if (is_numeric($value))
				{
					//print "pre".$value;
					$value = TableInfo::EpslionToString($value);
					//print "=".$value."<br>";
				}
				$query = "SELECT uid FROM product WHERE (uid='$value' OR barcode='$value');";
			//	print $query."<br>";
				$result = Database::QueryGetResult($query);
				$fvalue = $result['uid'];
				if ($reference)
				{
					if ($fvalue==false)
					{
						if ($fieldinfo['type']!="productartist") 
						{
							$value = ereg_replace( '[^0-9]+', '', $value );
							if ($value!="")
								return TableInfo::_errorNotFound($value);
						}
					}
					else
					{
						$value = $fvalue;
						break;
					}
				}
				else
				{
					if ($fvalue!=false)
					{
						return TableInfo::_errorDuplicate();
					}
				}
				if ($fieldinfo['type']!="productartist") break;
			}
			case "artist":
			{
				if ($reference)
				{
					if (is_numeric($value))
					{
						// do nothing
					}
					else
					{
						$value = Database::FindUID("artist","artistname",$value);
						if ($value==false) 
							return TableInfo::_errorNotFound($value);
					}
				}
				break;
			}
			case "band":
			{
				if ($reference)
				{
					if (is_numeric($value))
					{
						// do nothing
					}
					else
					{
						$value = Database::FindUID("bands","name",$value);
						if ($value==false) 
							return TableInfo::_errorNotFound($value);
					}
				}
				break;
			}
			case "title":
			{
				if ($reference)
				{
					$value = Database::FindUID("track","title",$value);
					if ($value==false) 
						return TableInfo::_errorNotFound($value);
				}

				break;
			}
      case "integer":
      {
        if ($value=="")
          $value = 0;
        if (!is_numeric($value))
        {
			if ($value[0]=='(')
			{
				$value = str_replace('(','',$value);
				$value = str_replace(')','',$value);
				if (!is_numeric($value))
				{
		          return TableInfo::_errorGarbage();			
				}
				$value = -$value;
			}
			//print $value.",";
          return TableInfo::_errorGarbage();
        } 
        break;
      }
			case "partner":
			{
				if ($reference)
				{
					if (is_numeric($value)) break;
					
					$value = Database::FindUID("partner","name",$value);
					if ($value==false) 
						return TableInfo::_errorNotFound($value);
				}
				break;
			}
		}
		$value = str_replace("%","pc",$value);
		return $value;
	}
	
	function Display($table,$field,$value)
	{
		$fieldinfo = TableInfo::GetField($table,$field);
		return TableInfo::DisplayFromType($fieldinfo['type'],$value);
	}
	
	function DisplayFromType($type,$value)
	{
		switch ($type)
		{
			case "partner":
			case "partners":
			case "distributorshop":
			{
				$result = Database::Query("SELECT name FROM partners WHERE uid='".$value."';");
				if ($result->GetNum()>0)
				{
					$row=$result->GetNext();
					return $row['name'];
				}
				return "Unknown $value";
				break;
			}
			case "dealtype":
			{
				if ($value=='P') return "Profit Share";
				if ($value=='R') return "Royalty";
				if ($value=='L') return "Licence";
				if ($value=='S') return "Remix Split";
				return "Unknown Saletype";
			}
			case "saletype":
			{
				if ($value=='V') return "Download";
				if ($value=='P') return "Physical";
				if ($value=='S') return "Stream";
				return "Unknown Saletype";
			}
			case "date":
			{
				return displaydate($value);
			}
			case "currency":
			{
				return Currency::DATABASETODISPLAY($value);
			}
			case "licencetype":
			{
				if ($value=="T") return "Track";
				if ($value=="P") return "Product";
				if ($value=="A") return "Artist";
				break;
			}
			case "artist":
			{
				if ($value==0 || $value=="") return "Various Artists";
				$result = Database::Query("SELECT realname FROM artist WHERE uid='".$value."';");
				if ($result->GetNum()>0)
				{
					$row=$result->GetNext();
					return $row['realname'];
				}
				break;
			}
			case "licensee":
			{
				if ($value==0 || $value=="") return "";
				$result = Database::Query("SELECT name FROM partners WHERE uid='".$value."';");
				if ($result->GetNum()>0)
				{
					$row=$result->GetNext();
					return $row['name'];
				}
				break;
			}
			case "percentage":
			{
				return $value."%";
				break;
			}
			case "band":
			{
				if ($value==0 || $value=="") return "Various Artists";
				$result = Database::Query("SELECT name FROM bands WHERE uid='".$value."';");
				if ($result->GetNum()>0)
				{
					$row=$result->GetNext();
					return $row['name'];
				}
				break;
			}
			case "emailaddress":
			{
				if ($value!="") 
					return "<a href=\"mailto:".$value."\">".$value."</a>";
				break;
			}
			case "product":
			case "track":
			{
				if ($value=="") return "";
//				$value = preg_replace("[ \t]","",$value);
				
				$product = Database::QueryGetResult("SELECT title,artist FROM product WHERE uid='".$value."';");
				if ($product==false)
				{
					$product = Database::QueryGetResult("SELECT title,artist,version FROM tracks WHERE uid='".$value."';");
				}
				$band = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$product['artist']."';");
//				if ($band==false) $band['name'] = "Various Artists";
//				return $value." : ".$band['name']." - ".$product['title'];
				if ($type!='product')
					return $value." : ".$product['title']." ".$product['version'];
				else
					return $value." : ".$product['title'];
				
				break;
			}
			case "tracktitle":
			{
				if ($value=="") return "";
				$product = Database::QueryGetResult("SELECT title,version FROM tracks WHERE uid='".$value."';");
				return $product['title']." ".$product['version'];
				break;
			}
			case "producttitle":
			{
				if ($value=="") return "";
				$product = Database::QueryGetResult("SELECT title FROM product WHERE uid='".$value."';");
				return $product['title'];
				break;
			}
			case "trackshort":
			{
				$product = Database::QueryGetResult("SELECT title FROM tracks WHERE uid='".$value."';");
				return $product['title'];
				break;
			}
		}
		return $value;
	}
};

?>