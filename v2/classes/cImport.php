<?php
require_once("cDatabase.php");
require_once("util.php");
require_once("cEditField.php");
require_once("cForm.php");
require_once("cWorkSheet.php");
require_once("cFile.php");
require_once("cTableInfo.php");
require_once("cFile.php");
require_once('cImportFix.php'); 


define("ALLOW_ORPHAN_TRACK_SALES",true);
 

// Base Class
class Importer {

	public function LoadSheet($filename,$fixedfields) 
	{
	}
	
	public function GetFixedFields()
	{
	}
		
	public function GetData()
	{
	}
	
	public function Commit()
	{
	}
}


class Import extends Importer {
	var $m_name;
	var $m_headersrow;
	var $m_firstdatarow;
	var $m_table;
	var $m_fieldmap;
	var $m_currentworksheet;
	var $m_columns;
	
	
	
	
	public function LoadSheet($filename,$fixedvalues)
	{
		$this->ProcessFile($filename,$fixedvalues);
	}
	
	public function GetFixedFields()
	{
		return $this->GetFixedValues();
	}
	
	public function GetData()
	{
	}
	
	public function Commit()
	{
	}
	
	function	SetColumnRow($row)
	{
		$this->m_headersrow = $row;
		$this->m_firstdatarow = $row+1;
	}
	
	function	_isFieldUsed($field)
	{
		return ($this->m_fieldmap[$field]!="U");
	}
	
	function	GetUsingColumns($field)
	{
		$cols = explode(",",$this->m_fieldmap[$field]);
		
		if ($cols[0] == "F" || $cols[0]=="U") return false;
		return $cols;
	}
	
	function GetProductTrackUID($code)
	{
		if ($code=="") return;
	
		$code = addslashes($code);
	
		$result = Database::Query("SELECT uid FROM tracks WHERE uid='".$code."';");
		if ($result->GetNum()>0)
		{
			$row = $result->GetNext();
			$value = $row['uid'];	// this is a track
			Database::FinishQuery($result);
			return array("type"=>"track","uid"=>$value);
		}
		$result = Database::Query("SELECT uid FROM product WHERE barcode='".$code."';");
		if ($result->GetNum()>0)
		{
			$row = $result->GetNext();
			$value = $row['uid'];
			Database::FinishQuery($result);
			return array("type"=>"product","uid"=>$value);
		}
		$result = Database::Query("SELECT uid FROM product WHERE uid='".$code."';");
		if ($result->GetNum()>0)
		{
			$row = $result->GetNext();
			$value = $row['uid'];
			Database::FinishQuery($result);
			return array("type"=>"product","uid"=>$value);
		}
	}
	
	function Import()
	{
		$this->m_headersrow = 0;
		$this->m_firstdatarow = 1;
		$this->m_table = "sales";
	}

	function SetScript($script)
	{
		$this->m_name = $script;
	}	
	
	function 	FindFile($filename)
	{
		return File::GetPath('scripts/'.$filename);
	}

	function	Remove($filename)
	{
		if ($filename!="")
		{		
			$path = Import::FindFile($filename);
			if (file_exists($path))
			{
				unlink($path);
			}
		}
	}
	
	function	Load($filename)
	{
		$path = $this->FindFile($filename);
		if ($path!="")
		{
			$vars = unserialize(file_get_contents($path));
			foreach($vars as $key=>$val)
			{           
				eval("$"."this->$key = $"."vars['"."$key'];");
			}
		}
		
	}
	
	function 	Save($filename)
	{
		$filename = File::GetPath("scripts//".$filename.".imp");
		//"accounts//".$_SESSION['accountid']."//scripts"; 
//		$filename = $directory.'/'.$filename.".imp";
		
		$file = @fopen($filename,"wb");
		if($file)
        {
            if(@fwrite($file,serialize(get_object_vars($this))))
            {
                @fclose($file);
				chmod($filename,0777);
            }
            else 
			{
				die("Could not write to file ".$filename);
			}
        }
        else
		{
		// Open a known directory, and proceed to read its contents
			print $directory."<br>";
			if (is_dir($directory )) {
				if ($dh = opendir($directory)) {
					while (($file = readdir($dh)) !== false) {
						echo "filename: $file : filetype: " . filetype($dir . $file) . "<br>";
					}
					closedir($dh);
				}
			}
			die("Could not open file for writing ".$filename);
		}
	}

	
	function	Process()
	{
	}

	function 	EnumerateImports()
	{
		$directory =  File::GetPath("scripts"); 
		if (is_dir($directory))
		{
			if ($dh = opendir($directory)) 
			{
				while (($file = readdir($dh)) !== false) 
				{
					if (strpos($file,".imp")!=false)
					{
						$scripts[] = substr($file,0,strpos($file,".imp"));
					}
				}
				closedir($dh);
			}
		}
		if (!empty($scripts))
			return $scripts;	
	}


	static function	GetScripts($directory)
	{
		$i = 0;

		
		// Scan directory - first public ones
		$directory = "../accounts//".$directory."//scripts"; 
		if (is_dir($directory))
		{
			if ($dh = opendir($directory)) 
			{
				while (($file = readdir($dh)) !== false) 
				{
					if (strpos($file,".imp")!=false)
					{
						$scripts[$i]['display'] = substr($file,0,strpos($file,".imp")); 
						$scripts[$i++]['path'] = $file;
					}
				}
				closedir($dh);
			}
		}
		
		if (!empty($scripts))
			return $scripts;
	}

	function	LoadFromPost()
	{
		$this->m_headersrow = $_POST['headersrow'];
		$this->m_firstdatarow = $_POST['firstdatarow'];
		$this->m_table = $_POST['table'];
		$this->m_columns = $_POST['columns'];		
		$this->m_name = $_POST["name"];
		
		unset($this->m_fieldmap);
		$tableinfo = TableInfo::GetTable($this->m_table);

		foreach ($tableinfo as $field => $info)
		{
			if ($field=="quantity" || $field=="track")
			{
				$this->m_fieldmap[$field] = $_POST[$field."_column"].",".$_POST[$field."_column2"]; //.",".$_POST[$field."_column3"];
			}
			else
			{
				$this->m_fieldmap[$field] = $_POST[$field."_column"];
			}
		}
	}
	


	function GetFixedValues()
	{
		$tableinfo = TableInfo::GetTable($this->m_table);
		foreach ($tableinfo as $field => $info)
		{
			if ($this->m_fieldmap[$field][0]=="F")
			{
				if ($info['type']=="date") 
				{
					$fixed[$field] = getpostdate2($field);
				}
				else
				{
					$fixed[$field] = $_POST[$field];
				}
			}
		}
		$fixed['convert'] = $_POST['convert']; // special case
		if (!empty($fixed)) return $fixed;
	}
	
	function ProcessFile($file,$fixedvalues)
	{
//		print_r($fixedvalues);
		$sales = 0;
		// Load the worksheet from the file
//		$this->m_worksheet = WorkSheet::Load($file);
/*		if ($this->m_worksheet->GetNumSheets()>1)
		{
			print "<p><b>This excel file contains more than one worksheet. Mozaic will only read the first worksheet of any file. To fix this copy and paste the desired sheet into a new excel document and try again.</b></p>";
			return "";
		}
*/
		// Get the fields of the table we want
		$tableinfo = TableInfo::GetTable($this->m_table);

		// Start displaying the table
		print "<table border=1>";
		print "<tr><th>Row</th>";
		foreach ($tableinfo as $field => $info)
		{
			if ($this->_isFieldUsed($field) && $this->m_fieldmap[$field]!='F')
			{
				print "<th>".$info['description']."</th>";
			}
		}
		print "<th>Status</th></tr>";
	
	
		// Construct a comma seperated list of all the fields in the table
		$insertfields = "";
		$j=0;
		foreach ($tableinfo as $field=>$info)
		{
			if ($this->_isFieldUsed($field))
			{
				if ($j>0)
				{
					$insertfields .= ",";
				}
				++$j;
				$insertfields .= $field;
			}
		}

		$errorcount = 0;
		$querylist = "";
		$lasterrorcount = 0;
		$skippedrows = 0;
		$i = 1;					// row number

		ini_set('auto_detect_line_endings',TRUE);
		// Scan each row
		$import_file = fopen($file,"r");
		while($row = fgetcsv($import_file,1000,","))
		{
			if ($row==array() || $i< $this->m_firstdatarow) {
				$skippedrows++;
				$i++;		
				continue;
			} 

			print '<tr>';
			print '<td>'.$i.'</td>';
			$error = '<b>OK</b>';

			foreach ($tableinfo as $field=>$info)
			{
				$value = "";
				if ($this->_isFieldUsed($field)) {

					// Get the right cell, this grab the first of two cells
					$cols = $this->GetUsingColumns($field);
					//print $field."  "; print_r($cols); print '<br>';
					if ($cols != false) {
					  // check if a multiple column entry
					  	for ($coli=0;$coli<count($cols);++$coli) {
						  
						  	if (isset($row[$cols[$coli]-1])) {
						  	
							  	$value = $row[$cols[$coli]-1];
							  	if ($value=='`N/A' || $value=='N/A') $value = "";
							  	if (!empty($value)) break;
							  	}
						}
					}
					else if (!empty($fixedvalues[$field])) {
						// this is a fixed value
						$value = $fixedvalues[$field];
					}
					
					// Special cases
					$specialcase = false;
					// Special coding here - blah! 
					if ($this->m_table=="sale")
					{
						// Special cases for sales
						if ($field=="product" && $value=="")
						{
							global $validationerror;
							$validationerror = "<b>Not supplied and cannot resolve as ISRC ambigious</b>";						
							$validationresult = false;
							$specialcase = true;
							// Get the ISRC and try to work it out backwards from this
							// if ambigious then fail
							$cols = $this->GetUsingColumns("track");
							if (!isset($row[$cols[0]-1])) break; 
							$isrc = $row[$cols[0]-1];
							if ($isrc!="")
							{
								$products = Database::QueryGetResults("SELECT product FROM tracksproduct WHERE track='$isrc';");
								if (ALLOW_ORPHAN_TRACK_SALES) {
									$validationresult = $isrc;
								} else if (count($products)==1) {
									$validationresult = $products[0]['product'];
								} else {
									$validationresult = false;
								}
							}

						
						}
						if ($field=='saletype' && $this->m_fieldmap[$field]=="A")
						{
							// auto detection of saletype
							// divid net sales by units
							$mycol = $this->GetUsingColumns("quantity");
							if ($row[$mycol[0]-1]==0)
								$ratio = 1; // avoid divide by zero
							else
								$ratio = $row[$this->m_fieldmap['value']-1]/$row[$mycol[0]-1];
							$validationresult = 'V';
							if ($ratio<0.10) $validationresult = 'S';
							//print $mycol[0]." ".$row[$this->m_fieldmap['value']-1].",".$row[$mycol[0]-1]." = ".$ratio." (".$validationresult.")<br>";
							//if ($ratio>3) $validationresult = 'P';
							$specialcase = true;
						}
					}
					
					if (!$specialcase)
						$validationresult = TableInfo::Validate($this->m_table,$field,$value);
					

	
					if ($validationresult!==false)
					{
						// do currency conversion
						if ($field=='value' || $field=='baseprice' || $field=='mechanicals' || $field=='distfee')
						{
							$validationresult = Currency::PRODUCTOF($validationresult,$fixedvalues['convert']);
						}
					
					
						$insert[$i][ $field ]= $validationresult;
						if ($this->m_fieldmap[$field]!='F')
						{
							if ($validationresult!="")
								print "<td>".TableInfo::Display($this->m_table,$field,$validationresult)."</td>";
							else
								print "<td></td>";
						}
					}
					else
					{
						print "<td><b>".TableInfo::GetValidationError()."</b></td>";
						$error = "<b>FAIL</b>";
						++$errorcount;
					}

				}
			}

			if ($errorcount>$lasterrorcount)
			{
				unset($insert[$i]);
			}
			$lasterrorcount = $errorcount;
			print "<td>".$error."</td>";
			print "</tr>";

			$i++;
			
		}// end while
		
		print "</table>";
		
		if ($errorcount>0)
		{
			print "<p>$errorcount errors detected. Please check the data</p>";
		}
		
		fclose($import_file);
		
		$querylist ="";
		if (empty($insert))
		{
			print "<p>Empty Sheet</p>";
			return;
		}
			
		foreach ($insert as $index => $row)
		{
			$querylist .= "INSERT INTO ".$this->m_table." ($insertfields) VALUES (";
			
			$i = 0;
			foreach($row as $field=>$value)
			{
				if ($i>0) $querylist .= ",";
				$querylist .= "'".Database::Validate($value)."'";
				++$i;
			} 
			$querylist .= ");\n";	
		}
		return $querylist;
	}
	
	// Process sales - new code
	function ProcessSales()
	{
		// Match col -> var type, for each col user can specify the type of data from the following:
		//  ISRC, Track Name, UPC, Product Name, Catolgue Number, Artist Name, Sales Count, Sales Value, Territory, Shop, Not Used 
		// from this do the following
		//    identify the track - use the track name/isrc (also artist)

		$rowout['track']='';
		if ($columnids['isrc']!=-1 && $rowin[$columnid['isrc']]!='')
		{
			// search isrc
			// SELECT uid,isrc FROM tracks WHERE isrc=
		}


		//    identify the product - use product name, upc or cat no (also artist).
		//    identify sales count - sum columns
		//    identify sales cost - sum columns
		//    territory (single), shop (single), dist (single), ...
		
	}

	function	SetTable($table)
	{
		$this->m_table = $table;
	}
	
	function	PresentFixedForm(&$form)
	{
		$tableinfo = TableInfo::GetTable($this->m_table);
		foreach ($tableinfo as $field => $info)
		{
			if (!empty($this->m_fieldmap[$field]) && $this->m_fieldmap[$field][0]=="F" )
			{
				$form->AddField($info['description'],$field,$info['type'],"");
			}
		}
		$form->AddField("Currency Conversion","convert","float","1.0");

	}
	
	function	PresentEditForm(&$form,$file)
	{
//		$form = new Form;
		$form->AddField("Import Script Name","name","text",$this->m_name);
		$form->AddHidden("table",$this->m_table);

		$form->AddField("Row # Column Headers","headersrow","integer",$this->m_headersrow);
		$form->AddField("Row # First Data","firstdatarow","integer",$this->m_firstdatarow);

		$tableinfo = TableInfo::GetTable($this->m_table);				
		if ($file!="")
		{
			$this->m_worksheet = WorkSheet::Load($file);
			$countcolumns = count($this->m_worksheet[$this->m_headersrow]);;
			$columnslist = "";
			if ($countcolumns>26) $countcolumns=26;
			for ($i=1;$i<=$countcolumns;++$i)
			{
				if ($i>1) $columnslist = $columnslist.",";
				$columnnames[$i] = "Column ".chr(ord('A')+($i-1))." : ".$this->m_worksheet[$this->m_headersrow][$i];
				$columnslist = $columnslist.$i."=".$columnnames[$i];
			}
			$columnslist = $columnslist.",F=Fixed Value,U=Unused";
			$this->m_columns = $columnslist;
		}
		else
		{
			if (empty($this->m_columns))
			{
				$columnslist = "";
				for ($i=1;$i<=26;++$i)
				{
					if ($i>1) $columnslist = $columnslist.",";
					$columnslist = $columnslist.$i."=Column ".chr(ord('A')+($i-1));
				}
				$columnslist = $columnslist.",F=Fixed Value,U=Unused";
				$this->m_columns = $columnslist;
			}
			else
			{
				$columnslist = $this->m_columns;
			}
		}

		$form->AddHidden("columns",$this->m_columns);
				
//		print "<tr><td><b>Data Expected</b></td><td><b>Column Assignment</b></td><td></td></tr>";
		foreach ($tableinfo as $field => $info)
		{
			$col2 = "U";
			$col = "U";
			if (isset($this->m_fieldmap))
			{
				$col = $this->m_fieldmap[$field];
				if ($field=="quantity" || $field=="track")
				{
					$col2 = substr($col,strpos($col,",")+1);
					$col = substr($col,0,strpos($col,","));
				}
			}
			
			// Italicize fields that are optional
			if ($info['optional']==false)
			{
				$description = $info['description']."*";
				if ($col=="U") $col="F";
			}
			else
				$description = $info['description'];
			
			if ($field=='saletype')
				$form->AddSelectionList($description,$field."_column",$columnslist.",A=Auto Detect",$col);
			else			
				$form->AddSelectionList($description,$field."_column",$columnslist,$col);
			if ($field=="quantity" || $field=="track")
			{
				$form->AddSelectionList("",$field."_column2",$columnslist,$col2);
			}
		}

//		print "<tr><td><font size=1><i>* mandatory fields</i></font></td></tr>";
		// Get the fields 
		$form->AddHidden("importscript",$this->m_name);
		$form->AddHidden("importfile",$file);
	}

	function GetScriptName()
	{
		return $this->m_name;
	}
	
	function	Finalise()
	{
	}


};


?>