<?php
// my update

include_once("cWorkSheet.php");
include_once("cTableInfo.php");
include_once("cCurrency.php");
require_once("cFile.php");

define("TRANSACTION_SALE","0");
define("TRANSACTION_EXPENSE","1");
define("TRANSACTION_DUMMY","4");

// UPDATE TO TABLE
// ALTER TABLE `sale` ADD `baseprice` VARCHAR( 16 ) NOT NULL AFTER `quantity` ;
// ALTER TABLE `deals` ADD `packagingdeductions` FLOAT NOT NULL DEFAULT '0' AFTER `reserve` ;


// override util 

function displaydate2($complexdatestring) 
{
	$datestring = explode(" to ",$complexdatestring);
	if (empty($datestring[1]))
		return displaydate($datestring[0]);

	return displaydate($datestring[0])." - ".displaydate($datestring[1]);
}



class Statement
{
	var $m_display;
	var $m_errormessage;
	var $m_worksheet;
	var $m_complete;
	var $m_errortrap;
	var $m_dealset;
	var $usedlicences;

	function Statement()
	{
		$this->m_display = "";
		$this->m_errormessage = "";
		$this->m_artistid = -1;
		//$this->m_errortrap = false;
		$this->m_complete = true;
		$this->usedlicences = array();
	}
	
	function _print($string)
	{
		$this->m_display = $this->m_display.$string;
	}
	
	function Display()
	{
		$this->m_worksheet->DisplayAsTable();
	}
	
	function IsValid()
	{
		return ($this->m_artistid!=-1);
	}

	function	SortResults($assoc, $sortfield)
	{
		foreach ($assoc as $key=>$row)
		{
			$sort[$key] = $row[$sortfield];
		}
		array_multisort($sort,SORT_ASC,$assoc);
		return $assoc;
	}

	function	CollateResults(&$assoc, $sortfieldscsv )
	{
		if (empty($assoc)) return;
		
		// Sort
		$sortfields = explode(",",$sortfieldscsv);

		// clear the current vars
		foreach ($sortfields as $sortfield)
		{
			$collaterow[$sortfield] = "";
		}
		
		// Go through each row
		foreach ($assoc as $row)
		{
			$changerow = false;
			foreach ($sortfields as $sortfield)
			{
				if ($row[$sortfield] != $collaterow[$sortfield])
				{
					if ($collaterow[$sortfield]!="") 
					{
						$outrow[] = $collaterow;			// flush the previous row
						$changerow = true;
					}
					$collaterow[$sortfield] = $row[$sortfield];
				}
			}	
			
			// and each value in each row
			foreach ($row as $key=>$value)
			{
				// is this cell a sortfield?
				if (array_search($key,$sortfields)===false)
				{
					if ($changerow)
						$collaterow[$key] = 0;
						
					if (empty($collaterow[$key]))
					{
						$collaterow[$key] = $value;
 					
 						if ($key=="baseprice")
						{
		      				$collaterow[$key] = Currency::PRODUCTOF($value,$row['quantity']);
          	   			}
          	   		}
					else  // value is not empty
					{
						// here we decided how we collate the columns - do we add them, replace, range etc..
						switch($key)
						{
						case "quantity":
							$collaterow[$key] += $value;
							break;
						case "value":
						case "amount":	
							$collaterow[$key] = Currency::SUMOF($collaterow[$key],$value);
							break;
						case "baseprice":
							$roy = Currency::PRODUCTOF($value,$row['quantity']);
							$collaterow[$key] = Currency::SUMOF($collaterow[$key],$roy);
							break;
	           			case "date":
							if ($collaterow[$key]=="") 
							{
								$collaterow[$key]=$value;
							} 
							else
							{
								$split = explode(" to ",$collaterow[$key]);
								if (empty($split[1]))
								{
									if ($split[0]<$value) 
										$collaterow[$key] = $split[0]." to ".$value;
									else if ($split[0]>$value)
										$collaterow[$key] = $value." to ".$split[0];
								}
								else
								{
									if ($split[0]>$value && $split[1]>$value)	$split[0] = $value;
									if ($split[1]<$value && $split[0]<$value)	$split[1] = $value;
									$collaterow[$key] = $split[0]." to ".$split[1];
								}
									
							}
							break;
						default:				
							// default copy over			
							$collaterow[$key] = $value;
						}
					}
				}
			}
		}
		$outrow[] = $collaterow;
		

		return $outrow;
	}


	// Calculate a bands split of a compilation
	// Returns an array like 1 / 3 
	function GetBandSplitOfCompilation($band,$product)
	{
		$productinfo = Database::QueryGetResult("SELECT artist,uid FROM product WHERE uid='".$product."';");
		$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product='".$product."';");
		if ($tracks!=false)
		{
			$numtracks = count($tracks);
			$tracklist="";
			foreach ($tracks as $index=>$track)
			{
				if ($index>0) $tracklist.=',';
				$tracklist .= "'".$track['track']."'";
			}
			$bandtracks = Database::QueryGetResult("SELECT COUNT(uid) FROM tracks WHERE uid IN ($tracklist) AND artist='".$band."';");
			// Split is the number of bands tracks divided by the number of tracks
			$split['top'] = $bandtracks['COUNT(uid)'];
			$split['bottom'] =  $numtracks;			
		}
		else
		{
			$split['top'] = 1; $split['bottom'] = 1;
		}
		return $split;
	}

	function GetDigitalAlbumSales(&$deal)
	{
		$sales = Database::QueryGetResults("SELECT date,distributor,quantity,product,value,baseprice FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND track='' AND saletype='V' AND product='".$deal['product']."' ORDER BY distributor;");
		if (empty($sales)) $sales=array();	
		// Also want to add in here any track sales on a comp of a various artist
		//if (IsCompilation($deal))
		{
//			"SELECT uid FROM tracks WHERE artist='0' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$deal['product']."')");
			$mixsales = Database::QueryGetResults("SELECT date,distributor,quantity,product,value,baseprice,track FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND track IN (SELECT uid FROM tracks WHERE artist='0' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$deal['product']."')) AND product='".$deal['product']."' AND saletype='V' ORDER BY distributor;");
			//PrintTable($mixsales);
			if (!empty($mixsales))
				$sales = array_merge($sales,$mixsales);
		}
		
		return $sales;
	}


//				$dealsales = $this->GetDigitalSales($deal['startdate'],$deal['enddate'],$deal['band'],'',$deal['track']);

	// Get all digital track sales related to this deal
	function GetDigitalSales($startdate,$enddate,$band,$product,$track)
	{
		print "GetDigitalSales startdate=$startdate,enddate=$enddate,band=$band,product=$product,track=$track<br>";
		if ($track=='')
			$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$startdate."' AND '".$enddate."' AND saletype='V' AND track IN (SELECT uid FROM tracks WHERE artist='".$band."' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$product."')) ORDER BY track,distributor;";
		else
			$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$startdate."' AND '".$enddate."' AND saletype='V'  AND track='".$track."' ORDER BY track,distributor;";
					
		$sales = Database::QueryGetResults($query);

		//$remixsales = GetRemixSplitsRemixer($deal);
		//$sales = array_merge($sales,$remixsales);

	
		return $sales;
	}

	// Get all digital track sales related to this deal
	function GetStreams(&$deal)
	{
		if ($deal['track']=="")
			$query = "SELECT track,product,shop,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$deal['product']."')) ORDER BY track,shop;";
		else
			$query = "SELECT track,product,shop,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND track='".$deal['track']."' ORDER BY track,shop;";
			
		$streams = Database::QueryGetResults($query);
		//print $query;
		//PrintTable($streams);
		return $streams;
	}


	/*
		Returns the Remixers Fees if a remix split has been agreed - this is added as an expense to the artist
	
		- Find all the digital track sales related to this 
		- Take off the label cut
		- Then take off the artist cut
	*/
	function GetRemixSplitsExpenses(&$deal)
	{
		
		// Find any remixer split deals for the product we are calculating
		$res = Database::QueryGetResults("SELECT * FROM deals WHERE product='".$deal['product']."' AND dealtype='S';");
		if ($res==false) return;
			
		foreach($res as $key=>$rdeal)
		{
			// Get Rate
			$terms = Database::QueryGetResults("SELECT * FROM dealterms WHERE dealid='".$rdeal['uid']."';");
			$rate = $terms[0]['rate'];
			
			// retreive all digital track sales/streams for the tracks in this product
			$query = "SELECT * FROM sale WHERE track='".$rdeal['track']."' AND (saletype='S' OR saletype='V') AND date BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."';";
			$sales = Database::QueryGetResults($query);
			if (!empty($sales))
			{
				$salestotal = 0;
				foreach($sales as $sale)
				{
					$salestotal += Currency::GETRAWVALUE($sale['value']) * ($rate / 100);
					
				}
				
				$tracktit = Database::QueryGetResult("SELECT title FROM tracks WHERE uid='".$rdeal['track']."';");
				$salestotalcur = new Currency();
				$salestotalcur->SetValue($salestotal);
				$remixexpense[] = array('invoicedate'=>'','type'=>'Remix Split for Original Artist: '.$tracktit['title'],'product'=>$rdeal['product'],'amount'=>$salestotalcur->GetDatabaseFormat());
				
			}
		}
		if (!empty($remixexpense)) 
			return $remixexpense;
	}	
	
	

	function GetExpenses(&$deal,$iscompilation)
	{
		if ($_SESSION['accountid']==10)
			$expenses = Database::QueryGetResults("SELECT invoicedate,type,product,amount FROM expenses WHERE product='".$deal['product']."' AND paiddate BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."';");
			else
			$expenses = Database::QueryGetResults("SELECT invoicedate,type,product,amount FROM expenses WHERE product='".$deal['product']."' AND invoicedate BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."';");
			
		if ($expenses==false) $expenses=array();
		// Try putting remix splits in here
		if (!$iscompilation && $deal['dealtype']!='S') $remixexpenses = $this->GetRemixSplitsExpenses($deal);
		if (!empty($remixexpenses)) 
			$expenses = array_merge($expenses,$remixexpenses);
		return $expenses;
	}
	
	
	
	// Get all licences relating to band
	function GetDealLicences($deal,$iscompilation)
	{
		$band = $deal['band'];	
		$cataloguelist = "'".$deal['product']."'";

		// Don't include any licencing for a tracks on compilations these should be included in the main statement
		if ($iscompilation)
		{
			$excludelist = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product IN (SELECT uid FROM product WHERE artist='$band');");
			$excludecsv="";
			if (!empty($excludelist))
			{	
				foreach ($excludelist as $i=>$exclude)
				{
					if ($i>0) $excludecsv .= ",";
					$excludecsv .= "'".$exclude['track']."'";
				}
				$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
														(SELECT track FROM tracksproduct WHERE product='".$deal['product']."')
														AND uid NOT IN ($excludecsv);");
			}
			else
			{
				$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
														(SELECT track FROM tracksproduct WHERE product='".$deal['product']."');");
			}
		}
		else
		{
			$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
													(SELECT track FROM tracksproduct WHERE product='".$deal['product']."');");
		}

		if (!empty($catalogue))
		{
			foreach ($catalogue as $index=>$item)
			{
				$cataloguelist .= ",'".$item['uid']."'";
			}
			$tracklicences = Database::QueryGetResults("SELECT uid FROM licences WHERE ((licencetype='T') AND licenceduid IN ($cataloguelist));");
		}
		
		if ($iscompilation && !empty($tracklicences))
		{
			// These are compilation only track licences - however we can't include in here so add them to a magic list!
			foreach ($tracklicences as $licence)
			{
				if (empty($this->m_additionallicences) || array_search($licence['uid'],$this->m_additionallicences)===false)
					$this->m_additionallicences[] = $licence['uid'];
			}
			unset($tracklicences);
		}

/*		if ($iscompilation)
		{
			// If it is a compilation then return all licenses for this product
			$productlicences = Database::QueryGetResults("SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';");
		}
		else
		{
			// If its not a compilation then get the artist and then get all product licenses
			$artist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$deal['product']."';");
			if ($artist['artist']!='')
				$productlicences = Database::QueryGetResults("SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';");
		}
*/

		$query = "SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';";
		$productlicences = Database::QueryGetResults($query);
	

	
	  	// merge these together
	  	if (empty($tracklicences) && !empty($productlicences)) $licences = $productlicences;
	  	else if (!empty($tracklicences) && empty($productlicences)) $licences = $tracklicences;
	  	else if (!empty($tracklicences) && !empty($productlicences)) $licences = array_merge($tracklicences,$productlicences);

		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				$licenceuids[] = $licence['uid'];
			}
		}
		if (!empty($licenceuids))
			return $licenceuids;
	}
	
	
	
	function GetLicenceMonies(&$licenceuids,$startdate,$enddate)
	{
		if (empty($licenceuids)) return;
		$licenceuids_csv = implode(",",$licenceuids);
		
		
		// Get all advances that are applicable
		$licences = Database::QueryGetResults("SELECT uid,description,advance,advancepaiddate,licenceduid,licencetype FROM licences WHERE uid IN ($licenceuids_csv);");
		
		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				// Any advances?
				if ($licence['advancepaiddate']>=$startdate && $licence['advancepaiddate']<=$enddate) {
					$item[] = array('date'=>$licence['advancepaiddate'],'description'=>"Licensing Advance: ".$licence['description'],'catno'=>$licence['licenceduid'],'value'=>$licence['advance'],'licencetype'=>$licence['licencetype']);
					}
				
				// Any statements
				$query = "SELECT paiddate,amount,licenceuid FROM licencestatements WHERE licenceuid='".$licence['uid']."' AND paiddate BETWEEN '$startdate' AND '$enddate';";
				$licencestatements = Database::QueryGetResults($query);
				
				if (!empty($licencestatements))
				{
					foreach ($licencestatements as $statement)
					{
						$item[] = array('date'=>$statement['paiddate'],'description'=>"Licensing Royalty: ".$licence['description'],'catno'=>$licence['licenceduid'],'value'=>$statement['amount'],'licencetype'=>$licence['licencetype']);
					}
				}
			}
		}

		if (!empty($item))
			return $item;		

	}
			
	function ProcessLicensing(&$deals,&$worksheet,&$splits,$iscompilation)
	{	
		
		foreach($deals as $deal) {
			$products[] = $deal['product'];		// array of all products in deal
		}

		// Product Licences		
		$product_licences = Database::QueryGetResults("SELECT * FROM licences WHERE licencetype='P' AND licenceduid IN ('".implode("','",$products)."')");

		if (!empty($product_licences))
			$product_licence_revenues = $this->GetLicenceMonies($product_licences,$this->m_startdate,$this->m_enddate); 
		if (!$iscompilation) {
			$trackslist = array();
			$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product IN ('".implode("','",$products)."')");
			foreach($tracks as $track) {
				// don't pick tracks that aren't by this artist
				$trackinfo = Database::QueryGetResults("SELECT artist FROM tracks WHERE uid='".$track['track']."'");
				if ($trackinfo[0]['artist']==$deals[0]['band'])
					$trackslist[] = $track['track'];
			}
			$query = "SELECT * FROM licences WHERE licencetype='T' AND licenceduid IN ('".implode("','",$trackslist)."')";
			$track_licences = Database::QueryGetResults($query);
//			print_r($track_licences); print '<br>';
			if (!empty($track_licences)){
				foreach($track_licences as $track_licence) {
					if (!in_array($track_licence['uid'],$this->usedlicences)) {
						$licences[] = $track_licence['uid'];
						$this->usedlicences[] = $track_licence['uid'];
					}
				}
			}
			
			if (!empty($licences))
				$track_licence_revenues = $this->GetLicenceMonies($licences,$this->m_startdate,$this->m_enddate); 


		}

		$licence_revenues = array();
		if (!empty($product_licence_revenues))  $licence_revenues = array_merge($licence_revenues,$product_licence_revenues);
		if (!empty($track_licence_revenues))    $licence_revenues = array_merge($licence_revenues,$track_licence_revenues);
		
		$licencing_total = 0;
		if (!empty($licence_revenues))
		{
			foreach ($licence_revenues as $item)
			{
				$value = 0;
	/*			if ($item['licencetype']=='P')
				{
					print_r($item);
					print '<br>'.$splits[$deal['uid']].'<br>'.count($deals).'<br>'.$deal[0]['product'].'<br>';
					// If this a compilation and this is not the compilations dealset then ignore it
					if ($splits[$deal['uid']]==1 || (count($deals)==1 && $deals[0]['product']==$item['catno']))
					{
						$value = Currency::GETRAWVALUE($item['value']); // * $splits[$deal['uid']];
						$worksheet->AddRow($item['description'],"",Currency::DISPLAY(round($value)));
					}
				}
				else*/
				{
					$value = Currency::GETRAWVALUE($item['value']);
					$worksheet->AddRow($item['description'],"",Currency::DISPLAY(round($value)));
				}
					
				$licencing_total += $value;
			}
		}
		
		
		return $licencing_total;
	}


	function	MakeStatementHash($artist,$bandlist,$startdate,$enddate)
	{
		return md5($artist."_".$bandlist."_".$startdate."_".$enddate);
	}

	function	MakeStatementFilename2($pdfhash)
	{
		return File::GetPath("statement_".$pdfhash.".pdf");
	}

	function	MakeStatementFilename($artist,$bandlist,$startdate,$enddate)
	{
	
		return File::GetPath("statement_".$artist."_".$bandlist."_".$startdate."_".$enddate.".pdf");
	}

	// This is a private function - it creates the html that we insert into the worksheet
	// this gives us the form "CARRYOVER BALANCE".
	function CreateBalanceHTML($crossdealid)
	{
		$html = "<form method=post action=index.php><input type=hidden name=page value=artists>";
		$html .="<input size=6 type=text name='carryover".$crossdealid."' value=''>";
		foreach($_POST as $key=>$value)
		{
			if ($key=='bands')
				$html .="<input type=hidden name='bands[]' value='".implode(",",$_POST['bands'])."'>";
			else
				$html .= "<input type=hidden name='".$key."' value='".$value."'>";
		}
		$html .="<input type=submit class=submitsmall name=submit value='SET'>";
		$html .="<input type=hidden name=transactionid value='".$_SESSION['transactionid']."'>";
		$html .="</form>";
		return $html;
	}

	function ProcessAdvances(&$deals,&$worksheet)
	{
		$advance_total = 0;
		foreach ($deals as $deal)
		{
			if ($deal['advancepaiddate']>=$this->m_startdate && $deal['advancepaiddate']<=$this->m_enddate)
			{
			
				$advance = Currency::GETRAWVALUE($deal['advance']);
				// Subtract advances
				if ($advance!=0)
				{
					$worksheet->AddHeaderRow("ARTIST ADVANCE ".$deal['product'],"",Currency::DISPLAY(round(-$advance)));
					$advance_total += $advance;
				}
			}
		}
		return $advance_total;
	}
	

	function sumUp($records)
	{
	}

		
	function processPhysicalSales($product,$startdate,$enddate)
	{
		$physical_value = 0;	
		$physical_quantity = 0;
			
		$sales = Database::QueryGetResults("SELECT date,distributor,quantity,product,value,baseprice FROM sale WHERE date BETWEEN '".$startdate."' AND '".$enddate."' AND track='' AND saletype='P' AND product='".$product."' ORDER BY distributor;");

		if (!empty($sales))
		{
			// need to seperate -sales for +sales for returns seperate path
			unset($salesneg);
			foreach($sales as $key=>$sale)
			{
				if ($sale['quantity']<0)
				{
					$salesneg[] = $sales[$key];
					unset($sales[$key]);
				}
			}
		
			$sales = $this->CollateResults($sales,"distributor");
			if (!empty($salesneg))
			{
				if (empty($sales))
					$sales = $this->CollateResults($salesneg,"distributor");
				else
					$sales = array_merge($sales,$this->CollateResults($salesneg,"distributor"));
			}
			
			$isempty = false;
			foreach ($sales as $sale)
			{
				$items[] = array('description'=>TableInfo::DisplayFromType('distributorshop', $sale['distributor']).' physical sales of '.$sale['product'],'quantity'=>$sale['quantity'],'value'=>Currency::GETRAWVALUE($sale['value']));
			}

		return $items;
		}		
		return;
	}
	
	
	function sumSales($insales,$key1,$key2='')
	{
	//	PrintTable($insales);
		foreach($insales as $index=>$sale)
		{
			$sale['value'] = Currency::GETRAWVALUE($sale['value']);
			if ($index==0)
				$outrow = $sale;
			else	
			{
				if ($key2=='') $test = true;
				else $test = ($sale[$key2]==$outrow[$key2]); 
				// If for same item then sum instead of outputting new row
				if ($sale[$key1]==$outrow[$key1] && $test)
				{
					$outrow['quantity'] += $sale['quantity'];
					$outrow['value'] += $sale['value'];
				}
				else
				{
					$outsales[] = $outrow;
					$outrow = $sale;
				}
				
			}	
		}
		if (isset($outrow)) $outsales[] = $outrow;
		return $outsales;
	}
	
	
/*	function processRemixSplits($deal)
	{
		$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sales WHERE track='".$deal['track']."';";
		$sales = Database::QueryGetResults($query);
		foreach ($sales as $sale)
		{
		}
	}
*/	
	/*
		Process a set of deals - these all get cross calculated
		Inputs: previous_carryover
		Outputs: artist_due, carryover
	*/
	function ProcessDealSet(&$worksheet,$deals,$carryover,$reservepercentage,$iscompilation,$lasttime,$treatasthirdpartycomp,$transferloss)
	{

		ts_assert(!(count($deals)>1 && $iscompilation),"Internal error - bad compilation deal set. Please inform support");
	/*
		print $deals[0]['product']; print '<br>';
		print_r($deals); print '<br>';
		if ($lasttime) print 'Lasttime<br>';
		if ($iscompilation) print 'Compilation<br>';
		print_r($carryover);
		print'<br><br>';
	*/	
		$isempty = true;
		$balance = 0;
		
		$artist_due = 0;
		$newcarryover = 0;
		
		$rate = $deals[0]['rate'];
		$dealtype = $deals[0]['dealtype'];
		
		// Worksheet header
		$bandname = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$deals[0]['band']."';");
		$bandname = strtoupper($bandname['name']);
	
		if ($iscompilation)	
			$split = $this->GetBandSplitOfCompilation($deals[0]['band'],$deals[0]['product']);
		
		if ($iscompilation)
			$title = "Accounts for Compilation ".$deals[0]['product'];
		else
		{
			if ($dealtype=='S') $title='Remix Split Calculation';
			if ($dealtype=='P') $title='Profit Split Calculation';
			if ($dealtype=='R') $title='Royalty Calculation';
		}
		//	$title = "Main Accounts"; (dealset ".$deals[0]['crossdealid'].")";
		

		$worksheet->AddTitleRow($title);
		$worksheet->AddHeaderRow("DESCRIPTION","QUANTITY","VALUE");


		//--------------------------------------------------------------
		// PHYSICAL SALES
		$physical_value = 0;	
		$physical_quantity = 0;
		foreach($deals as $deal)
		{
			if ($dealtype=='S') continue;
			if ($dealtype=='P' && $rate!=$deal['rate']) 
			{
				print "dealtype = $dealtype, rate = $rate<br>";
				print_r ($deals); print "<br>";
				die("Rate not consistent across deal set");
			}
			
			if ($dealtype!=$deal['dealtype']) 
			{
				die("Dealtype not consistent across deal set");
			}
			
			// Skip if a compilation (already calculated)
			if ($this->IsCompilation($deal['product']) && !$iscompilation)
				continue;

			$items = $this->processPhysicalSales($deal['product'],$deal['startdate'],$deal['enddate']);
			if (!empty($items))
			{
				foreach ($items as $item)
				{
					$worksheet->AddRow($item['description'],$item['quantity'],Currency::DISPLAY(round($item['value'])));
					$physical_value += $item['value'];					
					$physical_quantity += $item['quantity'];
				}
			}
			
		}
		if ($physical_quantity!=0)
			$worksheet->AddHeaderRow("",$physical_quantity,Currency::DISPLAY(round($physical_value)));
		$balance += round($physical_value);


		//--------------------------------------------------------------
		// DIGITAL BUNDLED SALES
		$digitalalbum_value = 0;
		$digitalalbum_quantity = 0;
		foreach($deals as $deal)
		{
			if ($dealtype=='S') continue;		// don't process for remix splits
			
			if ($this->IsCompilation($deal['product']) && !$iscompilation)
				continue;

			ts_assert(!($dealtype=='P' && $rate!=$deal['rate']),"Rate not consistent across deal set"); 
			ts_assert(!($dealtype!=$deal['dealtype']),"Dealtype not consistent across deal set");
		
			$sales = $this->GetDigitalAlbumSales($deal);
			if (!empty($sales))
			{
//				$sales = Statement::CollateResults($sales,"distributor");
				$sales = $this->sumSales($sales,'distributor','product');


				foreach ($sales as $sale)
				{
					$value = Currency::GETRAWVALUE($sale['value']);
					$digitalalbum_value += $value;
	
					$worksheet->AddRow(TableInfo::DisplayFromType('distributorshop',
					$sale['distributor']).' digital sales of '.$sale['product'],$sale['quantity'],Currency::DISPLAY($value));
					$isempty = false;
					$digitalalbum_quantity += $sale['quantity'];
				}
				
				$sheetempty = false;
			}
		}
		$balance += round($digitalalbum_value);
		if ($digitalalbum_quantity!=0)
			$worksheet->AddHeaderRow("",$digitalalbum_quantity,Currency::DISPLAY(round($digitalalbum_value)));
			


		if (!$iscompilation)
		{
		//--------------------------------------------------------------
		// DIGITAL TRACK SALES

			$digitaltrack_sales = 0;

			$sales = array();
			$productlist = '';
			$tracklist = '';
			foreach($deals as $deal)
			{
				ts_assert(!($rate!=$deal['rate']),"Rate not consistent across deal set");
				ts_assert(!($dealtype!=$deal['dealtype']),"Dealtype not consistent across deal set");

				// Get all the tracks for the artist in these deals
				if ($productlist!='') $productlist .= ',';
				$productlist .= "'".$deal['product']."'";	
							
				if ($tracklist!='') $tracklist .= ',';
				$tracklist .= "'".$deal['track']."'";	
							
				
			}
			
			$freerangehack = "AND product IN (".$productlist.")";
			if ($_SESSION['accountid']==1) $freerangehack = "";
			
			if ($deal['dealtype']=='S')
							$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='V' AND track IN (".$tracklist.") ORDER BY track,distributor";
			else			
				$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='V' ".$freerangehack." AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) ORDER BY track,distributor";


			$sales = Database::QueryGetResults($query);		
			$quantity_total = 0;

			if (!empty($sales))
			{
				$sales = $this->sumSales($sales,'distributor','track');
				// replace track ids by name
				foreach($sales as $i=>$sale)
				{
					$sales[$i]['track'] = TableInfo::DisplayFromType("tracktitle",$sale['track']);
				}
				$sales = $this->SortResults($sales, 'track');
				
				foreach ($sales as $sale)
				{
					$value = Currency::GETRAWVALUE($sale['value']);
        			$worksheet->AddRow(TableInfo::DisplayFromType('distributorshop',
					$sale['distributor']).' digital sales of '.$sale['track'].' ('.$sale['product'].')',$sale['quantity'],Currency::DISPLAY($value));

					$isempty = false;
					$quantity_total += $sale['quantity'];
					$digitaltrack_sales += $value;					
				}
			}

			$balance += $digitaltrack_sales;	
			if ($digitaltrack_sales!=0)
				$worksheet->AddHeaderRow("",$quantity_total,Currency::DISPLAY(round($digitaltrack_sales)));
			
		//--------------------------------------------------------------
		// DIGITAL STREAMS (SINGLES)
		// different cases
		// - product + track present = assign to specific deal
		// - product present only = this is a bundle
		// - track present only = make this invalid (ambigious)
		// - neither - invalid

			$digitalstreams_sales = 0;
			if ($deal['dealtype']=='S')
				$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND track IN (".$tracklist.") ORDER BY track,distributor";
			else			
				$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' ".$freerangehack."AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) ORDER BY track,distributor";
			$sales = Database::QueryGetResults($query);		
			$quantity_total = 0;

			if (!empty($sales))
			{
				$sales = $this->sumSales($sales,'track');
				
				// replace track ids by name
				foreach($sales as $i=>$sale)
				{
					$sales[$i]['track'] = TableInfo::DisplayFromType("tracktitle",$sale['track']);
				}
				$sales = $this->SortResults($sales, 'track');

				foreach ($sales as $sale)
				{
					$value = Currency::GETRAWVALUE($sale['value']);
        			$worksheet->AddRow('Streams '.$sale['track'],$sale['quantity'],Currency::DISPLAY($value));

					$isempty = false;
					$quantity_total += $sale['quantity'];
					$digitalstreams_sales += $value;					
				}
			}

/*			$balance += $digitalstreams_sales;	
			if ($digitalstreams_sales!=0)
				$worksheet->AddHeaderRow("",$quantity_total,Currency::DISPLAY(round($digitalstreams_sales)));
*/
		//--------------------------------------------------------------
		// DIGITAL STREAMS (BUNDLES)

//			$digitalstreams_sales = 0;
			$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND (product='".$deal['product']."' AND track='') ORDER BY track,distributor";
			$sales = Database::QueryGetResults($query);		

			if (!empty($sales))
			{
				$sales = $this->sumSales($sales,'track');
				
				// replace track ids by name
				foreach($sales as $i=>$sale)
				{
					$sales[$i]['track'] = TableInfo::DisplayFromType("tracktitle",$sale['track']);
				}
				$sales = $this->SortResults($sales, 'track');

				foreach ($sales as $sale)
				{
					$value = Currency::GETRAWVALUE($sale['value']);
        			$worksheet->AddRow('Streams of '.$sale['product'],$sale['quantity'],Currency::DISPLAY($value));

					$isempty = false;
					$quantity_total += $sale['quantity'];
					$digitalstreams_sales += $value;					
				}
			}

			$balance += round($digitalstreams_sales);	
			if ($digitalstreams_sales!=0)
				$worksheet->AddHeaderRow("",$quantity_total,Currency::DISPLAY(round($digitalstreams_sales)));

		}			
			
		//--------------------------------------------------------------
		// LICENSING
		$license_quantity = 0;
		$tempworksheet = new Worksheet();
		$licensing_total = $this->ProcessLicensing($deals,$tempworksheet,$split,$iscompilation);
		$worksheet->Append($tempworksheet);
		
		if (!$iscompilation && !empty($this->m_receipts[$deals[0]['crossdealid']]))
		{
			foreach ($this->m_receipts[$deals[0]['crossdealid']] as $receipt)
			{
				$worksheet->AddRow('Compilation revenues from '.$receipt['product'].' '.TableInfo::DisplayFromType("producttitle",$receipt['product']),"",Currency::DISPLAY(round($receipt['payable'])));
				$licensing_total += $receipt['payable'];
				$license_quantity++;
			}
		}

		$balance += $licensing_total;
		if ($license_quantity>0)
			$worksheet->AddHeaderRow("","",Currency::DISPLAY(round($licensing_total)));



		//--------------------------------------------------------------
		// EXPENSES

	
		if ($dealtype=='P' || $dealtype=='S')
		{
			$expense_total = 0;
			foreach($deals as $deal)
			{
				if ($this->IsCompilation($deal['product']) && !$iscompilation)
					continue;

	
				$expenses = $this->GetExpenses($deal,$iscompilation);

				if (!empty($expenses))
				{
					// sort these by track
					foreach ($expenses as $key=>$row)
					{
						$sort[$key] = $row['type'];
					}	
					array_multisort($sort,SORT_ASC,$expenses);
					unset($sort);
					if ($this->m_collate)
						$expenses = Statement::CollateResults($expenses,"type");

					foreach ($expenses as $expense)
					{
						$expense_amount = -(Currency::GETRAWVALUE($expense['amount']));
						$worksheet->AddRow('Expenses : '.$expense['type'].' for '.$expense['product'],"",Currency::DISPLAY($expense_amount));
						$expense_total += $expense_amount;
					}
				}
			}
			$balance += $expense_total;
			if ($expense_total!=0)
				$worksheet->AddHeaderRow("","",Currency::DISPLAY(round($expense_total)));
		}
		

		
		//--------------------------------------------------------------
		// TOTALS

		// Check if the balance is set for this deal - if not display it at the top and set the value to 0
		// If set then save this amount
		if (!$carryover['present'])
		{
			if (!$iscompilation)
				$worksheet->AddHeaderRow("BALANCE FROM PREVIOUS STATEMENT","",Statement::CreateBalanceHTML($deals[0]['crossdealid']));
			else
				$worksheet->AddHeaderRow("BALANCE FROM PREVIOUS STATEMENT","",Statement::CreateBalanceHTML('C'.$deals[0]['uid']));
			$this->m_complete = false;
			$isempty = false;
		}
		
    	$isempty = false;
		$profits = $balance;
		$balanceused = false;
		if ($dealtype=='S')
		{
			// We need to get the original artists split
			$artist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$deal['product']."'");
			$query = "SELECT * FROM deals WHERE product='".$deal['product']."' AND band='".$artist['artist']."' AND dealtype!='S';";
			$original_deal = Database::QueryGetResults($query);
			ts_assert(count($original_deal)!=0,"You have set up a remix split deal for ".$deal['product']." but the original artist has no deal set up.");
			ts_assert(count($original_deal<=1),"Found multiple deals for ".$deal['product']);

			$rate = Database::QueryParam("SELECT * FROM dealterms WHERE dealid=%1",$original_deal[0]['uid']);

			$worksheet->AddHeaderRow("ORIGINAL ARTIST SHARE (".(100-$deal['rate'])."% OF ".Currency::DISPLAY(round($balance)).")","",Currency::DISPLAY(round((100-$deal['rate'])*(-$balance)/100)));
			$balance = $balance * ($deal['rate']/100);

			$label_cut = $balance*((100-$rate[0]['rate'])/100);
			$worksheet->AddHeaderRow("LABEL CUT (".(100-$rate[0]['rate'])."% OF ".Currency::DISPLAY(round($balance)).")","",Currency::DISPLAY(round(-$label_cut)));			
			$balance -= $label_cut;
			$artist_due = $balance;

			$worksheet->AddHeaderRow("LEAVING YOU","",Currency::DISPLAY(round($artist_due)));
		}
		else if ($dealtype=='P' || $dealtype=='R')
		{
			if ($dealtype=='P') 
				$worksheet->AddHeaderRow("THE PROFIT (RECEIPTS-EXPENSES)","",Currency::DISPLAY(round($balance)));
			else if (!$iscompilation)
			{
				$balance *= ($deal['rate']/100);
				$worksheet->AddHeaderRow("YOUR ROYALTIES (".$deal['rate']."%)","",Currency::DISPLAY(round($balance)));
			}
			
			// If a negative balance from previous statement add this in
			if ($carryover['present'] && ($carryover['value']<0))  // was || $iscomp too!! 
			{
				$worksheet->AddHeaderRow("PREVIOUS NEGATIVE BALANCE","",Currency::DISPLAY(round($carryover['value'])));
				$balance += $carryover['value'];
			}
				
			$label_due = 0;
			
			// don't do this for comps unless they are 'treat as third party'
			if (!($iscompilation && !$treatasthirdpartycomp))
			{
				// Calculate label balance
				if ($balance>0 && $dealtype!='R')
				{					
					$label_due = ($balance) * (1-($rate/100));
					$worksheet->AddHeaderRow("LABEL CUT (".(100-$rate)."% OF ".Currency::DISPLAY(round($balance)).")","",Currency::DISPLAY(round(-$label_due)) );
					$balance -= $label_due;
				}
			}
			
			// Display advances for this deal (not for compilations)
//			if (!$iscompilation || $treatasthirdpartycomp)
			{
				if (!$iscompilation)
				{
					// ----- ADVANCES -----//
					$advances_worksheet = new Worksheet;
					$advances = -$this->ProcessAdvances($deals,$advances_worksheet);
					$worksheet->Append($advances_worksheet);
					unset($advances_worksheet);
					$balance += $advances;
				}			
			
				// Display the carryover
				if ($carryover['present'] && $carryover['value']>=0) 
				{
					$worksheet->AddHeaderRow("RESERVE FROM LAST STATEMENT","",Currency::DISPLAY(round($carryover['value'])));
					$balance += $carryover['value'];
				}
				
				$artist_due = 0;			
				if ($balance>0)
				{
					if ($iscompilation)
						$worksheet->AddHeaderRow("LEAVING THE ARTISTS","",Currency::DISPLAY(round($balance)));
					else
						$worksheet->AddHeaderRow("LEAVING YOU","",Currency::DISPLAY(round($balance)));
					$artist_due = $balance;

					// --- Calculate Reserve --- // 
					$reserve = $artist_due * ($reservepercentage/100);	
					$artist_due -= $reserve;
					$worksheet->AddHeaderRow("RESERVE TO CARRY OVER (".$reservepercentage."%)","",Currency::DISPLAY(round(-$reserve)));
					$balance = $reserve;
					
					// --- Minimum Payout --- //
					if (!$iscompilation && $artist_due<Currency::GETRAWVALUE($deal['minpayout']))
					{
						$balance += $artist_due;
						$worksheet->AddHeaderRow("BALANCE TO CARRY OVER (<".Currency::DATABASETODISPLAY($deal['minpayout']).")","",Currency::DISPLAY(round($balance)));
						$artist_due = 0;
					}
		
					if ($iscompilation)
						$worksheet->AddHeaderRow("ARTISTS ARE DUE","",Currency::DISPLAY(round($artist_due)));
					else
						$worksheet->AddHeaderRow("YOU ARE DUE","",Currency::DISPLAY(round($artist_due)));
				}
				else
				{
					$artist_due = 0;				
					// we've made a loss - if this is a compilation and we've made a loss (We are going to transfer)
					if ($iscompilation && $transferloss)
					{
						$artist_due = $balance; // transfer the loss to the artists
					}
				
//					$newcarryover = $artist_due;
					if ($iscompilation)
						$worksheet->AddHeaderRow("ARTISTS DUE","",Currency::DISPLAY(round($artist_due)));
					else
						$worksheet->AddHeaderRow("ARTIST DUE","",Currency::DISPLAY(round($artist_due)));
					
					$worksheet->AddHeaderRow("BALANCE TO NEXT STATEMENT","",Currency::DISPLAY(round($balance)));
				}
				
		/*		if ($iscompilation)
				{
					if ($balance>0)
					{
						$balance *= $split['top'] / $split['bottom'];
						$worksheet->AddHeaderRow("YOUR PRO-RATA SHARE (".$split['top']."/".$split['bottom'].")","",Currency::DISPLAY($balance));
						$artist_due = $balance;
					}
				}
*/
			}
		}
		else
		{
			// Different for royalties
			$worksheet->AddHeaderRow("TOTAL SALES","",Currency::DISPLAY($balance));
			if ($carryover['present'])
			{
				$worksheet->AddHeaderRow("BALANCE FROM PREVIOUS STATEMENT","",Currency::DISPLAY(round($carryover['value'])));
				$balance = $carryover['value']+$balance;
			}
			$artist_due = $balance;
			$newcarryover = 0;
			if (!$iscompilation && $artist_due<Currency::GETRAWVALUE($deal['minpayout']))
			{
				$newcarryover = $artist_due;
				$worksheet->AddHeaderRow("MIN PAYOUT CARRYOVER (<".Currency::DATABASETODISPLAY($deal['minpayout']).")","",Currency::DISPLAY(round($artist_due)));
				$artist_due = 0;
			}			
		}
		
		if ($iscompilation)
		{
//			print_r($artist_due);
			if ($artist_due>0 || $transferloss)
			{
				$artist_due = $artist_due * ($split['top'] / $split['bottom']);
				$worksheet->AddHeaderRow("YOUR PRO-RATA SHARE (".$split['top']."/".$split['bottom'].")","",Currency::DISPLAY(round($artist_due)));
			}
			
			// however if we transfer the loss then the balance is zeroed
			if ($transferloss)
				$balance = 0;
			
			$this->m_receipts[$deals[0]['crossdealid']][] = array('product'=>$deals[0]['product'],'payable'=>$artist_due);
			$this->m_dealset['C'.$deals[0]['uid']] = array('totalup'=>false,'band'=>$deals[0]['band'],'payable'=>round($artist_due),'balance'=>round($balance));

			if ($balance<0)
				$worksheet->AddHeaderRow("BALANCE TO CARRY OVER","",Currency::DISPLAY(round($balance)));
		}
		else 
		{ 
			$this->m_dealset[$deals[0]['crossdealid']] = array('totalup'=>true,'band'=>$deals[0]['band'],'payable'=>round($artist_due),'balance'=>round($balance));
//			$this->m_dealset[$deals[0]['crossdealid']] = array('totalup'=>true,'band'=>$deals[0]['band'],'payable'=>round($artist_due),'balance'=>round($newcarryover));
		}
		
		return $isempty;
	}
		
	
	function GetEmptyCarryovers($bands,$date)
	{
		$bandstemp = str_ireplace(',',"','",$bands);
		$deals = Database::QueryParam("SELECT uid FROM deals WHERE band IN (%1) GROUP BY crossdealid;",$bandstemp);
		$statements = Database::QueryParam("SELECT reserve FROM artiststatements WHERE artist=%1 AND enddate<%2",$bandstemp,$date);
//		PrintTable($deals);
//		PrintTable($statements);
		
	}
	
	/*
		Get Carry Over from previous statement
		Inputs: deal, iscomp
		Output: Previous statement carryover
	*/
	function GetCarryover($band,$deal,$iscomp)
	{
		$carryover['present'] = false;
		$carryover['value']= 0;
		// is this the master one of the dealset?
		if (!$iscomp)
		{
			$query= "SELECT reserve FROM artiststatements WHERE artist='".$band."' AND crossdealid='".$deal['crossdealid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC,uid DESC  LIMIT 1;";
			$laststatement = Database::QueryGetResult($query);
//			print $query;
//			print_r($laststatement);
			
			if (empty($laststatement))
			{
				$query = "SELECT reserve FROM artiststatements WHERE artist='".$band."' AND crossdealid='0' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC,uid DESC LIMIT 1;";
				$laststatement = Database::QueryGetResult($query);
				if (empty($laststatement))
				{
					$laststatement = Database::QueryGetResult("SELECT reserve FROM artiststatements WHERE artist='".$band."' AND crossdealid='".$deal['crossdealid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC, uid DESC LIMIT 1;");
				}
			}
		}
		else
			$laststatement = Database::QueryGetResult("SELECT reserve FROM artiststatements WHERE artist='".$band."' AND crossdealid='C".$deal['uid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC, uid DESC LIMIT 1;");
		
		if (!empty($laststatement))
		{
			$carryover['present'] = true;
			$carryover['value'] = Currency::GETRAWVALUE($laststatement['reserve']);
		}
		else
		{
			if ($iscomp)
			{
				$postid = 'carryoverC'.$deal['uid'];
			}
			else
			{
				$postid = 'carryover'.$deal['crossdealid'];
			}
			if (isset($_POST[$postid]))
			{
				$carryover['present'] = true;
				$carryover['value'] = Currency::GETRAWVALUE(Currency::DISPLAYTODATABASE($_POST[$postid]));
			}
		}
		return $carryover;
	}



	/*
		Process all band deals
	*/
	function ProcessBand(&$worksheet,$band,$treatasthirdpartycomp,$transferloss)
	{
		// Check if any deals missing
		$missingproducts = Database::QueryGetResults("SELECT uid FROM product WHERE artist='".$band."' AND uid NOT IN (SELECT product FROM deals WHERE artist='".$band."');");
		if (!empty($missingproducts))
		{
			foreach($missingproducts as $missing)
			{
				$tmp[] = $missing['uid'];
			}
			
			$this->AddWarning(TableInfo::DisplayFromType("band",$band)." has no deals set up for product(s) ".implode(",",$tmp).". Therefore they will not be accounted for any sales found.");
		}
	
		// Check any drifting track sales
		$drifttracks = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='".$band."' AND uid NOT IN (SELECT track FROM tracksproduct) AND uid NOT IN (SELECT track FROM deals WHERE band='".$band."' AND product='');");
		if (!empty($drifttracks))
		{
			foreach($drifttracks as $track)
			{
				$tmp[] = $track['uid'];
			}
			
			$this->AddWarning(TableInfo::DisplayFromType("band",$band)."'s track(s) ".implode(",",$tmp)." are not assigned to any product or have individual deals for and therefore they will not be accounted for any sales found (if any).");
		}
		
	
		// Get all unique crossdeal set
		$crossdeals = Database::QueryGetResults("SELECT DISTINCT crossdealid FROM deals WHERE band='".$band."' AND (startdate<='".$this->m_enddate."' AND (enddate>='".$this->m_startdate."' OR enddate='0000-00-00'))");
		if (empty($crossdeals)) 
		{
		   $this->AddWarning("No deals for band $band (".TableInfo::DisplayFromType("band",$band).") within specified dates");
			return;
		}
		
		foreach($crossdeals as $crossdealindex=>$crossdeal)
		{
			unset($compdeals);
			unset($maincalc);
			
			$dealterm = Database::QueryGetResult("SELECT * FROM dealterms WHERE dealid=".$crossdeal['crossdealid']." AND `key`='' LIMIT 1;");
			if ($dealterm==false) 
				die ("No default rates found please contact admin");
			$deals = Database::QueryGetResults("SELECT * FROM deals WHERE crossdealid='".$crossdeal['crossdealid']."';");
			foreach($deals as $deal)
			{
				$deal['rate'] = $dealterm['rate'];

				// check product exists - consitency test
				if (!DataTable::GetIDExist('product',$deal['product']))
				{
					$this->AddWarning("Product ".$deal['product']." does not exist but there is a deal set up for it. This product will be skipped.");
					continue;
				}
				
				// is there an advance for this period? if so set it 
				if ($deal['advancepaiddate']<$this->m_startdate)
					$deal['advance'] = 0;
	
				// clip the start/enddates to the period we are looking at
				$deal['enddate']	= $this->m_enddate;
				$deal['startdate']	= $this->m_startdate;

				if ($this->IsCompilation($deal['product']))
				{
					$compdeals[] = $deal;
				}

				$maincalc[] = $deal;
				
			}
						
			// Process comp deals first
			if (!empty($compdeals))
			{
				foreach ($compdeals as $compindex=>$comp)
				{
					$tempset[0] = $comp;		// put in seperate set
					$carryover = $this->GetCarryover($band,$comp,true);
					$dealworksheet = new Worksheet;
					$iscomplete = $this->m_complete;
//					$isempty = $this->ProcessDealSet($dealworksheet,$tempset,$carryover,$comp['reserve'],true,(($compindex==count($compdeals)-1) && ($crossdealindex==count($crossdeals)-1)),$treatasthirdpartycomp,$transferloss);
					$isempty = $this->ProcessDealSet($dealworksheet,$tempset,$carryover,$comp['reserve'],true,false,$treatasthirdpartycomp,$transferloss);
					if (!$isempty && $this->m_includecomps) 
					{
						$worksheet->Append($dealworksheet);
					}
					else
					{
						$this->m_complete = $iscomplete;
					}			
					unset($dealworksheet);
				}
			}
			
			if (!empty($maincalc))
			{
	//			print "  doing main<br>";
				$carryover = $this->GetCarryover($band,$maincalc[0],false);
//				if (empty($compdeals) || count($compdeals)<count($maincalc))
					$this->ProcessDealSet($worksheet,$maincalc,$carryover,$maincalc[0]['reserve'],false,($crossdealindex==count($crossdeals)-1),$treatasthirdpartycomp,$transferloss);	
//				else
//					$this->ProcessDealSet($worksheet,$maincalc,0,0,false,true,$treatasthirdpartycomp,$transferloss);
			}
			
		}
//		print_r($this->m_dealset);
	}

	
	
	


	
	/*
		 CALL THIS FUNCTION TO GENERATE A NEW STATEMENT
	*/
	function Generate($includecomps,					// high detail or low detail report
					  $artistid,					// UID of the artist this statement is for
					  $bandlist,					// CSV list of band UID's to include in this statement
					  $startdate,$enddate,			// start and end dates in SQL format
					  $displaywarnings,
					  $treatasthirdpartycomp,
					  $transferloss
					  )
	{
		// Store the parameters in this object
		$this->m_artistid = $artistid;
		$this->m_startdate = $startdate;
		$this->m_enddate = $enddate;
		$this->m_includecomps = $includecomps;
		$this->m_collate = true;
		
		$this->usedlicences = array();
		unset($this->m_receipts); 
		unset($this->m_dealset);
		
	
			
		// Title and period
		$worksheet = new WorkSheet;
		$worksheet->AddTitleRow(TableInfo::DisplayFromType("artist",$artistid));
		$worksheet->AddHeaderRow("PERIOD ".displaydate2($startdate)." TO ".displaydate2($enddate));


		// For each band in the list, process them
		$bands = explode(",",$bandlist);
		foreach ($bands as $band)
		{
			// Get the split from the database
			$split = Database::QueryGetResult("SELECT split FROM bandartist WHERE artist='$artistid' AND band='$band';");
			if (empty($split)) die( "Line ".__line__ .": Artist is not a member of the specified band. Cannot calculate split");
			$bandsplit[$band] = $split['split'];

			// Add a new account to the statement
			$worksheet->AddTitleRow(strtoupper(TableInfo::DisplayFromType("band",$band)));
			// Go do it
			$this->ProcessBand($worksheet,$band,$treatasthirdpartycomp,$transferloss);
		}
	
		// If more than one dealset then add in summary to tot these up
		if (!empty($this->m_dealset))
		{
			$worksheet->AddTitleRow("SUMMARY");
			$grandtotal = 0;
			$value = 0;
			$currentband = 0;
			foreach ($this->m_dealset as $dealset)
			{
				if ($dealset['totalup'])
				{
					if ($currentband!=$dealset['band'] && $currentband!=0)
					{
						$worksheet->AddHeaderRow(strtoupper(TableInfo::DisplayFromType("band",$currentband))." (".$bandsplit[$currentband]."%)","",Currency::DISPLAY(round($value)));
						$grandtotal += $value;
						$value = 0;
					}
					$value += $dealset['payable'] * ($bandsplit[$dealset['band']]/100);
					$currentband = $dealset['band'];
				}
			}
//			if ($grandtotal!=0)
			{
				$worksheet->AddHeaderRow(strtoupper(TableInfo::DisplayFromType("band",$dealset['band']))." (".$bandsplit[$dealset['band']]."%)","",Currency::DISPLAY(round($value)));
				$grandtotal += $value;
			}
			$worksheet->AddHeaderRow("TOTAL PAYABLE THIS PERIOD","",Currency::DISPLAY(round($grandtotal)));
		}
		
//		print_r($_POST);print "<br><br>";

		// Print warnings
		if ($displaywarnings && !empty($this->m_warnings))
		{
			print '<div style="background-color: #758e98;">';
			print "The following warnings were generated by this statement:<br><br>";
			foreach($this->m_warnings as $warning)
			{
				print "<b>Warning: ".$warning."</b><br>";
			}
			print "<br>Please check and correct before saving.<br></div>";
		}

		// Store this worksheet	
		$this->m_worksheet = $worksheet;
		
	}
	
	function GetResults()
	{
		if (!empty($this->m_dealset))
			return $this->m_dealset;
	}
	
	//--- Accesssor functions
	function GetStartDate()
	{
		return $this->m_startdate;
	}
	
	function GetEndDate()
	{
		return $this->m_enddate;
	}
	
	function GetAmountDue()
	{
		return $this->m_amountdue;
	}
	
	function GetReserve()
	{
		return $this->m_reserve;
	}
	
	function IsComplete()
	{
		return $this->m_complete && !empty($this->m_dealset);
	}
	
	function SaveAsCSV($filename)
	{
		$this->m_worksheet->SaveAsCSV($filename);
	}

	function SaveAsPDF($filename)
	{
		$this->m_worksheet->SaveAsPDF($filename);
	}

	var $m_warnings;
	
	function AddWarning($string)
	{
		$this->m_warnings[] = $string;
	}
	
	function IsCompilation($productid)
	{
		$productartist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$productid."';");
		if ($productartist['artist']==0) return true;
		return false;
	}


}

?>