<?php

require_once('cDatabase.php');

class BeatportExport {

private $out;
private $tab;

private function Tabs()
{
	$tabs = '';
	for ($i=0;$i<$this->tab;++$i)
	{
		$tabs .= "  ";
	}
	return $tabs;
}

private function startElem($element)
{
	
	$this->out[] = $this->Tabs()."<$element>";
	$this->tab += 1;
}

private function endElem($element)
{
	$this->tab -= 1;
	$this->out[] = $this->Tabs()."</$element>";
}

private function addElem($element,$value)
{
	
	$this->out[] = $this->Tabs()."<$element>$value</$element>";
}

// returns the file contents
public function Process($catno)
{
	$this->tab = 0;
	$product = Database::QueryGetResult("SELECT * FROM product WHERE uid='$catno' LIMIT 1;");
	$tracks = Database::QueryGetResults("SELECT * FROM tracksproduct WHERE product='$catno' ORDER BY position;");

	$this->startElem('release');
	$this->addElem('aggregatorName',$_SESSION['accountname']);
	$this->addElem('labelName',$_SESSION['accountname']);
	$this->addElem('UPC_EAN',$product['barcode']);
	$this->addElem('catalogNumber',$product['uid']);
	$this->addElem('coverArtFilename',$product['uid']."Cover.jpg");
	$this->addElem('releaseTitle',$product['title']);
	$this->addElem('releaseDescription','');
	$this->startElem('tracks');
	foreach($tracks as $track)
	{
		$trackinfo = Database::QueryGetResult("SELECT * FROM tracks WHERE uid='".$track['track']."' LIMIT 1;");
		$band = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$trackinfo['artist']."' LIMIT 1;");
		$this->startElem('track');
		$this->addElem('albumOnly','0');
	  	$this->addElem('trackNumber',$track['position']+1);
	  	$this->addElem('ISRC',$trackinfo['uid']);
	  	$this->addElem('trackPublisher',$_SESSION['accountname']);
	  	
	  	// for now filter out brackets
	  	if (stripos($trackinfo['title'],'(')!==false)
		{  	$title = substr($trackinfo['title'],0,stripos($trackinfo['title'],'(')-1);
	  		$mixversion = substr($trackinfo['title'],stripos($trackinfo['title'],'(')+1,stripos($trackinfo['title'],')')-stripos($trackinfo['title'],'(')-1);
	  	}
	  	else
	  	{
	  		$title = $trackinfo['title'];
	  		$mixversion = '';
	  	}
	  		
	  	$this->addElem('trackTitle',$title);
	  	$this->addElem('trackMixVersion',$mixversion);	// need to sort this
	  	
	  	$this->addElem('originalReleaseDate',$product['releasedate']);
	  	$this->startElem('beatportExclusive');
	  	$this->addElem('exclusivePeriod',4);
	  	$this->endElem('beatportExclusive');
	  	$this->startElem('trackArtists');
	  	$this->addElem('artistName',$band['name']);
	  	$this->endElem('trackArtists');
	  	$this->addElem('trackRemixers','');
	  	$this->startElem('trackAudioFile');
	  	$this->addElem('audioFilename',$product['uid']."_".($track['position']+1).".wav");
	  	$this->addElem('digestType','MD5'); 
		$this->addElem('digestValue','');
		$this->endElem('trackAudioFile');
		$this->startElem('countriesAvailable');
		$this->addElem('country','WW');
	    $this->endElem('countriesAvailable');
	  	$this->addElem('trackGenre','Deep House');
		$this->endElem('track');
	}

	$this->endElem('tracks');
	$this->endElem('release');
	
	return implode("\n",$this->out);
}

}


?>