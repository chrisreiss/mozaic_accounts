<?php
// Put at the top of every page
ini_set("include_path", "./:./classes:./thirdparty:./localise");
require_once('util.php' );
require_once('cForm.php');
require_once('cDataTable.php');
require_once('cList.php');
require_once('cAspect.php');
require_once('cDatabase.php');
require_once('cValidatedForm.php');
require_once('cEmail.php');
require_once("class.ezpdf.php");
require_once('cFile.php');
require_once('cDataView.php');
require_once("template.php");

Template::Authenticate();
Template::Top();
Template::TopLevelMenuBar("promos");
Database::Init();

print '<div id="main">';

$aspect = new Aspect;
$aspect->Start("promos","VIEW");

function GeneratePromoMail($name,$uid,$blurb,$message)
{
	$prefs = Database::QueryGetResult("SELECT * FROM szirtest_mozaic.prefs WHERE accountid='".$_SESSION['accountid']."';");
	$url = 'http://www.mozaicaccounts.com/promo/?account='.$_SESSION['accountid'].'&id='.$uid;

	$body = '<html>
<head><title>'.$_SESSION['accountname'].' Promo</title></head>
<body><table width="550" border="3" align="center" cellpadding="5" cellspacing="0" bordercolor="#'.$prefs['colpromoborder'].'" bgcolor="#'.$prefs['colpromobg'].'" bg-color='.$prefs['colpromobg'].'>
<tr><td align="justify" valign="top"><a href="'.$prefs['homesite'].'">
<img src="http://www.mozaicaccounts.com/logos/'.$_SESSION['accountid'].'.jpg" alt="'.$_SESSION['accountname'].'" width="550" align="center" border=0 /></a>
<p style="color: #'.$prefs['colpromotext'].'; font-family: '.$prefs['fontpromo'].'; font-size: 12px;">';
 
	$body .= "Dear ".$name."<br><br>".stripslashes($message)."<br><br>";
	$body .= str_replace("\n","<br>",$blurb)."<br><br>";
	$body .= "Here is your unique promo link:<br><a href='$url'><font color=#".$prefs['colpromomid'].">$url</font></a>";
	
	$body .= "</p></td></tr></table></body></html>";
	return $body;
}

function CalculateQuota($month)
{
  $year = date('Y',time());
  $startdate = date("Y-m-d H:i:s",mktime(0,0,0,$month,1,$year));
  $enddate = date("Y-m-d H:i:s",mktime(0,0,0,$month+1,1,$year));
  $query = "SELECT promo,COUNT(uid),SUM(hitcount),SUM(downloadcount) FROM reactions WHERE reactiondate>='$startdate' AND reactiondate<'$enddate' GROUP BY promo;";
  //print $query."<br>";
  $results = Database::QueryGetResults($query);
  if (empty($results)) return;
  //PrintTable($results);
  // find out the size of the previews
  $totalpreview = 0;
  $totaldownload = 0;
  foreach($results as $row)
  {
	$promo = Database::QueryGetResult("SELECT * FROM promos WHERE uid='".$row['promo']."';");
	$tracksref = Database::QueryGetResults("SELECT track,position FROM tracksproduct WHERE product='".$promo['product']."' ORDER BY position;");
	foreach($tracksref as $i=>$trackref)
	{
		$filename = "../promo/preview/".$_SESSION['accountid']."/".$promo['product']."_".($trackref['position']+1)."_preview.mp3";
		$totalpreview += filesize($filename) * $row['SUM(hitcount)'];
	}
	$filename = "../promo/secure/".$_SESSION['accountid']."/".$promo['product']."_Promo.zip";
	$totaldownload += filesize($filename) * $row['SUM(downloadcount)'];
  }
  print "Download usage this month: ".(round(($totalpreview+$totaldownload)/10000000)/100)."Gb / 5Gb<br>";
}


// define actions here

while (!$aspect->IsComplete())
{

	/*print "<div id='menu'>\n";
	print '<!-- menu bar -->'."\n";
	$tabBar2 = new TabNav;
	$tabBar2->AddTab("Campaigns","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->AddTab("Mail List","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->AddTab("Files","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->AddTab("Settings","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->SetActiveTab($page);
	$tabBar2->Display();
	print '</div>';
*/


//	print_r($_POST); print "<BR>";

	switch($aspect->GetAction())
	{
		case "REMOVE RECIPIENTS":
		{
			foreach($_POST['recipient'] as $recipient)
			{
				Database::Query("DELETE FROM recipients WHERE uid='$recipient';");
				Database::Query("DELETE FROM reactions WHERE recipient='$recipient';");
			}
			// Let drop through
		}
		case "EDIT MAILLIST":
		{
			$dataview = new DataView('recipientid');
		
			// Set up the view drop down, grab all the groups
			$groups = Database::QueryGetResults("SELECT DISTINCT mailgroup FROM recipientgroups");
			// make this into a drop down
			
			$views['all'] = 'All Recipients';
			foreach($groups as $group)
				$views[$group['mailgroup']] = $group['mailgroup']." members";
			
			$dataview->AddViewDropDown($views,'all');
			$currentview = $dataview->GetCurrentView();
			switch ($currentview)
			{
				case "all":
					$recipients = Database::QueryGetResults("SELECT uid,name FROM recipients ORDER BY name;");
					break;
				default:
					$recipients = Database::QueryGetResults("SELECT uid,name FROM recipients WHERE uid IN (SELECT recipient FROM recipientgroups WHERE mailgroup='$currentview') ORDER BY name;");
					break;
			}
			
			// Get stats for these recipients
	//		Database::QueryGetResults("SELECT COUNT('uid') FROM reactions GROUP BY recipient");
			
			foreach ($recipients as $i=>$r)
			{
				$viewed = Database::QueryGetResult("SELECT COUNT(uid) as chit FROM reactions WHERE recipient='".$r['uid']."' AND hitcount>0;");
				$sent = Database::QueryGetResult("SELECT COUNT(uid) as creact FROM reactions WHERE recipient='".$r['uid']."' ");
				$reacted = Database::QueryGetResult("SELECT COUNT(uid) as creact FROM reactions WHERE recipient='".$r['uid']."' AND reaction!='';");
				
				$recipients[$i]['sent'] = $sent['creact'];
				$recipients[$i]['viewed'] = $viewed['chit'];
				$recipients[$i]['reacted'] = $reacted['creact'];
				unset($recipients[$i]['uid']);
	
			}

			
			$dataview->SetFromDatabaseResults($recipients);			
			$dataview->AddAction("ADD RECIPIENT",false);
			$dataview->AddAction("EDIT RECIPIENT",true);
			$dataview->AddAction("REMOVE RECIPIENT",true);
			//$dataview->SetConfirm("REMOVE RECIPIENT","Are you sure?");
			$dataview->AddAction("IMPORT",false);
			$dataview->AddAction("BACK TO PROMOS",false);

			$aspect->Attach($dataview);
			$aspect->Present();
		
			break;
		}
		case "CREATE MAILGROUP":
		{
			$form = new Form;
			$form->AddField("Mailgroup Name","name","text64","");
			$form->AddAction("CREATE NEW GROUP");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "DELETE MAILGROUP":
		{
			$aspect->SetNextAction("EDIT MAILLIST");
			Database::Query("DELETE FROM recipientgroups WHERE mailgroup='".$_POST['lastviewselect']."'");
			break;
		}
		case "IMPORT":
		{
			$form = new Form;
			$form->AddField("VCF","importfile","file","");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddAction("PROCESS");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "PROCESS":
		{
			$aspect->SetNextAction("VIEW");
			$fp = fopen($_FILES['importfile']['tmp_name'],"r");
			$entries = 0;
			while (($data = fgets($fp)) !== FALSE) 
			{
				$data = str_replace("\0","",$data);
				if (substr($data,0,3)=="FN:")
					$name = substr($data,3);
				if (substr($data,0,5)=="EMAIL")
				{
					$email = substr($data,strpos($data,":")+1);
					print "Importing contact $name at ($email)<br>";
					Database::Query("INSERT INTO recipients (name,email) VALUES ('$name','$email');");
					$entries++;
				}
			}
			print "Processed $entries entires.<br>";
			fclose($fp);
			$form = new Form;
			$form->AddAction("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;

		}
		case "ADD RECIPIENT":
		{
			$form = new ValidatedForm("recipients","name,email,affiliations");
			$form->AddField("Mailgroups","mailgroups","mailgroups",true);
			$form->AddAction("SUBMIT NEW RECIPIENT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW RECIPIENT":
		{
			$aspect->SetNextAction("EDIT MAILLIST");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			DataTable::InsertFromPost("recipients","name,email,affiliations",$aspect);
			// Process mailgroups
			if (!empty($_POST['mailgroups']))
			{
				foreach($_POST['mailgroups'] as $i=>$group)
				{
					Database::Query("INSERT INTO recipientgroups (`recipient`,`mailgroup`) VALUES ('".$aspect->GetVar('recipientid')."','".$group."')");
				}
			}
			break;
		}
		case "SUBMIT EDIT RECIPIENT":
		{
			$aspect->SetNextAction("EDIT MAILLIST");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			DataTable::UpdateFromPost("recipients",$aspect->GetVar('recipientid'),"name,email,affiliations",$aspect);

			Database::Query("DELETE FROM recipientgroups WHERE recipient='".$aspect->GetVar('recipientid')."'");
			// Process mailgroups
			if (!empty($_POST['mailgroups']))
			{
				foreach($_POST['mailgroups'] as $i=>$group)
				{
					Database::Query("INSERT INTO recipientgroups (`recipient`,`mailgroup`) VALUES ('".$aspect->GetVar('recipientid')."','".$group."')");
				}
			}
			break;
		}
		case "EDIT RECIPIENT":
		{
			$recipientid = $_POST['recipientid'][0];
			$form = new ValidatedForm("recipients","name,email,affiliations");
			$form->SetValuesFromDB("recipients",$recipientid);
			$form->AddField("Mailgroups","mailgroups","mailgroups",true);
			$res = Database::QueryGetSingleResults("SELECT mailgroup FROM recipientgroups WHERE recipient='".$recipientid."'");
			if (!empty($res))
				$form->SetValue('mailgroups',implode(",",$res));
			$form->AddAction("SUBMIT EDIT RECIPIENT",true);	// validate before sending
			$form->AddHidden('recipientid',$recipientid);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE RECIPIENT":
		{
			$aspect->SetNextAction("EDIT MAILLIST");
			foreach($_POST['recipientid'] as $recipient)
			{
				DataTable::Delete("recipients",$recipient);
				Database::Query("DELETE FROM recipientgroups WHERE recipient='".$recipientid."'");
			}
			break;
		}		
		case "VIEW REACTIONS":
		{
			$reactions = Database::QueryGetResults("SELECT * FROM reactions WHERE promo='".$aspect->GetVar('promoid')."' AND reactiondate!='0000-00-00 00:00:00' ORDER BY reactiondate DESC;");
			
			$html = "<table width=100%><tr><td><b>Include</b></td><td><b>Name</b></td><td><b>Date</b></td><td width=50%><b>Reaction</b></td></tr>\n";
			foreach ($reactions as $reaction)
			{
				$recipient = Database::QueryGetResult("SELECT name FROM recipients WHERE uid='".$reaction['recipient']."' LIMIT 1;");
	
				$html .= "<tr><td><input type=checkbox name='display[]' value='".$reaction['recipient']."' checked></td><td>";
				$html .=  $recipient['name'];
				$html .= "</td><td>";
				$html .= $reaction['reactiondate'];
				$html .= "</td><td>";
				if ($reaction['reaction']=="")
					$html .= "Didn't Like";
				else
					$html .= $reaction['reaction'];
				$html .= "</td></tr>";
			}
			$html .= "</table>\n";
			$form = new Form;
			$form->InsertHTML($html);
			$form->AddHidden("promo",$aspect->GetVar('promoid'));
			$form->AddAction("MAIL ME REACTIONS");
			$form->AddAction("GENERATE PRESS RELEASE");
			$form->AddCancel("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			
			print "<br>";
			print "Visited but did not react<br>";
			$reactions = Database::QueryGetResults("SELECT recipient,hitcount FROM reactions WHERE promo='".$aspect->GetVar('promoid')."' AND hitcount>0 AND reactiondate='0000-00-00 00:00:00';");
			print "<table width=100%><tr><td><b>Name</b></td><td><b>Visits</b></td>/tr>\n";
			foreach ($reactions as $reaction)
			{
				$recipient = Database::QueryGetResult("SELECT name FROM recipients WHERE uid='".$reaction['recipient']."' LIMIT 1;");
				print "<tr><td>";
				print $recipient['name'];
				print "</td><td>";
				print $reaction['hitcount'];
				print "</td></tr>\n";
			}
			print "</table>\n";
			break;
		}

	   default:
	   {
	   		$aspect->DefaultAction();
	   }
	}
	
}	

$aspect->End();
print '</div>';
Template::Bottom();
?>

