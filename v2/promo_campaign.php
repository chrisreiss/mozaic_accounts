<?php
// Put at the top of every page
ini_set("include_path", "./:./classes:./thirdparty:./localise");
require_once('util.php' );
require_once('cForm.php');
require_once('cDataTable.php');
require_once('cList.php');
require_once('cAspect.php');
require_once('cDatabase.php');
require_once('cValidatedForm.php');
require_once('cEmail.php');
require_once("class.ezpdf.php");
require_once('cFile.php');
require_once('cDataView.php');
require_once("template.php");

Template::Authenticate();
Template::Top();
Template::TopLevelMenuBar("promos");
Database::Init();

print '<div id="main">';

$aspect = new Aspect;
$aspect->Start("promos","VIEW");

function GeneratePromoMail($name,$uid,$blurb,$message)
{
	$prefs = Database::QueryGetResult("SELECT * FROM szirtest_mozaic.prefs WHERE accountid='".$_SESSION['accountid']."';");
	$url = 'http://www.mozaicaccounts.com/promo/?account='.$_SESSION['accountid'].'&id='.$uid;

	$body = '<html>
<head><title>'.$_SESSION['accountname'].' Promo</title></head>
<body><table width="550" border="3" align="center" cellpadding="5" cellspacing="0" bordercolor="#'.$prefs['colpromoborder'].'" bgcolor="#'.$prefs['colpromobg'].'" bg-color='.$prefs['colpromobg'].'>
<tr><td align="justify" valign="top"><a href="'.$prefs['homesite'].'">
<img src="http://www.mozaicaccounts.com/logos/'.$_SESSION['accountid'].'.jpg" alt="'.$_SESSION['accountname'].'" width="550" align="center" border=0 /></a>
<p style="color: #'.$prefs['colpromotext'].'; font-family: '.$prefs['fontpromo'].'; font-size: 12px;">';
 
	$body .= "Dear ".$name."<br><br>".stripslashes($message)."<br><br>";
	$body .= str_replace("\n","<br>",$blurb)."<br><br>";
	$body .= "Here is your unique promo link:<br><a href='$url'><font color=#".$prefs['colpromomid'].">$url</font></a>";
	
	$body .= "</p></td></tr></table></body></html>";
	return $body;
}

function CalculateQuota($month)
{
  $year = date('Y',time());
  $startdate = date("Y-m-d H:i:s",mktime(0,0,0,$month,1,$year));
  $enddate = date("Y-m-d H:i:s",mktime(0,0,0,$month+1,1,$year));
  $query = "SELECT promo,COUNT(uid),SUM(hitcount),SUM(downloadcount) FROM reactions WHERE reactiondate>='$startdate' AND reactiondate<'$enddate' GROUP BY promo;";
  //print $query."<br>";
  $results = Database::QueryGetResults($query);
  if (empty($results)) return;
  //PrintTable($results);
  // find out the size of the previews
  $totalpreview = 0;
  $totaldownload = 0;
  foreach($results as $row)
  {
	$promo = Database::QueryGetResult("SELECT * FROM promos WHERE uid='".$row['promo']."';");
	$tracksref = Database::QueryGetResults("SELECT track,position FROM tracksproduct WHERE product='".$promo['product']."' ORDER BY position;");
	foreach($tracksref as $i=>$trackref)
	{
		$filename = "../promo/preview/".$_SESSION['accountid']."/".$promo['product']."_".($trackref['position']+1)."_preview.mp3";
		$totalpreview += filesize($filename) * $row['SUM(hitcount)'];
	}
	$filename = "../promo/secure/".$_SESSION['accountid']."/".$promo['product']."_Promo.zip";
	$totaldownload += filesize($filename) * $row['SUM(downloadcount)'];
  }
  print "Download usage this month: ".(round(($totalpreview+$totaldownload)/10000000)/100)."Gb / 5Gb<br>";
}


while (!$aspect->IsComplete())
{

	/*print "<div id='menu'>\n";
	print '<!-- menu bar -->'."\n";
	$tabBar2 = new TabNav;
	$tabBar2->AddTab("Campaigns","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->AddTab("Mail List","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->AddTab("Files","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->AddTab("Settings","subpage","import",STRING_TOOLTIP_BUTTON_INOUT);
	$tabBar2->SetActiveTab($page);
	$tabBar2->Display();
	print '</div>';
*/


//	print_r($_POST); print "<BR>";

	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			CalculateQuota(1);
			
			$dataview = new DataView('promoid');
		
			// Set up the view drop down, grab all the groups
			$promos = Database::QueryGetResults("SELECT uid,product FROM promos");
			// make this into a drop down
			
			foreach($promos as $promo)
				$views[$promo['uid']] = $promo['product']." campaign";
			
			$dataview->AddViewDropDown($views,$promos[0]['uid']);
			$currentview = $dataview->GetCurrentView();
			$reactions = Database::QueryGetResults("SELECT recipients.name,reactions.reaction FROM recipients INNER JOIN reactions ON reactions.recipient=recipients.uid WHERE reactions.promo='$currentview' AND reactions.reaction!='';");
			$dataview->SetFromDatabaseResults($reactions);			
			$dataview->AddAction("ADD PROMO",false);
			$dataview->AddAction("EDIT PROMO",true);
			$dataview->AddAction("UPLOAD MEDIA",true);
			$dataview->AddAction("TEST",true);
			$dataview->AddAction("PUBLISH",true);
			$dataview->AddAction("REMOVE PROMO",true);
			//$list->SetConfirm("REMOVE PROMO","Are you sure?");
//			$dataview->AddAction("EDIT MAILLIST",false);
			$dataview->AddAction("VIEW REACTIONS",true);
			$dataview->AddAction("FIND LINK",true);

	//		$list->AddAction("STATS");
			$aspect->Attach($dataview);
			$aspect->Present();
			break;
       }	
	   case "REMOVE PROMO":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// Delete reactions also
			Database::Query("DELETE FROM reactions WHERE promo='".$_POST['promoid']."';");
			DataTable::Delete("promos",$_POST['promoid']);
			break;
		}
		case "EDIT PROMO":
		{
			$form = new ValidatedForm("promos","product,startdate,expiredate,blurb");
			$form->SetValuesFromDB("promos",$aspect->GetVar("promoid"));
			$form->AddAction("SUBMIT EDIT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->PreserveVar('promoid');
			$aspect->Present();
			break;
		}
		case "ADD PROMO":
		{
			$form = new ValidatedForm("promos","product,startdate,expiredate,blurb");
			$form->AddAction("SUBMIT NEW",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			DataTable::InsertFromPost("promos","product,startdate,expiredate,blurb",$aspect);
			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			DataTable::UpdateFromPost("promos",$aspect->GetVar('promoid'),"product,startdate,expiredate,blurb",$aspect);
			break;
		}
		case "TEST":
		{
			Database::ConnectAdmin("szirtest_mozaic");
			$prefs = Database::QueryGetResult("SELECT * FROM prefs WHERE accountid='".$_SESSION['accountid']."';");
			Database::Connect();
			$promo = Database::QueryGetResult("SELECT product,blurb FROM promos WHERE uid='".$aspect->GetVar('promoid')."';");
			$product = Database::QueryGetResult("SELECT title,artist FROM product WHERE uid='".$promo['product']."';");
			$artist = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$product['artist']."';");
			$user = Database::QueryGetResult("SELECT email FROM szirtest_mozaic.users WHERE userid='".$_SESSION['userid']."';");
			$uid = md5($aspect->GetVar('promoid')."test");
			Database::Query("INSERT INTO reactions (uid,promo,recipient,sentdate) VALUES ('".$uid."','".$aspect->GetVar('promoid')."','0',NOW());");
			$subject = $_SESSION['accountname']." Promo (TEST) : ".$artist['name']." - ".$product['title'];
			$body = GeneratePromoMail($_SESSION['displayname'],$uid,$promo['blurb'],"This is a test promo delivery.");
			Email::SendHTML($user['email'], $body, $subject, $prefs['senderemail'], $_SESSION['accountname']." Promo Service");
			print "Test promo sent to ".$user['email']."<br>";
			$form = new Form;
			$form->AddAction("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE RECIPIENTS":
		{
			foreach($_POST['recipient'] as $recipient)
			{
				Database::Query("DELETE FROM recipients WHERE uid='$recipient';");
				Database::Query("DELETE FROM reactions WHERE recipient='$recipient';");
			}
			// Let drop through
		}
		case "FIND LINK":
		{
			$recipients = Database::QueryGetResults("SELECT * FROM recipients ORDER BY name;");
			$list = "";
			foreach ($recipients as $index=>$recipient)
			{
				if ($index>0) $list.=",";
				$list .= $recipient['uid']."=".$recipient['name'];
			}			
			$form = new Form;
			$form->AddHidden("promoid",$aspect->GetVar('promoid'));
			$form->AddSelectionList("Recipients","recipients",$list,"A");
			$form->AddAction("LINK");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "LINK":
		{
			$uid = md5($aspect->GetVar('promoid').$_SESSION['accountid'].$aspect->GetVar('recipients'));
			$url = 'http://www.mozaicaccounts.com/promo/?account='.$_SESSION['accountid'].'&id='.$uid;
			print $url."<br>";
			$aspect->Present();
			break;
		}
		case "PUBLISH":
		{
			$recipients = Database::QueryGetResults("SELECT * FROM recipients ORDER BY name;");
			$list = "A=ALL RECIPIENTS";
			// add in mailgroups
			$groups = Database::QueryGetSingleResults("SELECT DISTINCT mailgroup FROM recipientgroups;");
			if (!empty($groups))
			{
				foreach ($groups as $group)
				{
					$list .= ",*".$group."=".strtoupper($group)." Members";
				}
			}
			$list.=",";
			foreach ($recipients as $recipient)
			{
				$list .= ",".$recipient['uid']."=".$recipient['name'];
			}			
			$form = new Form;
			$form->AddHidden("promoid",$aspect->GetVar('promoid'));
			$form->AddSelectionList("Recipients","recipients",$list,"A");
			$form->AddField("Email Message","message","multitext","We have sent you a link to our latest promo download. We would be very grateful if you have time to have a listen and give us your feedback.");
			
			
			$form->AddSelectionList("Send Options","sendoptions","0=Send to all selected,1=Send only if not sent before,2=Send only if not reacted",0);
			//$form->AddField("Only send to people who haven't reacted","nonreactonly","checkbox",true);
			//$form->AddField("Don't mail people I've mailed previously","noduplicates","checkbox",true);
			$form->AddAction("SEND");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SEND":
		{
			Database::ConnectAdmin("szirtest_mozaic");
			$prefs = Database::QueryGetResult("SELECT * FROM prefs WHERE accountid='".$_SESSION['accountid']."';");
			Database::Connect();

			$promo = Database::QueryGetResult("SELECT product,blurb FROM promos WHERE uid='".$aspect->GetVar('promoid')."';");
			$product = Database::QueryGetResult("SELECT title,artist FROM product WHERE uid='".$promo['product']."';");
			$artist = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$product['artist']."';");

			$sendoption = $aspect->GetVar('sendoptions');
			$target = $aspect->GetVar('recipients');
			if ($target=='A')
			{
				//if ($_POST['nonreactonly'])
				$recipients = Database::QueryGetResults("SELECT * FROM recipients;");
			}
			else if (substr($target,0,1)=='*')
			{
				$group = substr($target,1);
				$recipients = Database::QueryGetResults("SELECT * FROM recipients WHERE uid IN (SELECT recipient FROM recipientgroups WHERE mailgroup='$group')");
			}
			else
				$recipients = Database::QueryGetResults("SELECT * FROM recipients WHERE uid='".$target."';");
			
		
			$count = 0;
			$oldcount = 0;
			foreach ($recipients as $recipient)
			{
				// generate unique id;
				$uid = md5($aspect->GetVar('promoid').$_SESSION['accountid'].$recipient['uid']);
				$existuid = Database::QueryGetResult("SELECT uid,reactiondate FROM reactions WHERE uid='$uid';");
				if ($existuid!=false)
				{
					$oldcount++;
					if ($sendoption==1) continue;
					if ($sendoption==2 && $existuid['reactiondate']!='0000-00-00 00:00:00') continue;
				}
				else
				{
					$query = "INSERT INTO reactions (uid,promo,recipient,sentdate) VALUES ('".$uid."','".$aspect->GetVar('promoid')."','".$recipient['uid']."',NOW());";
					Database::Query($query);
				}
				$count++;
				print "Mailing: ".$recipient['name']." at ".$recipient['email']."<br>";
				
				$subject = $_SESSION['accountname']." Promo : ".$artist['name']." - ".$product['title'];
				$body = GeneratePromoMail($recipient['name'],$uid,$promo['blurb'],$aspect->GetVar('message'));
				Email::SendHTML($recipient['email'], $body, $subject, $prefs['senderemail'], $_SESSION['accountname']." Promo Service");
				
				// TEST DEbG CODE - comment OUT
//				Email::SendHTML("tomszirtes@googlemail.com", $body, $subject, "promos@darkenergyrecordings.com", "Dark Energy Recordings Promo Service");
//				break;
			}
			print "<br>$count new promos have been sent<br>";
			print "$oldcount promos sent previously<br>";
			$form = new Form;
			$form->AddAction("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "UPLOAD MEDIA":
		{
			print "Upload media (we suggest you break this up into small chunks as html uploading is a bit unreliable)<br>";
			$promo = Database::QueryGetResult("SELECT product,blurb,buylink FROM promos WHERE uid='".$aspect->GetVar('promoid')."';");
			$form = new Form;
			$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product='".$promo['product']."' ORDER BY position");
			foreach($tracks as $index=>$track)
			{
				$destname = $promo['product']."_".($index+1)."_preview.mp3";
				$trackinfo = Database::QueryGetResult("SELECT title FROM tracks WHERE uid='".$track['track']."'");
				if (!file_exists(File::GetPromoPath("preview",$destname)))
					$form->AddField("<b>".$trackinfo['title']." preview mp3</b>",$index+1,"file","");
				else
					$form->AddField($trackinfo['title']." preview mp3 (uploaded)",$index+1,"file","");
			}

			$destname = $promo['product']."promo.jpg";
			if (!file_exists(File::GetPromoPath("preview",$destname)))
				$form->AddField("<b>300x300 jpeg image</b>","image","file","");
			else
				$form->AddField("300x300 jpeg image (uploaded)","image","file","");

			$destname = $promo['product'].".zip";
			if (!file_exists(File::GetPromoPath("secure",$destname)))
				$form->AddField("<b>Product Pack Zip</b>","zip","file","");
			else
				$form->AddField("Product Pack Zip (uploaded)","zip","file","");

			
			$form->AddHidden("promo",$aspect->GetVar('promoid'));
			$form->AddHidden("product",$promo['product']);
			$form->AddAction("UPLOAD");
			$form->AddCancel("CANCEL");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "UPLOAD":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			foreach($_FILES as $name=>$file)
			{
				if ($file['error']==0)
				{
					print 'Processing '.$file['name'].' ... ';
					if ($name=='image'){
						$destname = $aspect->GetVar('product')."promo.jpg";
						$destpath = File::GetPromoPath("preview",$destname);
					}
					else if ($name=='zip') {
						$destname = $aspect->GetVar('product').".zip";
						$destpath = File::GetPromoPath("secure",$destname);
					}
					else {
						$destname = $aspect->GetVar('product')."_".$name."_preview.mp3";
						$destpath = File::GetPromoPath("preview",$destname);
					}
					
					if (move_uploaded_file($file['tmp_name'],$destpath)==FALSE)
					{
					 	print "Failed<br>";
					}
					else
					{
						print "Uploaded to ".$destname."<br>";
					}
				}
			}			
			break;
		}
		case "VIEW REACTIONS":
		{
			$reactions = Database::QueryGetResults("SELECT * FROM reactions WHERE promo='".$aspect->GetVar('promoid')."' AND reactiondate!='0000-00-00 00:00:00' ORDER BY reactiondate DESC;");
			
			$html = "<table width=100%><tr><td><b>Include</b></td><td><b>Name</b></td><td><b>Date</b></td><td width=50%><b>Reaction</b></td></tr>\n";
			foreach ($reactions as $reaction)
			{
				$recipient = Database::QueryGetResult("SELECT name FROM recipients WHERE uid='".$reaction['recipient']."' LIMIT 1;");
	
				$html .= "<tr><td><input type=checkbox name='display[]' value='".$reaction['recipient']."' checked></td><td>";
				$html .=  $recipient['name'];
				$html .= "</td><td>";
				$html .= $reaction['reactiondate'];
				$html .= "</td><td>";
				if ($reaction['reaction']=="")
					$html .= "Didn't Like";
				else
					$html .= $reaction['reaction'];
				$html .= "</td></tr>";
			}
			$html .= "</table>\n";
			$form = new Form;
			$form->InsertHTML($html);
			$form->AddHidden("promo",$aspect->GetVar('promoid'));
			$form->AddAction("MAIL ME REACTIONS");
			$form->AddAction("GENERATE PRESS RELEASE");
			$form->AddCancel("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			
			print "<br>";
			print "Visited but did not react<br>";
			$reactions = Database::QueryGetResults("SELECT recipient,hitcount FROM reactions WHERE promo='".$aspect->GetVar('promoid')."' AND hitcount>0 AND reactiondate='0000-00-00 00:00:00';");
			print "<table width=100%><tr><td><b>Name</b></td><td><b>Visits</b></td>/tr>\n";
			foreach ($reactions as $reaction)
			{
				$recipient = Database::QueryGetResult("SELECT name FROM recipients WHERE uid='".$reaction['recipient']."' LIMIT 1;");
				print "<tr><td>";
				print $recipient['name'];
				print "</td><td>";
				print $reaction['hitcount'];
				print "</td></tr>\n";
			}
			print "</table>\n";
			break;
		}
		case "MAIL ME REACTIONS":
		{
		/*	$additional_headers="From: promos@darkenergyrecordings.com";
	$additional_parameters="-f promos@darkenergyrecordings.com";

			sendmail_mail("tomszirtes@googlemail.com","test sendmail","are you receiving this?",$additional_headers,$additional_parameters);
*/
			$aspect->SetNextAction("VIEW");

			$body = "DJ Reactions\n\n";
			$reactions = Database::QueryGetResults("SELECT * FROM reactions WHERE promo='".$aspect->GetVar('promo')."' AND reactiondate!='0000-00-00 00:00:00' ORDER BY reactiondate DESC;");
			foreach ($reactions as $reaction)
			{
				$recipient = Database::QueryGetResult("SELECT name FROM recipients WHERE uid='".$reaction['recipient']."' LIMIT 1;");
				if (array_search($reaction['recipient'],$_POST['display']))
				{
					$body.= "'".str_replace("\n","",$reaction['reaction'])."'\n".$recipient['name']."\n\n";
				}
			}

/* 			print "<table class='spreadsheet' width=100%><tr><td>";
 			print str_replace("\n","<br>",$body);
 			print "</td></tr></table>";
*/
			Email::Send($_SESSION['email'], $body, "Reactions", "promos@darkenergyrecordings.com", "Dark Energy Recordings");
			print "Sent mail to ".$_SESSION['email'];
 
			break;
		}
		case "GENERATE PRESS RELEASE":
		{
			$promo = Database::QueryGetResult("SELECT product,blurb,buylink FROM promos WHERE uid='".$aspect->GetVar('promo')."';");
			$product = Database::QueryGetResult("SELECT uid,title,artist,format,releasedate FROM product WHERE uid='".$promo['product']."';");
			$artist = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$product['artist']."';");

			$prefs = Database::QueryGetResult("SELECT * FROM szirtest_mozaic.prefs WHERE accountid='".$_SESSION['accountid']."';");
			// put all variable data in vars here...
			$color['background'] = '#'.$prefs['colpromobg']; //'#00000';
			$color['border'] = '#'.$prefs['colpromoborder']; //'#444444';
			$color['text'] = '#'.$prefs['colpromotext']; //'#FFFFFF';
			$color['link'] = '#'.$prefs['colpromolink']; //'#999999';
			$font = $prefs['fontpromo']; //'Verdana';
			$website = $prefs['homesite'];  //'http://www.darkenergyrecordings.com';
			$artworkpath = 'promo/preview/'.$_SESSION['accountid'].'/'.$product['uid'].'promo.jpg';
			$artworkurl = 'http://www.mozaicaccounts.com/'.$artworkpath;
			$buypath = $promo['buylink'];
			
			$labellogopath = "http://www.mozaicaccounts.com/logos/".$_SESSION['accountid'].".jpg";
			
		
			$body =  '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
			$body .= '<html><head><title>'.$_SESSION['accountname'].' Release</title></head>';
			$body .= '<body>';
			$body .= '<table width="600" border="3" align="center" cellpadding="5" cellspacing="0" bordercolor="'.$color['border'].'" bgcolor="'.$color['background'].'" bg-color="'.$color['background'].'">';
			$body .= '<tr><td align="center" valign="top">';
		//	if (file_exists($labellogopath))
				$body .= '<img src="'.$labellogopath.'" width=600>';
		//	else
		//		$body .= '<strong>'.$_SESSION['accountname'].'</strong>';
			$body .= '</td></tr><tr><td>';
			$body .= '<table border=0><tr><td width=350>';
			$body .= '<p style="color: '.$color['text'].'; text-align: justify; font-family: '.$font.', monospace; font-size: 12px;">';
			$body .= 'Artist: '.$artist['name'].'<br />';
			$body .= 'Title: '.$product['title'].'<br />';
			$body .= 'Format: '.$product['format'].'<br />';
			$body .= 'Cat No: '.$product['uid'].'<br />';
			$body .= 'Release Date: '.$product['releasedate'];
			$body .= '<br><br>';
			$body .= '<a href="'.$buypath.'"><font color="'.$color['link'].'">Click here to Buy</font></a><br /><br /><br />';
			$body .= str_replace("\n",'<br />',strip_tags($promo['blurb']));
			$body .= '</p></td>';
 			$body .= '<td valign="top"><p><a href="'.$buypath.'"><img src="'.$artworkurl.'" width=250 align="left" border=0 /></a></p></td>';
			$body .= '</tr></table></td></tr><tr><td>'."\n";
     	    $body .= '<p style="color: '.$color['text'].'; font-family: '.$font.', monospace; font-size: 12px;">';
    		$body .= '<strong>DJ Reactions</strong><br /><br />'."\n";

			$reactions = Database::QueryGetResults("SELECT * FROM reactions WHERE promo='".$aspect->GetVar('promo')."' AND reactiondate!='0000-00-00 00:00:00' ORDER BY reactiondate DESC;");
			foreach ($reactions as $reaction)
			{
				$recipient = Database::QueryGetResult("SELECT name FROM recipients WHERE uid='".$reaction['recipient']."' LIMIT 1;");
				if (array_search($reaction['recipient'],$_POST['display']))
				{
					$react = $reaction['reaction'];
					$react = strip_tags($react);
					$react = str_replace("\n","",$react);
					$react = str_replace("\r","",$react);
					$body .= '<i>&quot;'.$react.'&quot;</i><br />'.str_replace("\r","",str_replace("\n","",$recipient['name'])).'<br /><br />'."\n"; 
				}
			}

 			$body .= '</p></td></tr></table></body></html>';
    
    		$filename = $promo['product']."PressRelease.htm";
    		$path = File::GetPath($filename);
			$fd = fopen($path ,"wb");
			fwrite($fd,$body,strlen($body));
			fclose($fd);
			print "<p>Press Release Generated</p>";
			print "<a href='download.php?file=".$filename."'>Click here to download</a>";
			
			Email::SendHTML($_SESSION['email'], $body, "Press Release ".$promo['product'], "info@darkenergyrecordings.com", $_SESSION['accountname']);

			$form = new Form();
			$form->AddAction("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
	   default:
	   {
	   		$aspect->DefaultAction();
	   }
	}
	
}	

$aspect->End();
print '</div>';
Template::Bottom();
?>

