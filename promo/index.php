<?php
	include_once('../cDatabase.php');
	if (!isset($_GET['id']) || !isset($_GET['account'])) 
	{
		die ("No access to this page"); 
	}
	Database::Connect("mozaic");
	$query = "SELECT * FROM accounts WHERE uid='".$_GET['account']."'";
	$accountinfo = Database::QueryGetResult($query);
	$accountname = $accountinfo['name'];
	$database = $accountinfo['database'];
	$prefs = Database::QueryGetResult("SELECT * FROM prefs WHERE accountid='".$_GET['account']."'");
	Database::Connect("mozaic_".$database);

	$reaction = Database::QueryGetResult("SELECT * FROM reactions WHERE uid='".$_GET['id']."' LIMIT 1;" );
	if ($reaction===false) die ("Invalid request");
	$promo = Database::QueryGetResult("SELECT * FROM promos WHERE uid='".$reaction['promo']."';");
	if ($promo===false) die ("Invalid promo");
	if ($promo['expiredate']<date('Y-m-d')) die ("Sorry but this promo has now expired");
	Database::Query("UPDATE reactions SET hitcount=(hitcount+1) WHERE uid='".$_GET['id']."';" );
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?=$accountname?> Promo Service</title>

<style type="text/css">
<!--
body,textarea {
	font-family: 'Myriad Pro',<?=$prefs['fontpromo']?>;
	font-size: 12pt;
	text-align: justify;
	}

body {
	color: #<?=$prefs['colpromotext']?>;
	background-color: #<?=$prefs['colpromobg']?>;

}
td{
	background-color: blue;
}

h2 {
	font-size: 14pt;
	font-weight: bold;
}

img { border: none; }

.container {
	width: 640px;
	margin: 0 auto;
}

.artwork {
	width: 240px;
	float: left;	
	margin-right: 20px;
}

.logo {
	margin-left: 20px;
	padding-bottom: 20px;
}

textarea { width: 640px; background-color: white; color: black; border:0; padding: 10px; margin-bottom: 10px}
textarea:focus {
	outline: none;
}

input {
	border: 0px;
	background-color: #505050;
	font-family: 'Myriad Pro';
	font-size: 12pt;
	padding: 10px;
	cursor: pointer;
}

input:hover{
	background-color: #808080;
}

a,a:visited{
	color: white;
}

.audiojs {
	width: 100%;
}
ol{ background: #404040; margin-top: 0; padding-top:0;}
ol a{ text-decoration: none; color: #a0a0a0; line-height: 20pt;}
ol a:hover{ color: white; }

.playing a{ color: white; }

-->
</style>
</head>

<body>
<script src="audiojs/audio.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script>
      $(function() { 
        // Setup the player to autoplay the next track
        var a = audiojs.createAll({
          trackEnded: function() {
            var next = $('ol li.playing').next();
            if (!next.length) next = $('ol li').first();
            next.addClass('playing').siblings().removeClass('playing');
            audio.load($('a', next).attr('data-src'));
            audio.play();
          }
        });
        
        // Load in the first track
        var audio = a[0];
            first = $('ol a').attr('data-src');
        $('ol li').first().addClass('playing');
        audio.load(first);

        // Load in a track on click
        $('ol li').click(function(e) {
          e.preventDefault();
          $(this).addClass('playing').siblings().removeClass('playing');
          audio.load($('a', this).attr('data-src'));
          audio.play();
        });
        // Keyboard shortcuts
      });
    </script>

<div class="container">

	<img class="logo" src="http://www.mozaicaccounts.co.uk/logos/<?=$_GET['account']?>.jpg">
	
	<div class="description">
	<?php 
		$product = Database::QueryGetResult("SELECT * FROM product WHERE uid='".$promo['product']."';");
		$band = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$product['artist']."';");
		$coverpath = 'preview/'.$_GET['account'].'/'.$product['uid'].'promo.jpg';
	?>
		<img class="artwork" src='<?=$coverpath?>'>
		<h2><?=$band['name']?></h2>
		<h2><?=$product['title']?></h2>
		<h2>Release date : <?=$product['releasedate']?></h2><br>
		
	<?php	
		print str_replace("\n","<br>",$promo['blurb']);
		
		$tracksref = Database::QueryGetResults("SELECT track,position FROM tracksproduct WHERE product='".$promo['product']."' ORDER BY position;");
		$titles = "";
		$mp3s = "";
		foreach($tracksref as $i=>$trackref)
		{
			$track = Database::QueryGetResult("SELECT title,version FROM tracks WHERE uid='".$trackref['track']."';");
			$filename = "http://www.mozaicaccounts.co.uk/promo/preview/".$_GET['account']."/".$promo['product']."_".($trackref['position']+1)."_preview.mp3";
			/*if ($i>0) 
			{
				$titles .= '|'; 
				$mp3s .= '|'; 
			}*/	
			if ($track['version']=="") $titles = $track['title']; else $titles = $track['title']." (".$track['version'].")";
			$mp3s[] = array('title'=>$titles,'file'=>$filename);
		
		} 
	?>
	</div>
	<div class="listen">
	
	<h2>Listen</h2>
	<audio preload></audio><ol>
<?php foreach($mp3s as $mp3) : ?>
	<li><a href="#" data-src="<?=$mp3['file']?>"><?=$mp3['title']?></a></li>
<?php endforeach; ?>	
</ol>
	<form method="post" name="myform" action="promosubmit.php" id="form"><br>
		<h2>React</h2>
		<textarea name="reaction" id="reaction" placeholder="type your reaction here"></textarea><br>
		<input type="submit" name="submit1" id="mysubmit" value="React and Download" onclick="if (myform.reaction.value.length==0) { alert('Please enter reaction'); return false; } else { if (this.value=='React and Download') { alert('Thank-you for your feedback, click OK to start download or check the link below'); this.disabled=true; $('#link').show(300); myform.submit(); } else { return false; }} ">
	<p id="link" style="display: none;"><b>Download not started?</b> Then please right click and 'save as' on this <a href="secure/<?=$_GET['account']?>/<?=$promo['product']?>Promo.zip">link</a></p>
		<p>Previews are low quality, on reaction a zip containing high quality files will start downloading</p>
		<p>Promo service powered by <a href="http://www.mozaicaccounts.co.uk" target="_blank">www.mozaicaccounts.co.uk</a></p>
		<input type="hidden" name="id" value="<?=$_GET['id']?>">
		<input type="hidden" name="account" value="<?=$database?>"> 
		<input type="hidden" name="accountid" value="<?=$_GET['account']?>">
	</form>

	</div>
</div>



</body>
</html>