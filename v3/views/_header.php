<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Mozaic Accounts</title>
	<META NAME="keywords" CONTENT="royalties,accounting,music,record label,application,statements,licencing,calculating">
	<META NAME="description" CONTENT="Affordable On-line Royalty Accounting Solutions for the Music Industry">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../theme1.css" type="text/css">
	<link rel="stylesheet" href="../AutoComplete.css" media="screen" type="text/css">
	<SCRIPT type="text/javascript" SRC="../thirdparty/md5.js"></SCRIPT>
	<SCRIPT type="text/javascript" SRC="../javascript/macc.js" ></SCRIPT>
	<SCRIPT type="text/javascript" SRC="../javascript/ajax.js" ></SCRIPT>
	<SCRIPT type="text/javascript" SRC="../thirdparty/DynamicOptionList.js" ></SCRIPT>
	<SCRIPT type="text/javascript" SRC="../thirdparty/autocomplete.js" ></SCRIPT>
	<SCRIPT type="text/javascript" SRC="../thirdparty/sortable/sortable.js"></SCRIPT>
</head>

<body onLoad="displayPopups(); initDynamicOptionLists();">
<SCRIPT type="text/javascript" SRC="../thirdparty/wz_tooltip.js" ></SCRIPT>
<div id='container'>

	<div id="banner">
		<a href="../v2/index.php">Click here</a> to go back to the original version
	</div>
	
	<div id='header'>
		<img src="../images/logo.png">	
