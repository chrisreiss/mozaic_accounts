<?php
require_once('cAuthorize.php');
require_once('cTabNav.php');

function doHTMLHeader()
{
	print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>'."\n";
	print '<head>'."\n";
	print '<title>Mozaic Accounts</title>'."\n";
	print '<META NAME="keywords" CONTENT="royalties,accounting,music,record label,application,statements,licencing,calculating">';
	print '<META NAME="description" CONTENT="Affordable On-line Royalty Accounting Solutions for the Music Industry">';
	print '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'."\n";
	print '<link href="theme1.css" rel="stylesheet" type="text/css">'."\n";
    print '<link rel="stylesheet" href="AutoComplete.css" media="screen" type="text/css">'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/md5.js"></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="javascript/macc.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="javascript/ajax.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/DynamicOptionList.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/autocomplete.js" ></SCRIPT>'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/sortable/sortable.js"></SCRIPT>'."\n";
	print '</head>'."\n\n";
}

function doBanner($login)
{
/*	print '<div id="banner"><a href="../v2/index.php">Click here</a> to go back to the original version</div>';
*/
	print "<div id='header'>\n";
	// Top menu

	print '<img src="images/logo.png">';
	
	
	if ($login==1)
	{
		if (isset($_SESSION) && isset($_SESSION['accountid']))
		{
			print "<div id='status'>You are logged in as <b>".$_SESSION['displayname'];
		/*	$logofilename = "../logos/".$_SESSION['accountid'].".jpg";
			if (file_exists($logofilename))
			{
				$size = getimagesize($logofilename);
				if ($size[0]>320) { $size[1] *= (320/$size[0]); $size[0] = 320; }
				if ($size[1]>55) { $size[0] *= (55/$size[1]); $size[1] = 55; }
				print "<p align=right><img src='$logofilename' width='".$size[0]."' height='".$size[1]."'></p>";
			}*/
			print "| <a href='mailto:support@mozaicaccounts.co.uk'>Support</a> | <a href='logout.php'>Logout</a></div>";
		}
		else
		{
		}
	}

	displayMenuBar();
	
	print "</div>\n";
}

function	displayHeader()
{
	doHTMLHeader();
	print '<body onLoad="displayPopups(); initDynamicOptionLists();">'."\n";
	print '<SCRIPT type="text/javascript" SRC="thirdparty/wz_tooltip.js" ></SCRIPT>'."\n";
	print "<div id='container'>\n";
	doBanner(1);
}

function displayMenuBar()
{
	global $page;
	print "<div id='menu'>\n";
	print '<!-- menu bar -->'."\n";
	$tabBar = new TabNav;
	if ($_SESSION['userid']=="tom") 
		$tabBar->AddTab("ADMIN","page","admin",STRING_TOOLTIP_BUTTON_HOME);
	
//	$tabBar->AddTab("Dashboard","page","home",STRING_TOOLTIP_BUTTON_HOME);
	$tabBar->AddTab("BANDS","page","bands",STRING_TOOLTIP_BUTTON_BANDS);
	$tabBar->AddTab("TRACKS","page","tracks",STRING_TOOLTIP_BUTTON_TRACKS);
	$tabBar->AddTab("PRODUCT","page","products",STRING_TOOLTIP_BUTTON_PRODUCT);
	$tabBar->AddTab("DEALS","page","deals",STRING_TOOLTIP_BUTTON_DEALS);
	$tabBar->AddTab("SALES","page","sales",STRING_TOOLTIP_BUTTON_SALES);
	if (array_search('licensing',$_SESSION['packages'])!==FALSE) 
	{
		$tabBar->AddTab("LICENSING","page","licensors",STRING_TOOLTIP_BUTTON_LICENCING);
	}
	$tabBar->AddTab("EXPENSES","page","expenses",STRING_TOOLTIP_BUTTON_EXPENDITURE);
	//if ($_SESSION['userid']=='tom') $tabBar->AddTab("Accounting","page","accounting",STRING_TOOLTIP_BUTTON_ACCOUNTING);
	$tabBar->AddTab("PARTNERS","page","partners",STRING_TOOLTIP_BUTTON_PARTNERS);
//	$tabBar->AddTab("In/Out","page","import",STRING_TOOLTIP_BUTTON_INOUT);
//	$tabBar->AddTab("Statements","page","statements",STRING_TOOLTIP_BUTTON_INOUT);
//	print_r($_SESSION);
	if (array_search('promo',$_SESSION['packages'])!==FALSE) 
	{
		$tabBar->AddTab("PROMOS","page","promos",STRING_TOOLTIP_BUTTON_HOME);
	}
	
	$tabBar->SetActiveTab($page);
	$tabBar->Display();
	print '</div>';
}

function displayStartDynamic()
{
	print "<div id='main'>";
}

function displayEndDynamic()
{
	print "</div>\n";
}

function displayFooter()
{
//	print '</table></td></tr></table></td></tr></table></center>
	
	print "<div id='footer'>\n";
	print "<center>".$_SERVER['SERVER_NAME']."</center>";
	print "</div>";
	print "</div>";
//	print $_SESSION['querylog'];
//	$_SESSION['querylog'] = "";
	print "</body></html>";
}


?>
