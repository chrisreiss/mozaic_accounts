<?php
// Text file _ localise

// Tool tips
//define("TOOLTIP_FIELD_XXX","");
define("STRING_TOOLTIP_BUTTON_CONFIRM","Confirm your changes");
define("STRING_TOOLTIP_BUTTON_CANCEL","Cancel your changes");

// Misc
define("STRING_MISC_CROSSDEALSPLITS_PROFITSHARE","This profit share deal is being cross calculated with other deals therefore they must all share the same split and reserve rates. Changing these values below will be reflected in all cross related deals.");
define("STRING_MISC_CROSSDEALSPLITS_ROYALTY","This royalty deal is being cross calculated with other deals therefore whilst the royalty rate can vary they must all share the same reserve rate. Changing the reserve rate below will be copied into all cross related deals.");

define("STRING_DEMO","You are currently running in demo mode. You are not permitted to change any of the data on the system. Command ignored.");
define("STRING_DEMO_DISABLED","This feature is currently disabled in Demo Mode.");
define("STRING_ERROR_NOT_LICENSEE","The selected partner does not have any active licences with you.");

// Dialog Box Messages
define("STRING_DIALOG_CONFIRM_REMOVEEXPENSES","Are you sure that you want to remove the selected expenses. This cannot be undone.");
define("STRING_DIALOG_CONFIRM_REMOVETRACKS","If this track is a member of a product then it will be removed from this product also. Are you sure?");
define("STRING_DIALOG_CONFIRM_DELETESALES","This will delete all sales matching the above criteria - this cannot be undone. Are you sure?");
define("STRING_DIALOG_CONFIRM","This action cannot be undone. Are you sure?");

// Button tooltips
define("STRING_TOOLTIP_BUTTON_LICENCING_MARKPAID","Enter a date for when this licence statement was paid");
define("STRING_TOOLTIP_BUTTON_LICENCING_INVOICE","Generate an invoice for this statement");
define("STRING_TOOLTIP_BUTTON_LICENCING_REMIND","Generate a reminder that this royalty statement is due");
define("STRING_TOOLTIP_BUTTON_LICENCING_EDITSTATEMENT","Edit the selected royalty statement details");
define("STRING_TOOLTIP_BUTTON_LICENCING_EDITLICENCEFROMSTATEMENT","Edit the original licence details for this statement");
define("STRING_TOOLTIP_BUTTON_LICENCING_LASTSTATEMENT","Tell the system not to expect any more royalty statements for this licence");
define("STRING_TOOLTIP_BUTTON_LICENCING_LICENCESTATEMENTS","Manage the royalty statements for this licence. You can send reminders from here.");
define("STRING_TOOLTIP_BUTTON_LICENCING_OUTSTANDINGSTATEMENTS","Track the status of all your licencing statements by clicking here");
define("STRING_TOOLTIP_BUTTON_LICENCING_OUTSTANDINGADVANCES","Scan the database for any licencing advances that haven't been paid yet.");

define("STRING_TOOLTIP_BUTTON_HOME","Read alerts, manage users, edit your profile, generate reports");
define("STRING_TOOLTIP_BUTTON_BANDS","Manage your bands and bandmembers. Generate royalty statements");
define("STRING_TOOLTIP_BUTTON_PARTNERS","Manage your manufacturer, distributors and shops");
define("STRING_TOOLTIP_BUTTON_TRACKS","Manage your song catalogue. You cannot sell tracks directly they have to part of a product first.");
define("STRING_TOOLTIP_BUTTON_PRODUCT","Manage your product catalogue, track stock, check cash flow. Products are things you can sell.");
define("STRING_TOOLTIP_BUTTON_DEALS","Manage the deals you have with your bands for things like royalty rates and advances.");
define("STRING_TOOLTIP_BUTTON_SALES","Log all sales here");
define("STRING_TOOLTIP_BUTTON_LICENCING","Manage your licencing. Check statements, send reminders and invoicing");
define("STRING_TOOLTIP_BUTTON_EXPENDITURE","Log all your expenses here");
define("STRING_TOOLTIP_BUTTON_INOUT","Get data in and out of the system. Read Excel speadsheets, input sales returns.");
define("STRING_TOOLTIP_BUTTON_ACCOUNTING","Calculate and manage artist royalty statements");


define("STRING_ACTION_ADDSTOCKMOVEMENT","Record a stock movement");
define("STRING_ACTION_STOCKMOVEMENTHISTORY","View stock levels");
define("STRING_ACTION_PRODUCTPROFITANALYSIS","Quick profit analysis");
define("STRING_ACTION_ADDSALE","Add a sale");
define("STRING_ACTION_ADDLICENCE","Add a licence");
define("STRING_ACTION_ADDEXPENSE","Add an expense");
define("STRING_ACTION_ADDPRODUCTFORMAT","Add a format");
define("STRING_ACTION_EXPORTMETADATA","Export metadata");
define("STRING_ACTION_EDIT","Edit");
define("STRING_ACTION_DELETE","Delete");

define("STRING_TOOLTIP_BUTTON_TRACKS_ADDLICENCE","Add a new licence for this track (shortcut)");
define("STRING_TOOLTIP_BUTTON_TRACKS_SEARCHBYIRSC","Find track details from an IRSC code");

define("STRING_TOOLTIP_BUTTON_PARTNERS_REGISTERSALES","Register multiple sales for this partner");
define("STRING_TOOLTIP_BUTTON_PARTNERS_STOCKLEVELS","Display the current stock levels of your product at this partner");

define("STRING_TOOLTIP_BUTTON_BANDS_NEWARTIST","Add a new artist. Artists are people you generate royalty statements for who are members of bands");
define("STRING_TOOLTIP_BUTTON_BANDS_NEWBAND","Add a new band. Make sure you've added the individual band members first as artists");
define("STRING_TOOLTIP_BUTTON_BANDS_REMOVEARTIST","Remove an artist from the system and all bands they are a member of");
define("STRING_TOOLTIP_BUTTON_BANDS_REMOVEBAND","Remove a band from the system, this will not remove the individual artists.");
define("STRING_TOOLTIP_BUTTON_BANDS_EDITARTIST","Edit the artists contact details");
define("STRING_TOOLTIP_BUTTON_BANDS_EDITBAND","Edit the band members and there relative splits");
define("STRING_TOOLTIP_BUTTON_BANDS_ARTISTADVANCES","Log an advance made to an individual artist that isn't specfically tied to a product");
define("STRING_TOOLTIP_BUTTON_BANDS_MANAGESTATEMENTS","Create a royalty statement for the selected artist");

define("STRING_TOOLTIP_BUTTON_INOUT_NEWIMPORTER","Create a new importer profile - this tells the system how to interpret a spreadsheet that has the information you require");
define("STRING_TOOLTIP_BUTTON_INOUT_NEWEXPORTER","Create a new exporter profile - this tells the system what data you want exported to a spreadsheet");
define("STRING_TOOLTIP_BUTTON_INOUT_EDIT","Edit the currently selected importer/exporter.");
define("STRING_TOOLTIP_BUTTON_INOUT_REMOVE","Remove the currently selected import/exporter profile.");
define("STRING_TOOLTIP_BUTTON_INOUT_RUN","Run the currently selected importer / exporter profile. This will take you through the steps necessary to get your data in and out of the system");

define("STRING_TOOLTIP_BUTTON_HOME_READMAIL","Read the selected message");
define("STRING_TOOLTIP_BUTTON_HOME_REPORTS","See interesting statistics about your label");
define("STRING_TOOLTIP_BUTTON_HOME_EDITPROFILE","Edit your contact information and preferences regarding your account");
define("STRING_TOOLTIP_BUTTON_HOME_MANAGEUSERS","(Administrator only) Add/Remove users to the system");

define("STRING_INTERNALERROR","Internal error occured please reset your browser");
define("STRING_BADLOGIN","The username/password entered was incorrect. ");
define("STRING_FORGOTTENPASSWORD","Forgotten your password?");
define("STRING_NOTREGISTERED","That email address is not registered on the system");
define("STRING_CHECKEMAIL","A reminder has been sent to your email box, please check it before trying it again.");
define("STRING_REMINDER","This email contains the login details for your mozaic account");



?>