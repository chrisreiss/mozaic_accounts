<?php

function	IsDemoAccount()
{
	if ($_SESSION['accountid']==3 && $_SESSION['userid']!='tom') return true;
}

function	JavaScript_Begin()
{
	print '<script type="text/javascript">'."\n"; //<!--'."\n";
}

function	JavaScript_Write($script)
{
	print $script."\n";
}

function	JavaScript_WriteNB($script)
{
	print $script;
}

function	JavaScript_End()
{
	print "</script>\n";
}

function HeaderBar($text="")
{
	print '<div id="formheader">'.$text.'</div>';
}

function	IsUsingExplorer()
{
	if (isset($_SERVER['HTTP_USER_AGENT']) && 
		(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
		return true;
	else
		return false;
}

function getSalesBatches()
{
	$res = Database::QueryGetSingleResults('SELECT DISTINCT batchid FROM sale WHERE batchid!="";');
	if ($res==FALSE)
		return array();
	return $res;
}

function getBands()
{
	$bands = Database::QueryGetResults('SELECT uid,name FROM bands ORDER BY name;');
	return $bands;
}

function getProducts()
{
	$bands = Database::QueryGetResults('SELECT uid,title FROM product;');
	return $bands;
}

function getProductsByBand($bandid)
{
	$bands = Database::QueryGetResults('SELECT uid,title FROM product WHERE artist="'.$bandid.'";');
	return $bands;
}

function getTracks()
{
	$bands = Database::QueryGetResults('SELECT uid,title FROM tracks;');
	return $bands;
}

function setVar($name,$value="")
{
	$_POST[$name]=$value;
}

function getVar($name,$default="")
{
	if (!empty($_POST[$name]))
		return $_POST[$name];
	else
		return $default;
}

function getVarS($name,$default="")
{
	$res = getVar($name,$default);
	if ($res!="" && is_array($res))
		return $res[0];
	else 
		return $res;
}

function getTracksByProduct($productid)
{
	$query = 'SELECT tracksproduct.track,tracks.title FROM tracksproduct JOIN tracks WHERE tracksproduct.track=tracks.uid AND tracksproduct.product="'.$productid.'";';
	$results = Database::QueryGetResults($query);
	return $results;
}


function formStart($title="", $name="mainform",$action="index.php",$javascript="") {
 	print '<u>'.$title.'</u><br>'; 
	print '<form name="'.$name.'" method="post" action="'.$action.'" id="validateform"><table>'."\n"; 
	}
function formLine($description,$field) { print '<tr><td>'.$description.'</td><td>'.$field.'</td></tr>'."\n"; }
function formText($text) { print "<tr><td>$text</td></tr>"; }
function formLine2($description,$field) { print '<tr><td>'.$description.'</td><td>'.$field.'</td></tr>'."\n"; }
function formEnd() { print '</table></form>'."\n"; }
//function formSubmit() { print '<button>'..'</button>'."\n"; }

function createKeyArray($array,$index)
{
	foreach($array as $row) { $out[$row[$index]]=$row; }
	return $out;
}

function formfieldSelectFromArray($name,$options,$selected="",$insertcode="")
{
	$html = '<select name="'.$name.'" '.$insertcode.'>';
	foreach($options as $row) 
	{ 
		if ($row==$selected)
			$html .= '<option value="'.$row.'" selected>'.$row.'</option>'; 
		else
			$html .= '<option value="'.$row.'">'.$row.'</option>'; 
	}
	$html .= '</select>';
	return $html;
}

define ('MAX_DROPDOWN_FIELD_LENGTH',40);

function formfieldSelectFromArrayWithKey($name,$options,$selected="",$insertcode="")
{
	$html = '<select name="'.$name.'" '.$insertcode.'>';
	foreach($options as $row) 
	{ 
		$value = current($row);
		$displayed = next($row);
		if (strlen($displayed)>MAX_DROPDOWN_FIELD_LENGTH) $displayed=substr($displayed,0,MAX_DROPDOWN_FIELD_LENGTH-3)."...";
		if ($value==$selected)
			$html.= '<option value="'.$value.'" selected>'.$displayed.'</option>'; 
		else
			$html.= '<option value="'.$value.'">'.$displayed.'</option>'; 
	}
	$html.= '</select>';
	return $html;
}

function formatTrackTitle($title,$version)
{
	if ($version=="") return $title;
	return $title.' ('.$version.')';
}

 function csv_explode($str, $delim = ',', $qual = "\"")
   {
       $skipchars = array( $qual, "\\" );
       $len = strlen($str);
       $inside = false;
       $word = '';
       for ($i = 0; $i < $len; ++$i) {
           if ($str[$i]==$delim && !$inside) {
               $out[] = $word;
               $word = '';
           } else if ($inside && in_array($str[$i], $skipchars) && ($i<$len && $str[$i+1]==$qual)) {
               $word .= $qual;
               ++$i;
           } else if ($str[$i] == $qual) {
               $inside = !$inside;
           } else {
               $word .= $str[$i];
           }
       }
       $out[] = $word;
       return $out;
   }

function 	CreateNewCatNumber($lastuid)
{
	$i=0; 
	$len = strlen($lastuid);
	while (!is_numeric($lastuid[$i]) && $len>=$i) ++$i;
	if ($i==$len) return $lastuid."2";
	
	$numlen = strlen(substr($lastuid,$i));
	$trail = "";
	if ($len!=$i)
	{
		$codenum = substr($lastuid,$i);
		$numlen2 = strlen($codenum+1);
		if (strlen($codenum + 1)<$numlen) 
		{
			while ($numlen2<$numlen)
			{
				$trail = "0".$trail;
				$numlen2++;
			}
		}
		
		$uid = substr($lastuid,0,$i).$trail.($codenum+1);
	}
	return $uid;
}

function SortResultsByCol(&$array,$col)
{	
	$data = $array;
	
	foreach ($data as $key => $row) {
		$sortme[$key]  = $row[$col];
	}
	array_multisort($sortme, SORT_DESC, $data);
	return $data;
}

// ---- Takes user input and converts it to internal representation of currency
function storemoney($text)
{


	
	// remove any extra stuff
	$text = str_replace(" ","",$text);	// remove spaces
	$text = str_replace(",","",$text);	// remove commas

	$format = "GBP";
	$negate = false;

	// get first character
	if (strpos($text,"$")!==false) $format = "USD";
	if (strpos($text,"�")!==false) $format = "GBP";
	if (strpos($text,"-",0)!==false) $negate = true;
	
	$i = 0;
	$len = strlen($text);
	$digits = "";
	// find first numeral
	while($i<$len)
	{
		if ($text[$i]>="0" && $text[$i]<="9")
			break;
		++$i;
	}
	while($i<$len)
	{
		if (($text[$i]<"0" || $text[$i]>"9") && $text[$i]!=".")
			break;
		$digits .= $text[$i];
		++$i;
	}

	$value = round($digits*100);
	if ($negate==true) 
		$value = -$value;
//	print $text." >> ".$value.$format;
	return $value.$format;
}

function reverse_date_order($date)
{
	if ($date=="")
		return $date;
		
	$dates = explode('/',$date);
	return $dates[1].'/'.$dates[0].'/'.$dates[2];		
}

function todate($day,$month,$year)
{
	return $year."-".$month."-".$day;
}

function getpostdate()
{
	return getpostdate2("date");
}

function getpostdate2($stem)
{
	$date = "";
	if (!empty($_POST[$stem.'_year']) && !empty($_POST[$stem.'_month']) && !empty($_POST[$stem.'_day']))
	{
		$date = $_POST[$stem.'_year']."-".$_POST[$stem.'_month']."-".$_POST[$stem.'_day'];
		if ($date=="--") return "";
	}
	return $date;
}

function MakeDate($datestring)
{
	$daymonthyear = explode("-",$datestring);
	if (strlen($daymonthyear[2])>2)
		return $daymonthyear[2]."-".$daymonthyear[1]."-".$daymonthyear[0];
	else
		return $datestring;
}

function displaydate($datestring)
{
	$daymonthyear = explode("-",$datestring);
	if (strlen($daymonthyear[0])>2)
		return $daymonthyear[2]."-".$daymonthyear[1]."-".$daymonthyear[0];
	else
		return $datestring;
}

function ts_assert($bool,$message)
{
	if (!$bool)
		die ($message);
}

function button_go_page($page,$text)
{
	print '<form action="index.php" method="post"><input type="hidden" name="page" value="'.$page.'"><input type="submit" class="submit" name="submit" value="'.$text.'"></form>';
}

function getartisttitle($id,$linkID)
{
	// check if track or album from id
	$result = Database::Query("SELECT artist,title FROM tracks WHERE id='".$id."';",$linkID);
	if ($result==false)
	{
		// check album
		$result = Database::Query("SELECT artist,title FROM product WHERE product='".$id."';",$linkID);
		if ($result==false)
		{
			// can find
			return "Unknown ".$id;
		}
	}

	$row = $result->fetch(PDO::FETCH_ASSOC);
	return ($row[0]." - ".$row[1]." ($id)");
}

function GetEnumDescription($field,$value)
{
	if ($field=="type")
	{
		switch($value)
		{
			case "D":
				return "Distributor";	
			case "M":
				return "Manufacturer";
			case "V":
				return "Download Service";
			case "L":
				return "Licensor";
			case "S":
				return "Retail Outlet";
		}
	}
	return $value;	
}

function popupError($message)
{
	print '<popup>Error: '.$message.'</popup><br>';
}
function popup($message)
{
	print '<popup>'.$message.'</popup><br>';
}

function Error($error,$returnpage)
{
	print "<p>$error</p>\n";
	button_go_page($returnpage,"ACKNOWLEDGE");
}

function Success($returnpage)
{
	button_go_page($returnpage,"RETURN");
}

function Cancel($returnpage)
{
	button_go_page($returnpage,"CANCEL");
}

function DisplayAddressFromID($name,$addressid)
{
	$address = DataTable::GetEntryFromID("addresses",$addressid);
	return DisplayAddress($name,$address);
}

function DisplayAddress($name,&$address)
{
	$display =  $name."<br>";
	$display .= $address['address1']."<br>";
	if (!empty($address['address2'])) 
		$display .= $address['address2']."<br>";
	$display .= $address['city']."<br>";
	$display .= $address['country']."<br>";
	$display .= $address['postcode']."<br>";
	return $display;
}

function Debug($text)
{
	if ($_SESSION['userid']!='tom') return;
	
   	print $text;
}

function Debug_r($text)
{
	if ($_SESSION['userid']!='tom') return;
	
   	print_r($text);
}


function PrintTable($array, $title="")
{
    if ($_SESSION['userid']!='tom')
         return;
         
	if (empty($array) || !is_array($array))
	{	
		print "empty table $title<br>";
		return;
	}
	
	
	print '<table class="debug" border=1>';
	if (!empty($title)) print "<tr><td>$title</td></tr>";
//	print_r($array);
	print '<tr><td><b>Key</b></td>';
	if (is_array(current($array)))
	{
		foreach(current($array) as $key=>$value)
			print '<td><b>'.$key.'<b></td>';
	}
	else { print '<td></td>'; }
	print '</tr>';
	reset($array);
	foreach($array as $index=>$row)
	{
		print '<tr><td>'.$index.'</td>';
		if (is_array($row))
		{
			foreach($row as $cell)
			{
				print '<td>';
				if (is_array($cell))
					PrintTable($cell);//recurse
				else
					print $cell;
				print '</td>';
			}
		}
		else
		{
			print "<td>$row</td>";
		}
		print '</tr>';
	}
	print '</table>';
}


?>