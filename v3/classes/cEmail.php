<?php
//ini_set("include_path", "./:./classes:./thirdparty:./localise");

require_once("./thirdparty/mimemessage/email_message.php");


class 	Email {


	static function CleanHTML($in)
	{
		$eol="\r\n";

		$in = str_replace("</p>",$eol,$in);
		$in = str_replace("<br>",$eol,$in);

		// replace links
		for($i=0;$i<10;++$i)
		{
			$off1 = 0;
			$off1 = stripos($in,'<a href="');
			if ($off1===false) break;
			$off2 = stripos($in,'">',$off1+9);
			$link = substr($in,$off1+9,$off2-$off1-9);
			$off2 = stripos($in,'</a>',$off2+3);
			$off2 += 4;
			$out = substr($in,0,$off1) . $link . substr($in,$off2);
			$in = $out;
		}
		
		$out = strip_tags($out).$eol;
		return $out;
	}


	static function SendHTML($to, $body, $subject, $fromaddress, $fromname)
	{
		$email_message = new email_message_class;
		$email_message->SetEncodedEmailHeader("To",$to,$to);
    	$email_message->SetEncodedEmailHeader("From",$fromaddress,$fromname);
    	$email_message->SetEncodedEmailHeader("Reply-To",$fromaddress,$fromname);
    	$email_message->SetHeader("Sender",$fromaddress);
    	if(defined("PHP_OS") && strcmp(substr(PHP_OS,0,3),"WIN"))
        	$email_message->SetHeader("Return-Path",$fromaddress);

    	$email_message->SetEncodedHeader("Subject",$subject);
    	$html_message = $body;
    	$email_message->CreateQuotedPrintableHTMLPart($html_message,"",$html_part);
   		$text_message = "This is an HTML message. Please use an HTML capable mail program to read this message.\n";
    	$text_message .= Email::CleanHTML($body);
    	$email_message->CreateQuotedPrintableTextPart($email_message->WrapText($text_message),"",$text_part);
		$alternative_parts=array(
      	  $text_part,
      	  $html_part
    	);
    	$email_message->CreateAlternativeMultipart($alternative_parts,$alternative_part);
    	$related_parts=array(
	        $alternative_part
	    );
    	$email_message->AddRelatedMultipart($related_parts);

		$error=$email_message->Send();
		return;
	}

	static function Send($to, $body, $subject, $fromaddress, $fromname, $attachments=false)
	{
		$email_message = new email_message_class;
		$email_message->SetEncodedEmailHeader("To",$to,$to);
    	$email_message->SetEncodedEmailHeader("From",$fromaddress,$fromname);
    	$email_message->SetEncodedEmailHeader("Reply-To",$fromaddress,$fromname);
    	$email_message->SetHeader("Sender",$fromaddress);
		if(defined("PHP_OS") && strcmp(substr(PHP_OS,0,3),"WIN"))
			$email_message->SetHeader("Return-Path",$_SESSION['email']);

    	$email_message->SetEncodedHeader("Subject",$subject);

     	$text_message = $body;
    	$email_message->AddQuotedPrintableTextPart($email_message->WrapText($text_message));

    	
    	if ($attachments!==false)
    	{
    		$text_attachment=array(
				"Data"=>"Attached file",
				"Name"=>$attachments,
				"Content-Type"=>"automatic/name",
				"Disposition"=>"attachment"
			);
			$email_message->AddFilePart($text_attachment);
		}
	
		$error=$email_message->Send();
		if(strcmp($error,""))
			echo "Error: $error\n";
		return;
	}
}