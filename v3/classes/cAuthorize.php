<?php
require_once('cDatabase.php');

class Authorize
{
	static function IsAuthorized()
	{
		session_start();

		if (isset($_SESSION['sessionkey']))
		{
			// session time still in the future?
			if ($_SESSION['timeout']>time())
			{
				$_SESSION['timeout'] = time()+(60*30);  //update tiemout
				return true;
			}
		}
		session_unset();
		return false;
	}
	
	static function Logout()
	{
		session_start();

		$userid = $_SESSION['userid'];
		Database::Connect("mozaic");
		Database::Query("UPDATE mozaic.users SET lastlogout='".date('Y-m-d H:i:s')."' WHERE userid='".$userid."';");
		session_unset();
		session_destroy();
		unset($_SESSION);
	}
}
?>