<?php

require_once('strings.php');


class TabNav
{
	var $m_tabs;
	var $m_numtabs = 0;
	var $m_activetab = -1;

// construtor
	function TabNav()
	{
		$m_numtabs = 0;
		$m_activetab = -1;
	}

	function AddTab($text,$var,$value,$tooltip)
	{
		$this->m_tabs[$this->m_numtabs] = array('text' => $text,'var' => $var, 'value' => $value, 'tooltip' => $tooltip);
		++$this->m_numtabs;
		return $this->m_numtabs;
	}
	
	function SetActiveTab($value)
	{
		for ($i=0;$i<$this->m_numtabs;++$i)
		{
			if ($this->m_tabs[$i]['value']==$value)
			{
				$this->m_activetab = $i;
				break;
			}
		} 
	}


	function Display()
	{
//		print '<div class="menuwrapper">';
		print "<form method=post action='index.php' name='tabnav' >";
		
		for ($i=0;$i<$this->m_numtabs;++$i)
		{
			if ($i==$this->m_activetab)
			{
				print "<button disabled type='submit' name='page' class='tabnav' value='".$this->m_tabs[$i]['text']."' >".$this->m_tabs[$i]['text']."</button>\n";
			}
			else
			{
				print "<button type='submit' name='page' class='tabnav' value='".$this->m_tabs[$i]['value']."' onmouseover=\"Tip('".$this->m_tabs[$i]['tooltip']."',DELAY,1000,WIDTH,300);\" onclick=\"document.tabby".$i.".submit()\">".$this->m_tabs[$i]['text']."</button>\n";
			}
		}
		print '</form>';
//		print '</div>';
//		print "<br>\n";
	}

}

	
?>
