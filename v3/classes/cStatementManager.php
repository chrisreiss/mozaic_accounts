<?php

class StatementManager {

	function GetStatements()
	{
		$statements =  Database::QueryGetResults("SELECT artist,crossdealid,startdate,enddate,amount,pdfhash FROM artiststatements WHERE type!='L' ORDER BY pdfhash;"); //artist,startdate,enddate;');
		
		$laststatement = Array('startdate'=>'');
		$amount = 0;
		
		foreach($statements as $statement)
		{
			if ($statement['startdate']!=$laststatement['startdate'])
			{
				if (substr($statement['startdate'],5,5)=="01-01" && substr($statement['enddate'],5,5)=="06-30")
					$statement['period'] = substr($statement['startdate'],0,4)."A";
				else if (substr($statement['startdate'],5,5)=="07-01" && substr($statement['enddate'],5,5)=="12-31")
					$statement['period'] = substr($statement['startdate'],0,4)."B";
				else
					$statement['period'] = $statement['startdate']." - ".$statement['enddate'];
				
				$statement['amounttotal'] = Currency::DISPLAY($amount);
				$procstatements[] = $statement;
				$amount = 0;

			}
			$amount += Currency::GETRAWVALUE($statement['amount']);
			$laststatement = $statement;
		}
		
		return $procstatements;
	}
	

	function GetNextPeriodForArtist($artist)
	{
		return Array('start'=>'2009-01-01','end'=>'2009-06-31');
	}	
};

?>