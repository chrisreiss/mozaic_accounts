<?php
require_once('cDatabase.php');
require_once('cTableInfo.php');

class DataTable 
{
	static function GetEntryFromIDUsingUID($table,$id,$uidlabel)
	{
		$query = "SELECT * FROM $table WHERE ".$uidlabel." = '$id';";
		$result = Database::Query($query);
		if ($result->GetNum()==0) 
		{
//			print mysql_error();
			return false;
		}
		$row = $result->GetNext();
		return $row;
	}
	
	static function Count($table,$condition)
	{
		$result = Database::Query("SELECT COUNT(uid) FROM $table WHERE $condition;");
		if ($result->GetNum()==0)
		{
			return 0;
		}
		$row = $result->GetNextRow();
		return $row[0];
	}
	
	static function GetFieldFromTable($table,$uid,$field)
	{
		$result = Database::Query("SELECT $field FROM $table WHERE uid='$uid';");
		$row = $result->GetNext();
		$value = $row[$field];
		Database::FinishQuery($result);
		return $value;
	}
	
	static function DeleteMultiple($table,$condition)
	{
		Database::Query("DELETE FROM $table WHERE $condition;");
	}
			
	
	static function GetEntryFromID($table,$id)
	{
		return DataTable::GetEntryFromIDUsingUID($table,$id,"uid");
	}
	
	static function GetID($table,$uniquecolumn,$value)
	{
		$result = Database::Query("SELECT uid FROM $table WHERE $uniquecolumn='$value';");
		if ($result->GetNum()==0) return false;
		$row = $result->GetNext();
		Database::FinishQuery($result);
		return $row['uid'];
	}
	
	static function GetIDExist($table,$id)
	{
		$query = "SELECT uid FROM $table WHERE uid = '$id';";
		$result = Database::Query($query);
		if ($result->GetNum()==0)
		{
			return false;
		}
		return true;
	}
	
	static function Delete($table,$id)
	{
		$query = "delete from $table where uid='$id';";
		Database::Query($query);
	}
	
	// tablename, fieldnames, fieldvalues.... //
	static function Insert($table,$fields)
	{
		$args = func_get_args();
		
		$query = "INSERT INTO $table (`".str_replace(",","`,`",$fields)."`) VALUES (";
		
		$fieldarray = explode(",",$fields);
		$numfields = count($fieldarray);
		$i = 2;
		foreach ($fieldarray as $index=>$field)
		{
			$value = TableInfo::Validate($table,$field,$args[$i]);
			$value = Database::Validate($value);		// get rid of any escape codes

			$query .= "'".$value."'";
			if ($index==($numfields-1))
			{
					$query .= ");";
			}
			else
			{
				$query .= ",";
			}
			++$i;	
		}
		$result = Database::Query($query);
		return Database::LastInsertId();
	}


	static function InsertFromArray($table,$array)
	{
		if (func_num_args()>2)
			$rawcopy = func_get_arg(2);
		else
			$rawcopy = false;
			
		foreach ($array as $key=>$value)
		{
			$fieldarray[] = $key;
		}
		
		$fields = implode(",",$fieldarray);
		
		$query = "INSERT INTO $table ($fields) VALUES (";

		$numfields = count($fieldarray);
		$index = 0;
		foreach ($array as $field=>$rawvalue)
		{
			if ($rawcopy)
			{
				$value = $rawvalue;
			}
			else
			{
				$value = TableInfo::Validate($table,$field,$rawvalue);
				if ($value===false)
				{
					echo("Validating Field <i>".TableInfo::GetDescription($table,$field)."</i> - ".TableInfo::GetValidationError());
					return false;
				}
			}
			
			$value = Database::Validate($value);		// get rid of any escape codes

			$query .= "'".$value."'";
			if ($index==($numfields-1))
			{
					$query .= ");";
			}
			else
			{
				$query .= ",";
			}
			++$index;
		}
		$result = Database::Query($query);
		return Database::LastInsertId();
	}

	static function InsertFromPost($table,$fields,&$aspect)
	{
		$fieldarray = explode(",",$fields);
		$query = "INSERT INTO $table ($fields) VALUES (";

		$numfields = count($fieldarray);
		foreach ($fieldarray as $index=>$field)
		{
			$value = TableInfo::Validate($table,$field,$aspect->GetVar($field));
			if ($value===false)
			{
				$aspect->Error("Validating Field <i>".TableInfo::GetDescription($table,$field)."</i> - ".TableInfo::GetValidationError());
				return false;
			}
			$value = Database::Validate($value);		// add escape codes

			$query .= "'".$value."'";
			if ($index==($numfields-1))
			{
					$query .= ");";
			}
			else
			{
				$query .= ",";
			}
		}
		$result = Database::Query($query);
		return Database::LastInsertId();
	}

	
		// tablename, id, fieldnames, fieldvalues.... //
	static function Update()
	{
		$numargs = func_num_args();
		assert("$numargs>=4");
		
		$args = func_get_args();
		$query = "UPDATE $args[0] SET ";
		$fieldnames = explode(",",$args[2]);
		$i = 3;
		while ($i<$numargs)
		{
			$query = $query.$fieldnames[$i-3]."='".$args[$i]."'";
			++$i;
			if ($i!=$numargs)
				$query = $query.", ";
			else
				$query = $query." WHERE uid = '$args[1]';";
		}
		$result = Database::Query($query);
	}
	
			// tablename, id, fieldnames //
	static function UpdateFromPost($table,$id,$fields,&$aspect)
	{
		$query = "UPDATE $table SET ";
		$fieldarray = explode(",",$fields);
		$numfields = count($fieldarray);
		foreach ($fieldarray as $index=>$field)
		{
			$value = $aspect->GetVar($field);

			// Is this update keeping the uid the same - in which case skip this field
			if ($field=="uid" && $value==$id)
			 	continue;
				
			$value = TableInfo::Validate($table,$field,$value);
			if ($value===false)
			{
				$aspect->Error("Validating Field <i>$field</i> - ".TableInfo::GetValidationError());
				return false;
			}

			$value = Database::Validate($value);		// get rid of any escape codes

			$query .= $field."='".$value."'";
			if ($index<($numfields-1))
				$query .= ", ";
			else
				$query .= " WHERE uid = '".$id."';";
		}
		$result = Database::Query($query);
		return true;
	}
};


?>