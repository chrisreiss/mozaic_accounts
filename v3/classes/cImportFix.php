<?php

require_once('cAuthorize.php');
require_once('cDatabase.php');
require_once('util.php');
require_once('cTableInfo.php');
require_once('cWorkSheet.php');
require_once('cCurrency.php');
// New code to upload spreadsheet from popular stores

/*
Requirements:
	- need to be able to enumerate the different formats
	- need to be able to preview data before commiting
	- want to be able to write new format interpreters very quickly
	- need to be able to read from csv/excel
	
	
	// Sales data
	uid
	date
	product (bundle - optional)
	track
	distributor (agregator)
	quantity
	baseprice
	value
	saletype (vp)
	termkey
	territory
	shop
	mechanicals
	distfee
	batchid

	// Find track (isrc, artist/track)

*/

define ('colA',0);
define ('colB',1);
define ('colC',2);
define ('colD',3);
define ('colE',4);
define ('colF',5);
define ('colG',6);
define ('colH',7);
define ('colI',8);
define ('colJ',9);
define ('colK',10);
define ('colL',11);
define ('colM',12);
define ('colN',13);
define ('colO',14);
define ('colP',15);
define ('colQ',16);
define ('colR',17);
define ('colS',18);
define ('colT',19);
define ('colU',20);
define ('colV',21);
define ('colW',22);
define ('colX',23);
define ('colY',24);
define ('colZ',25);
define ('colAA',26);
define ('colAB',27);
define ('colAC',28);
define ('colAD',29);
define ('colAE',30);
define ('colAF',31);


// TEST CODE
/*
Authorize::IsAuthorized();
$importer = new SalesImporter();



$importer->Process(
					array(
					array('Tronicsole','5/2/10 14:28','','TSOLE 078','','Natural High (includes Francois Dubois remix)','Natural High','Francois Dubois remix','','','','FERRAND  Steve','320kbs MP3 track','1','1.32','Italy','0.69','0.1','Sale'),
					array('Tronicsole','7/2/10 11:50','','TSOLE 078','','Natural High (includes Francois Dubois remix)','',             '',                    '','','','FERRAND  Steve','320kbs MP3 release (3 tracks)','1','2.54','Sweden','1.32','0.2','Sale')
					)
					
					,"formatJuno");

*/
// IMPLEMENTATION CODE BELOW

function CleanString($in)
{
	$in = str_replace(array(" ", "\r", "\r\n", "\n"), '', $in);
//	$in = mysql_real_escape_string($in);
//	print $in."<br>";
//	$in = addslashes($in);
	return $in;
}

function FindCATNO($catno)
{
	$catno = CleanString($catno);
	if ($catno=='')
		return $catno;
	$query = "SELECT uid FROM product WHERE UPPER(uid)=UPPER('$catno');";
	$catno = Database::QueryGetResult($query);
	if (empty($catno) || $catno==false)
		return '';
	return $catno['uid'];
}

function FindISRC($isrc)
{
	$isrc = CleanString($isrc);
	$isrc = str_replace(array("-", "_"), '', $isrc);
	if ($isrc=='')
		return '';
	//print "SELECT uid FROM tracks WHERE UPPER(uid)=UPPER('$isrc'); ";
	$isrc = Database::QueryGetResult("SELECT uid FROM tracks WHERE UPPER(uid)=UPPER('$isrc');");
	if (empty($isrc) || $isrc==false)
		return '';
	return $isrc['uid'];
}

function FindUPC($isrc)
{
	$isrc = CleanString($isrc);
	if ($isrc=='')
		return '';
	$isrc = Database::QueryGetResult("SELECT uid FROM product WHERE barcode='$isrc';");
	if (empty($isrc) || $isrc==false)
		return '';
	return $isrc['uid'];
}

function FindTrackFromName($product,$trackname,$remixname)
{
//	$trackname = mysql_real_escape_string($trackname);
//	$remixname = mysql_real_escape_string($remixname);

	$query = "SELECT uid FROM tracks WHERE UPPER(title)=UPPER('$trackname') AND UPPER(version)=UPPER('$remixname');";
	$tracks = Database::QueryGetResults($query);
	if (count($tracks)>0)
	{
		if (count($tracks)>1)
		{
			print "Warning multiple tracks have the same name of ".$trackname." (".$remixname.")<br>";
			print_r($tracks);
		}
		return $tracks[0]['uid'];
	}
//	print $query."<Br>";
//	PrintTable($tracks);
	
	$compositename = $trackname;
	if ($remixname!="") $compositename .= "(".$remixname.")";
	
	$query = "SELECT uid FROM tracks WHERE UPPER(title)=UPPER('$compositename');";
	$tracks = Database::QueryGetResults($query);
	if ($tracks===false)
		return '';
	if (count($tracks)>1)
	{
		print "Warning multiple tracks have the same name of ".$compositename."<br>";
		print_r($tracks);
		//die ('Ambigious track');
	}
	return $tracks[0]['uid'];
	
}

class SalesImporter {

	private $validated;
	private $fixeddata;
	
	
	static public $formats = array(
							array('id'=>'formatBeatport','name'=>'Beatport Sales','requiredfields'=>array('distributor')),
							array('id'=>'formatJuno','name'=>'Juno Digital Sales','requiredfields'=>array('date','distributor')),
							array('id'=>'formatTraxsource','name'=>'Traxsource Sales','requiredfields'=>array('date','distributor')),
							array('id'=>'formatStompy','name'=>'Stompy Sales','requiredfields'=>array('date','distributor')),
							array('id'=>'formatWPP','name'=>'WhatPeoplePlay Sales','requiredfields'=>array('distributor'))
							);
	

	public function ProcessRow($call,$row)
	{
		$this->$call($row);
	}

	// a-Label,B-Licensee,C-Datereport,D-PeriodSt,E-PeriodE,F-DayofDownload,G-Timeofdownload,H-Portal,I-Country
	// J-Transaction,K-DistributionChannel,L-Format,M-Quality,N-Sales,O-UPC,P-ISRC,Q-CatNo,R-Artist,S-Peanuts,T-Tronicsole,
	// U-GrossPrice,V-VAT,W-RoyaltyBasis,X-Mechanicals,Y-Currency,Z-ExchangeRate,AA-LabelShare,AB-PPD,AC-Revenue,AD-Currency,AE-ExchangeRate,AF-MechnicalsPayedBy?
	public function formatWPP($rowindex,$row)
	{
		$track=''; $product='';	
		$row[colQ] = ereg_replace("[^A-Za-z0-9'.]", "", $row[colQ]);
	
		$product = FindCATNO($row[colQ]);
		if ($product=='')
			$error[] = "Cannot find product '".$row[colQ]."' specified in column Q"; 
			
		$track = FindISRC($row[colP]);
		if ($track=='')
		{
			$trackname = ereg_replace("[^A-Za-z0-9']", "", $row[colS]);
			$track = FindTrackFromName($product,$trackname,"");
			if ($track=='')
			{
				$error[] = "Cannot find the track '$trackname' as part of product '$product' in database."; 
				$track='?'.$trackname.'?';
			}
		}

		$row[colAC] = ereg_replace("[,]", ".", $row[colAC]);
		$row[colW] = ereg_replace("[,]", ".", $row[colW]);
		$row[colX] = ereg_replace("[,]", ".", $row[colX]);

		$this->validated[$rowindex] = array( 'error'=>$error,'date'=>date('Y-m-d',strtotime($row[colF])),'distributor'=>$this->fixeddata['distributor'],
											'product'=>$product,'track'=>$track,
											'quantity'=>$row[colN],'baseprice'=>Currency::IN($row[colW]),
									'value'=>Currency::IN($row[colAC]),'saletype'=>'V','territory'=>$row[colI],
									'shop'=>'Whatpeopleplay','mechanicals'=>Currency::IN($row[colX]));
	}
	
	// a-upc,b-catno,c-isrc,d-trackid,e-label,f-release,g-tracktitle,h-trackartist,i-remixername,j-remix,k-territory,
	// l-type,m-format,n-delivery,o-transactiondate,p-contenttype,q-quantity,r-grossmp3,s-grosswav,t-paidprice,u-netcontent,v-netwav,w-withheld,x-nettotal,y-saletype
	public function formatBeatport($rowindex,$row)
	{
		// quick hack (already!) because the form is not validated so doesn't create the date properly
	//	$this->fixeddata['date'] = $_POST['date_year'].'-'.$_POST['date_month'].'-'.$_POST['date_day'];
	
		$track=''; $product='';
		
		$row[colB] = preg_replace("[^A-Za-z0-9]", "", $row[colB]);
	
		$product = FindCATNO($row[colB]);
		if ($product=='')
			$error[] = "Cannot find product '".$row[colB]."' specified in column B"; 
			
		$track = FindISRC($row[colC]);
		if ($track=='')
		{
			$row[colG] = preg_replace("[^A-Za-z0-9 ']", "", $row[colG]);
			$row[colJ] = preg_replace("[^A-Za-z0-9 ']", "", $row[colJ]);
			
			if ($row[colG]!='')
			{
//				if ($row[colJ]!='' && $row[colJ]!='Original Mix')
//					$trackname = $row[colG]." (".$row[colJ].")";
//				else
					$trackname = $row[colG];
				$trackname = str_replace("'","\'",$trackname);
				$remixname = str_replace("'","\'",$row[colJ]);
				$track = FindTrackFromName($product,$trackname,$remixname);
				if ($track=='')
				{
					$error[] = "Cannot find the track '$trackname ($remixname)' as part of product '$product' in database."; 
					$track='?'.$trackname.'?';
				}
			}
		}

		$this->validated[$rowindex] = array( 'error'=>$error,'date'=>$row[colO],'distributor'=>$this->fixeddata['distributor'],'product'=>$product,'track'=>$track,'quantity'=>$row[colQ],'baseprice'=>Currency::IN($row[colT]),
									'value'=>Currency::IN($row[colX]),'saletype'=>'V','territory'=>$row[colK],'shop'=>'Beatport','mechanicals'=>Currency::IN($row[colW]));

	}

	
	//Label name,Date/Time,Your Release Ref,Catalogue No.,UPC,Release,Track,Mix,Track Your Ref,Track ISRC,Track MCPS,Release Artist,File Type,Quantity,Value,Country,Royalty,Mechanicals,Sale/Refund
	//A:Date/Time,B:LabelName,C:Your Release Ref,D:Catalogue No.,E:UPC,F:Release,G:Artist,H:Track,I:Mix,J:Track Your Ref,K:Track ISRC,L:File Type,M:Value,N:Royalty,O:Mechanicals,P:Country,Q:TransactionType
	public function formatJuno($rowindex,$row)
	{
	//	print $rowindex." : "; print_r($row); print '<br>';
		// quick hack (already!) because the form is not validated so doesn't create the date properly
		$this->fixeddata['date'] = $_POST['date_year'].'-'.$_POST['date_month'].'-'.$_POST['date_day'];	
		$track=''; $product='';
		if (!strncmp($row[colQ],'Sale',4)) 
		{
			$product = FindCATNO($row[colD]);
			if ($product=='')
				$error[] = "Cannot find cat no '".$row[colD]."' specified in column D";
				
			// find out what is being sold
			// no isrc
			$track = FindISRC($row[colK]);
			if ($track=='')
			{
				if ($row[colH]!='')
				{
					if ($row[colI]!='')
						$trackname = $row[colH]." (".$row[colI].")";
					else
						$trackname = $row[colH];
				
					$track = FindTrackFromName($product,$trackname,"");
					if ($track=='')
					{
						$error[] = "Cannot find the track '$trackname' as part of product '$product' in database."; 
						$track='?????';
					}
				}
			}
		}
		else
		{ 
			$error[] = "Does not appear to be a sales row - ignoring";
		}


		$this->validated[$rowindex] = array( 'error'=>$error,'date'=>$this->fixeddata['date'],'distributor'=>$this->fixeddata['distributor'],'product'=>$product,'track'=>$track,'quantity'=>1,'baseprice'=>Currency::IN($row[colM]),
									'value'=>Currency::IN($row[colN]),'saletype'=>'V','territory'=>$row[colP],'shop'=>'Juno','mechanicals'=>Currency::IN($row[colO]));

	}


	// label, date(b), upc(c), isrc(d), cat no(e), artist(f), title(g), trackno, trackname(i),bundle,territory,bitrate,pricepoint(m),trans%,perc,unitrate,qty(q),mechanical(r),amountdue(s)
	public function formatTraxsource($rowindex,$row)
	{
		// quick hack (already!) because the form is not validated so doesn't create the date properly
		$this->fixeddata['date'] = $_POST['date_year'].'-'.$_POST['date_month'].'-'.$_POST['date_day'];
	
		$track=''; $product='';
	
//		print_r($row); print '<br>';
		{
			$product = FindCATNO($row[colE]);
			if ($product=='')
				$error[] = "No product specified in column E";
				
			// find out what is being sold
			// no isrc
			$track = FindISRC($row[colD]);
			if ($track=='')
			{
				$track = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product='$product' AND position='".$row[colH]."' LIMIT 1;");
				if ($track===false)
				{
					$error[] = "Cannot find the track num '".$row[colH]."' as part of product '$product' in database."; 
					$track='?????';
				}
				else
				{
					$track = $track[0]['track'];
				}
			}
		}



		$this->validated[$rowindex] = array( 'error'=>$error,'date'=>$this->fixeddata['date'],'distributor'=>$this->fixeddata['distributor'],'product'=>$product,'track'=>$track,'quantity'=>$row[colQ],'baseprice'=>Currency::IN($row[colP]),
									'value'=>Currency::IN($row[colS]),'saletype'=>'V','territory'=>$row[colK],'shop'=>'Traxsource','mechanicals'=>Currency::IN($row[colR]));

	}




	// a - artist, b - title, c - label, d - isrc, e - upc, f - format, g - price, h - qty, i - royalties
	public function formatStompy($rowindex,$row)
	{
		//print_r($row); print '<br><br>';
		// quick hack (already!) because the form is not validated so doesn't create the date properly
		$this->fixeddata['date'] = $_POST['date_year'].'-'.$_POST['date_month'].'-'.$_POST['date_day'];
	
	
		if ($row[colB]!='')
		{
			$track=''; $product='';
					
			$track = FindISRC($row[colD]);
			if ($track=='')
			{
				if ($row[colE]!='')
				{	
					$product = FindUPC($row[colE]);
					if ($product=='')
						$error[] = "Cannot find UPC '".$row[colE]."' in database";
				}
				
				{
					// search by track name
					$track = Database::QueryGetResults("SELECT uid FROM tracks WHERE UPPER(title)=UPPER('".str_replace("'","\'",$row[colB])."');");
					if ($track==false)
					{
						$error[] = "Cannot find track '".$row[colB]."'";
						$track = '?'.$row[colB].'?';
					}
					else
					{
						if (count($track)>1)
						{
							// cut down by artist
							$artist = Database::QueryGetResults("SELECT uid FROM band WHERE UPPER(name)=UPPER('".$row[colA]."');");
							if ($artist===false)
							{
								$error[] = "Cannot find artist '".$row[colA]."'";
							}
							else
							{
								$track = Database::QueryGetResults("SELECT uid FROM tracks WHERE UPPER(title) =UPPER('".$row[colB]."') AND artist='".$artist[0]['uid']."';");
								if ($track===false)
								{
									$error[] = "Cannot identify track";
								}
								else
								{
								//	print_r($track); print "<br>";
									$track = $track[0]['uid'];
								}
							}
						}
						else
						{
							$track = $track[0]['uid'];
							//print "$track<br>";
						}
					if ($product=='')
					{
					// try to work out product backwards
					$products = Database::QueryGetResults("SELECT product FROM tracksproduct WHERE track='$track';");
					if (count($products)==1)
					{
					$product = $products[0]['product'];
					}
					}
					}

			
				}
			}
		}
		else
		{
			$error[] = "Not sale row - skipping";
		}

	// a - artist, b - title, c - label, d - isrc, e - upc, f - format, g - price, h - qty, i - royalties

		$this->validated[$rowindex] = array( 'error'=>$error,'date'=>$this->fixeddata['date'],'distributor'=>$this->fixeddata['distributor'],'product'=>$product,'track'=>$track,'quantity'=>$row[colH],'baseprice'=>Currency::IN($row[colG]),
									'value'=>Currency::IN($row[colI]),'saletype'=>'V','territory'=>'','shop'=>'Stompy','mechanicals'=>'');

	}

	// param - $inputarray, a multidimensional array of data representing the spreadsheet
	// return assoc array error=>line
	public function Process($inputarray,$format)
	{
		unset($this->validated);
		foreach( $inputarray as $rowindex => $row )
		{
			$this->$format($rowindex,$row);
		}
//		return $output;
	}
	
	public function LoadAndProcess($format,$importfile,$fixeddata)
	{
//		$data = WorkSheet::Load($importfile);

		$this->fixeddata = $fixeddata;

		ini_set('auto_detect_line_endings',TRUE);
		$file = fopen($importfile,"r");
		while($data = fgetcsv($file,1000,","))
		{
			$this->Process($data,$format);
		}
		
		$query = '';

		print '<div id="spreadsheet">';
		print '<table class="spreadsheet" border=1 width=100%>';
		print '<tr>';
		foreach(current($this->validated) as $key=>$value)
		{ 
			if ($key=='error') 
				print '<td>row</td>';
			else
			{
				foreach($this->fixeddata as $key2=>$data) 
						if ($key==$key2) continue 2;
				
				print '<td><b>'.$key.'<b></td>';
	
			}
		}
		print '</tr>';
		reset($this->validated);
	
		foreach($this->validated as $rowindex=>$row)
		{
			$err=false;
			foreach ($row as $key=>$value)
			{
				foreach($this->fixeddata as $key2=>$data) 
					if ($key==$key2) continue 2;
				
				if ($key=='error')
				{
					if (!empty($value))
					{
						$err = true;
						print "<td><a href='#' onmouseover=\"Tip('".addslashes($value[0])."',WIDTH,300,DELAY,1000)\">$rowindex<img src='images/exclamation.png' width=15 height=15></a></td>";
					}
					else
					{
						print "<td>$rowindex</td>";
					}
				}
				else
				{
					if ($err==true) 
						print '<td><font color=grey>'.$value.'</font></td>';
					else
						print '<td>'.TableInfo::Display("sale",$key,$value).'</td>';
				}	
			}
			print '</tr>';
			// convert this to a query;
		}
		print '</table>';
		print '</div>';


		$fieldlist = "date,product,track,distributor,quantity,baseprice,value,saletype,termkey,territory,shop,mechanicals,distfee,batchid";
		$fields = explode(',',$fieldlist);
		$querylist = "";
		// Convert to queries
		foreach($this->validated as $rowindex=>$row)
		{
			$row['batchid'] = $this->fixeddata['batchid']; // copy
			if (empty($row['error']))
			{
				$query = 'INSERT INTO sale ('.$fieldlist.') VALUES (';
				foreach($fields as $i=>$cell)
				{
					if ($i!=0) $query.=',';
					$query .= "'".Database::Validate($row[$cell])."'";
				}
				$query .= ");\n";		
			//	print $rowindex." : ".$query."<br>";
				$querylist .= $query;
			}	
		}

		
		return $querylist;
	}					

	
	public function GetFieldsNeedingFillingByUser($formatid)
	{
		//print $formatid;
		foreach(SalesImporter::$formats as $i=>$format)
		{
			if ($format['id']==$formatid)
			{
				//print_r(SalesImporter::$formats[$i]['requiredfields']);
				return SalesImporter::$formats[$i]['requiredfields'];
			}
		}
		die('No such format "'.$formatid.'" found');
	}

	// returns an association with the name->id of the different importers
	public static function Enumerate()
	{
		return SalesImporter::$formats;
	}


	
	// commits the currently stored validated data to the database
	public function CommitValid()
	{
	}
	
	// purge any stored data
	public function Purge()
	{
	}
	
	

}



?>