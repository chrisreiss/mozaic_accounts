<?php
require_once('cEditField.php');

class Form
{
	var $m_group;
	var $m_tableformat;
	var $m_target;
	var $m_fields;
	var $m_title;
	var $m_parent;
	var $m_insertedhtml;
	
	function SetTarget($target)
	{
		$this->m_target = $target;
	}
	
	function SetTitle($title)
	{
		$this->m_title = $title;
	}

	function Begin($target)
	{
		$this->m_target = $target;
		$this->m_group = "";
		$this->m_tableformat = "horizontal";
	}

	function	InsertHTML($html)
	{
		$this->m_insertedhtml = $html;
	}
	
	function AddTitle($title)
	{
		$this->m_title = $title;
	}

	function AddNewLine()
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'newline','name'=>'','value'=>'','description'=>'','type'=>'');
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'newline','name'=>'','value'=>'','description'=>'','type'=>'');
	}
	
	function AddField($descrip,$name,$template,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'field','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'field','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
	}
	
	function AddCustom($descrip,$name,$code)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'field','name'=>$name,'description'=>$descrip,'type'=>'custom','code'=>$code);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'field','name'=>$name,'description'=>$descrip,'type'=>'custom','code'=>$code);
	}
	
	function AddSelectionList($descrip,$name,$list,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'list','name'=>$name,'value'=>$value,'description'=>$descrip,'list'=>$list);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'list','name'=>$name,'value'=>$value,'description'=>$descrip,'list'=>$list);
	}

	function AddFixed($descrip,$name,$template,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'fixed','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'fixed','name'=>$name,'value'=>$value,'description'=>$descrip,'type'=>$template);
	}
	
	function AddHidden($name,$value)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'hidden','name'=>$name,'value'=>$value);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'hidden','name'=>$name,'value'=>$value);
	}

	// fields
	function StartGroupSet($startid,$endid)
	{
		$this->m_fields[] = array('property'=>'groupset','name'=>'groupset1','startid'=>$startid,'endid'=>$endid);
		$this->m_group = 'groupset1';
	}
	
	
	function EndGroupSet()
	{
		$this->m_group = '';
	}
	
	function AddCancel()
	{
		$args = func_get_args();
		if (!empty($args[0]))
			$value = $args[0];
		else
			$value = "BACK";
			
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'button','name'=>'submitvalue','value'=>$value,'description'=>$value);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'button','name'=>'submitvalue','value'=>$value,'description'=>$value);
	}
	
	function AddAction($submit)
	{
		if ($this->m_group=="")
			$this->m_fields[] = array('property'=>'button','name'=>'submitvalue','value'=>$submit);
		else
			$this->m_groupfields[$this->m_group][] = array('property'=>'button','name'=>'submitvalue','value'=>$submit);
	}
	
	function End()
	{
		die("Depreciated end function in form");
	}

	function	_presentField(&$field,$id)
	{
		switch($field['property'])
		{
		case 'newline' :
			print "</tr><tr>";
			break;
		case 'button':
			print "<td><button class='submit' type='submit' name='".$field['name'].$id."' value='".$field['value']."'>".$field['value']."</button></td>";
			break;
		case 'field':
			if ($this->m_group=="" || $this->m_tableformat!="horizontal") 
				print "<td><label>".$field['description']."</label></td>";
			print "<td>";
			if ($field['type']=='custom')
				print $field['code'];
			else
				EditField($field['name'].$id,$field['type'],$field['value']);
			print "</td>";
			break;
		case 'list':
			if ($this->m_group=="" || $this->m_tableformat!="horizontal") 
				print "<td>".$field['description']."</td>";
			print "<td>";
			
//			DropListEnumerated($field['list'],$field['name'],$field['value'],"");

			
			print "<select id='".$field['name']."' name='".$field['name']."'>\n";
			$found = false;
			
			$values = explode(",",$field['list']);
			foreach($values as $v)
			{
				print_r($v);
				if ($v=="")
				{
					print '<option disabled="disabled">---------</option>';
				}
				else
				{

					$parts = explode("=",$v);
					$enum = $parts[0];
					$enumname = $parts[1];		
					if ($enum==$field['value'])
					{
						$found = true;
						print "<option selected value='".$enum."'>".$enumname."</option>\n";
					}
					else
						print "<option value='".$enum."'>".$enumname."</option>\n";
				}
			}
			if ($found==false)
			{
				print "<option selected value='".$field['value']."' >".$field['value']."</option>\n";
			}
			print "</select>\n";

			print "</td>";
			break;
		case 'fixed':
			if ($this->m_group=="" || $this->m_tableformat!="horizontal") 
				print "<td><label>".$field['description']."</label></td>";
			print "<td class='fixedfield'>";
			print $field['value'];
			print "</td>";
			// let fall through to hidden!!!
		case 'hidden':
			print "<input type=hidden name='".$field['name'].$id."' value='".$field['value']."'>";
			break;
		default:
			die("Unknown form field property");
		}
	}
	
	function SetAspect(&$aspect)
	{
		$this->m_parent = $aspect;
	}
	
	function Present()
	{
		print '<div class="mform">';

		// Start form
		print "<form name='form1' id='validateform' method='post' action='index.php' enctype='multipart/form-data'>\n
			   <input type='hidden' name='page' value='".$this->m_target."'>\n";

		if ($this->m_title!="")
			print "<span class='formtitle'>".$this->m_title."</span>";
			
		print "<table>\n";
		
		if (!empty($this->m_insertedhtml))
			print $this->m_insertedhtml;
			
		foreach ($this->m_fields as $field)
		{
			if ($field['property']!='groupset')
			{
				print "<tr>";
				$this->_presentField($field,"");
				print "</tr>\n";
			}
			else
			{
				// is horizontal
				$this->m_group = $field['name'];
				if ($this->m_tableformat=="horizontal")
				{
					print "<tr>";
					foreach ($this->m_groupfields[$field['name']] as $field2)
					{
						print "<td>";
						if (!empty($field2['description']))
							print $field2['description'];
						print "</td>";
					}
					print "</tr>\n";
				} 
				for ($id = $field['startid'];$id <= $field['endid'];++$id)
				{
					print "<tr>";
					if (isset($this->m_groupfields[$field['name']]))
					{
						foreach ($this->m_groupfields[$field['name']] as $field2)
						{
							$this->_presentField($field2,$id);
						}
					}
					else
					{
						print "<td>Undefined groupset ".$field['name']."</td>";
					}
					print "</tr>\n";
				}
				$this->m_group = "";
			}
		}

		print "</table>\n";
		print "</form>\n";
		print '</div>';
	}

}

?>
