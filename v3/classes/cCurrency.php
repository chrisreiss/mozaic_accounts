<?php


define('DENOMINATION_GBP','GBP');
define('DENOMINATION_USD','USD');
define('DENOMINATION_EUR','EUR');

function xmlStartElement($parser,$name,$attr)
{
	if ($name=="CUBE" || $name=="Cube")
	{
		if (!empty($attr['CURRENCY'])) 
		{
			ExchangeRate::SetRate($attr['CURRENCY'],$attr['RATE']);
		}
	}
}

function xmlContents($parser,$data)
{
	// do nothing
}
	
function xmlEndElement($parser,$data)
{
	// do nothing
}

//ExchangeRate::Init();


class ExchangeRate
{
	static function Init()
	{
		// if we don't have currency rates stored then access from the ECB
		ExchangeRate::SetRate('EUR',1.0);
	/*	$xml_parser  =  xml_parser_create(); 
		xml_set_element_handler($xml_parser, "xmlStartElement", "xmlEndElement");
		xml_set_character_data_handler($xml_parser, "xmlContents"); 
		$data = file_get_contents("http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml");
		if(!(xml_parse($xml_parser, $data, true)))
		{
			die("Error on line " . xml_get_current_line_number($xml_parser));
		}
		xml_parser_free($xml_parser);*/
	}
	
	static function SetRate($currency,$rate)
	{
		$_SESSION['currencyrates'][$currency] = $rate;
//		print $currency. " = " . $rate;
	}
	
	static function GetRate($currency)
	{
		return $_SESSION['currencyrates'][$currency];
	}
	
	static function Convert($value,$source,$destination)
	{
		if (!isset($_SESSION['currencyrates']))
		{
			ExchangeRate::Init();
		}
//		print "value=$value, source=$source, destination=$destination <br>";
//		print_r($_SESSION['currencyrates']);
    	if ($source==$destination) 
    		return $value;
		
		if (!empty($_SESSION['currencyrates'][$destination]))
			return ( ($value/$_SESSION['currencyrates'][$source]) * $_SESSION['currencyrates'][$destination] );
		else
			return $value;
	}
}


/*!
	Currency Class - represents a montary value
*/
class Currency
{
	var	$_value;				// value in cents
	var $_denomination;			// denomination - three letter code GBP, USD or EUR

	// Constructor
	function	Currency()
	{
		$this->_value = 0;
		$this->_denomination = $_SESSION['currencydenomination']; 
//		$this->_denomination = DENOMINATION_GBP;
	}
	
	function	Clear()
	{
		$this->_value = 0;
	}

	// Static function
	static function 	DISPLAYTODATABASE($value)
	{
		$currency = new Currency;
		$currency->SetFromDisplayFormat($value);
		return $currency->GetDatabaseFormat();
	}

	// Static function
	static function 	IN($value)
	{
		$currency = new Currency;
		$currency->SetFromDisplayFormat($value);
		return $currency->GetDatabaseFormat();
	}

	// Static function
	static function 	DATABASETODISPLAY($value)
	{
		$currency = new Currency;
		$currency->SetFromDatabaseFormat($value);
		return $currency->GetDisplayFormat();
	}

	static function 	DISPLAY($value)
	{
		$currency = new Currency;
		//$currency->SetFromDatabaseFormat($value);
		$currency->_value = $value;
		return $currency->GetDisplayFormat();
	}
	
	
	// Static function
	static function 	GETRAWVALUE($value)
	{
		$currency = new Currency;
		$currency->SetFromDatabaseFormat($value);
		$currency->ConvertTo($_SESSION['currencydenomination']); //DENOMINATION_GBP);
		return $currency->GetValue();
	}
	
	
	static function	SUMOF($lhs,$rhs)
	{
		$lhscurrency = new Currency();
		$rhscurrency = new Currency();
		$lhscurrency->SetFromDatabaseFormat($lhs);
		$rhscurrency->SetFromDatabaseFormat($rhs);
		$lhscurrency->Add($rhscurrency);
		return $lhscurrency->GetDatabaseFormat();
	}

	static function	PRODUCTOF($lhs,$rhs)
	{
		$lhscurrency = new Currency();
		$lhscurrency->SetFromDatabaseFormat($lhs);
		$lhscurrency->Multiply($rhs);
		return $lhscurrency->GetDatabaseFormat();
	}

	function	GetDenominationSymbol($denomination)
	{
		switch($denomination)
		{
		case DENOMINATION_GBP:
			return "&#163;"; //"�"; //
		case DENOMINATION_USD:
			return "$";
		case DENOMINATION_EUR:
			return "EUR"; //&#8364;";
		}
		return "?";
	}

	// Returns a string ready for display
	function	GetDatabaseFormat()
	{
		return $this->_denomination.$this->_value;
	}
	
	function	GetDisplayFormat()
	{
		// If negative put in brackets
		$value = $this->_value / 100;
		$strlen = strlen($value);
		$decimalpos = strpos($value,".");
		if ($decimalpos===false) 
			$value .= ".00";
		else
		{
			if ($strlen-$decimalpos<3) $value .= "0";
		}
		return Currency::GetDenominationSymbol($this->_denomination).($value);
	}

	function	ConvertTo($denomination)
	{
		$this->_value = ExchangeRate::Convert($this->_value,$this->_denomination,$denomination);
		$this->_denomination = $denomination;
	}
	
	function	IsValidatedFormat($input_str)
	{
//		if (!preg_match("/^([$|�]?)\d+(?:\.\d{0,2})?$/",$text))
//			return false;
		return true;
	}
	
	function	SetFromDatabaseFormat($input_str)
	{
		if (empty($input_str))
			return true;
		
		$this->_denomination = substr($input_str,0,3);
		$this->_value = substr($input_str,3);

		// Check for old database values... if no denomination then use GBP
		if (Currency::GetDenominationSymbol($this->_denomination)=="?")
		{
			$this->_denomination = DENOMINATION_GBP;
			$this->_value = $input_str;
		}

		return true;
	}
	
	function CheckCurrencyType($input_str,$str,$code) {
		if (strpos($input_str,$str)!==false) {
			$this->_denomination = $code;
			$input_str = str_replace($str,"",$input_str);
	//		return $input_str;
		}
		return $input_str;
	}
	
	function	SetFromDisplayFormat($input_str)
	{
	//	print $input_str." -> ";
		if (Currency::IsValidatedFormat($input_str)===false)
			return false;

		// remove any extra stuff
		$input_str = str_replace(" ","",$input_str);	// remove spaces
		$input_str = str_replace(",","",$input_str);	// remove commas
	
		$negate = false;
		
		$this->_denomination = $_SESSION['currencydenomination'];
	
		$input_str = $this->CheckCurrencyType($input_str,"$",DENOMINATION_USD);
		$input_str = $this->CheckCurrencyType($input_str,"�",DENOMINATION_GBP);
		$input_str = $this->CheckCurrencyType($input_str,"&#163;",DENOMINATION_GBP);
		$input_str = $this->CheckCurrencyType($input_str,"EUR",DENOMINATION_EUR);
		$input_str = $this->CheckCurrencyType($input_str,"GBP",DENOMINATION_GBP);
		$input_str = $this->CheckCurrencyType($input_str,"USD",DENOMINATION_USD);
		/*
		// Work out the denomination
		if (strpos($input_str,"$")!==false) $this->_denomination = DENOMINATION_USD;
		if (strpos($input_str,"�")!==false) $this->_denomination = DENOMINATION_GBP;
		if (strpos($input_str,"&#163;")!==false) $this->_denomination = DENOMINATION_GBP;
		if (strpos($input_str,"EUR")!==false) $this->_denomination = DENOMINATION_EUR;
		if (strpos($input_str,"GBP")!==false) $this->_denomination = DENOMINATION_GBP;
		if (strpos($input_str,"USD")!==false) $this->_denomination = DENOMINATION_USD;
*/
		// positive or negative?
		
		if (strpos($input_str,"(",0)!==false) { 
				$negate = true; 
				$input_str = str_replace("(","",$input_str);
				$input_str = str_replace(")","",$input_str);
		}
		
		$this->_value = floatval($input_str);
		if ($negate)
			$this->_value *= -1.0;
			
		$this->_value *= 100.0;
			

	//	print $this->_value."<br>";

	/*		
		if (strpos($input_str,"-",0)!==false) { 
			$negate = true;
			} else if (strpos($input_str,"(",0)!==false) { 
				$negate = true; 
				$input_str = str_replace("(","",$input_str);
				$input_str = str_replace(")","",$input_str);
		}
		
		
		
		$i = 0;
		$len = strlen($input_str);
		$digits = "";
		// find first numeral
		while($i<$len)
		{
			if ($input_str[$i]>="0" && $input_str[$i]<="9")
				break;
			++$i;
		}
		while($i<$len)
		{
			if (($input_str[$i]<"0" || $input_str[$i]>"9") && $input_str[$i]!=".")
				break;
			$digits .= $input_str[$i];
			++$i;
		}
	
//		$value = round($digits*100);
		if ($negate==true) 
			$this->_value = -$digits*100; //-round($digits*100);
		else
			$this->_value = $digits*100; //round($digits*100);
			
//		print_r($this);
	*/
	
	
	}
	
	function	GetDenomination()
	{
		return $this->_denomination;
	}
	
	function	GetValue()
	{
		return $this->_value;
	}

	function	SetValue($value)
	{
		$this->_value = $value;
	}
	
	function	Add($rhs)
	{
		if ($rhs->GetDenomination()!=$this->_denomination)
		{
			$rhs->ConvertTo($this->_denomination);
		}
		$this->_value += $rhs->GetValue();
	}
	
	function	Subtract($rhs)
	{
		if ($rhs->GetDenomination()!=$this->_denomination)
		{
			$rhs->ConvertTo($this->_denomination);
		}
		$this->_value -= $rhs->GetValue();
	}
	
	function	Multiply($rhs)
	{
		$this->_value = $this->_value * $rhs;
	}
	
	function	Negate()
	{
		$this->_value *= -1;
	}
};

?>