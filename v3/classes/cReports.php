<?php
require_once('util.php');
require_once('cTableInfo.php');

class Reports {

	function	Process($report)
	{
		switch ($report)
		{
			case "SALES CHART":
			{
			}
		
		}
	
	}



	function _SalesChart() //($startdate,$enddate,$limit)
	{
/*		$products = Database::QueryGetResults("SELECT uid FROM product WHERE accountid='".$_SESSION['accountid']."';");
		foreach ($products as $product)
		{
			$product_in
		}
	*/	
                Database::Connect();
		$worksheet = new Worksheet;
//		$combinedsales = Database::QueryGetResults("SELECT product,SUM(quantity) AS cnt,SUM(value) FROM sale WHERE accountid='".$_SESSION['accountid']."' GROUP BY product ORDER BY cnt DESC LIMIT 20;");
		$combinedsales = Database::QueryGetResults(
		"SELECT product,SUM(quantity) AS cnt FROM sale WHERE saletype='P' GROUP BY product ORDER BY cnt DESC LIMIT 20;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top Physical Sales");
			$worksheet->AddHeaderRow("Product","Units Sold");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow(TableInfo::DisplayFromType("product",$productsales['product']),$productsales['cnt']);
			}
		}

		$worksheet->AddRow("");
	/*	
		
		$combinedsales = Database::QueryGetResults(
		"SELECT product,SUM(quantity),SUM(value) AS cnt FROM sale WHERE accountid='".$_SESSION['accountid']."' AND product NOT LIKE 'GB%' GROUP BY product ORDER BY cnt DESC LIMIT 20;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top 20 Products by Gross Receipts");
			$worksheet->AddHeaderRow("Product","Value of Sales","Units Sold");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow(TableInfo::DisplayFromType("product",$productsales['product']),Currency::DATABASETODISPLAY($productsales['cnt']),$productsales['SUM(quantity)']);
			}
		}
*/
		$worksheet->AddRow("");
		$combinedsales = Database::QueryGetResults(
		"SELECT product,SUM(quantity) AS cnt FROM sale WHERE saletype='V' AND track='' GROUP BY product ORDER BY cnt DESC LIMIT 20;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top bundle downloads");
			$worksheet->AddHeaderRow("Track","Units Sold");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow(TableInfo::DisplayFromType("track",$productsales['product']),$productsales['cnt']);
			}
		}

		$worksheet->AddRow("");
		$combinedsales = Database::QueryGetResults(
		"SELECT track,SUM(quantity) AS cnt FROM sale WHERE saletype='V' AND track!='' GROUP BY track ORDER BY cnt DESC LIMIT 20;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top individual track downloads");
			$worksheet->AddHeaderRow("Track","Units Sold");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow(TableInfo::DisplayFromType("track",$productsales['track']),$productsales['cnt']);
			}
		}
		$worksheet->AddRow("");
		$combinedsales = Database::QueryGetResults(
		"SELECT distributor,SUM(quantity) AS cnt FROM sale GROUP BY distributor ORDER BY cnt DESC LIMIT 20;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top sales vendor by units sold");
			$worksheet->AddHeaderRow("Sales Vendor","Units Sold");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow(TableInfo::DisplayFromType("partners",$productsales['distributor']),$productsales['cnt']);
			}
		}
/*		$worksheet->AddRow("");

		$combinedsales = Database::QueryGetResults(
		"SELECT licenceduid,COUNT(*) AS cnt,SUM(advance) FROM licences WHERE accountid='".$_SESSION['accountid']."' AND licencetype='T' GROUP BY licenceduid ORDER BY cnt DESC LIMIT 10;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top 10 Most Licenced Tracks");
			$worksheet->AddHeaderRow("Track","No. of Licences","Advances Value");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow(TableInfo::DisplayFromType("track",$productsales['licenceduid']),$productsales['cnt'],Currency::DATABASETODISPLAY($productsales['SUM(advance)']));
			}
		}
*/
/*		$combinedsales = Database::QueryGetResults(
		"SELECT licenceduid,advance FROM licences WHERE accountid='".$_SESSION['accountid']."' GROUP BY licenceduid;"
		);
		if ($combinedsales!=false)
		{
			$worksheet->AddTitleRow("Top 20 Tracks by Licencing Revenues");
			$worksheet->AddHeaderRow("Track","Units Sold","Value of Sales");
			foreach ($combinedsales as $i=>$productsales)
			{
				$worksheet->AddRow($productsales['product'],$productsales['cnt'],displaymoney($productsales['SUM(value)']));
			}
		}*/
		
		$worksheet->DisplayAsTable();

	}
	

};

?>