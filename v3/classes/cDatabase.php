<?php

require_once('cDebug.php');
require_once('config.php');


class DatabaseResult
{
	var $m_result;
	
	function DatabaseResult($result)
	{
		$this->m_result = $result;
	}
	
	function GetNum()
	{
		if ($this->m_result==false)
		{
			return 0;
		}
		
		return $this->m_result->rowCount();
	}
	
	function GetNext()
	{
		return $this->m_result->fetch(PDO::FETCH_ASSOC);
	}

	function GetNextRow()
	{
		return $this->m_result->fetch(PDO::FETCH_ASSOC);
	}
	
	function Get($index,$type = PDO::FETCH_ASSOC)
	{
		return $this->m_result->fetch($type,PDO::FETCH_ORI_ABS, $index);
	}


};



$db = null;
$db_dbSelected = false;
$db_results = false;



class Database
{
	
	static function Init()
	{
		$db = null;
		$db_dbSelected = false;
		$db_results = false;
		Database::Connect();
	}
	

	static function CheckConnect(){
		global $db;
		if ($db==null){
			Database::Connect();
		}
	}

	static function Quote($str){
		global $db;
		return $db->quote($str);
	}

	// Wraps a database connection request
	static function Connect()
	{
		$args = func_get_args();
		if (empty($args[0])) 
			$database = "mozaic";
		else
			$database = $args[0];
		
		global $db;
		global $db_dbSelected;

		try {
			$db = new PDO('mysql:host=127.0.0.1;dbname='.$database.';charset=utf8',USER,PASSWORD);
		} catch (PDOException $ex){
			die("An error occurred Database::Connect :".$ex->getMessage());

		}
	}

	static function QueryParam($query)
	{
		//$query = func_get_arg(0);
		for ($i=1;$i<func_num_args();++$i)
		{
			$count = 0;
			$varl = "'".(string)func_get_arg($i)."'";
			$query = str_replace("%".strval($i),$varl,$query,$count);
			if ($count==0) break;
		}
	//	print $query."<br>";
		return Database::QueryGetResults($query);
	}

	// This is not slash safe!
	static function Query($query)
	{
		global $db;
//		if (!isset($_SESSION['querycount'])) $_SESSION['querycount'] = 1; else $_SESSION['querycount'] +=1;
//		if (!isset($_SESSION['querylog'])) $_SESSION['querylog'] = $query."<br>"; else $_SESSION['querylog'] .= $query."<br>";

		// check there is only one semi colon - to avoid hacking
		if (substr_count($query,";")>1)
		{
			$query = substr($query,0,strpos($query,";"));
			Trace(1,"Removed multiple query");
		}
//		print "QUERY : ".$query."<br>";
		$result = $db->query($query);
		if ($result===FALSE){
			$errorInfo = $db->errorInfo();
			die($query." Error = ".$errorInfo[2]);
			//Trace(1,$query." Error = ".$errorInfo[2]);
			//return false;
		}
		return (new DatabaseResult($result));
	}
	
	// Return the first value
	static function QueryGetValue($field,$table,$condition)
	{
		$result = Database::QueryGetResult("SELECT $field FROM $table WHERE ".$condition);
		return $result[$field];
	}
		// This is not slash safe!
	static function QueryGetResults($query)
	{
		global $db;
		Database::CheckConnect();
		// check there is only one semi colon - to avoid hacking
		if (substr_count($query,";")>1)
		{
			$query = substr($query,0,strpos($query,";"));
			Trace(1,"Removed multiple query");
		}
//		print "QUERY : ".$query."<br>";
		$result = $db->query($query);

		if ($result===FALSE){
			$errorInfo = $db->errorInfo();
			die($query." Error = ".$errorInfo[2]);
			//Trace(1,$query." Error = ".$errorInfo[2]);
			//return false;
		}
		
		//return $result->fetchAll(PDO::FETCH_ASSOC);

		$numresult = $result->rowCount(); 

		for ($i=0;$i<$numresult;++$i)
		{
			$results[] = $result->fetch(PDO::FETCH_ASSOC);
		}
		//mysql_free_result($result);
		if ($numresult!=false)
			return ($results);
	}

	static function QueryGetSingleResults($query)
	{
		$results = Database::QueryGetResults($query);
		if (!empty($results))
		{
			foreach($results as $row)
			{
				$arr[] = $row[key($row)];
			}
			return $arr;
		}
	}
	
	static function QueryGetResult($query)
	{
		$results = Database::QueryGetResults($query);
		if ($results==false)
			return false;
		return $results[0];
	}

	
	static function FindUID($table,$searchcol,$searchvalue)
	{
		$result = Database::Query("SELECT uid FROM ".$table." WHERE ".$searchcol." = '".$searchvalue."';");
		if ($result->GetNum()==0)
		{
			return false;
		}
		$row = $result->GetNext();
		Database::FinishQuery($result);
		return $row['uid'];
	}

	static function FinishQuery(&$result)
	{
		unset ($result);
	}
	
	static function Validate($string)
	{
		global $db;

		if ($string=="") return $string;
		
        if(get_magic_quotes_gpc()) 
		{
            $string = stripslashes($string);
  		} else {
  			$string = addslashes($string);
  		}
		
//		$string = mysql_real_escape_string($string);	
		return $string;
	}
	
	static function LastInsertId(){
		global $db;
		return $db->lastInsertId();
	}

	static function Disconnect()
	{
		global $db;
		$db = null;
	}
	
}
?>
