<?php

require_once("config.php");

class File {
	
	static function GetPath($filename)
	{
		return BASEPATH.'accounts/'.$_SESSION['accountid'].'/'.$filename;
	}
	
	static function GetPromoPath($dir,$filename)
	{
		return BASEPATH.'promo/'.$dir.'/'.$_SESSION['accountid'].'/'.$filename;
	}
}

?>