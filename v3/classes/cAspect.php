<?php

class Aspect {
	var $m_name;
	var	$m_bComplete;
	var $m_action;
	var $m_defaultaction;
	var $m_objects;
	
	function Error($string)
	{
		print "<table border=1><tr><td><b>$string</b></td></tr><table><br>\n";
	}
	
	function Start($name,$default)
	{
		print "\n<!-- Start Aspect -->\n";
		$this->m_name = $name;
		$this->m_bComplete = false;
		$this->m_defaultaction = $default;
		// Get the next action
		if (empty($_POST['submitvalue']))
		{
			if (empty($_POST['action']))
			{
				$this->m_action = $this->m_defaultaction;
			}
			else
			{
				$this->m_action = $_POST['action'];
			}
		}
		else
		{
			$this->m_action = $_POST['submitvalue'];
		}
		
		if ($_SESSION['userid']!="tom")
		{
			// Check for back button
			if ($this->m_action!="VIEW" && isset($_POST['transactionid']))
			{
				if (isset($_SESSION['transactionid']))
				{
					if (false) //($_POST['transactionid']!=$_SESSION['transactionid'])
					{
						print "<table border=1><tr><td><b>Error:</b> Please do not use browser BACK or REFRESH buttons. <i>Operation Cancelled.</i></td></tr></table><br>";
						$this->m_action = "VIEW";
					}
					else
					{
						// increment transaction id
						++$_SESSION['transactionid'];
					}
				}
				else
				{
					$_SESSION['transactionid'] = 1;
					$_POST['transactionid'] = 1;
				}
			}
		}
	}
	
	function PreserveVar($name)
	{
		if (!empty($this->m_objects))
		{
			foreach($this->m_objects as $i => $object)
			{
				if ($this->_isForm($object))
				{
					$this->m_objects[$i]->AddHidden($name,$_POST[$name]);
				}
			}
		}
	}
	
	function GetVar($name)
	{
		if (isset($_POST[$name]))
			return $_POST[$name];
	}
	
	function GetMultipleVar($name)
	{
		$value = array();
	// replace wild card with index
		if (stripos($name,"*"))
		{
			$i=0;
			while(true)
			{
				$newname = str_ireplace("*",'_'.$i,$name);
				if (!isset($_POST[$newname])) break;
				$value[] = $_POST[$newname];
				++$i;
			}
			return $value;
		}
	}
	
	
	function SetVar($name,$value)
	{
		$_POST[$name] = $value;
	}
	
	function IsVarSet($name)
	{
		return isset($_POST[$name]);
	}

	function SetNextAction($action)
	{
		$this->m_bComplete = false;
		$this->m_action = $action;
		$_POST['submitvalue']=$action;
	}

	function BackButton()
	{
		print "<form method=post action='index.php'><input type=hidden name=page value='".$this->m_name."'>";
		print "<input type=submit class=submit value=BACK></form>";
	}
	
	function Present()
	{
		if (!empty($this->m_objects))
		{
			foreach ($this->m_objects as $object)
			{
				$object->Present();
			}
		}
		
		$this->m_bComplete = true;
	}
	
	function IsComplete()
	{
		return $this->m_bComplete;
	}
	
	function  GetAction()
	{
		$data = explode(":",$this->m_action);
		return $data[0];
	}
	
	function	GetActionData()
	{
		$data = explode(":",$this->m_action);
		return $data[1];
	}
	
	function DefaultAction()
	{
		$this->SetNextAction($this->m_defaultaction);
	}
	
	function _isForm($object)
	{
		return (get_class($object)=="form" || get_class($object)=="validatedform" ||
				get_class($object)=="Form" || get_class($object)=="ValidatedForm" ||
				get_class($object)=="tablelist" || get_class($object)=="TableList");
	}
	
	function Attach(&$object)
	{
		$index = count($this->m_objects);
		$this->m_objects[$index] = $object;
		if ($this->_isForm($object))
		{
					
			$this->m_objects[$index]->SetTarget($this->m_name);
			$this->m_objects[$index]->SetAspect($this);
			$this->m_objects[$index]->AddHidden("transactionid",$_SESSION['transactionid']);
		}
				
	}
	
	function End()
	{
		print "\n<!-- End Aspect -->\n";
	}

}

?>