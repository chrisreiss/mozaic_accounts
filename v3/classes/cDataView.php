<?php

// A Data view

define('SL_NOSELECTION',0);
define('SL_SINGLESELECTION',1);
define('SL_MULTIPLESELECTION',2);
define('SL_MAXITEMS',3000);

class DataView {
	private $rows;
	private $columns;
	private $sortcolumn;
	private $sortdirection;
	private $name;
	private $views;
	private $defaultview;
	private $hidden;
	private $indexcol;

// items - these can be autoadded from a database, or added manually
// should be selectable - single and multiple (optional)
// generate events on selection
// should be resortable

	public function DataView($tablename)
	{
		$this->name = $tablename;
		$this->indexcol = 0;
	}


	public function AddViewDropDown($views,$defaultview)
	{
		$this->views = $views;
		$this->defaultview = $defaultview;
	}
	
	public function GetCurrentView()
	{
		if (empty($_POST[$this->name.'viewselect']))
			return $this->defaultview;
			
		return $_POST[$this->name.'viewselect'];
	}


 public function AddRowFromArray($array)
 {
 	$this->rows[] = $array;
 }

 public function SetFromDatabaseResults($results,$indexcol=0)
 {
 	
	foreach(current($results) as $key=>$value)
		$cols[] = $key;
	$this->SetColumns($cols);
	$this->indexcol = $indexcol;
	
	reset($results);
	foreach($results as $index=>$row)
	{
		unset($newrow);
		foreach($row as $cell)
			$newrow[] = $cell;
		$this->AddRowFromArray($newrow);	
	}

 }

 
 public function SetColumns($columns)
 {
 	$this->columns = $columns;
 }


 public function AddAction($action,$requiresselection)
 {
 	$this->action[] = array('action'=>$action,'requiresselection'=>$requiresselection);
 }
 
 private function DisplayActions()
 {
 		print '<div class="simplelistactions">';
		print '<div class="simplelistactionheader"></div>';

 	foreach($this->action as $action)
 	{
 		$type = SL_NOSELECTION;
 		if ($action['requiresselection']) $type = SL_SINGLESELECTION;

		print '<button value="'.$action['action'].'"  name="actionbutton'.$type.'" class="actionbutton" ';		
		print 'onClick="document.mainform.submitvalue.value=this.value; document.mainform.submit();" ';
		if ($type!=SL_NOSELECTION) print ' disabled';
		print '>'.$action['action'].'</button><br>';
 	}

 	print '</div>';
 }

 private function ViewSelect()
 {
	if (!empty($this->views)) {
		print "<script type=\"text/javascript\">
				function refreshview()
				{
					try {
					myform = document.getElementById('viewform');
					myform.submit();
					} catch(ex)
					{
						alert(ex);
					}
				}
				</script>";

		print '<div id="filterheader">';
		print "<form name=\"viewform\" id=\"viewform\" action=\"index.php\" method=\"post\">
			   <input type=hidden name=\"page\" value=\"".$_POST['page']."\">";

		if (!empty($_POST['submitvalue']))
			   print "<input type=hidden name=\"submitvalue\" value=\"".$_POST['submitvalue']."\">";
			   
		print "View ";
	 	print '<select name="'.$this->name.'viewselect" onchange="javascript:refreshview();">';
 		foreach($this->views as $viewkey=>$viewdescription){
			print '<option value="'.$viewkey.'" ';
			if ($this->GetCurrentView()==$viewkey) print 'selected';
			print ' >'.$viewdescription.'</option>';
		}

		print '</select>';
		print '</form>';
		print'</div>';
	}
}

 public function SetVar($key,$value)
 {
 	$this->hidden[] = array('key'=>$key,'value'=>$value);
 }

 public function Present()
 {
 	print '<div class="simplelist">';

 	$this->ViewSelect();
 	
 	print '<form method=post action="index.php">';
 	print '<input type=hidden name=page value="promos">';
	
	$this->DisplayActions();

	// display the header - set up javascript for resort events
	// display the cells

	print "<div class='simplelisttable'>";
	print '<table class="sortable" id="simpleList">';
 	print '<thead><tr><th></th>';
 	foreach($this->columns as $colindex=>$col)
 	{
 		if ($colindex!=$this->indexcol)
 			print '<th>'.$col.'</th>';
 	}
 	print '</tr></thead>';
 	
 	print '<tbody>';
 	
 	foreach($this->rows as $rowindex=>$row)
 	{
 		if ($rowindex%2==0)
		 	print "<tr>";
		else
		 	print "<tr class='odd'>";
		
		print '<td>'; // onClick="DVClickRow(this,'.$rowindex.',false)">';
		print '<input type=checkbox id=checkbox name='.$this->name.'[] value='.$row[$this->indexcol].' ></td>';
		print '</td>';
		
		foreach ($row as $colindex=>$cell)
 		{
 			if ($colindex!=$this->indexcol)
 			{ 
 				print '<td onClick="DVClickRow(this,'.$rowindex.',false)">';
				print $cell;
 				print '</td>';
 			}
 		}
 		print '</tr>';
 	}
 	
 	print '</tbody>';
 	print '</table>';
 	print '</div>'; // end scrolltable
 	
 //	print '</div>'; // end simplelist
 	
 	// add hidden vars
 	if (!empty($this->hidden))
 	{
 		foreach($this->hidden as $hidden)
 		{
 			print '<input type="hidden" name="'.$hidden['key'].'" value="'.$hidden['value'].'">'."\n";
 		}
 	}
 	
 	print '</form>';
 	print '</div>';
 }



};



?>