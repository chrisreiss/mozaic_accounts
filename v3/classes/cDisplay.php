<?php
require_once('util.php');
require_once('cTableInfo.php');
require_once('cCurrency.php');

class Display
{

	function 	GetString($table,$field,$value)
	{
		$info = TableInfo::GetField($table,$field);
				
		switch ($fieldinfo['type'])
		{
			case "creditor":
			{
				$result = Database::Query("SELECT name FROM creditor WHERE uid='".$value."';");
				if ($result->GetNum()>0)
				{
					$row=$result->GetNext();
					return $row['name'];
				}
			}
			case "date":
			{
				return displaydate($value);
			}
			case "currency":
			{
				return Currency::DATABASETODISPLAY($value);
			}
		}
		return $value;
	}

	// This is a useful function to resolve s formatted string using the datatables
	// string has format:
	// #table:field.table:field ... #
	function	ResolveVar($string,$row)
	{
		$string = str_replace("#","",$string);
		
		$nest = explode(".",$string);
		if ($nest==false)
		{
			$nest[0] = $string;
		}
		
		$value = "";
		$fieldname = "";
		for ($i=0;$i<count($nest);++$i)
		{
			$lhsrhs = explode(":",$nest[$i]);
			if (count($lhsrhs)==1)
			{
				$lhsrhs[0] = "";
				$lhsrhs[1] = $nest[$i];
			}
			if (count($lhsrhs)>2)
			{
				return "Error: To many colon delimiters";
			}
			if ($i==0 && $lhsrhs[0]=="" && empty($row))
			{
				return "Error: No table specified - requires row data";
			}
			
			if ($lhsrhs[0]=="")
			{
				$value = $row[$lhsrhs[1]];
				$fieldname = $lhsrhs[1];
			}
			else
			{
				if ($value=="")
				{
					if ($fieldname=="artist")
					{
						return "Various Artist";
					}
					else
					{
						return "";
						//return "Error: bad index ".$lhsrhs[0]." ".$lhsrhs[1];
					}
				}
				
				$tables = explode("|",$lhsrhs[0]);
				for ($j=0;$j<count($tables);++$j)
				{
					// need to get a table
					$query = "SELECT ".$lhsrhs[1]." FROM ".$tables[$j]." WHERE uid = '".$value."';";
					$result = Database::Query($query);
					if ($result->GetNum()>0)
					{
						$row = $result->GetNext();
						$value = $row[$lhsrhs[1]];
						$fieldname = $lhsrhs[1];
						Database::FinishQuery($result);
						break;
					}
				}
				if ($value=="")
				{	
					if ($fieldname=="artist" && $value=="")
					{
						return "Various Artists";
					}
					return "??"; //.$value.".".$lhsrhs[0].":".$lhsrhs[1]."?";
				}
			}
		}
		if ($fieldname=="value" || $fieldname=="amount")
		{
			return Currency::DATABASETODISPLAY($value);
		}
		else if ($fieldname=="date" || $fieldname=="invoicedate" || $fieldname=="paiddate" || $fieldname=="startdate")
		{
			return displaydate($value);
		}
		else if ($fieldname=="dealtype")
		{
			return TableInfo::DisplayFromType("dealtype",$value);
		}
		if ($fieldname=="artist" && ($value=="" || $value==0))
		{
			return "Various Artists";
		}
		
		return $value;
	}

	function ResolveString($display,$assoc)
	{
		while(true)
		{
			$delimit1 = strpos($display,'%',0);
			if ($delimit1===false)
			{
				return $display;
			}
			$delimit2 = strpos($display,'%',$delimit1+1);
			$item = substr($display,$delimit1+1,$delimit2-$delimit1-1);
			$replaceitem = Display::ResolveVar($item,$assoc);
			$display = substr_replace($display,$replaceitem,$delimit1,$delimit2-$delimit1+1);
		}	
		return $display;
	}
}

?>