<?php
require_once('cDatabase.php');


define('SL_NOSELECTION',0);
define('SL_SINGLESELECTION',1);
define('SL_MULTIPLESELECTION',2);
define('SL_MAXITEMS',3000);

/*-------------
	SIMPLE LIST
  -------------*/

class SimpleList {
	var $_rows;		// data in the table
	var $_listname; // name of the form
	var $_key;		// the column to use as the key (passed as result of form submit)
	var $_hidekey;  // should the key be hidden or displayed?
	var $_actioncount;
	
	public function __construct()
	{
		$this->validate = false;
		// empty
	}

	public function SimpleList($name)
	{
		$this->_listname = $name;
	}

	public function Present()
	{
		$startid = getVar('startid',0);
		$this->populate($startid);	// child class overrides this
		$this->display();
	}

	protected function actions()
	{
		// do nothing in base class
	}
	
	protected function containerpage()
	{
		return "home";
	}
	
	// $type = 0 - NO SELECTION REQUIRED, 1 - SINGLE SELECTION ONLY, 2 - MULTIPLE SELECTION ALLOWED
	protected function addaction($cmd,$description,$type=SL_NOSELECTION)
	{
		print '<button value="'.$cmd.'"  name="actionbutton'.$type.'" class="actionbutton" ';		
		print 'onClick="document.mainform.submitvalue.value=this.value; document.mainform.submit();" ';
		if ($type!=SL_NOSELECTION) print ' disabled';
		print '>'.$description.'</button><br>';
		$this->actioncount++;
	}

	protected function addconfirmaction($cmd,$description,$type=SL_NOSELECTION)
	{
		print '<button value="'.$cmd.'"  name="actionbutton'.$type.'" class="actionbutton" ';		
		print 'onClick="if (confirmbutton()) { document.mainform.submitvalue.value=this.value; document.mainform.submit(); } else return false;" ';
		if ($type!=SL_NOSELECTION) print ' disabled';
		print '>'.$description.'</button><br>';
		$this->actioncount++;
	}

	protected function addfilter($field,$description,$values)
	{
		print $description.'<select name="'.$field.'" class="filter"> ';
		foreach($values as $row)
		{
			print '<option value="'.$row.'">'.$row.'</option>';
		}
		print '</select>';
	}

	
	protected function filters()
	{
			
	// do nothing in base class
	}
	
	protected function footer()
	{
		// do nothing in base class
	}

	private function display()
	{

		print '<div class="simplelist">';
		print '<form method="post" name="mainform" action="index.php">';
		print '<input type="hidden" name="page" value="'.$this->containerpage().'">';
		print '<input type="hidden" name="submitvalue">';
		
		print '<div id="filterheader">';
		$this->filters();
		print'</div>';
		// Forward backwards pages	
		$startid = getVarS('startid',0);
	/*	if (count($this->_rows)>=SL_MAXITEMS || $startid!=0)
		{
			print '<span class="rowcount">Showing rows ';
	$text = 'try {but = document.createElement(\'INPUT\'); but.type = \'hidden\'; but.name = \'startid\'; but.value = \''.($startid-SL_MAXITEMS).'\'; document.mainform.appendChild(but); document.mainform.submit(); } catch(ex) { alert(ex); }';
			
			if ($startid>0)
				print '<a class="rowcount" href="#" onClick="'.$text.'"><<</a> '; 
			else
				print '<a class="rowcount" href="#" disabled><<</a> ';
			print $startid.' - '.($startid+SL_MAXITEMS);
	
	$text = 'try {but = document.createElement(\'INPUT\'); but.type = \'hidden\'; but.name = \'startid\'; but.value = \''.($startid+SL_MAXITEMS).'\'; document.mainform.appendChild(but); document.mainform.submit(); } catch(ex) { alert(ex); }';
	
			print ' <a class="rowcount" href="#" onClick="'.$text.'">>></a> </span>'; 
	
	
		}*/
		print '<div class="simplelistactions">';
		print '<div class="simplelistactionheader"></div>';
		$this->actioncount = 0;
		$this->actions();
		print '</div>';

		print "<div class='simplelisttable'>";

		print '<table class="sortable" id="simpleList">';
		print "<thead>";
		print "<tr><th class='unsortable'></th>";
		foreach ($this->_header as $col)
		{
			print '<th>'.$col.'</th>';
		}
		print "</tr></thead>"; 
		print "<tbody>";
		if (!empty($this->_rows))
		{
			foreach ($this->_rows as $i=>$row)
			{
				print '<tr onClick="listClick(this);">';
				if (isset($this->_key)) $key = $row[$this->_key]; else $key=$i;
				print '<td><input type="checkbox" name="'.$this->_listname.'[]" value="'.$row[$this->_key].'" ></td>'."\n";
				//onClick="toggleAllButtons(this);"
				foreach ($row as $key=>$cell)
				{
					if (!$this->_hidekey || $key!=$this->_key)
						print '<td>'.$cell.'</td>';
				}
				print '</tr>';
			}
		}
		print '</tbody>';
		print '</table>';
		print '</div>';
		

		print '</form>';
		$this->footer();

		print '</div>';

	}

}

?>