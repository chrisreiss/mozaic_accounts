<?php
// This class display a list of items in a table together with a bunch of views
// and some actions

require_once("cDisplay.php");

define("ACTION_ENABLELIST",0);
define("ACTION_TOOLTIP",1);
define("ACTION_CONFIRM",2);

class TableList {

	var $m_listname;
	var $m_views;
	var $m_numviews;
	var $m_currentview;
	var $m_parent;
	var $m_actions;
	var $m_uidfield = "uid";
	var $m_items;
	var $m_hidden;
	var $m_title = "";
	var $m_splitmode = false;
	var $m_selected;
	var $m_ajaxhandler = "";
	var $m_footer = "";
	var $m_sortfield;	
	var $m_sortdirection = SORT_ASC;
	var $m_extraconditions = "";
	var $m_multipleselection = false;
	
	function	SetInfoBoxHandler($handler)
	{
		$this->m_ajaxhandler = $handler;
	}
	
	function	SetExtraConditions($sqlcondition)
	{
		$this->m_extraconditions = $sqlcondition;
	}
	
	function	Begin($name,$parent)
	{
		$this->m_parent = $parent;
		$this->m_listname = $name;
		$this->m_numviews = 0;
		$this->m_actioncount = 0;
		$this->m_selected = "";
		$this->m_ajaxhandler = "";
		$this->m_sortdirection = SORT_ASC;
		
		if (empty($_POST['viewselect']))
		{
			$this->m_currentview = 0;
		}
		else
		{
			$this->m_currentview = $_POST['viewselect'];
		}
	}
	
	function AddFooter($string)
	{
		$this->m_footer = $string;
	}
	
/*	function	SetValueField($fieldname)
	{
		$this->m_uidfield = $fieldname;
	}*/
	
	function SetSplitMode($bool)
	{
		$this->m_splitmode = $bool;
	}
	
	function AddTitle($title)
	{
		$this->m_title = $title;
	}

	function SetUidField($field)
	{
		$this->m_uidfield = $field;
	}
	
	// AddAction( name_of_action, boolean_is_disabled_without_selection, comma_seperated_list_of_values_enable_to )
	// if no third value then will always enable with any selection
	function AddAction()
	{
		$numargs = func_num_args();
		$args = func_get_args();
		$action["name"] = $args[0];
		if (isset($args[1])) 
			$action["disabledstate"] = $args[1];
		else
			$action['disabledstate'] = false;
		$action["values"] = "";
		
		$i = 2;
		while ($numargs-$i>0)
		{

			switch ($args[$i])
			{
				case ACTION_ENABLELIST:
				{
					if ($args[$i+1]=="") 
						$action['values'] = "*none*";
					else
						$action['values'] = $args[$i+1];
					break;
				}
				case ACTION_TOOLTIP:
				{
					$action['tooltip'] = $args[$i+1];
					break;
				}
				case ACTION_CONFIRM:
				{
					$action['confirm']=$args[$i+1];
					break;
				}
			}
			$i += 2;			
		}
		$this->m_actions[] = $action;
	}
	
	// Adds a confirm message to the action
	function SetConfirm($actionname,$confirmtext)
	{
		foreach ($this->m_actions as $index=>$action)
		{
			if ($action['name']==$actionname)
			{
				$this->m_actions[$index]['confirm']=$confirmtext;
				break;
			}
		}
	}
	
	function DisplayActions()
	{
		print "\n<script type=\"text/javascript\">\n";
		print "function toggleAllButtons(ob)\n";
		print "{\n";
		
		// update the info box
		print "var updateInfoBox = function(str) { var infobox = document.getElementById('infobox'); infobox.innerHTML=str; }";
		print "\n";
		
		print "	var nav,numselected=0;\n";
		print " selected = new Array();\n";
		print " for (var i = 0; i < ob.options.length; i++) if (ob.options[ i ].selected) { numselected++; selected.push(ob.options[ i ].value); }\n";
		print " if (numselected>0) {\n";	
		$actioncount = count($this->m_actions);
		for ($i=0;$i<$actioncount;++$i)
		{
			if ($this->m_actions[$i]['disabledstate']==true)
			{

				print " nav = document.getElementById('cViewMenu".$i."');\n";
				if ($this->m_actions[$i]['values']!="")
				{
					$values = explode(",",$this->m_actions[$i]['values']);
					$negate = false;
					if ($values[0][0]=="~")
					{
						$values[0] = substr($values[0],1);
						$negate = true;
						print "  nav.disabled = false;\n";
					}
					else
					{
						print "  nav.disabled = true;\n";
					}
					$numvalues = count($values);
					for ($j=0;$j<$numvalues;++$j)
					{
						if ($values[$j]!="")
						{
							if (!$negate)
								print "  if (selected[0]=='".$values[$j]."') nav.disabled = false;\n";
							else
								print "  if (selected[0]=='".$values[$j]."') nav.disabled = true;\n";
						}
					}
				}
				else
				{	
					print "  nav.disabled = false;\n";
				}
			}
		}
		print "}\n";
		// Do ajax stuff!
		if ($this->m_ajaxhandler!="")
		{	
			print " var infobox = document.getElementById('infobox'); infobox.innerHTML='Updating...'; \n";
			print " try { var ajaxtest = new Ajax();\n";
			print " ajaxtest.doGet('ajaxservice.php?handler=".$this->m_ajaxhandler."&id='+selected[0],updateInfoBox); } catch (e) { alert(e) }\n";
		}

		print "}\n";
		print "</script>\n";

		print "<table>\n";
		print "<input type=hidden name=submitvalue>";
		for ($i=0;$i<$actioncount;++$i)
		{
			print "<tr><td>";

			print "<button value=\"".$this->m_actions[$i]['name']."\" id=\"cViewMenu".$i."\" class=\"submit\" ";
			// Check if mousetip?
			if (!empty($this->m_actions[$i]['tooltip']))
				print "onmouseover=\"Tip('".addslashes($this->m_actions[$i]['tooltip'])."',WIDTH,300,DELAY,800)\" ";
			// Check if confirm
			if (!empty($this->m_actions[$i]['confirm']))
				print "onClick=\"if (confirm('".$this->m_actions[$i]['confirm']."')){ document.mainform.submitvalue.value='".$this->m_actions[$i]['name']."'; document.mainform.submit()}\" ";
			else
				print "onClick=\"document.mainform.submitvalue.value='".$this->m_actions[$i]['name']."'; document.mainform.submit()\" ";
			
			if ($this->m_actions[$i]['disabledstate']==true)
				print "disabled ";
			print ">".$this->m_actions[$i]['name']."</button>";

			print "</td></tr>\n";
		}
		print "</table><br>\n";
		
		// Display info box
		if ($this->m_ajaxhandler!="")
		{
			print "<table class=previewpane><tr><td bgcolor=#8CA9B5><b><i>Overview</i></b></td></tr><tr><td id='infobox'></td></tr></table>";
		}
	}

	function	SetMultipleSelection($bool)
	{
		$this->m_multipleselection = $bool;
	}

	// $viewname, $table, $itemdisplay, $groupby, $orderby, $groupbydisplay, $filter (where condition)
	function 	AddView()
	{
		$numargs = func_num_args();
		$args = func_get_args();
		if ($numargs==5)
			$args[5] = "%uid%";
			
		if (!isset($args[6])) $args[6]="";
			
		$this->m_views[$this->m_numviews] = array("viewname"=>$args[0], "table"=>$args[1], "itemdisplay"=>$args[2], "groupby"=>$args[3], "orderby"=>$args[4], "groupbydisplay"=>$args[5], "where"=>$args[6]);
		$this->m_numviews++;
	}
	
	function	AddFilter($filtername, $associatedviews, $field)
	{
	}
	
	function 	SelectView($viewname)
	{
		$this->m_currentview = 0;
		for ($i=0;$i<$this->m_numviews;++$i)
		{
			if ($this->m_views[i]["viewname"]==$viewname)
			{
				$this->m_currentview = $i;
				return;
			}
		}
		
	}
	
	function 	AddItem($group,$item,$description)
	{
		$args = func_get_args();
		if (isset($args[3]))
			$sort = $args[3];
		else
			$sort = $description;
		$this->m_items[] = array("group"=>$group,"item"=>$item,"description"=>$description,"sort"=>$sort);
	}

	function 	AddHidden($name,$value)
	{
		$this->m_hidden[] = array("name"=>$name,"value"=>$value);
	}
	
	function	GetFieldsFromDisplayItem($itemdisplay)
	{
		// stub for now
		return "*";
	}
	
	// subroutine
	function	DisplayItem($assoc)
	{
		$display = $this->m_views[$this->m_currentview]["itemdisplay"];
		return Display::ResolveString($display,$assoc);
	}
	
	// Sub routine
	function 	DisplayViewSelect()
	{
	
	//print_r($_POST);
		// Display the drop down
		if ($this->m_numviews>1)
		{
			print "<script type=\"text/javascript\">
					function refreshview()
					{
						try {
						myform = document.getElementById('viewform');
						myform.submit();
						} catch(ex)
						{
							alert(ex);
						}
					}
					</script>";

			print "<form name=\"viewform\" id=\"viewform\" action=\"index.php\" method=\"post\">
				   <input type=hidden name=\"page\" value=\"".$this->m_parent."\">";
			if (!empty($_POST['submitvalue']))
				   print "<input type=hidden name=\"submitvalue\" value=\"".$_POST['submitvalue']."\">";
				   
			print "View ";
			print "<select name='viewselect' onchange=\"javascript:refreshview();\">\n";
			for ($i=0;$i<$this->m_numviews;++$i)
			{
				print "<option value=".$i;
				if ($this->m_currentview==$i) print " selected ";
				print ">".$this->m_views[$i]["viewname"]."</option>";
			}
			print "</select>\n";
			print "</form>";
		}
	}
	
	function	SetSortReverse($bool)
	{
		if ($bool)
			$this->m_sortdirection = SORT_DESC;
		else 
			$this->m_sortdirection = SORT_ASC;
	}
	
	function 	PresentSingleView()
	{
		// sort the items
		if (!empty($this->m_items))
		{
			foreach ($this->m_items as $key => $row) 
			{
				$sort1[$key]  = $row['sort'];
				$sort2[$key]  = $row['group'];
			}
			array_multisort($sort2, $this->m_sortdirection,  
							$sort1, $this->m_sortdirection,
							$this->m_items);
		}

		// ??
		$numrows = count($this->m_items);
		print "<select name=\"".$this->m_listname;
		if ($this->m_multipleselection==true) print "[]\" multiple ";
		print "\" size=40 class=\"list\" onClick=\"javascript:toggleAllButtons(this);\">";
		$currentgroup="";
		for ($i=0;$i<$numrows;++$i)
		{
			if ($this->m_items[$i]['group']!=$currentgroup)
			{
				if ($currentgroup!="")
				{
					print "</optgroup>\n";
				}
				$currentgroup = $this->m_items[$i]['group'];
				print '<optgroup label=\''.$currentgroup.'\'>';
				
			}
			print "<option value='".$this->m_items[$i]['item']."'>";
			print $this->m_items[$i]['description'];
			print "</option>";
		}
		print "</select>";
	}
	
	function ReverseDirection($bool)
	{
		if ($bool)
			$this->m_sortdirection = SORT_DESC;
		else
			$this->m_sortdirection = SORT_ASC;
	}
	
	function SetTarget($name)
	{
		$this->m_parent = $name;
	}
	
	function SetAspect($aspect)
	{
		// ignore this for the moment??
	}
	
	function _AddItemsFromView()
	{
		$view = $this->m_views[$this->m_currentview];
		$tablefields = $this->GetFieldsFromDisplayItem($view["itemdisplay"]);
		
		if ($view["groupby"]=="")
		{
			$sqlquery = "SELECT ".$tablefields." FROM ".$view["table"]." ";
			if ($this->m_extraconditions!="") $sqlquery .= "WHERE ".$this->m_extraconditions." ";
			else if ($view['where']!="") $sqlquery .= "WHERE ".$view['where']." ";
      $sqlquery .= "ORDER BY ".$view["orderby"]." LIMIT 5000;";
		}
		else
		{
			$sqlquery = "SELECT ".$tablefields." FROM ".$view["table"]." ";
			if ($this->m_extraconditions!="") $sqlquery .= "WHERE ".$this->m_extraconditions." ";
			else if ($view['where']!="") $sqlquery .= "WHERE ".$view['where']." ";
      $sqlquery .="ORDER BY ".$view["groupby"].",".$view["orderby"]." LIMIT 5000;";
		}
//		print "<br>".$sqlquery." SDFS ".$this->m_extraconditions;
//		print_r($view);
//print $sqlquery."<br>";
		$result = Database::Query($sqlquery);
		$numrows = $result->GetNum();
		if ($view["groupby"]=="")
		{
			for ($i=0;$i<$numrows;++$i)
			{
				$row = $result->GetNext();
				$this->m_items[] = array("group"=>"","sort"=>$row[$view['orderby']],"item"=>$row[$this->m_uidfield],"description"=>$this->DisplayItem($row));
			}		
		}
		else
		{
			if ($numrows===0) return; 
			
			$lastgroup = "-";
			for ($i=0;$i<$numrows;++$i)
			{
				$row = $result->GetNext();
				if ($lastgroup!=$row[$view["groupby"]])
				{
					$groupdisplay = GetEnumDescription($view["groupby"],Display::ResolveString($view["groupbydisplay"],$row));
					$lastgroup = $row[$view["groupby"]];
				}
				$rawlist [ $groupdisplay ] [] = $row;
			}
			Database::FinishQuery($result);
	
			// Sort by key
			ksort($rawlist);
			
			$numgroups = count($rawlist);
			foreach ($rawlist as $group=>$groupitems)
			{
				foreach ($groupitems as $item)
				{
					$this->m_items[] = array("group"=>$group,"sort"=>$item[$view['orderby']],
					"item"=>$item[$this->m_uidfield],"description"=>$this->DisplayItem($item));
				}
			}
		}			
	}
	
	
	function 	PresentDualPane()
	{
		// for brevity
		print "<table border=0 cellspacing=0 cellpadding=0><tr><td>";
		$view = $this->m_views[$this->m_currentview];
		$tablefields = $this->GetFieldsFromDisplayItem($view["itemdisplay"]);
		$sqlquery = "SELECT DISTINCT ".$view["groupby"]." FROM ".$view["table"]." ";
		if ($this->m_extraconditions!="") $sqlquery .= "WHERE ".$this->m_extraconditions." ";
    $sqlquery .= "ORDER BY ".$view["groupby"].";";
 //   print $sqlquery."<br>";
		$result = Database::Query($sqlquery);
		$numrows = $result->GetNum();
		print "<select name=\"".$this->m_listname."2\" class=\"list\" onChange=\"javascript:updateDualPanelist(this);\">";
		$selectedgroup="";
		if (!empty($_POST['selectedgroup']))
		{
			$selectedgroup = $_POST['selectedgroup'];
		}
//		print "numrow=".$numrows;
		for ($i=0;$i<$numrows;++$i)
		{
//		print $i." ";
			$row = $result->GetNext();
			if ($selectedgroup=="" && $i==0)
				$selectedgroup = $row[$view['groupby']];

			print "<option value='".$row[$view["groupby"]]."'";
			if ($selectedgroup==$row[$view["groupby"]])
			{
				print " selected ";
			}
			print ">".
				GetEnumDescription($view["groupby"],Display::ResolveString($view["groupbydisplay"],$row)).
					"</option>\n";
		}
		print "</select>";
		print "</td></tr><tr><td>";
		Database::FinishQuery($result);
		$tablefields = $this->GetFieldsFromDisplayItem($view["itemdisplay"]);
		$sqlquery = "SELECT ".$tablefields." FROM ".$view["table"]." WHERE ".$view["groupby"]."='".$selectedgroup."' ";
    	if ($this->m_extraconditions!="") $sqlquery.= "AND ".$this->m_extraconditions." ";
    	$sqlquery .= "ORDER BY ".$view["orderby"].";";
//  	  print $sqlquery."<br>";
		$result = Database::Query($sqlquery);
//		print_r($result);
		$numrows = $result->GetNum();
		print "<select name=\"".$this->m_listname."[]\" ";
		if ($this->m_multipleselection==true) print "multiple ";
		print "size=32 class=\"list\" onClick=\"javascript:toggleAllButtons(this);\">";
		for ($i=0;$i<$numrows;++$i)
		{
			$row = $result->GetNext();
			print "<option value='".$row[$this->m_uidfield]."'>";
			print $this->DisplayItem($row);
//			print_r ($row);
			print '</option>';
		}
		print "</select>";
		print "</td></tr></table>";
		Database::FinishQuery($result);
	}
		

	function	AddHiddenFields()
	{
		for ($i=0;$i<count($this->m_hidden);++$i)
		{
			print "<input type=hidden name='".$this->m_hidden[$i]['name']."' value='".$this->m_hidden[$i]['value']."'>\n";
		}
	}
	
	function	Select($index)
	{
		$this->m_selected = $index;
	}
	
	// Display the list
	function 	End()
	{		
		if ($this->m_title!="")
		{
			print '<span class="formtitle">'.$this->m_title.'</span>';
		}
		
		$this->DisplayViewSelect();

		print "<form id=\"mainform\" name=\"mainform\" action=\"index.php\" method=\"post\">\n<input type=\"hidden\" name=\"page\" value=\"".$this->m_parent."\">\n";
		if ($this->m_numviews==0)
		{
			$this->PresentSingleView();
		}
		else
		{
			if ($this->m_splitmode)
			{
				$this->PresentDualPane();
			}
			else
			{
				$this->_AddItemsFromView();
				$this->PresentSingleView();
			}
		}
		$this->DisplayActions();
		print $this->m_footer;
		$this->AddHiddenFields();
		print "</form>\n";
	}
	
			
	function 	Present()
	{
		$this->End();
	}


}
?>
