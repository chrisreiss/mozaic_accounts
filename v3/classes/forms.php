<?php

function _selectListFromResult($result)
{
	$num_result = $result->GetNum();
	$list = "";
	for ($j=0;$j<$num_result;++$j)
	{
		$creditor = $result->GetNextRow();
		if ($j<$num_result-1)
			$list = $list.$creditor[0]."=".$creditor[1].",";
		else
			$list = $list.$creditor[0]."=".$creditor[1];
	}
	return $list;
}

// take a comma seperated list and do a drop list
function _dropListEnumerated($list,$name,$value,$addcode)
{
	$out = "\n<select id='".$name."' name='".$name."' $addcode>\n";
	$found = false;
	
	$values = explode(",",$list);
	foreach($values as $v)
	{
		if ($v=="")
		{
			$out .= '<option disabled="disabled">---------</option>';
		}
		else
		{
			$parts = explode("=",$v);
			$enum = $parts[0];
			$enumname = $parts[1];		
			if ($enum==$value)
			{
				$found = true;
				$out .= "<option selected value='".$enum."'>".$enumname."</option>\n";
			}
			else
				$out .= "<option value='".$enum."'>".$enumname."</option>\n";
		}
	}
	if ($found==false)
	{
		$out .= "<option selected value='".$value."' >".$value."</option>\n";
	}
	$out .= "</select>\n";
	return $out;
}	


function form_Begin($title="", $name="mainform",$action="index.php",$javascript="") 
{
	print '<form name="'.$name.'" method="post" class="mform" action="'.$action.'" id="validateform"><table>'."\n"; 
 	print '<u>'.$title.'</u><br>'; 
}

function form_End()
{ 
	print '</table></form>'."\n"; 
}

function button_Submit($description,$value,$addcode="")
{
	$out = "<button type=submit class=submit name=submitvalue id='".$value."' value='".$value."'";
	$out .= $addcode.">$description</button>";
	return $out;
}

function form_Field($description,$input)
{
	print '<tr><td><label>'.$description.'</label></td><td>'.$input.'</td></tr>'."\n";
}

function input_Text($name,$value,$addcode="")
{
	return '<input type="text" name="'.$name.'" id="'.$name.'" maxlength="48" size="48" value="'.$value.'" '.$addcode.'>';
 
}

function input_ArtistDropdown($name,$value,$addcode="")
{
	$result = Database::Query("SELECT uid,realname FROM artist ORDER BY realname");
	$list = _selectListFromResult($result);
	return _dropListEnumerated($list,$name,$value,$addcode);	
}

function input_Hidden($name,$value)
{
	return '<input type=hidden name="'.$name.'" value="'.$value.'">';
}


?>