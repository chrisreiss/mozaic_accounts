<?php
require_once("reader.php");
require_once("class.ezpdf.php");

/***
* if you want you can change 'iconv' to mb_convert_encoding:
* $data->setUTFEncoder('mb');
*
**/

/***
* By default rows & cols indeces start with 1
* For change initial index use:
* $data->setRowColOffset(0);
*
**/



/***
*  Some function for formatting output.
* $data->setDefaultFormat('%.2f');
* setDefaultFormat - set format for columns with unknown formatting
*
* $data->setColumnFormat(4, '%.3f');
* setColumnFormat - set format for column (apply only to number fields)
*
**/


class WorkSheet
{
	var $m_cells;
	var $m_row = 0;
	var $m_cell = 0;
	var $m_numsheets = 0;
	
	function GetNumSheets()
	{
		return $this->m_numsheets;
	}
	
	function GetNumRow()
	{
		return count($this->m_cells[0]);
	}

	function GetNumCol()
	{
		return count($this->m_cells);
	}
	
	function IsEmpty()
	{
		return ($this->m_row==0 && $this->m_cell==0);
	}
	
	function Append(&$worksheet)
	{
		if (empty($worksheet->m_cells)) return;
		$numrows = $worksheet->m_row;
		foreach($worksheet->m_cells as $row)
		{
			$this->m_cells[] = $row;
		}		
		$this->m_row += $numrows;
		$this->m_cell = 0;
	}
	
	    /**
     * Create a 2D array from a CSV string
     *
     * @param mixed $data 2D array
     * @param string $delimiter Field delimiter
     * @param string $enclosure Field enclosure
     * @param string $newline Line seperator
     * @return
     */
    function parseCSV($data, $delimiter = ',', $enclosure = '"', $newline = "\n"){
        $pos = $last_pos = -1;
        $end = strlen($data);
        $row = 0;
        $quote_open = false;
        $trim_quote = false;

        $return = array();

        // Create a continuous loop
        for ($i = -1;; ++$i){
            ++$pos;
            // Get the positions
            $comma_pos = strpos($data, $delimiter, $pos);
            $quote_pos = strpos($data, $enclosure, $pos);
            $newline_pos = strpos($data, $newline, $pos);
			$rline_pos = strpos($data, "\r", $pos);
			//print $newline_pos.",".$rline_pos."<br>";
			if ($rline_pos!==false) 
				$newline_pos = $rline_pos;
			//print $newline_pos."<br>";
            // Which one comes first?
            $pos = min(($comma_pos === false) ? $end : $comma_pos, ($quote_pos === false) ? $end : $quote_pos, ($newline_pos === false) ? $end : $newline_pos);

            // Cache it
            $char = (isset($data[$pos])) ? $data[$pos] : null;
            $done = ($pos == $end);

            // It it a special character?
            if ($done || $char == $delimiter || $char == $newline){

                // Ignore it as we're still in a quote
                if ($quote_open && !$done){
                    continue;
                }

                $length = $pos - ++$last_pos;

                // Is the last thing a quote?
                if ($trim_quote){
                    // Well then get rid of it
                    --$length;
                }

                // Get all the contents of this column
                $return[$row][] = ($length > 0) ? str_replace($enclosure . $enclosure, $enclosure, substr($data, $last_pos, $length)) : '';

                // And we're done
                if ($done){
                    break;
                }

                // Save the last position
                $last_pos = $pos;

                // Next row?
                if ($char == $newline){
                    ++$row;
                }

                $trim_quote = false;
            }
            // Our quote?
            else if ($char == $enclosure){

                // Toggle it
                if ($quote_open == false){
                    // It's an opening quote
                    $quote_open = true;
                    $trim_quote = false;

                    // Trim this opening quote?
                    if ($last_pos + 1 == $pos){
                        ++$last_pos;
                    }

                }
                else {
                    // It's a closing quote
                    $quote_open = false;

                    // Trim the last quote?
                    $trim_quote = true;
                }

            }

        }

        return $return;
    }


   	
	function Load($filename)
	{
		$type = substr($filename,strrpos($filename,".")+1);

		if (is_file($filename))
		{
			if (strcasecmp($type,"csv")==0)
			{
				$j =1;
			/*	$fp = fopen($filename,"r");
				while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) 
				{
				    for ($i=1;$i<=count($data);++$i)
					{
						$worksheet[$j][$i] = $data[$i-1];
					}
					++$j;
			    }
				fclose($fp);
*/
				$data = file_get_contents($filename);
				if (strpos($data,"\n")===false)
				{
					if (strpos($data,"\r")===false)
					{
						die ("Not CSV recognised");
					}
					else
					{
						$rows = explode("\r",$data);
					}
				}
				else
				{
					$rows = explode("\n",$data);
				}
				$worksheet[] = array();
				// guess the delimiter
				$delimiter = ",";
				if (substr_count($rows[0],";")>5)
					$delimiter = ";";
					
				foreach($rows as $row)
				{
					$worksheet[] = csv_explode($delimiter.$row,$delimiter); 
				}
				//print_r($worksheet);
				//return WorkSheet::parseCSV($data, ',','"', "\n");

				return $worksheet;
			}
			else if (strcasecmp($type,"xls")==0)
			{
				$data = new Spreadsheet_Excel_Reader();
				$data->setOutputEncoding('CP1251');
				$data->read($filename);
				print "processing ".$data->sheets[0]['numRows']." rows x ".$data->sheets[0]['numCols']." cols<br>";
				
				for ($j=1;$j<=$data->sheets[0]['numRows'];++$j)
				{
					for ($i=1;$i<=$data->sheets[0]['numCols'];++$i)
					{
						if (isset($data->sheets[0]['cells'][$j][$i]))
							$worksheet[$j][$i] = addslashes($data->sheets[0]['cells'][$j][$i]);
						else
							$worksheet[$j][$i] = "";
					}
				}

				return $worksheet;
			}
			else
			{
				print "cWorksheet: Unsupported type '".$type."'";
			}
		}
		else
		{
			print "cWorksheet: Cannot load $filename";
		}
	}
	
	function SetCell($row,$col,$value)
	{
		$this->m_cells[$row][$col] = $value;
	}
	
	function GotoCell($row,$col)
	{
		$this->m_row = $row;
		$this->m_col = $col;
	}

	function AddCell($value)
	{
		$this->m_cells[$this->m_row][$this->m_col] = $value;
		$this->m_col += 1;
	}
	
	function GotoNextRow()
	{
		$this->m_row++;
		$this->m_cell=0;
	}

	function AddEmptyRow()
	{
		$this->AddRow("*");
	}

	function AddTitleRow()
	{
		$this->AddRow(" ");
		$args = func_get_args();
		foreach ($args as $i=>$arg)
		{
			$this->SetCell($this->m_row,$i,"<b><u>".$arg."</u></b>");
		}
		$this->m_row++;
		$this->m_cell=0;
	}
	
	function AddRow()
	{
		$col = 0;
		$row = $this->m_row;
		$args = func_get_args();
		foreach ($args as $arg)
		{
			$this->SetCell($row,$col,$arg);
			++$col;
		}
		++$row;
		$this->GotoCell($row,$col);
	}

	// bold them
	function AddHeaderRow()
	{
		$col = 0;
		$row = $this->m_row;
		$args = func_get_args();
		foreach ($args as $arg)
		{
			$this->SetCell($row,$col,"<b>".$arg."</b>");
			++$col;
		}
		++$row;
		$this->GotoCell($row,$col);
	}
	
	function DisplayAsTable()
	{
		print "<table class=spreadsheet width=100%>\n";
		foreach ($this->m_cells as $row)
		{
			print "<tr>";
			foreach ($row as $cell)
			{
				print "<td>".$cell."</td>";
			}
			print "</tr>\n";
		}	
		print "</table>\n";
	}
	
 	function SaveAsCSV($filename)
	{
		$csv = "";
		foreach ($this->m_cells as $row)
		{
			foreach ($row as $cell)
			{
				$csv .= strip_tags($cell).",";
			}
			$csv .= "\n"; 
		}	
		$fd = fopen($filename,"wb");
		fwrite($fd,$csv,strlen($csv));
		fclose($fd);
	}
	
	
	function SaveAsPDF($filename)
	{
		$pdf = new Cezpdf();
		$pdf->selectFont('./thirdparty/fonts/Helvetica.afm');

		$maxwidth = 0;
		foreach ($this->m_cells as $row)
		{
			$count = count($row);
			if ($count>$maxwidth) $maxwidth=$count;
		}

		foreach ($this->m_cells as $row)
		{
			for ($j=0;$j<$maxwidth;++$j)
			{
				if (empty($row[$j]))
					$newrow[$j] = "";
				else
				{
				  // need to check for pound signs - pdf doesn't like the html format
					$newrow[$j] = str_replace("&#163;","�",$row[$j]);
				}
			}
			$data[] = $newrow;
		}	
		
		$pdf->ezTable($data,"","",array("showLines"=>0,"fontSize"=>8,"width"=>500,"showHeadings"=>0,"shaded"=>0));
		$pdfdata = $pdf->ezOutput();

		$folderPath = $_SERVER['DOCUMENT_ROOT'] . '/accounts/' . $_SESSION['accountid'] . "/";
		$file = basename($filename);
		
		$fd = fopen($folderPath . $file,"wb");
		if ($fd===FALSE) die('Could not save file '.$filename);
		fwrite($fd,$pdfdata,strlen($pdfdata));
		fclose($fd);
	}
	

}

?>
