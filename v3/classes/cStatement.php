<?php

include_once("cWorkSheet.php");
include_once("cTableInfo.php");
include_once("cCurrency.php");
require_once("cFile.php");

define("TRANSACTION_SALE","0");
define("TRANSACTION_EXPENSE","1");
define("TRANSACTION_DUMMY","4");

define("DEBUG_MODE","0");		// displays additional heuristics

// UPDATE TO TABLE
// ALTER TABLE `sale` ADD `baseprice` VARCHAR( 16 ) NOT NULL AFTER `quantity` ;
// ALTER TABLE `deals` ADD `packagingdeductions` FLOAT NOT NULL DEFAULT '0' AFTER `reserve` ;


// override util 

function displaydate2($complexdatestring)
{
	$datestring = explode(" to ",$complexdatestring);
	if (empty($datestring[1]))
		return displaydate($datestring[0]);

	return displaydate($datestring[0])." - ".displaydate($datestring[1]);
}



class Statement
{
	var $m_display;
	var $m_errormessage;
	var $m_worksheet;
	var $m_complete;
	var $m_errortrap;
	var $m_dealset;
	var $m_lastdealset;
	var $m_minpayout;
	var $usedlicences;
	var $metaCrossCalculate;
	
	function Statement()
	{
		$this->m_display = "";
		$this->m_errormessage = "";
		$this->m_artistid = -1;
		//$this->m_errortrap = false;
		$this->m_complete = true;
		$this->usedlicences = array();
		$this->metaCrossCalculate = true;
	}
	
	function _print($string)
	{
		$this->m_display = $this->m_display.$string;
	}
	
	function Display()
	{
		print "<form method=post action=index.php><input type=hidden name=page value=artists>";
		foreach($_POST as $key=>$value)
		{
			if ($key=='bands')
				print "<input type=hidden name='bands[]' value='".implode(",",$_POST['bands'])."'>";
			else
				print "<input type=hidden name='".$key."' value='".$value."'>";
		}
		$this->m_worksheet->DisplayAsTable();
	//	print "<input type=submit class=submitsmall name=submit value='SET'>";
		print "<input type=hidden name=transactionid value='".$_SESSION['transactionid']."'>";
		print"</form>";
	}
	
	function IsValid()
	{
		return ($this->m_artistid!=-1);
	}

	function	SortResults($assoc, $sortfield)
	{
		foreach ($assoc as $key=>$row)
		{
			$sort[$key] = $row[$sortfield];
		}
		array_multisort($sort,SORT_ASC,$assoc);
		return $assoc;
	}

	function	CollateResults(&$assoc, $sortfieldscsv )
	{
		if (empty($assoc)) return;
		
		// Sort
		$sortfields = explode(",",$sortfieldscsv);

		// clear the current vars
		foreach ($sortfields as $sortfield)
		{
			$collaterow[$sortfield] = "";
		}
		
		// Go through each row
		foreach ($assoc as $row)
		{
			$changerow = false;
			foreach ($sortfields as $sortfield)
			{
				if ($row[$sortfield] != $collaterow[$sortfield])
				{
					if ($collaterow[$sortfield]!="") 
					{
						$outrow[] = $collaterow;			// flush the previous row
						$changerow = true;
					}
					$collaterow[$sortfield] = $row[$sortfield];
				}
			}	
			
			// and each value in each row
			foreach ($row as $key=>$value)
			{
				// is this cell a sortfield?
				if (array_search($key,$sortfields)===false)
				{
					if ($changerow)
						$collaterow[$key] = 0;
						
					if (empty($collaterow[$key]))
					{
						$collaterow[$key] = $value;
 					
 						if ($key=="baseprice")
						{
		      				$collaterow[$key] = Currency::PRODUCTOF($value,$row['quantity']);
          	   			}
          	   		}
					else  // value is not empty
					{
						// here we decided how we collate the columns - do we add them, replace, range etc..
						switch($key)
						{
						case "quantity":
							$collaterow[$key] += $value;
							break;
						case "value":
						case "amount":	
							$collaterow[$key] = Currency::SUMOF($collaterow[$key],$value);
							break;
						case "baseprice":
							$roy = Currency::PRODUCTOF($value,$row['quantity']);
							$collaterow[$key] = Currency::SUMOF($collaterow[$key],$roy);
							break;
	           			case "date":
							if ($collaterow[$key]=="") 
							{
								$collaterow[$key]=$value;
							} 
							else
							{
								$split = explode(" to ",$collaterow[$key]);
								if (empty($split[1]))
								{
									if ($split[0]<$value) 
										$collaterow[$key] = $split[0]." to ".$value;
									else if ($split[0]>$value)
										$collaterow[$key] = $value." to ".$split[0];
								}
								else
								{
									if ($split[0]>$value && $split[1]>$value)	$split[0] = $value;
									if ($split[1]<$value && $split[0]<$value)	$split[1] = $value;
									$collaterow[$key] = $split[0]." to ".$split[1];
								}
									
							}
							break;
						default:				
							// default copy over			
							$collaterow[$key] = $value;
						}
					}
				}
			}
		}
		$outrow[] = $collaterow;
		

		return $outrow;
	}


	// Calculate a bands split of a compilation
	// Returns an array like 1 / 3 
	function GetBandSplitOfCompilation($band,$product)
	{
		$productinfo = Database::QueryGetResult("SELECT artist,uid FROM product WHERE uid='".$product."';");
		$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product='".$product."';");
		if ($tracks!=false)
		{
			$numtracks = count($tracks);
			$tracklist="";
			foreach ($tracks as $index=>$track)
			{
				if ($index>0) $tracklist.=',';
				$tracklist .= "'".$track['track']."'";
			}
			$bandtracks = Database::QueryGetResult("SELECT COUNT(uid) FROM tracks WHERE uid IN ($tracklist) AND artist='".$band."';");
			// Split is the number of bands tracks divided by the number of tracks
			$split['top'] = $bandtracks['COUNT(uid)'];
			$split['bottom'] =  $numtracks;			
		}
		else
		{
			$split['top'] = 1; $split['bottom'] = 1;
		}
		return $split;
	}

	function GetDigitalAlbumSales(&$deal)
	{
		// echo '<pre>';
		// print_r($deal);
		// echo '</pre>';
		$sales = Database::QueryGetResults("SELECT `date`,distributor,quantity,product,value,baseprice FROM sale WHERE DATE(`date`) BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND track='' AND saletype='V' AND product='".$deal['product']."' ORDER BY distributor;");
		if (empty($sales)) $sales=array();	
		// Also want to add in here any track sales on a comp of a various artist
		//if (IsCompilation($deal))
		{
//			"SELECT uid FROM tracks WHERE artist='0' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$deal['product']."')");
			$mixsales = Database::QueryGetResults("SELECT `date`,distributor,quantity,product,value,baseprice,track FROM sale WHERE DATE(`date`) BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND track IN (SELECT uid FROM tracks WHERE artist='0' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$deal['product']."')) AND product='".$deal['product']."' AND saletype='V' ORDER BY distributor;");
			if (!empty($mixsales))
				$sales = array_merge($sales,$mixsales);
		}

		return $sales;
	}


//				$dealsales = $this->GetDigitalSales($deal['startdate'],$deal['enddate'],$deal['band'],'',$deal['track']);
/*
	// Get all digital track sales related to this deal
	### CM NOTE - this doesn't seem to be used ###
	function GetDigitalSales($startdate,$enddate,$band,$product,$track)
	{
	//	print "GetDigitalSales startdate=$startdate,enddate=$enddate,band=$band,product=$product,track=$track<br>";
		if ($track=='')
			$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE DATE(`date`) BETWEEN '".$startdate."' AND '".$enddate."' AND saletype='V' AND track IN (SELECT uid FROM tracks WHERE artist='".$band."' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$product."')) ORDER BY track,distributor;";
		else
			$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE DATE(`date`) BETWEEN '".$startdate."' AND '".$enddate."' AND saletype='V'  AND track='".$track."' ORDER BY track,distributor;";
					
		$sales = Database::QueryGetResults($query);

		//$remixsales = GetRemixSplitsRemixer($deal);
		//$sales = array_merge($sales,$remixsales);

	
		return $sales;
	}

	// Get all digital track sales related to this deal
	### CM NOTE - this doesn't seem to be used ###
	function GetStreams(&$deal)
	{
		if ($deal['track']=="")
			$query = "SELECT track,product,shop,quantity,baseprice,value FROM sale WHERE DATE(`date`) BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$deal['product']."')) ORDER BY track,shop;";
		else
			$query = "SELECT track,product,shop,quantity,baseprice,value FROM sale WHERE DATE(`date`) BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND track='".$deal['track']."' ORDER BY track,shop;";
			
		$streams = Database::QueryGetResults($query);

		return $streams;
	}

*/
	/*
		Returns the Remixers Fees if a remix split has been agreed - this is added as an expense to the artist
	
		- Find all the digital track sales related to this 
		- Take off the label cut
		- Then take off the artist cut
	*/
	function GetRemixSplitsExpenses(&$deal)
	{
		
		// Find any remixer split deals for the product we are calculating
		$res = Database::QueryGetResults("SELECT * FROM deals WHERE product='".$deal['product']."' AND dealtype='S' AND (startdate<='".$this->m_enddate."' AND (enddate>='".$this->m_startdate."' OR enddate='0000-00-00')) ");
		if ($res==false) return;
			
		foreach($res as $key=>$rdeal)
		{
			// Get Rate
			$terms = Database::QueryGetResults("SELECT * FROM dealterms WHERE dealid='".$rdeal['uid']."';");
			$rate = $terms[0]['rate'];
			
			// retreive all digital track sales/streams for the tracks in this product
			$query = "SELECT * FROM sale WHERE track='".$rdeal['track']."' AND (saletype='S' OR saletype='V') AND DATE(`date`) BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."';";
			$sales = Database::QueryGetResults($query);
			if (!empty($sales))
			{
				$salestotal = 0;
				foreach($sales as $sale)
				{
					$salestotal += Currency::GETRAWVALUE($sale['value']) * ($rate / 100);
					
				}
				
				$tracktit = Database::QueryGetResult("SELECT title FROM tracks WHERE uid='".$rdeal['track']."';");
				$salestotalcur = new Currency();
				$salestotalcur->SetValue($salestotal);
				$remixexpense[] = array('invoicedate'=>'','type'=>'Remix Split for Original Artist: '.$tracktit['title'],'product'=>$rdeal['product'],'amount'=>$salestotalcur->GetDatabaseFormat());
				
			}
		}
		if (!empty($remixexpense)) 
			return $remixexpense;
	}	
	
	

	function GetExpenses(&$deal,$iscompilation,$digitalonly)
	{
		if ($digitalonly)
			$expense_query = "SELECT invoicedate,type,product,amount FROM expenses WHERE product='".$deal['product']."' AND DATE(`invoicedate`) BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."' AND type='Digital';";			
		else
			$expense_query = "SELECT invoicedate,type,product,amount FROM expenses WHERE product='".$deal['product']."' AND DATE(`invoicedate`) BETWEEN '".$this->m_startdate."' AND '".$this->m_enddate."' AND type!='Digital';";
		
	//		print $expense_query."<br>";
			$expenses = Database::QueryGetResults($expense_query);	
	
			if ($expenses==false) $expenses=array();
		// Try putting remix splits in here
		if (!$iscompilation && $deal['dealtype']!='S') $remixexpenses = $this->GetRemixSplitsExpenses($deal);
		if (!empty($remixexpenses)) 
			$expenses = array_merge($expenses,$remixexpenses);
		return $expenses;
	}
	
	// Get all licences relating to band
/*	##CM NOTE function not used!
	function GetDealLicences($deal,$iscompilation)
	{
		$band = $deal['band'];
		
		$cataloguelist = "'".$deal['product']."'";

		// Don't include any licencing for a tracks on compilations these should be included in the main statement
		if ($iscompilation)
		{
			$excludelist = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product IN (SELECT uid FROM product WHERE artist='$band');");
			$excludecsv="";
			if (!empty($excludelist))
			{	
				foreach ($excludelist as $i=>$exclude)
				{
					if ($i>0) $excludecsv .= ",";
					$excludecsv .= "'".$exclude['track']."'";
				}
				$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
														(SELECT track FROM tracksproduct WHERE product='".$deal['product']."')
														AND uid NOT IN ($excludecsv);");
			}
			else
			{
				$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
														(SELECT track FROM tracksproduct WHERE product='".$deal['product']."');");
			}
		}
		else
		{
			$catalogue = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='$band' AND uid IN 
													(SELECT track FROM tracksproduct WHERE product='".$deal['product']."');");
		}

		if (!empty($catalogue))
		{
			foreach ($catalogue as $index=>$item)
			{
				$cataloguelist .= ",'".$item['uid']."'";
			}
			$tracklicences = Database::QueryGetResults("SELECT uid FROM licences WHERE ((licencetype='T') AND licenceduid IN ($cataloguelist));");
		}
		
		if ($iscompilation && !empty($tracklicences))
		{
			// These are compilation only track licences - however we can't include in here so add them to a magic list!
			foreach ($tracklicences as $licence)
			{
				if (empty($this->m_additionallicences) || array_search($licence['uid'],$this->m_additionallicences)===false)
					$this->m_additionallicences[] = $licence['uid'];
			}
			unset($tracklicences);
		}

/*		if ($iscompilation)
		{
			$productlicences = Database::QueryGetResults("SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';");
		}
		else
		{
			$artist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$deal['product']."';");
			if ($artist['artist']!='')
				$productlicences = Database::QueryGetResults("SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';");
		}
*/
/*	  
	  	$query = "SELECT uid FROM licences WHERE (licencetype='P') AND licenceduid='".$deal['product']."';";
		$productlicences = Database::QueryGetResults($query);

	  
	  	// merge these together
	  	if (empty($tracklicences) && !empty($productlicences)) $licences = $productlicences;
	  	else if (!empty($tracklicences) && empty($productlicences)) $licences = $tracklicences;
	  	else if (!empty($tracklicences) && !empty($productlicences)) $licences = array_merge($tracklicences,$productlicences);

		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				$licenceuids[] = $licence['uid'];
			}
		}
		if (!empty($licenceuids))
			return $licenceuids;
	}
	
*/	
	function GetLicenceMonies(&$licenceuids,$startdate,$enddate)
	{
		if (empty($licenceuids)) return;
		$licenceuids_csv = implode(",",$licenceuids);
		// Get all advances that are applicable
		$licences = Database::QueryGetResults("SELECT uid,description,advance,advancepaiddate,licenceduid,licencetype FROM licences WHERE uid IN ($licenceuids_csv);");
		if (!empty($licences))
		{
			foreach ($licences as $licence)
			{
				// Any advances?
				if ($licence['advancepaiddate']>=$startdate && $licence['advancepaiddate']<=$enddate)
					$item[] = array('date'=>$licence['advancepaiddate'],'description'=>"Advance ".$licence['description'],'catno'=>$licence['licenceduid'],'value'=>$licence['advance'],'licencetype'=>$licence['licencetype']);

				
				// Any statements
				$licencestatements = Database::QueryGetResults("SELECT paiddate,amount,licenceuid FROM licencestatements WHERE licenceuid='".$licence['uid']."' AND DATE(`paiddate`) BETWEEN '$startdate' AND '$enddate';");
	
				if (!empty($licencestatements))
				{
					foreach ($licencestatements as $statement)
					{
						$item[] = array('date'=>$statement['paiddate'],'description'=>"Royalties ".$licence['description'],'catno'=>$licence['licenceduid'],'value'=>$statement['amount'],'licencetype'=>$licence['licencetype']);
					}
				}
			}
		}

		if (!empty($item))
			return $item;		

	}
	
	function ProcessLicensing(&$deals,&$worksheet,&$splits,$iscompilation)
	{	
	
		foreach($deals as $deal) {
			$products[] = $deal['product'];		// array of all products in deal
		}

		// Product Licences		
		$product_licences = Database::QueryGetResults("SELECT * FROM licences WHERE licencetype='P' AND licenceduid IN ('".implode("','",$products)."')");

		if (!empty($product_licences)) {
			foreach($product_licences as $l) {
					$pl[] = $l['uid'];
			}
			$product_licence_revenues = $this->GetLicenceMonies($pl,$this->m_startdate,$this->m_enddate); 
			}

		if (!$iscompilation) {
			$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product IN ('".implode("','",$products)."')");
			$trackslist = array();
			foreach($tracks as $track) {
				// don't pick tracks that aren't by this artist
				$trackinfo = Database::QueryGetResults("SELECT artist FROM tracks WHERE uid='".$track['track']."'");
				if ($trackinfo[0]['artist']==$deals[0]['band'])
					$trackslist[] = $track['track'];
			}
			$query = "SELECT * FROM licences WHERE licencetype='T' AND licenceduid IN ('".implode("','",$trackslist)."')";
			$track_licences = Database::QueryGetResults($query);
			if (!empty($track_licences)){
				foreach($track_licences as $track_licence) {
					if ($track_licence['dealset']=='' || $track_licence['dealset']==$deals[0]['crossdealid']) {
						if (!in_array($track_licence['uid'],$this->usedlicences)) {
							$licences[] = $track_licence['uid'];
							$this->usedlicences[] = $track_licence['uid'];
						}
					}
				}
			}				
			if (!empty($licences))
				$track_licence_revenues = $this->GetLicenceMonies($licences,$this->m_startdate,$this->m_enddate); 

		}

		$licence_revenues = array();
		if (!empty($product_licence_revenues))  $licence_revenues = array_merge($licence_revenues,$product_licence_revenues);
		if (!empty($track_licence_revenues))    $licence_revenues = array_merge($licence_revenues,$track_licence_revenues);
		
		$licencing_total = 0;
		if (!empty($licence_revenues))
		{
			foreach ($licence_revenues as $item)
			{
				$value = Currency::GETRAWVALUE($item['value']);
				$worksheet->AddRow($item['description'],"",Currency::DISPLAY(round($value)));
				$licencing_total += $value;
			}
		}
		return $licencing_total;
	}
	
	
	static function	MakeStatementHash($artist,$bandlist,$startdate,$enddate)
	{
		return md5($artist."_".$bandlist."_".$startdate."_".$enddate);
	}

	static function	MakeStatementFilename2($pdfhash)
	{
		return File::GetPath("statement_".$pdfhash.".pdf");
	}

	static function	MakeStatementFilename($artist,$bandlist,$startdate,$enddate)
	{
	
		return File::GetPath("statement_".$artist."_".$bandlist."_".$startdate."_".$enddate.".pdf");
	}




	function ProcessAdvances(&$deals,&$worksheet)
	{
		// was the last period negative in which case search backward
		$startdate = $this->m_startdate;
		/*
		$query = "SELECT * FROM artiststatements WHERE artist='".$deals[0]['band']."' AND crossdealid='".$deals[0]['crossdealid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC;";
		$previousstatements = Database::QueryGetResults($query);
		print $query."<br>";
		print_r($previousstatements);
		foreach($previousstatements as $statement){
			if (Currency::GETRAWVALUE($statement['reserve'])<0){
				$startdate = $statement['startdate'];
			}
		}

		print "startdate = ".$startdate."<br>";
		*/
		
	//	print_r($deals);
		$advance_total = 0;
		foreach ($deals as $deal)
		{
			if ($deal['advancepaiddate']>=$startdate && $deal['advancepaiddate']<=$this->m_enddate)
			{
			
				$advance = Currency::GETRAWVALUE($deal['advance']);
		//		print 'getadvance = '.$deal['advance'].'<br>';
				// Subtract advances
				if ($advance!=0)
				{
					$worksheet->AddHeaderRow("ARTIST'S ADVANCES PAID THIS PERIOD (".$deal['product'].")","",Currency::DISPLAY(-$advance));
					$advance_total += $advance;
				}
			}
		}
		
		// get previous recoupable
		$query = "SELECT * FROM artiststatements WHERE artist='".$deals[0]['band']."' AND crossdealid='".$deals[0]['crossdealid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC LIMIT 1;";		
		$res = Database::QueryGetResult($query);
		$advancetorecoup = -$res['advancerecoup'];
		if ($advancetorecoup!=0){
			$worksheet->AddHeaderRow("ARTIST'S PREVIOUS ADVANCES TO RECOUP","",Currency::DISPLAY(-$advancetorecoup));
		}
		$advance_total += $advancetorecoup;
		
		$deals[0]['advancerecoup'] = $advance_total;
		return $advance_total;
	}
	

	function sumUp($records)
	{
	}

		
	function processPhysicalSales($product,$startdate,$enddate)
	{
		$physical_value = 0;	
		$physical_quantity = 0;
		
		//print 'Process Physical Sales = '.$product.' '.$startdate.'-'.$enddate.'<br>';
			
		$query = "SELECT date,distributor,quantity,product,value,baseprice FROM sale WHERE DATE(`date`) BETWEEN '".$startdate."' AND '".$enddate."' AND track='' AND saletype='P' AND product='".$product."' ORDER BY distributor;";
		$sales = Database::QueryGetResults($query);

		if (!empty($sales))
		{
			// need to seperate -sales for +sales for returns seperate path
			unset($salesneg);
			foreach($sales as $key=>$sale)
			{
				if ($sale['quantity']<0)
				{
					$salesneg[] = $sales[$key];
					unset($sales[$key]);
				}
			}
		
			$sales = $this->CollateResults($sales,"distributor");
			if (!empty($salesneg))
			{
				if (empty($sales))
					$sales = $this->CollateResults($salesneg,"distributor");
				else
					$sales = array_merge($sales,$this->CollateResults($salesneg,"distributor"));
			}
			
			$isempty = false;
			foreach ($sales as $sale)
			{
				$items[] = array('description'=>TableInfo::DisplayFromType('distributorshop', $sale['distributor']).' physical sales of '.$sale['product'],'quantity'=>$sale['quantity'],'value'=>Currency::GETRAWVALUE($sale['value']));
			}

		return $items;
		}		
		return;
	}
	
	
	function sumSales($insales,$key1,$key2='')
	{
		// echo '<pre>';
		// print_r($insales);
		// echo '</pre>';
		foreach($insales as $index=>$sale)
		{
			$sale['value'] = Currency::GETRAWVALUE($sale['value']);
			if ($index==0)
				$outrow = $sale;
			else	
			{
				if ($key2=='') $test = true;
				else $test = ($sale[$key2]==$outrow[$key2]); 
				// If for same item then sum instead of outputting new row
				if ($sale[$key1]==$outrow[$key1] && $test)
				{
					$outrow['quantity'] += $sale['quantity'];
					$outrow['value'] += $sale['value'];
				}
				else
				{
					$outsales[] = $outrow;
					$outrow = $sale;
				}
				
			}	
		}
		if (isset($outrow)) $outsales[] = $outrow;
		return $outsales;
	}
	
	
/*	function processRemixSplits($deal)
	{
		$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sales WHERE track='".$deal['track']."';";
		$sales = Database::QueryGetResults($query);
		foreach ($sales as $sale)
		{
		}
	}
*/	


	/*
		Process a set of deals - these all get cross calculated
		Inputs: previous_carryover
		Outputs: artist_due, carryover
	*/
	function ProcessDealSet(&$worksheet,
							$deals,
							$carryover,
							$reservepercentage,
							$iscompilation,
							$treatasthirdpartycomp,
							$include_individual_sales_in_comp,
							$adjustment
							)
	{
		//echo '<pre>'; print_r($deals); echo '</pre>';
		ts_assert(!(count($deals)>1 && $iscompilation),"Internal error - bad compilation deal set. Please inform support");

		$advancetorecoup = 0;
		
		$isempty = true;
		$balance = 0;
		
		$artist_due = 0;
		$newcarryover = 0;
		
		$rate = $deals[0]['rate'];
		$dealtype = $deals[0]['dealtype'];
		
		// Worksheet header
		$bandname = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$deals[0]['band']."';");
		$bandname = strtoupper($bandname['name']);
	
		if ($iscompilation)	
			$split = $this->GetBandSplitOfCompilation($deals[0]['band'],$deals[0]['product']);
		
		if ($iscompilation)
			$title = "Accounts for Compilation ".$deals[0]['product'].' '.TableInfo::DisplayFromType("producttitle",$deals[0]['product']);
		else {
			if ($deals[0]['descrip']=='default')
				$title = "Main Accounts (dealset ".$deals[0]['crossdealid'].")";
			else
				$title = $deals[0]['descrip'];
		}
		
		// This is for debugging purposes
		if (DEBUG_MODE==1) {
			print "Processing Dealset : ".$title.'<br>';
			PrintTable($deals);
			if ($iscompilation) print 'Compilation<br>';
			if ($treatasthirdpartycomp) print 'Treat As Third Party Compilation<br>';
			if ($include_individual_sales_in_comp) print 'Include Individual Sales In Comp<br>';
			print '<br>';
		}
		if ($title=="") $title = "Main Dealset";

		$worksheet->AddTitleRow($title);
		$worksheet->AddHeaderRow("DESCRIPTION","QUANTITY","VALUE");


		//--------------------------------------------------------------
		// PHYSICAL SALES
		$physical_value = 0;	
		$physical_quantity = 0;
		foreach($deals as $deal)
		{
//			print_r($deal); print '<br>';
		
			// Don't include physical sales for deals that are only digital
			if ($dealtype=='S' || $deal['salesfilter']=='D' || $deal['salesfilter']=='DS' || $deal['salesfilter']=='DB') continue;
			if ($dealtype=='P' && $rate!=$deal['rate']) 
			{
				print "dealtype = $dealtype, rate = $rate<br>";
				print_r ($deals); print "<br>";
				die("Rate not consistent across deal set");
			}
			
			if ($dealtype!=$deal['dealtype']) 
			{
				die("Dealtype not consistent across deal set");
			}
			
			// Skip if a compilation (already calculated)
			if ($this->IsCompilation($deal['product']) && !$iscompilation)
				continue;

			$items = $this->processPhysicalSales($deal['product'],$deal['startdate'],$deal['enddate']);
			if (!empty($items))
			{
				foreach ($items as $item)
				{
					$worksheet->AddRow($item['description'],$item['quantity'],Currency::DISPLAY(round($item['value'])));
					$physical_value += $item['value'];					
					$physical_quantity += $item['quantity'];
				}
			}
			
		}
		if ($physical_quantity!=0){
			$worksheet->AddHeaderRow("",$physical_quantity,Currency::DISPLAY(round($physical_value)));
			$isempty = false;
		}
		$balance += round($physical_value);


		//--------------------------------------------------------------
		// DIGITAL BUNDLED SALES
		$digitalalbum_value = 0;
		$digitalalbum_quantity = 0;
		foreach($deals as $deal)
		{
			if ($dealtype=='S' || $deal['salesfilter']=='P' || $deal['salesfilter']=='DS') continue;		// don't process for remix splits
			
			if ($this->IsCompilation($deal['product']) && !$iscompilation)
				continue;

			ts_assert(!($dealtype=='P' && $rate!=$deal['rate']),"Rate not consistent across deal set"); 
			ts_assert(!($dealtype!=$deal['dealtype']),"Dealtype not consistent across deal set");
		
			$sales = $this->GetDigitalAlbumSales($deal);
			if (!empty($sales))
			{
//				$sales = Statement::CollateResults($sales,"distributor");
				$sales = $this->sumSales($sales,'distributor','product');

				foreach ($sales as $sale)
				{
					$value = Currency::GETRAWVALUE($sale['value']);
					$digitalalbum_value += $value;
	
					$worksheet->AddRow(TableInfo::DisplayFromType('distributorshop',
					$sale['distributor']).' digital sales of '.$sale['product'],$sale['quantity'],Currency::DISPLAY(round($value)));
					$isempty = false;
					$digitalalbum_quantity += $sale['quantity'];
				}
			}
		}
		$balance += round($digitalalbum_value);
		if ($digitalalbum_quantity!=0){
			$worksheet->AddHeaderRow("",$digitalalbum_quantity,Currency::DISPLAY(round($digitalalbum_value)));
			$isempty = false;
		}
			
			

		
						// Special case if this is a comp and the sales filter is digital sales then force the sales in here
/*		if ($iscompilation && $deals[0]['salesfilter']=='DS') {
			print 'forced';
			$include_individual_sales_in_comp = true;
		}

		if ($include_individual_sales_in_comp) print '$include_individual_sales_in_comp<br>';
*/		

		if (($include_individual_sales_in_comp && $iscompilation) || 
		    (!$include_individual_sales_in_comp && !$iscompilation))
		{


	//	if ($include_individual_sales_in_comp) print "Including digital sales in compilation<br>";
	//	if ($include_individual_sales_in_comp) print "Compilation Calculation<br>";

		//--------------------------------------------------------------
		// DIGITAL TRACK SALES

			$digitaltrack_sales = 0;

			$sales = array();
			$productlist = '';
			$non_comp_productlist = '';
			$tracklist = '';
			// echo '<pre>';
			// print_r($deals);
			// echo '</pre>';
			foreach($deals as $deal)
			{
				ts_assert(!($rate!=$deal['rate']),"Rate not consistent across deal set");
				ts_assert(!($dealtype!=$deal['dealtype']),"Dealtype not consistent across deal set");

				if ($deal['salesfilter']=='P' || $deal['salesfilter']=='DB') continue; // don't process physical only deals
				// Get all the tracks for the artist in these deals
				if ($productlist!='') $productlist .= ',';
					$productlist .= "'".$deal['product']."'";				// contains all products in a dealset
				
				if (!$this->IsCompilation($deal['product']))
					$non_comp_productlist .= "'".$deal['product']."'";		// contains all non-comps in a dealset
											
				if ($tracklist!='') $tracklist .= ',';
				$tracklist .= "'".$deal['track']."'";	
			}
			

	//		print "Productlist = ".$productlist."<br>";
	//		print "Tracklist = ".$tracklist."<br>";

			
			if ($productlist!='' || $tracklist!='')
			{
				$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE (DATE(`date`) BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."') AND saletype='V' AND track!='' ";
				
					
				if ($deal['dealtype']=='S') {
					$query .= "AND track IN (".$tracklist.") ORDER BY track,distributor";
				} else {			
					if (!$iscompilation && $include_individual_sales_in_comp)
						$query .= "AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$non_comp_productlist."))) AND product IN (".$non_comp_productlist.") ORDER BY track,distributor;";
					else
	//					$query .= "AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) AND product IN (".$productlist.") ORDER BY track,distributor;";
						$query .= "AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) AND (product IN (".$productlist.") OR product = '') ORDER BY track,distributor;";
				}	
//				print "Query = ".$query."<br>";
				$sales = Database::QueryGetResults($query);		
//				print "Results = "; print_r($sales); print "<br>";

				// Check for empty sales
/*				foreach($sales as $i=>$sale){
					if ($sale['track']=='') {
						print "Unknown (line ".$i.") track = "; print_r($sale); print "<br>";
					}
				}
*/
				$quantity_total = 0;
	
				if (!empty($sales))
				{
					// echo '<pre>';
					// print_r($sales);
					// echo '</pre>';
					$sales = $this->sumSales($sales,'distributor','track');
					// replace track ids by name
					foreach($sales as $i=>$sale)
					{
						$sales[$i]['track'] = TableInfo::DisplayFromType("tracktitle",$sale['track']);
						//if ($sales[$i]['track']=="") $sales[$i]['track'] = 'Unknown: '.$sale['track'];
					}
					$sales = $this->SortResults($sales, 'track');
					
					foreach ($sales as $sale)
					{
						$value = Currency::GETRAWVALUE($sale['value']);
	        			$worksheet->AddRow(
						'Digital track sales of '.$sale['track'].' ('.TableInfo::DisplayFromType('distributorshop',$sale['distributor']).')',$sale['quantity'],Currency::DISPLAY(round($value)));
	
						$isempty = false;
						$quantity_total += $sale['quantity'];
						$digitaltrack_sales += $value;					
					}
				}
	
				$balance += $digitaltrack_sales;	
				if ($digitaltrack_sales!=0){
					$worksheet->AddHeaderRow("",$quantity_total,Currency::DISPLAY(round($digitaltrack_sales)));
					$isempty = false;
				}
					
				
			//--------------------------------------------------------------
			// DIGITAL STREAMS
	
				$digitalstreams_sales = 0;
				$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE DATE(`date`) BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' ";
				if ($deal['dealtype']=='S')
					$query .= "AND track IN (".$tracklist.") ORDER BY track,distributor";
				else			
					if (!$iscompilation && $include_individual_sales_in_comp)
						$query .= "AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$non_comp_productlist."))) AND product IN (".$non_comp_productlist.") ORDER BY track,distributor;";
					else
						//$query .= "AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) AND product IN (".$productlist.") ORDER BY track,distributor;";
					 	$query .= "AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) AND (product IN (".$productlist.") OR product = '') ORDER BY track,distributor;";


//					$query = "SELECT track,product,distributor,quantity,baseprice,value FROM sale WHERE date BETWEEN '".$deal['startdate']."' AND '".$deal['enddate']."' AND saletype='S' AND track IN (SELECT uid FROM tracks WHERE artist='".$deal['band']."' AND uid IN (SELECT track FROM tracksproduct WHERE product IN (".$productlist."))) ORDER BY track,distributor";
	//			print "Stream Query = ".$query.'<br>';
				$sales = Database::QueryGetResults($query);
	//			print "Result = "; print_r($sales); print "<br>";		
				$quantity_total = 0;
	
				if (!empty($sales))
				{
					$sales = $this->sumSales($sales,'track');
					
					// replace track ids by name
					foreach($sales as $i=>$sale)
					{
						$sales[$i]['track'] = TableInfo::DisplayFromType("tracktitle",$sale['track']);
					}
					$sales = $this->SortResults($sales, 'track');
	
					foreach ($sales as $sale)
					{
						$value = Currency::GETRAWVALUE($sale['value']);
	        			$worksheet->AddRow('Streams of '.$sale['track'],$sale['quantity'],Currency::DISPLAY(round($value)));
	
						$isempty = false;
						$quantity_total += $sale['quantity'];
						$digitalstreams_sales += $value;					
					}
				}
	
				$balance += $digitalstreams_sales;	
				if ($digitalstreams_sales!=0){
					$isempty = false;
					$worksheet->AddHeaderRow("",$quantity_total,Currency::DISPLAY(round($digitalstreams_sales)));
				}
					
	
			}
		}			
			
		//--------------------------------------------------------------
		// LICENSING
		$license_quantity = 0;
		$tempworksheet = new Worksheet();
		$licensing_total = $this->ProcessLicensing($deals,$tempworksheet,$split,$iscompilation);
		$worksheet->Append($tempworksheet);
		
		if (!$iscompilation && !empty($this->m_receipts[$deals[0]['crossdealid']]) && $deals[0]['salesfilter']!='DS')
		{
			foreach ($this->m_receipts[$deals[0]['crossdealid']] as $receipt)
			{
				$worksheet->AddRow('Compilation revenues from '.$receipt['product'].' '.TableInfo::DisplayFromType("producttitle",$receipt['product']),"",Currency::DISPLAY(round($receipt['payable'])));
				$licensing_total += $receipt['payable'];
				$isempty = false;
				$license_quantity++;
			}
		}

		$balance += $licensing_total;
		if ($license_quantity>0)
			$worksheet->AddHeaderRow("","",Currency::DISPLAY(round($licensing_total)));



		//--------------------------------------------------------------
		// EXPENSES

	
		if ($dealtype=='P' || $dealtype=='S')
		{
			$expense_total = 0;
			foreach($deals as $deal)
			{
				if ($this->IsCompilation($deal['product']) && !$iscompilation)
					continue;

	
				$expenses = $this->GetExpenses($deal,$iscompilation,($deal['salesfilter']=='D' || $deal['salesfilter']=='DS' || $deal['salesfilter']=='DB'));

				if (!empty($expenses))
				{
					// sort these by track
					foreach ($expenses as $key=>$row)
					{
						$sort[$key] = $row['type'];
					}	
					array_multisort($sort,SORT_ASC,$expenses);
					unset($sort);
					if ($this->m_collate)
						$expenses = Statement::CollateResults($expenses,"type");

					foreach ($expenses as $expense)
					{
						$expense_amount = -(Currency::GETRAWVALUE($expense['amount']));
						$worksheet->AddRow('Expenses : '.$expense['type'].' for '.$expense['product'],"",Currency::DISPLAY(round($expense_amount)));
						$isempty = false;
						$expense_total += $expense_amount;
					}
				}
			}
			$balance += $expense_total;
			if ($expense_total!=0)
				$worksheet->AddHeaderRow("","",Currency::DISPLAY(round($expense_total)));
		}
		

		
		//--------------------------------------------------------------
		// TOTALS
		
		// INPUTS:
		//	$balance        - total of all receipts
		//  $carryover		- any previous balances/reserves from last statement
		// OUTPUTS:
		//  $artist_due		- payable amount  
		//	$newcarryover   - balance / reserve to carry over to next statement 

		// If there is not a carryover set then we'll need to request this from the user as manual input
		if (!isset($carryover))
		{
			if (!$iscompilation) {
				$inputfield = "<input size=6 type=text name='carryover".$deals[0]['crossdealid']."' value=''>";
			} else {
				$inputfield = "<input size=6 type=text name='carryover".'C'.$deals[0]['uid']."' value=''>";
			}
			$inputfield .= "<input type=submit class=submitsmall name=submit value='SET'>";
			$worksheet->AddHeaderRow("BALANCE FROM PREVIOUS STATEMENT","",$inputfield);
			$this->m_complete = false;
			$isempty = false;
		}
		
		if (!isset($adjustment))
		{
			if (!$iscompilation) {
				$inputfield = "<input size=6 type=text name='adjustment".$deals[0]['crossdealid']."' value=''>";
			} else {
				$inputfield = "<input size=6 type=text name='adjustment".'C'.$deals[0]['uid']."' value=''>";
			}
			$inputfield .= "<input type=submit class=submitsmall name=submit value='SET'>";
			$worksheet->AddHeaderRow("ADJUSTMENT FROM PREVIOUS STATEMENT","",$inputfield);
			$this->m_complete = false;
			$isempty = false;
		}


    	if (!$iscompilation)
    		$isempty = false;
				
		// REMIX SPLIT DEAL CALCULATION
		if ($dealtype=='S')
		{
			// We need to get the original artists split
			$artist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$deal['product']."'");
			$query = "SELECT * FROM deals WHERE product='".$deal['product']."' AND band='".$artist['artist']."' AND dealtype!='S' AND (startdate<='".$this->m_enddate."' AND (enddate>='".$this->m_startdate."' OR enddate='0000-00-00')) ";
			$original_deal = Database::QueryGetResults($query);
			ts_assert(count($original_deal)!=0,"You have set up a remix split deal for ".$deal['product']." but the original artist has no deal set up.");
			ts_assert(count($original_deal<=1),"Found multiple deals for ".$deal['product']);

			$rate = Database::QueryParam("SELECT * FROM dealterms WHERE dealid=%1",$original_deal[0]['uid']);

			$worksheet->AddHeaderRow("ORIGINAL ARTIST SHARE (".(100-$deal['rate'])."% OF ".Currency::DISPLAY(round($balance)).")","",Currency::DISPLAY(round((100-$deal['rate'])*(-$balance)/100)));
			$balance = $balance * ($deal['rate']/100);

			$label_cut = $balance*((100-$rate[0]['rate'])/100);
			$worksheet->AddHeaderRow("LABEL CUT (".(100-$rate[0]['rate'])."% OF ".Currency::DISPLAY(round($balance)).")","",Currency::DISPLAY(round(-$label_cut)));			
			$balance -= $label_cut;
			$artist_due = $balance;

			$worksheet->AddHeaderRow("LEAVING YOU","",Currency::DISPLAY(round($artist_due)));
		}

		// DO CALCULATION FOR ROYALTY DEALS
		if ($dealtype=='P')
		{
			$advancetorecoup = 0;
			if ($balance>=0) {
				$worksheet->AddHeaderRow("PROFIT FOR THIS PERIOD OF","",Currency::DISPLAY(round($balance)));	
			}else{
				$worksheet->AddHeaderRow("LOSS FOR THIS PERIOD","",Currency::DISPLAY(round($balance)));
			}		
			
			// If a negative balance from previous statement add this in before the label takes their split
			if (isset($carryover)){
				if ($carryover<0) {
					$worksheet->AddHeaderRow("LOSS FROM PREVIOUS PERIOD CARRIED OVER","",Currency::DISPLAY($carryover));
					$balance += $carryover;
				}
			}		



			// don't do this for comps unless they are 'treat as third party'
			if (!($iscompilation && !$treatasthirdpartycomp)) {
				// Calculate label balance
				if ($balance>0) {					
					$label_due = ($balance) * (1-($rate/100));
					$worksheet->AddHeaderRow("LABEL'S ".(100-$rate)."% SHARE OF ".Currency::DISPLAY(round($balance))." PROFIT","",Currency::DISPLAY(-round($label_due)) );
					$balance -= $label_due;
				}
			}
			
			// If positive balance from previous period then add it after the label's cut because this belongs to the artist
			if (isset($carryover)){
				if ($carryover>0){
					$worksheet->AddHeaderRow("RESERVE FROM PREVIOUS PERIOD CARRIED OVER","",Currency::DISPLAY(round($carryover)));
					$balance += $carryover;
					$worksheet->AddHeaderRow("ARTISTS SPLIT PLUS PREVIOUS RESERVE","",Currency::DISPLAY(round($balance)));
				}
			}
/*
			// If positive balance from previous period then add it after the label's cut because this belongs to the artist
			if (isset($adjustment)){
				if ($adjustment>0){
					$worksheet->AddHeaderRow("ADJUSTMENT FROM PREVIOUS PERIOD CARRIED OVER","",Currency::DISPLAY(round($adjustment)));
					$balance += $adjustment;
					$worksheet->AddHeaderRow("ARTISTS SPLIT PLUS ADJUSTMENT","",Currency::DISPLAY(round($balance)));
				}
			}
*/
			$reserveoncomps = true;

			// Display advances for this deal (not for compilations)
			if (!$iscompilation || $reserveoncomps)
			{
				// Add in any advances
				

					//if ($balance>0)
					//	$balance -= $advances;
				
				// Set the reserve to carry over (if positive balance)
				if ($balance>0 && $reservepercentage>0) {
					$reserve = $balance * ($reservepercentage/100);	
					$worksheet->AddHeaderRow($reservepercentage."% RESERVE TO CARRY OVER","",Currency::DISPLAY(round(-$reserve)));
					// deduct the reserver
					$balance -= $reserve;
					$newcarryover = $reserve;	// and carry over the reserve
				}
			}

			// check if positive balance
			if ($balance>=0) {
				// Show how much money is left for the artist after the reserve / advance is included
				$worksheet->AddHeaderRow($iscompilation ? "LEAVING THE ARTISTS" : "ARTIST'S SHARE REMAINING ","",Currency::DISPLAY(round($balance)));
				$artist_due = $balance;
				
				$advancetorecoup = 0;
				$advances_worksheet = new Worksheet;
				$advances = -$this->ProcessAdvances($deals,$advances_worksheet);
				$worksheet->Append($advances_worksheet);
				unset($advances_worksheet);
				
				$artist_due += $advances;
				if ($artist_due<0){
					$advancetorecoup = $artist_due;
					$artist_due = 0;
				}

				// If a negative balance from previous statement add this in before the label takes their split
				if (isset($adjustment) && $adjustment!=0){	
					$worksheet->AddHeaderRow("ADJUSTMENT TO BALANCE","",Currency::DISPLAY($adjustment));
					$artist_due += $adjustment;
					if ($artist_due<0) {
						$advancetorecoup -= $artist_due;
						$artist_due = 0;
					}
				}

	
				$worksheet->AddHeaderRow($iscompilation ? "ARTISTS' SHARE" : "ARTIST DUE","",Currency::DISPLAY(round($artist_due)));
				$worksheet->AddHeaderRow("ARTIST'S ADVANCES TO RECOUP IN NEXT PERIOD","",Currency::DISPLAY(round(-$advancetorecoup)));

			} else {
				// Negative balance
				$advancetorecoup = 0;
				$advances_worksheet = new Worksheet;
				$advancetorecoup = -$this->ProcessAdvances($deals,$advances_worksheet);
				$worksheet->Append($advances_worksheet);
				unset($advances_worksheet);
			
				
				$artist_due = 0;				// artist owed nothing
				$worksheet->AddHeaderRow("ARTIST DUE THIS PERIOD","",Currency::DISPLAY(round($artist_due)));
				$worksheet->AddHeaderRow("BALANCE CARRIED TO NEXT PERIOD","",Currency::DISPLAY(round($balance)));
				$worksheet->AddHeaderRow("ADVANCES TO BE RECOUPED IN NEXT PERIOD","",Currency::DISPLAY(round($advancetorecoup)));
				$newcarryover = $balance;		// carry over the negative balance

			}
		}

		// ROYALTY DEAL
		if ($dealtype=='R')
		{
			// Display the total
			if (!$iscompilation) {
				$worksheet->AddHeaderRow("TOTAL","",Currency::DISPLAY(round($balance)));
				$balance *= ($deal['rate']/100);
				$worksheet->AddHeaderRow("YOUR ROYALTIES (".$deal['rate']."%)","",Currency::DISPLAY(round($balance)));
			}
			
			// Include negative balance from previous statement add this in
			if (isset($carryover) && $carryover<0) {
				$worksheet->AddHeaderRow("PREVIOUS NEGATIVE BALANCE","",Currency::DISPLAY($carryover));
				$balance += $carryover;
			}

						// Include negative balance from previous statement add this in
			if (isset($adjustment) && $adjustment<0) {
				$worksheet->AddHeaderRow("PREVIOUS NEGATIVE ADJUSTMENT","",Currency::DISPLAY($adjustment));
				$balance += $adjustment;
			}
										
			if (!$iscompilation) {
				// ADVANCES included
				$advances_worksheet = new Worksheet;
				$advances = $this->ProcessAdvances($deals,$advances_worksheet);
				$worksheet->Append($advances_worksheet);
				unset($advances_worksheet);
				$balance -= $advances;

				// RESERVE deduction (if positive balance)
				if ($balance>0) {
					$reserve = $balance * ($reservepercentage/100);	
					$worksheet->AddHeaderRow("RESERVE TO CARRY OVER (".$reservepercentage."%)","",Currency::DISPLAY(round(-$reserve)));
					$balance -= $reserve;  // deduct the reserve
					$newcarryover = $reserve;	// carry over the reserve
				}
			}

			if ($balance>=0){
				// POSITIVE BALANCE
				// Show how much money is left for the artist after the reserve / advance is included
				$worksheet->AddHeaderRow($iscompilation ? "LEAVING THE ARTISTS" : "LEAVING YOU","",Currency::DISPLAY(round($balance)));
				$artist_due = $balance; // artist owed the balance

				$worksheet->AddHeaderRow("ADVANCES TO CARRY OVER","",Currency::DISPLAY(round($advancetorecoup)));


				// RESERVE addition from the last statement
				if (isset($carryover) && $carryover>=0) {
					$worksheet->AddHeaderRow("RESERVE FROM LAST STATEMENT","",Currency::DISPLAY(round($carryover)));
					$artist_due += $carryover;
				}

				$worksheet->AddHeaderRow($iscompilation ? "ARTISTS' SHARE" : "ARTIST DUE","",Currency::DISPLAY(round($artist_due)));
			} else {
				if (isset($adjustment) && $adjustment!=0) {
					$worksheet->AddHeaderRow("ADJUSTMENT FROM LAST STATEMENT","",Currency::DISPLAY(round($adjustment)));
					$artist_due += $adjustment;
					if ($artist_due<0) {
						$advancetorecoup -= $artist_due;
						$artist_due = 0;
					}
				}
				// NEGATIVE BALANCE
				$artist_due = 0;			// no payout for artist
				$worksheet->AddHeaderRow("ARTIST DUE THIS PERIOD","",Currency::DISPLAY(round($artist_due)));
				$worksheet->AddHeaderRow("ADVANCES TO CARRY OVER","",Currency::DISPLAY(round($advancetorecoup)));
				
				
				$worksheet->AddHeaderRow("BALANCE TO NEXT STATEMENT","",Currency::DISPLAY(round($balance)));
				$newcarryover = $balance;		// carry over negative balance
			}
		}
		
		// FINAL TALLY AND STORAGE
		if ($iscompilation) {
			// PRO RATA SHARE display
			if ($artist_due>0) {
				$artist_due = $artist_due * ($split['top'] / $split['bottom']);
				$worksheet->AddHeaderRow("YOUR PRO-RATA SHARE (".$split['top']."/".$split['bottom'].")","",Currency::DISPLAY(round($artist_due)));
			}
			if ($balance<0) {
				$worksheet->AddHeaderRow("NEGATIVE BALANCE","",Currency::DISPLAY($balance));
			}		
			// Store
			$this->m_receipts[$deals[0]['crossdealid']][] = array('product'=>$deals[0]['product'],'payable'=>$artist_due);
			$this->m_dealset['C'.$deals[0]['uid']] = array('totalup'=>false,'band'=>$deals[0]['band'],'payable'=>round($artist_due),'balance'=>round($newcarryover),'advancerecoup'=>$advancetorecoup);
			

		} else {
			// Store for non compilation
			$this->m_lastdealset = $deals[0]['crossdealid'];
			$this->m_dealset[$deals[0]['crossdealid']] = array('totalup'=>true,'band'=>$deals[0]['band'],'payable'=>round($artist_due),'balance'=>round($newcarryover),'advancerecoup'=>$advancetorecoup);
		}
		
		return $isempty;
	}
		
	
	/*
		Get Carry Over from previous statement
		Inputs: deal, iscomp
		Output: Previous statement carryover
	*/
	function GetCarryover($band,$deal,$iscomp)
	{
	//	print 'Get Carryover = '.$deal['uid'].'<br>';
		
		// is this the master one of the dealset?
		if ($iscomp) {
			$laststatement = Database::QueryGetResult("SELECT reserve FROM artiststatements WHERE artist='".$band."' AND crossdealid='C".$deal['uid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC LIMIT 1;");
		} else {
			$laststatement = Database::QueryGetResult("SELECT reserve FROM artiststatements WHERE artist='".$band."' AND crossdealid='".$deal['crossdealid']."' AND enddate<'".$this->m_startdate."' ORDER BY enddate DESC LIMIT 1;");
		}	
				
		if (!empty($laststatement)) {
			return Currency::GETRAWVALUE($laststatement['reserve']);
		} else {
			if ($iscomp) {
				$postid = 'carryoverC'.$deal['uid'];
			} else {
				$postid = 'carryover'.$deal['crossdealid'];
			}
	//		print "get post ".$postid." : '".$_POST[$postid]."'<br>";
			if (isset($_POST[$postid]) && $_POST[$postid]!=="") {
				return Currency::GETRAWVALUE(Currency::DISPLAYTODATABASE($_POST[$postid]));
			}
		}
		// no carry over found need user input
		return;
	}

	/*
		Get Carry Over from previous statement
		Inputs: deal, iscomp
		Output: Previous statement carryover
	*/
	function GetAdjustment($band,$deal,$iscomp)
	{
		return 0;
	//	print 'Get Carryover = '.$deal['uid'].'<br>';
		if ($_SESSION['accountid']!=29)
			return 0;

			if ($iscomp) {
				$postid = 'adjustmentC'.$deal['uid'];
			} else {
				$postid = 'adjustment'.$deal['crossdealid'];
			}
			if (isset($_POST[$postid]) && $_POST[$postid]!=="") {
				return Currency::GETRAWVALUE(Currency::DISPLAYTODATABASE($_POST[$postid]));
			}

		// no carry over found need user input
		return;
	}


	/*
		Process all band deals
	*/
	function ProcessBand(&$worksheet,$band,$treatasthirdpartycomp,$include_individual_sales_in_comp)
	{
		// Check if any deals missing
		$missingproducts = Database::QueryGetResults("
			SELECT uid 
			FROM product 
			WHERE artist='".$band."' 
			AND uid NOT IN 
			(SELECT product FROM deals WHERE artist='".$band."');
			");
		if (!empty($missingproducts))
		{
			foreach($missingproducts as $missing)
			{
				$tmp[] = $missing['uid'];
			}
			
			$this->AddWarning(TableInfo::DisplayFromType("band",$band)." has no deals set up for product(s) ".implode(",",$tmp).". Therefore they will not be accounted for any sales found.");
		}
	
		// Check any drifting track sales
		$drifttracks = Database::QueryGetResults("SELECT uid FROM tracks WHERE artist='".$band."' AND uid NOT IN (SELECT track FROM tracksproduct) AND uid NOT IN (SELECT track FROM deals WHERE band='".$band."' AND product='');");
		if (!empty($drifttracks))
		{
			foreach($drifttracks as $track)
			{
				$tmp[] = $track['uid'];
			}
			
			$this->AddWarning(TableInfo::DisplayFromType("band",$band)."'s track(s) ".implode(",",$tmp)." are not assigned to any product or have individual deals for and therefore they will not be accounted for any sales found (if any).");
		}

		// Get all unique crossdeal set
		$crossdeals = Database::QueryGetResults("SELECT DISTINCT crossdealid FROM deals WHERE band='".$band."' AND (startdate<='".$this->m_enddate."' AND (enddate>='".$this->m_startdate."' OR enddate='0000-00-00')) ");

		if (empty($crossdeals)) 
		{
		   $this->AddWarning("No deals for band $band (".TableInfo::DisplayFromType("band",$band).") within specified dates");
			return;
		}
		
		foreach($crossdeals as $crossdealindex=>$crossdeal)
		{
			unset($compdeals);
			unset($maincalc);
			
			$dealterm = Database::QueryGetResult("SELECT * FROM dealterms WHERE dealid=".$crossdeal['crossdealid']." AND `key`='' LIMIT 1;");
			if ($dealterm==false) 
				die ("No default rates found please contact admin");
			$deals = Database::QueryGetResults("SELECT * FROM deals WHERE crossdealid='".$crossdeal['crossdealid']."' AND (startdate<='".$this->m_enddate."' AND (enddate>='".$this->m_startdate."' OR enddate='0000-00-00')) ");
			foreach($deals as $deal)
			{
				$deal['rate'] = $dealterm['rate'];
				$deal['descrip'] = $dealterm['descrip'];
				
				// check product exists - consitency test
				if (!DataTable::GetIDExist('product',$deal['product']))
				{
					$this->AddWarning("Product ".$deal['product']." does not exist but there is a deal set up for it. This product will be skipped.");
					continue;
				}
				
				// is there an advance for this period? if so set it 
	//			if ($deal['advancepaiddate']<$this->m_startdate)
	//				$deal['advance'] = 0;
	
				// clip the start/enddates to the period we are looking at
				$deal['enddate']	= $this->m_enddate;
				$deal['startdate']	= $this->m_startdate;

				if ($this->IsCompilation($deal['product']))
				{
					$compdeals[] = $deal;
				}

				$maincalc[] = $deal;
				
			}
						
//			print '<br><br>';
//			print 'CompDeals ('.$crossdeal['crossdealid'].') '.count($compdeals).' = '; print_r($compdeals); print '<br><br>';
						
						
			// Process comp deals first
			if (!empty($compdeals))
			{
				foreach ($compdeals as $compindex=>$comp)
				{
					$tempset[0] = $comp;		// put in seperate set
					$carryover = $this->GetCarryover($band,$comp,true);
					$adjustment = $this->GetAdjustment($band,$comp,true);
//					print 'Comp Carryover = ';	print_r($carryover); print '<br><br>';

					$dealworksheet = new Worksheet;
					$iscomplete = $this->m_complete;
					
					$isempty = $this->ProcessDealSet($dealworksheet,			// output worksheet
													 $tempset,					// dealset
													 $carryover,				// carryover from database or user input
													 $comp['reserve'],			// reserve for this compilation
													 true,						// is a compilation
//													 (($compindex==count($compdeals)-1) && ($crossdealindex==count($crossdeals)-1)),	// last time
													 $treatasthirdpartycomp,
													 $include_individual_sales_in_comp,
													 $adjustment);
					if (!$isempty && $this->m_includecomps) 
					{
						$worksheet->Append($dealworksheet);
					}
					else
					{
						$this->m_complete = $iscomplete;
					}			
					unset($dealworksheet);
				}
			}
			
			if (!empty($maincalc))
			{
				// echo '<pre>Maincalc - <br>';
				// print_r($maincalc[0]['crossdealid']);
				// echo '</pre>';
				// get the reserve of the master crossdeal
				$res = Database::QueryGetResults("
					SELECT * FROM deals 
					WHERE uid='".$maincalc[0]['crossdealid']."' 
					AND (startdate<='".$this->m_enddate."' AND (enddate>='".$this->m_startdate."' OR enddate='0000-00-00')) LIMIT 1");	
				$reserve = $res[0]['reserve'];
				
				// Need to check for edge case where there are just compilations in this dealset and no others
				// In which case the reserve needs to be set correctly = 0.
	
				$carryover = $this->GetCarryover($band,$maincalc[0],false);
				$adjustment = $this->GetAdjustment($band,$maincalc[0],false);
//				print 'Carryover2 = ';	print_r($carryover); print '<br><br>';

				$this->ProcessDealSet(	$worksheet,			// output worksheet
										$maincalc,			// dealset array of deals
										$carryover,			// carry over as from database or user input
										$reserve,			// reserve (see above)
										false,				// not a compilation
										$treatasthirdpartycomp,				// user option
										$include_individual_sales_in_comp,
										$adjustment);	// user option	
			}
			
		}
		if (DEBUG_MODE==1){
			print "OUTPUT DEALSETS<br>";
			PrintTable($this->m_dealset);
		}
	}

	
	
	


	
	/*
		 CALL THIS FUNCTION TO GENERATE A NEW STATEMENT
	*/
	function Generate($includecomps,					// high detail or low detail report
					  $artistid,					// UID of the artist this statement is for
					  $band,					// band to process
					  $startdate,$enddate,			// start and end dates in SQL format
					  $displaywarnings,
					  $treatasthirdpartycomp,
					  $include_individual_sales_in_comp
					  )
	{
		// Store the parameters in this object
		$this->m_artistid = $artistid;
		$this->m_startdate = $startdate;
		$this->m_enddate = $enddate;
		$this->m_includecomps = $includecomps;
		$this->m_collate = true;
			
		$this->usedlicences = array();
		unset($this->m_receipts); 
		unset($this->m_dealset);
		unset($this->m_lastdealset);
	
		// gEt the default minpayout from the session
		$this->m_minpayout = Currency::GETRAWVALUE($_SESSION['defaultminpayout']);
						
		// Title and period
		$worksheet = new WorkSheet;
		$worksheet->AddTitleRow(TableInfo::DisplayFromType("band",$band));
		$worksheet->AddHeaderRow();
		$worksheet->AddTitleRow("PERIOD ".$startdate." TO ".$enddate);

		// Process the band - only one in this version
		$this->ProcessBand($worksheet,$band,$treatasthirdpartycomp,$include_individual_sales_in_comp);
		
		$worksheet->AddHeaderRow("Summary");		

		
		if (DEBUG_MODE==1){
			print 'Before<br>';
			PrintTable($this->m_dealset);
		}


		$total = 0;

		// If more than one dealset then add in summary to tot these up
		if (!empty($this->m_dealset))
		{
			foreach ($this->m_dealset as $index=>$dealset)
			{
				if ($dealset['totalup']==1)
				{
					if ($dealset['balance']>=0) {
							$total += $dealset['payable'];
					} else {
						if ($this->metaCrossCalculate){
							$total += $dealset['balance'];
							$this->m_dealset[$index]['payable'] = $dealset['balance'];
							$this->m_dealset[$index]['balance'] = 0;		// if we've deducted the neg balance then reset it
						}
					}					
				}
			}

			// Check if we have a negative balance (only possible if cross calculating meta
			if ($total<0) {
				// if the total of all the dealsets is negative (i.e. nothing to payout)
				// then we move all the positive payouts to the balance to carry over to next payout
				foreach ($this->m_dealset as $index=>$dealset) {
					if ($dealset['totalup']==1) {
						$this->m_dealset[$index]['balance'] += $dealset['payable'];
						$this->m_dealset[$index]['payable'] = 0;
					}
				}

				$total = 0;
			}		
		


			// --- Minimum Payout --- //
			if ($total>0 && $total<$this->m_minpayout)
			{
				// add to last deal
				$this->m_dealset[$this->m_lastdealset]['balance'] += $total;
				$this->m_dealset[$index]['payable'] = 0;
				$worksheet->AddHeaderRow("MINIMUM PAYOUT IS £".($this->m_minpayout/100).", CARRYING OVER","",Currency::DISPLAY($this->m_dealset[$this->m_lastdealset]['balance']));
				$total = 0;
			}

			if ($total>0){
				// Get the split from the database
				$split = Database::QueryGetResult("SELECT split FROM bandartist WHERE artist='$artistid' AND band='$band';");
				$total = $total * ($split['split']/100);			
				$worksheet->AddHeaderRow(strtoupper(TableInfo::DisplayFromType("band",$band))." SPLIT (".$split['split']."%)","",Currency::DISPLAY($total));
			}	
		
			$worksheet->AddHeaderRow("TOTAL PAYABLE THIS PERIOD","",Currency::DISPLAY($total));

		}
		
		if (DEBUG_MODE==1){
			print 'After<br>';
			PrintTable($this->m_dealset);
		}
		
	

//		print_r($_POST);print "<br><br>";

		// Print warnings
		if ($displaywarnings && !empty($this->m_warnings))
		{
			print '<div style="background-color: #758e98;">';
			print "The following warnings were generated by this statement:<br><br>";
			foreach($this->m_warnings as $warning)
			{
				print "<b>Warning: ".$warning."</b><br>";
			}
			print "<br>Please check and correct before saving.<br></div>";
		}

		// Store this worksheet	
		$this->m_worksheet = $worksheet;
		
	}
	
	function GetResults()
	{
		if (!empty($this->m_dealset))
			return $this->m_dealset;
	}
	
	//--- Accesssor functions
	function GetStartDate()
	{
		return $this->m_startdate;
	}
	
	function GetEndDate()
	{
		return $this->m_enddate;
	}
	
	function GetAmountDue()
	{
		return $this->m_amountdue;
	}
	
	function GetReserve()
	{
		return $this->m_reserve;
	}
	
	function IsComplete()
	{
		return $this->m_complete && !empty($this->m_dealset);
	}
	
	function SaveAsCSV($filename)
	{
		$this->m_worksheet->SaveAsCSV($filename);
	}

	function SaveAsPDF($filename)
	{
		$this->m_worksheet->SaveAsPDF($filename);
	}

	var $m_warnings;
	
	function AddWarning($string)
	{
		$this->m_warnings[] = $string;
	}
	
	function IsCompilation($productid)
	{
		$productartist = Database::QueryGetResult("SELECT artist FROM product WHERE uid='".$productid."';");
		if ($productartist['artist']==0) return true;
		return false;
	}


}

?>