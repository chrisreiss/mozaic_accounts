<?php

require_once('cDatabase.php');
require_once('cMail.php');

class EventManager
{

	function CheckLicenceStatement($accountid)
	{
		return; 		// disabled for now
		$licences = Database::QueryGetResults("SELECT * FROM licences;");
		if ($licences==false) return;

		$unpaidcount = 0;
		$statementsduecount = 0;
		foreach ($licences as $licence)
		{
			unset($useddates);
			// get any licence statements already in the system
			$result = Database::Query("SELECT * FROM licencestatements WHERE licenceuid='".$licence['uid']."' ORDER BY statementdate ;");
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$licencestatement = $result->GetNext();
				$ddate = displaydate($licencestatement['statementdate']);
				$useddates[] = $ddate;
				if ($licencestatement['paiddate']=="0000-00-00")
				{
					++$unpaidcount;
				}
			}	
			$date = strtotime($licence['datestatement']);
			$nextdate = $date;
			$iteration = 0;
			while ($nextdate<time())
			{
				$ddate = date("d-m-Y",$nextdate);
				// NB - note that array_search after PHP 4.20 will return false not NULL
				if (empty($useddates) || array_search($ddate,$useddates)===NULL)
				{
					++$statementsduecount;
				}
				++$iteration;
				$nextdate  = mktime(0, 0, 0, date("m",$date)+($licence['royaltyinterval']*$iteration),  date("d",$date),  date("Y",$date));
			}
			Database::FinishQuery($result);
		}
		
		if ($statementsduecount>0 || $unpaidcount>0)
		{
			$body = "<p>This is an automated message from music.counts just to remind you that:</p>";
			$mail = new Mail;
			if ($statementsduecount>0)
				$body .= "<p>You have $statementsduecount licence royalty statements that are due.</p>";
			if ($unpaidcount>0)
				$body .= "<p>You have $unpaidcount licence royalty statements that remain unpaid.</p>";
			$mail->SetMail("Licencing Actions ".date("d-m-Y",time()),$body);
			$mail->SetAction("licensors","OUTSTANDING STATEMENTS");
			MailManager::Send($mail,MAIL_TARGET_ACCOUNT,$accountid);
		}
		
	}

// Create the mails
	function Update()
	{
		// Check any statements
		EventManager::CheckLicenceStatement($_SESSION['accountid']);
		
		
	}


}


?>