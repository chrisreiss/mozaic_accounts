<?php

class ViewMenu {

	var $idcount = 0;
	var $disablelist;
	var	$name;
	
	function AddOption($sName,$bRequiresSelection)
	{
		$this->name[$this->idcount]=$sName;
		$this->disablelist[$this->idcount]=$bRequiresSelection;
		++$this->idcount;
	}
	
	function Display()
	{
		print "<script type=\"text/javascript\">\n";
		print "function toggleAllButtons()\n";
		print "{\n";
		print "	var nav;\n";
		for ($i=0;$i<$this->idcount;++$i)
		{
			if ($this->disablelist[$i]==true)
			{
				print " nav = document.getElementById('cViewMenu".$i."')\n";
				print "	nav.disabled = false\n";
			}
		}
		print "}\n";
		print "</script>\n";

		print "<table>\n";
		for ($i=0;$i<$this->idcount;++$i)
		{
			print "<tr><td>";
			print "<input type=\"submit\" name=\"submit\" value=\"".$this->name[$i]."\" id=\"cViewMenu".$i."\" class=\"submit\" ";
			if ($this->disablelist[$i]==true)
				print "disabled ";
			print "></td></tr>\n";
		}
		print "</table>\n";
	}

}

?>