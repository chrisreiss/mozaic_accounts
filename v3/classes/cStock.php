<?php

require_once('cDatabase.php');

class Stock
{
	
	// Given the distributorid returns an array of maps (PRODUCTID => UNITS HELD)
	static function GetLevels($distributorid)
	{
		$product = array();
		
		// Get all products and set them to 0
		$result = Database::QueryGetResults("SELECT uid FROM product;");
		foreach($result as $productid)
		{
			$product[$productid['uid']] = 0;
		}
		
		// Work out all the stock moved into this distributor for each product
		$query = "SELECT product, quantity FROM stock WHERE destination='".$distributorid."';";
		$result = Database::Query($query);
		$numrow = $result->GetNum();
		for ($i=0;$i<$numrow;++$i)
		{
			$stock = $result->GetNext();
			$found = false;
			$product[$stock['product']] += $stock['quantity']; 
		}
		Database::FinishQuery($result);
		
		// Minus all the stock moved out of this distributor
		$result = Database::Query("SELECT product, quantity FROM stock WHERE source='".$distributorid."';");
		$numrow = $result->GetNum();
		for ($i=0;$i<$numrow;++$i)
		{
			$stock = $result->GetNext();
			$found = false;
			foreach ($product as $key => $value)
			{
				if ($key==$stock['product']) $found = true;
			}
			if ($found==true)
				$product[$stock['product']] -= $stock['quantity']; 
		}
		Database::FinishQuery($result);
	
		// Minus all the registered sales for each product
		$result = Database::Query("SELECT product, quantity FROM sale WHERE distributor='".$distributorid."';");
		$numrow = $result->GetNum();
		for ($i=0;$i<$numrow;++$i)
		{
	
			$sale = $result->GetNext();
			if (empty($product[$sale['product']]))
				$product[$sale['product']] = -$sale['quantity'];
			else
				$product[$sale['product']] -= $sale['quantity']; 
		}
		Database::FinishQuery($result);
	
		if (!empty($product))
			krsort($product);
		return $product;
	}

}


?>