<?php
/*! 
	Validated Form class
	This is the workhorse of all the forms on the website. It performs auto validation of fields on submit
*/


// Includes
require_once("util.php");
require_once("cTableInfo.php");
require_once("strings.php");

// Defines
define ("NO_GROUP", -1);

// Private functions start with underscore

class ValidatedForm {
	var $m_uid;
	var $m_values;
	var $m_target;
	var $m_actions;		
	var $m_hidden;		// association array of name => value
	var $m_parent;
	var $m_group;			// the current group that is being set
	var	$m_fields;
	var $m_customscript = "";
	var $m_triggers;
	var $m_addcode;	
	var $m_title;
	
	//-- Constructor - should be depreciated eventually
	function ValidatedForm()
	{
		$this->m_group = NO_GROUP;
		$args = func_get_args();
		
		if ( isset($args[0]) )
		{
			$this->AddTable($args[0],$args[1]);
		}
		
		if ( isset($args[2]) )
		{
			$fixedlist = explode(",",$args[2]);
			foreach($fixedlist as $name)
			{
				$this->SetFixed($name);
			}
		}
	}

	function SetEventCode($field,$code)
	{
		$this->m_addcode[$field] = $code;
	}

	function	_start()
	{
		print '<div class="mform">';
		$addcode = "";
		if (!empty($this->m_addcode['form'])) $addcode = $this->m_addcode['form'];
		
		print "<form name='validatedform' id='validateform' method='post' action='index.php' enctype='multipart/form-data' $addcode>\n
			   <input type='hidden' name='page' value='".$this->m_target."'>\n";
   		print "<table>\n";
	}
	
	function	SetAspect(&$aspect)
	{
		$this->m_parent = $aspect;
	}
	
	function 	GetValue($fieldname)
	{
		foreach ($this->m_fields as $index=>$field)
		{
			if ($field['name']==$fieldname)
			{
				return $field['value'];
			}
		}
		
	}
	
	function 	HasValue($fieldname)
	{
		foreach ($this->m_fields as $index=>$field)
		{
			if ($field['name']==$fieldname)
			{
					return (!empty($field['value']));
			}
		}
		return false;
	}
	
	
	function	_row($display,$field,$format,$value,$tooltip,$disabled)
	{
		if ($display!="")
		{ 
			print "<td valign=top>";
			if ($tooltip!="")
				print "<label onmouseover=\"Tip('".addslashes($tooltip)."',WIDTH,300,DELAY,1000)\">$display</label>";
			else
				print '<label>'.$display.'</label>';
			print "</td>";
		}
		print "<td>";
		
		if (isset($this->m_triggers[$field]))
		{
	//		print "trigger "; print_r($this->m_triggers); print "<br>";
			if ($this->m_triggers[$field]=="productband")
				$format .= ":product".$this->GetValue('product').",band".$this->GetValue('band');
			else
				$format .= ":".$this->m_triggers[$field].$this->GetValue($this->m_triggers[$field]);
		}

		$addcode = "onchange='javascript:bChanged=true'";
		
		if (isset($this->m_triggers) && ($key = array_search($field,$this->m_triggers))!==false)
		{
			$addcode = "onChange='RepostForm(this,\"".$this->m_parent->GetAction()."\")'";
		}
		else if ($format=="licencetype")
		{
			// a bit of a hack for licencetype since it has to reset the page
			if (isset($this->m_parent))
			{
				if (isset($_POST['licencetype']))
				{
					$value = $_POST['licencetype'];
				}
				else
				{
					$value = "T";
				}
				$addcode = "onChange='licencetypechanged(this,\"".$this->m_parent->GetAction()."\")'";
			}	
		}
		else if (isset($this->m_addcode[$field]))
		{
			$addcode = $this->m_addcode[$field];
		}
		
		if ($disabled) $addcode.=" disabled";

		// check for multiple field
/*		if (stripos($field,"*"))
		{
			$i=0;
			while(true)
			{
				$newname = str_ireplace("*",'_'.$i,$field);
				if (!$this->HasValue($newname)) break;
				if ($i==0)
					$value = $this->GetValue($newname);
				else
					$value .= $this->GetValue($newname).",";
			}
			$field = str_ireplace("*","",$field);
		}*/
		
		EditField($field,$format,$value,$addcode);
		print "</td>";
	}
	
	
	function 	_rowfixed($display,$field,$format,$displayvalue,$value,$tooltip)
	{
		if ($display!="")
		{
			if ($tooltip!="")
				print "<td valign=top><label onmouseover=\"Tip('".addslashes($tooltip)."',WIDTH,300,DELAY,1000)\">$display</label></td>";
			else
				print "<td valign=top><label>".$display."</label></td>";
		}
		print "<td>";
		if ($format=="date")
		{
			$time = strtotime(MakeDate($displayvalue));
			$day = date('d',$time);
			$month= date('m',$time);
			$year= date('Y',$time);
			print "<input class=fixed type=text size=2 disabled value='".$day."' name='".$field."_day' id='".$field."_day'>";
			print "<input class=fixed type=text size=2 disabled value='".$month."' name='".$field."month' id='".$field."_month'>";
			print "<input class=fixed type=text size=4 disabled value='".$year."' name='".$field."_year' id='".$field."_year'>";
			$value = $year."-".$month."-".$day;
			$this->_hidden($field,$value);
		}
		else if ($format=="currency")
		{
			print "<input class=fixed type=text disabled value='".$displayvalue."' name='$field' id='$field'>";
			$this->_hidden($field,$displayvalue);
		}
		else
		{
			print "<input class=fixed type=text disabled size=48 value='".$displayvalue."' name='$field' id='$field'>";
			$this->_hidden($field,$value);
		}
		
		print "</td>\n";
	}
	
	
	function	_submit($name,$value,$validate)
	{
		if ($value!="BACK")
			print "<button type=submit class=submit id=submitbutton name=submitvalue value='".$value."'";
		else
			print "<button type=submit class=submitback id=submitbutton name=submitvalue value='".$value."'";
		
		if ($validate!="")
			print " onClick='".$validate."' ";
		print ">$name</button> ";
	}
	
	function	_hidden($name,$value)
	{
		print "<input type=hidden name='".$name."' value='".$value."'>\n";
	}
	
	function _end()
	{
		print "</form>\n";
		print "</div>";
	}


	// Base function for adding a field	
	function _addField($group,$description,$name,$type,$optional,$helptext)
	{
		$this->m_fields[] = array('group'=>$group,'description'=>$description,'name'=>$name,'type'=>$type,'value'=>"",'optional'=>$optional,'fixed'=>false,'helptext'=>$helptext);
	}
	
	
	function SetFixed($name)
	{
		foreach ($this->m_fields as $index=>$field)
		{
			if ($field['name']==$name)
			{
				$this->m_fields[$index]['fixed']=true;
				return;
			}
		}
		die("Cannot find fixed field '$name'");
	}

	// This is functionally equivalent to calling ValidatedForm constructor	
	function AddTable($table,$exposelist)
	{
		$exposearray = explode(",",$exposelist);
		$tableinfo = TableInfo::GetTable($table);
		foreach ($tableinfo as $field=>$info)
		{
			if (empty($exposearray) || !in_array($field,$exposearray))
				continue;
		
			$this->_addField($this->m_group,$info['description'],$field,$info['type'],$info['optional'],$info['helptext']);
		}
	}
	
	function AddCustomScript($script)
	{
		$this->m_customscript .= $script."\n";
	}
	
	function AddField_FromTable($table,$field)
	{
		$info = TableInfo::GetField($table,$field);
		$this->_addField($this->m_group,$info['description'],$field,$info['type'],$info['optional'],$info['helptext']);
	}

	// Add a group - a collection of fields that can appear multiple times on the form
	//    $groupname = an arbitary name for this group so that we can attach fields to the group
	//    $startid - all fields in a group have an integer appended to the name, this is the first value
	//    $endid - as with the startid - but the last value allowed
	//    $extensible - true/false - if true the users is given +- button, otherwise they just appear
	function StartGroup($startid,$endid,$extensible)
	{
		if (empty($this->m_groups))
			$this->m_group = 0;
		else
			$this->m_group = count($this->m_groups);
		$this->m_groups[] = array('startid'=>$startid,'endid'=>$endid,'extensible'=>$extensible);
	}

	function EndGroup()
	{
		$this->m_group = NO_GROUP;
	}

	function AddFixed($description,$name,$type,$value)
	{
		$this->_addField($this->m_group,$description,$name,$type,false,"");
		$this->SetFixed($name);
		$this->SetValue($name,$value);
	}

	function AddField($description,$name,$type,$optional)
	{
		$args = func_get_args();
		if (empty($args[4])) $args[4] = "";
		
		$this->_addField($this->m_group,$description,$name,$type,$optional,$args[4]);
	}
	
	function AddAction($actionname,$validate,$tooltip="",$prop="",$value="")
	{
		//print "AddAction ".$actionname." ".$validate." ".$value."<br>";
		$this->m_actions[] = array('name'=>$actionname,'value'=>(($value=="") ? $actionname : $value),'validate'=>$validate);
		//print_r($this->m_actions);
	}
	
	function AddHidden($name,$value)
	{
		$this->m_hidden[$name] = $value;
	}
	
	function AddCancel()
	{
		$args = func_get_args();
		if (!empty($args[0]))
			$this->AddAction($args[0],false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
		else
			$this->AddAction("BACK",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
	}
	
	function SetTarget($target)
	{
		$this->m_target = $target;
	}
	
	function	_getscriptname()
	{
		return	"FormValidate";
	}
	
	function 	_initscript()
	{
		$this->m_script = "function ".$this->_getscriptname()."(form)\n{ try {\n";
	}
	
	function	_addemptyguard($field,$description,$type)
	{
		if ($type!="date")
			$this->m_script .= " if (!CheckEmpty(form,'$field')) { alert('Please enter $description'); return false; }\n";
		else
			$this->m_script .= " if (!checkemptydate(form,'$field')) { alert('Please enter $description'); return false; }\n";
	}

	function	_adddatevalidation($field,$description)
	{
		$this->m_script .= " if (!ValidateDate(form,'$field')) { alert('Please enter valid date for $description'); return false; }\n";
	}
	
	function	_addcurrencyvalidation($field,$description)
	{
		$this->m_script .= "if (!ValidateCurrency(form,'$field')) { alert('Please enter a valid currency'); return false; }\n";
	}
	
	function	_addpercentagevalidation($field,$description)
	{
		$this->m_script .= "if (!ValidatePercentage(form,'$field')) { alert('Please enter value between 0-100 for $description'); return false; }\n";	
	}

	function	_addtracksvalidation($field,$description)
	{
		$this->m_script .= 'try { var objs = document.getElementsByTagName("SELECT"); var tracklist="";'."\n";
		$this->m_script .= 'for (var i=0; i<objs.length; i++) {'."\n".'if (objs[i].name=="'.$field.'s") {'."\n";
		$this->m_script .= 'for (var j = 0; j < objs[i].options.length; j++) if (objs[i].options[j].selected) { value=objs[i].options[j].value; break;}';
		$this->m_script .= 'if (tracklist!="") tracklist=tracklist+",";';
		$this->m_script .= ' tracklist=tracklist+objs[i].value;} ';
		$this->m_script .= '}'."\n";
		$this->m_script .= 'document.getElementById("'.$field.'").value=tracklist; } catch (ex) { alert(ex); }';
	}
	function	_addCustomScript()
	{
		$this->m_script .= "\n";
		$this->m_script .= $this->m_customscript;
	}

	function	_getscript()
	{
		$this->m_script .= " return true; } catch (ex) { alert(ex); return false; }\n}\n";
		return $this->m_script;
	}
	
	function	SetValuesFromDB($table,$index)
	{
		$entry = DataTable::GetEntryFromID($table,$index);
		if ($entry!=false)
			$this->SetValues($entry);
	}
	
	
	function	SetValues(&$assoc)
	{
		foreach ($assoc as $name=>$value)
		{
			$this->SetValue($name,$value);
		}
	}
	
	// Set a value - if doesn't exist just drops out
	// if you specify a wild card in the name then it will treat the value as an array and set them consequtively substituting * with the index
	function	SetValue($name,$value)
	{
		if (stripos($name,"*"))
		{
			foreach ($value as $index=>$subvalue)
			{
				$newname = str_ireplace("*", '_'.$index, $name);
				$this->_setvalue($newname,$subvalue);
			}
		}
		else
		{
			$this->_setvalue($name,$value);
		}
	}
	
	function _setvalue($name,$value)
	{
		$args = func_get_args();
		foreach ($this->m_fields as $index=>$field)
		{
			if ($field['name']==$name)
			{
				$this->m_fields[$index]['value'] = $value;
				if (isset($args[2]))
					$this->m_fields[$index]['fixed'] = $args[2];
				break;
			}
		}
	}
	
	
	function SetDisabled($fieldname)
	{
		foreach ($this->m_fields as $index=>$field)
		{
			if ($field['name']==$fieldname)
			{
				$this->m_fields[$index]['disabled']=true;
				break;
			}
		}
	}
	
	function	_copybuttons()
	{
		print '<td><a href="#" onClick="try {AddGroup(this);}catch(ex){alert(ex);}" ><img border=0 src="images/plussign.png"></a>';
		print '<a href="#" onClick="try{RemoveGroup(this);}catch(ex){alert(ex);}" ><img border=0 src="images/minussign.png"></a></td>'."\n";
	}
	
	function	SetValuesFromPost(&$aspect)
	{		
		$args = func_get_args();
		if (isset($args[1]))
		{
			$include = explode(",",$args[1]);
			foreach ($include as $name)
			{
				if (isset($_POST[$name]))
				{
					$value = $_POST[$name];
					if(get_magic_quotes_gpc()) 
						$value = stripslashes($value);  // need to do this since it didn't passthrough DB Validate when refreshing
						
						
					$this->SetValue($name,$value);
				}
			}
		}
		else
		{
			foreach ($_POST as $name=>$value)
			{
			
				foreach ($this->m_fields as $index=>$field)
				{
					if ($field['name']==$name)
					{
						// this will just pass through if not a member of the post
						if (empty($field['value']))
						{
							
							if(get_magic_quotes_gpc()) 
								$value = stripslashes($value);  // need to do this since it didn't passthrough DB Validate when refreshing
							$this->SetValue($name,$value);	
						}
						break;
					}
				}
			}
		}
	}
	
	static function GetGroupFieldFromPost($field,$index)
	{
		$postname = $field."_".$index;
		
		if (isset($_POST[$postname]))
		{
			return $_POST[$postname];
		}
		
		return false;
	}
	
	
	function SetGroupValue($groupindex,$name,$value)
	{
		foreach ($this->m_fields as $index=>$field)
		{
			if ($field['name']==$name)
			{
				$valuestemp = explode(",",$field['value']);
				$valuestemp[$groupindex]=$value;
				$this->m_fields[$index]['value'] = implode(",",$valuestemp);
				break;
			}
		}
	}
	
	function _addValidationCode(&$field)
	{
		if ($field['optional']==false)
		{
			$this->_addemptyguard($field['name'],$field['description'],$field['type']);
		}
		switch($field['type'])
		{
			case "percentage":
				$this->_addpercentagevalidation($field['name'],$field['description']);
				break;
			case "date":
				$this->_adddatevalidation($field['name'],$field['description']);
				break;
			case "currency":
				$this->_addcurrencyvalidation($field['name'],$field['description']);
				break;
			case "tracks":
				//$this->_addtracksvalidation($field['name'],$field['description']);
				break;
			default:
		}	
	}
	
	function SetFilter($field,$target)
	{
		if ($field=="productband")
		{
			// little hack - otherwise won't create javascript
			if (empty($this->m_triggers) || array_search("product",$this->m_triggers)===false)
				$this->m_triggers["none1"] = "product";
			if (array_search("band",$this->m_triggers)===false)
				$this->m_triggers["none2"] = "band";
		}
		
		$this->m_triggers[$target] = $field;	
	}
	
	function SetTitle($title)
	{
		$this->m_title = $title;
	}
	
	function Present()
	{
	
		$mandatoryfields = false;

		if (isset($_POST['licencetypechanged']))
			$this->SetValuesFromPost($this->m_parent);
	
		if (isset($_POST['refreshedpost']))
			$this->SetValuesFromPost($this->m_parent);
	
		$this->_start();
		print '<span class="formtitle">'.$this->m_title.'</span>';
		
		$this->_initscript();
		// Go through each field and generate the necessary html/javascript
		$index = 0;
		$maxindex = count($this->m_fields);
		while ($index<$maxindex)
		{
			// Grab the current field we are processing
			$field = $this->m_fields[$index];
			$description = $field['description'];
			
			// we have been processing a group
			// have we finished yet??
			if ($field['group'] != NO_GROUP)
			{
				// scan forward and find end of group
				$groupstartindex = $index;
				$groupendindex = $index;
				while ($groupendindex<$maxindex && $this->m_fields[$groupendindex]['group']==$field['group'])
				{
					$values[$groupendindex] = explode(",",$this->m_fields[$groupendindex]['value']);
					++$groupendindex;
				}

				// print header
				$description = "";
				if ($groupendindex==$groupstartindex+1) 
				{
					$description = $field['description'];
				}
				else
				{	
					print "<tr>";
					for ($j=$groupstartindex;$j<$groupendindex;++$j)
					{
						print "<td><label>".$this->m_fields[$j]['description']."</label></td>";
					}
					print "</tr>\n";
				}
				
				$countgroup = count($values[$groupstartindex]);	 // even if empty this will be one which is what we want
				// Draw groups
				for ($groupid=0;$groupid<$countgroup;++$groupid)
				{
					print "<tr id='".$field['group']."_".$groupid."'>";
					for ($j=$groupstartindex;$j<$groupendindex;++$j)
					{
						$groupfield = $this->m_fields[$j];
	
						// Only do this the first time (and not for fixed fields)
						if (!$groupfield['fixed'] && $groupid==0)
							$this->_addValidationCode($groupfield);
					
						if ($groupfield['type']=="newline")
						{
							print "</tr><tr>\n";
						}
						else
						{
							// ADD THE FIELD
							if ($groupfield['fixed'])
								$this->_rowfixed($description,$groupfield['name']."_$groupid",$groupfield['type'],TableInfo::DisplayFromType($groupfield['type'],$values[$j][$groupid]),$values[$j][$groupid]);
							else
								$this->_row($description,$groupfield['name']."_$groupid",$groupfield['type'],$values[$j][$groupid],"",false);
						}
					}
					$this->_copybuttons();
					print "</tr>\n";			// terminate the row
				}
				$index = $groupendindex;
			}													
			else
			{
				print "<tr>";
				$description = $field['description'];
				if ($field['optional']==false) { $description .= "*"; $mandatoryfields=true; }
				// ADD THE FIELD
				if ($field['fixed'])
					$this->_rowfixed($description,$field['name'],$field['type'],TableInfo::DisplayFromType($field['type'],$field['value']),$field['value'],$field['helptext']);
				else
					$this->_row($description,$field['name'],$field['type'],$field['value'],$field['helptext'],!empty($field['disabled']));
				// ADD THE VALIDATION CODE - this generates the necessary javascript
				// this only wants to do be once per group and not for fixed values (cause you can't change em!)
				if (!$field['fixed'])
					$this->_addValidationCode($field);
				print "</tr>";
			}
						
			++$index;
		}

		print "</table><br>\n";
		if ($mandatoryfields) print '<span class="formfooter">* Manadatory fields<br><br></span>';
		
		if ($this->m_customscript!="")
			$this->_addCustomScript();
			
		// Output the script that we generated
		JavaScript_Begin();
		JavaScript_Write($this->_getscript());
		JavaScript_End();
		
		// Output the actions
		foreach ($this->m_actions as $action)
		{

			$addcode = "javascript:if (bChanged) return confirm(\"You have made changes to this form that will be lost on performing this action. Do you wish to continue?\");";
			// If the action requires a validate then call our java-script on Submit.
			if ($action['validate'])
				$addcode = "return ".$this->_getscriptname()."(this.form);";
			$this->_submit($action['name'],$action['value'],$addcode);
		}
		
		// Also add all our hidden values
		if (!empty($this->m_hidden))
		{
			foreach ($this->m_hidden as $name => $value)
			{
				$this->_hidden($name,$value);
			}
		}
		
		$this->_end();
	}
	
	
};

?>