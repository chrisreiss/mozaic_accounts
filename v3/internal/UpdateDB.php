<?php
// Very important file - this will update the database to various revisions.
// Need to be logged in as administrator


ini_set("include_path", "../:../classes:../thirdparty:../localise");

include_once("classes/cDatabase.php");
include_once("classes/cAuthorize.php");

// Put your queries here - you will need to be logged on
/*$queries[] = "ALTER TABLE `sale` ADD `baseprice` VARCHAR( 16 ) NOT NULL AFTER `quantity` ;";
$queries[] = "ALTER TABLE `deals` ADD `packagingdeductions` FLOAT NOT NULL DEFAULT '0' AFTER `reserve` ;";
	*/

$accountid = $_GET['accountid'];
$dummyrun = true;

function Query($query)
{
	print "Running query: ".$query."<br>";
	if (!$dummyrun) Database::Query($query);
}

if (Authorize::IsAuthorized() && ($_SESSION['userid']=="tom") )
{
	Database::Connect("mozaic");
	if ($accountid=='*')
		$accounts = Database::QueryGetResults("SELECT `uid`,`name`,`database` FROM accounts;");
	else
		$accounts = Database::QueryGetResults("SELECT `uid`,`name`,`database` FROM accounts WHERE uid=$accountid LIMIT 1;");
	
	if ($dummyrun) print "Dummy run - no transactions will occur<br><br>";

	foreach ($accounts as $account)
	{
		$accountdb = $account['database'];
		$accountid = $account['uid'];
		$accountname = $account['name'];
		
		print "Processing account '$accountname' ($accountid) on database 'szirtes_$accountdb'<br>";
		Database::Connect("mozaic");
	
	//	Query("ALTER TABLE  `tracks` ADD  `version` VARCHAR( 64 ) NOT NULL AFTER  `title` , ADD  `remixer` VARCHAR( 64 ) NOT NULL AFTER  `version`");	
		Query("ALTER TABLE  `deals` CHANGE  `dealtype`  `dealtype` ENUM(  'P',  'R',  'L',  'S' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  'P'");

	}

}
else
{
	die("Not authorised to run this script");
}	

?>