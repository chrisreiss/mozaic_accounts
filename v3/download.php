<?php
ini_set("include_path", "./:./classes:./thirdparty:./localise");

require_once('cAuthorize.php');
require_once('headerfooter.php');

if (Authorize::IsAuthorized())
{
	$filename = $_GET['file'];
	$fullpathname = $_SERVER['DOCUMENT_ROOT'] . '/accounts/' . $_SESSION['accountid']."/" . $filename;
//	$fullpathname = "../accounts/".$_SESSION['accountid']."/".$filename;
//	$fullpathname = "../accountdata/".$filename;
	
	if ($filename!="" && file_exists($fullpathname))
	{
	  if(strpos($filename,".pdf")!==FALSE)
		  header('Content-type: application/pdf');
		else
		  header('Content-type: text/plain');
		
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile($fullpathname);
	}
	else
	{
		displayHeader();
		displayMenuBar();
		displayStartDynamic();
		print "<b>Cannot find the requested file $filename</b>";
		displayEndDynamic();
		displayFooter();
	}
}
else
{
		displayHeader();
		displayStartDynamic();
		print "<b>You are not authorized to access this file</b>";
		displayEndDynamic();
		displayFooter();
}


?>
