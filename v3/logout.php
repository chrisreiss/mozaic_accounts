<?php
ini_set("include_path", "./:./classes:./thirdparty:./localise");
require_once('headerfooter.php');
require_once('cDatabase.php');
require_once('cAuthorize.php');

if (!Authorize::IsAuthorized()) 
{
	//session_destroy();
	//die("Access denied");
}

if (!isset($_SESSION))
{
	session_start();
}

Authorize::Logout();

if ($_SERVER['SERVER_NAME']=='local.mozaic')
	header("Location:http://localhost/moziacaccounts");
else
	header("Location:https://cyphon.com");
/*
displayHeader(0);
displayStartDynamic();
include "present.php";
displayEndDynamic();
displayFooter();
*/
?>