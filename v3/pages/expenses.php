<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cValidatedForm.php');

require_once('cSimpleList.php');
class ExpensesList extends SimpleList {
		
	protected function actions()
	{
		$this->addaction("ADD EXPENSES","Add Expenses",SL_NOSELECTION);
		$this->addaction("EDIT EXPENSE", STRING_ACTION_EDIT,SL_SINGLESELECTION);
		$this->addaction("REMOVE EXPENSES", "Remove Expenses",SL_MULTIPLESELECTION);
	}
	
	protected function populate()
	{

/*			$list->AddView("by product","expenses","%invoicedate% : %description% (%amount%)","product","invoicedate","%product% (%product.product:title%)");
			$list->AddView("by type","expenses","%invoicedate% : %product% - %description% (%amount%)","type","invoicedate","%type%");
*/

		// filters
		$where = "WHERE 1 ";
		$filter = getVar('expensesFilterProduct','');
		if ($filter!="") $where .= 'AND product=\''.$filter.'\' ';
		$rows = Database::QueryGetResults("SELECT uid,invoicedate,product,description,type,amount FROM expenses $where LIMIT 1000;");
	
		$this->_header = array('DATE','CAT NO','DESCRIPTION','CATEGORY','VALUE');
		if (!empty($rows))
		{
			foreach ($rows as $row)
			{
				$row['amount'] = Currency::DATABASETODISPLAY($row['amount']);
				$this->_rows[] = $row;
			}
		}
		
		// Store in session
	//	$_SESSION['expensesFilterProduct']=getVar('expensesFilterProduct');
		return true;
	}
	
	protected function filters()
	{
	//	print_r($_POST);
		$insertcode = 'onChange="document.mainform.submit();"';
		
		
		// filter by batch -> band -> product -> title
		print 'Display '; 
		$products = array(array('','all products')); 
		$productsindatabase = getProducts();
		foreach($productsindatabase as $i=>$p)
		{
			$productsindatabase[$i]['title'] = $p['uid'].' '.$p['title'];
		}
		$products = array_merge($products,$productsindatabase);
		print formfieldSelectFromArrayWithKey("expensesFilterProduct",$products,getVar('expensesFilterProduct'),$insertcode);	
	}
	
	protected function containerpage()
	{
		return "expenses";
	}

	public function ExpensesList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}

}



Database::Init();



$aspect = new Aspect;
$aspect->Start("expenses","VIEW");
 
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
		
			$saleslist = new ExpensesList('expenseid');
			$saleslist->Present();
			$aspect->Present();
			break;
		}
		case "REMOVE EXPENSES":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			foreach ($_POST['expenseid'] as $value)
			{
				DataTable::Delete("expenses",$value);
			}
			break;
		}
		case "EDIT EXPENSE":
		{
			HeaderBar('Edit Expense');
			$form = new ValidatedForm("expenses","product,invoicedate,type,description,invoice,amount,paiddate,comments");
			$form->SetValuesFromDB("expenses",$_POST['expenseid'][0]);
			$form->AddHidden("expenseid",$_POST['expenseid'][0]);
//			$form->AddHidden("expenseid2",$_POST['expenseid'][0]);
			$form->AddAction("SUBMIT EDIT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "NEW REPEAT":
		{
		}
		case "ADD EXPENSES":
		{
			if (!isset($_POST['addcount'])) $_POST['addcount']=1;
			HeaderBar('Add Expense ('.$_POST['addcount'].')');

			$form = new ValidatedForm("expenses","product,invoicedate,type,description,invoice,amount,paiddate,comments");
			if ($_POST['addcount']>1) 
				$form->SetValuesFromPost($aspect,"product,invoicedate,type");
//			else
//				$form->SetValue("product",$_POST['expenseid2']);
			$form->AddAction("SAVE",true);	// validate before sending
			$form->AddCancel("FINISHED");	// validate before sending
			$form->AddHidden('addcount',$_POST['addcount']+1);
			$aspect->Attach($form);
//			$aspect->PreserveVar('expenseid2');
			$aspect->Present();
			break;
		}
		case "SAVE":
		{
			$aspect->SetNextAction("NEW REPEAT");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("expenses","product,invoicedate,type,description,invoice,amount,paiddate,comments",$aspect);
			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			
			DataTable::UpdateFromPost("expenses",$aspect->GetVar('expenseid'),"product,invoicedate,type,description,invoice,amount,paiddate,comments",$aspect);
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}

	}
}

$aspect->End();

?>