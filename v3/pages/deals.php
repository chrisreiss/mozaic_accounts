<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once('cValidatedForm.php');
require_once('cDeal.php');
require_once('strings.php');

require_once('cFlexForm.php');


//---- This class creates the main view ----//
require_once('cSimpleList.php');
class DealsList extends SimpleList {
		
	public function actions()
	{
		$this->addaction("NEW DEAL","New",SL_NOSELECTION);
		$this->addaction("EDIT DEAL", STRING_ACTION_EDIT,SL_SINGLESELECTION);
		$this->addaction("REMOVE DEAL", STRING_ACTION_DELETE,SL_MULTIPLESELECTION);
//		$this->addaction("EXTEND DEAL",'Extend Deal',SL_SINGLESELECTION);
		$this->addaction("EDIT DEFAULTS",'Edit Defaults',SL_NOSELECTION);

	}
	
	protected function populate()
	{
		$where="1";
		$filter = getVar('filterBands','all bands deals');
		if ($filter!="all bands deals") $where = 'band=\''.$filter.'\' ';
		$this->_header = array('ARTIST','DATE','DEAL GROUP','DESCRIPTION');
		$deals = Database::QueryGetResults("SELECT uid,band,startdate,crossdealid,dealtype,product,salesfilter FROM deals WHERE ".$where.";");
		foreach($deals as $deal)
		{
			// rows = dealid, artist, start date, grouping, description 
			//if ($deal['description']=='')
			{
				$dealtype = 'Profit Split';
				if ($deal['dealtype']=='S') $dealtype='Remix Split';
				if ($deal['dealtype']=='R') $dealtype='Royalty';
				if ($deal['dealtype']=='L') $dealtype='License';
				$description = '<i>'.$dealtype.' for '.$deal['product'];
				if ($deal['salesfilter']=='DS') $description.= " (Singles only)";
				if ($deal['salesfilter']=='DB') $description.= " (Bundle only)";
				if ($deal['salesfilter']=='D') $description.= " (Digital only)";
				if ($deal['salesfilter']=='P') $description.= " (Physical only)";
				
			}
			$band = Database::QueryGetValue('name','bands','uid="'.$deal['band'].'"');
			
			$this->_rows[] = array('uid'=>$deal['uid'],'band'=>$band,'date'=>$deal['startdate'],'crossdealid'=>$deal['crossdealid'],'description'=>$description);
		}
//		$this->_rows = Database::QueryGetResults("SELECT deals.uid,bands.name,deals.startdate,deals.crossdealid,deals.description FROM deals JOIN bands WHERE bands.uid=deals.band;");
		return true;
	}
	
	protected function filters()
	{
		$insertcode = 'onChange="document.mainform.submit();" class="filter"';
		// filter by batch -> band -> product -> title
		print 'Display '; 
		$batches = array(array('','all bands deals')); $batches = array_merge($batches,getBands());
//		print_r($batches);
		print formfieldSelectFromArrayWithKey("filterBands",$batches,getVar('filterBands','all bands deals'),$insertcode);		
	}

	
	protected function containerpage()
	{
		return "deals";
	}

	public function DealsList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}

}


Database::Init();

// some javascript
$code = "'if (validateform.dealtype.value==\"L\") {validateform.licensee.disabled=false; validateform.crossdealid.disabled=true;} else {validateform.licensee.disabled=true; validateform.crossdealid.disabled=false;}'";


$aspect = new Aspect;
$aspect->Start("deals","VIEW");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW": 
		{
			$list = new DealsList('dealid');
			$list->Present();
			$aspect->Present();
			break;

/*			$list = new TableList;
			$list->Begin("dealid","deals");
			$list->AddView("deals by band","deals","%product% - %dealtype%","band","product","%band.bands:name%");
			$list->AddView("deals by product","deals","%product% - %band.bands:name%","product","startdate","%product.product:artist.bands:name% - %product.product:title%");
			$list->AddView("linked deals","deals","%band.bands:name% - %product%","crossdealid","product","%crossdealid% (%dealtype% %rate%pcnt)");
			$list->SetInfoBoxHandler("getdealinfo");
			$list->AddAction("NEW DEAL",false);
			$list->AddAction("REMOVE DEAL",true);
			$list->AddAction("EDIT DEAL",true);
			$list->AddAction("EXTEND DEAL",true);
			$list->AddAction("EDIT DEFAULT DEAL",false);
			if ($_SESSION['userid']=='tom')
			{
				$list->AddAction("RETRO DIGITAL DEALS",false);
			}
			$aspect->Attach($list);
			$aspect->Present();
			break;*/
		}
		case "REMOVE DEAL":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			// If we remove the linked original deal - we need to reset all the dependent deals to a new one
			$newcrossdealid = Database::QueryGetResult("SELECT uid FROM deals WHERE uid!='".getVarS('dealid')."' AND crossdealid='".getVarS('dealid')."' LIMIT 1;");
			if (!empty($newcrossdealid))
			{
				$currentdeal = Database::QueryGetResult("SELECT reserve,minpayout FROM deals WHERE uid='".getVarS('dealid')."';");
				if (empty($currentdeal)) 
					die ("Line ".__line__.": Deal to remove does not exist!");
				Database::Query("UPDATE deals SET reserve='".$currentdeal['reserve']."',minpayout='".$currentdeal['minpayout']."',crossdealid='".$newcrossdealid['uid']."' WHERE crossdealid='".getVarS('dealid')."'"); 
			}
			// Now delete the deal
			DataTable::Delete("deals",getVarS('dealid'));
			break;
		}
		case "EDIT DEFAULTS":
		{
//			print '<p>These values will be the default values used when you create new deals. You can change them for each deal, but this will just save you a bit of time as it prepopulates the form.</p>';
			Database::Connect("mozaic");
			HeaderBar('Edit Default Deal Terms');
			$form = new ValidatedForm("accounts","defaultdealtype,defaultdealrate,defaultdealreserve,defaultpackagingdeductions,defaultdealterm,defaultminpayout");

			$form->SetValuesFromDB("accounts",$_SESSION['accountid']);
			$form->AddCancel();
			$form->AddAction("SAVE",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM,"SAVE DEFAULTS");	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			Database::Disconnect();
			break;
		}
		case "SAVE DEFAULTS":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			Database::Connect("mozaic");
			Database::Query("UPDATE accounts SET defaultdealtype='".$aspect->GetVar('defaultdealtype')."',defaultdealrate='".$aspect->GetVar('defaultdealrate')."',defaultpackagingdeductions='".$aspect->GetVar('defaultpackagingdeductions')."',defaultminpayout='".Currency::DISPLAYTODATABASE($aspect->GetVar('defaultminpayout'))."',defaultdealreserve='".$aspect->GetVar('defaultdealreserve')."',defaultdealterm='".$aspect->GetVar('defaultdealterm')."' WHERE uid='".$_SESSION['accountid']."';");
			Database::Disconnect();
			break;
		}
		case "SUBMIT EDIT":
		{
//			print_r($_POST);
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			if ($aspect->GetVar("dealtype")=='L')
			{
				// Check if changed from non-licence before
				$result = Database::QueryGetResult("SELECT dealtype FROM deals WHERE uid='".$aspect->GetVar('dealid')."';");
				if ($result['dealtype']!='L')
				{
					$result = Database::QueryGetResult("SELECT uid FROM deals WHERE licensee='".$aspect->GetVar('licensee')."' AND crossdealid=uid LIMIT 1;");
					if ($result!==false)
						$_POST['crossdealid'] = $result['uid'];
					else
						$_POST['crossdealid'] = $aspect->GetVar('dealid');
				}
				else
				{
					$result = Database::QueryGetResult("SELECT crossdealid FROM deals WHERE uid='".$aspect->GetVar('dealid')."';");
					$_POST['crossdealid'] = $result['crossdealid'];
					
				}
			}
			else
			{
//				print_r($_POST);
				$errors =  Deal::Validate($_POST,true);
			}
			if (empty($errors))
			{
				
				DataTable::UpdateFromPost("deals",$aspect->GetVar('dealid'),"product,track,band,dealtype,licensee,advance,advancepaiddate,startdate,enddate,crossdealid",$aspect);
				$aspect->SetNextAction("EDIT TERMS");
			}
			else
			{
				$aspect->Error($errors);
				$aspect->SetNextAction("VIEW");
			}
			break;  
		} 
		case "EDIT TERMS":
		{
			$crossdealid = $aspect->GetVar("crossdealid");
			if ($crossdealid!='0')
			{
				if ($aspect->GetVar("dealtype")=="P")
	        		print "<p>".STRING_MISC_CROSSDEALSPLITS_PROFITSHARE." (Cross Deal id no. $crossdealid)</p>";
	       		else if ($aspect->GetVar("dealtype")=='R')
	         		print "<p>".STRING_MISC_CROSSDEALSPLITS_ROYALTY." (Cross Deal Id no. $crossdealid)</p>";
	      	}
	      	else
	      	{
		        // MAKING THIS DEAL SEPERATE FROM OTHERS
		        // Set the crossdealid of this deal to itself
		        Database::Query("UPDATE deals SET crossdealid='".$aspect->GetVar('dealid')."' WHERE uid='".$aspect->GetVar('dealid')."';");
				$crossdealid = $aspect->GetVar('dealid');
				
		        // Get any other deals that might be linked to this
		        $linkeddeals = Database::QueryGetResults("SELECT uid FROM deals WHERE crossdealid='".$aspect->GetVar('dealid')."' AND uid!='".$aspect->GetVar('dealid')."';");
		        // If there are some change their crossdealid so its the first deal in the list
		        if ($linkeddeals!=false)
		        {
		          $dealconditions = Database::QueryGetResult("SELECT reserve,packagingdeductions,minpayout FROM deals WHERE uid='".$aspect->GetVar('dealid')."';");
		          Database::Query("UPDATE deals SET reserve='".$dealconditions['reserve']."',packagingdeductions='".$dealconditions['packagingdeductions']."',minpayout='".$dealconditions['minpayout']."',crossdealid='".$linkeddeals[0]['uid']."' WHERE crossdealid='".$aspect->GetVar('dealid')."' AND uid!='".$aspect->GetVar('dealid')."';");
		        }
		    }	      
			if ($aspect->GetVar('dealtype')=='P')
			{
				$form = new ValidatedForm("deals","reserve,minpayout");
				$form->AddHidden("packagingdeductions",0);
			}
			else
			{
				$form = new ValidatedForm("deals","reserve,packagingdeductions,minpayout");
			}
	
			$result = Database::QueryGetResult("SELECT reserve,minpayout,packagingdeductions FROM deals WHERE uid='$crossdealid';");
			$form->SetValue("reserve",$result['reserve']);
			$form->SetValue("minpayout",$result['minpayout']);
			$form->SetValue("packagingdeductions",$result['packagingdeductions']);

			if ($aspect->GetVar('dealtype')=='L')
			{
				// Set up dealterm group
				$form->StartGroup(0,10,true);
				$form->AddField("Key Field","keyfield","keyfield",true);
				$form->AddField("Key","key","text16",true);
				$form->AddField("Rate","rate","percentage",false);
				$form->AddField("Base","base","basetype",false);
				$form->AddField("Description","descrip","text32",false);
				$form->EndGroup();
											
				// Populate with values
				$dealterms = Database::QueryGetResults("SELECT * FROM dealterms WHERE dealid='".$crossdealid."' ORDER BY uid;");
				if (!empty($dealterms))
				{
					$i=0;
					foreach($dealterms as $dealterm)
					{
						$form->SetGroupValue($i,"keyfield",$dealterm['keyfield']);
						$form->SetGroupValue($i,"key",$dealterm['key']);
						$form->SetGroupValue($i,"rate",$dealterm['rate']);
						$form->SetGroupValue($i,"base",$dealterm['base']);
						$form->SetGroupValue($i,"descrip",$dealterm['descrip']);
						++$i;  
					}
				}	
			}
			else
			{
				$form->AddField("Rate","rate","percentage",false);
				$form->AddField("Base","base","basetype",false);
				$dealterm = Database::QueryGetResult("SELECT * FROM dealterms WHERE dealid='$crossdealid' LIMIT 1;");
//				print_r($dealterm);
//				print $dealterm['rate'];
				$form->SetValue("rate",$dealterm['rate']);
				$form->SetValue("base",$dealterm['base']);
			}
						
	      // Do the rest
			$form->AddHidden("crossdealid",$crossdealid);
			$form->AddHidden("dealid",$aspect->GetVar('dealid'));
			$form->AddHidden("dealtype",$aspect->GetVar('dealtype'));
			$form->SetValuesFromDB("deals",$aspect->GetVar('crossdealid'));
			$form->AddAction("UPDATE CONDITIONS",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;			
		}
		case "UPDATE CONDITIONS":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// Do some validation first
			$dup = false;
			for ($i=0;$i<10;++$i)
			{
				$key = ValidatedForm::GetGroupFieldFromPost("key",$i);
				if ($key==false) break;
				if (isset($keyvalues[$key]))
				{
				  $dup = true;
				  break;
				}
				$keyvalues[$key] = 1;
			}
			if ($dup)
			{
			 	$aspect->Error("Detected Duplicate KEY values. Command aborted");
				$aspect->SetNextAction("VIEW");
				break;
			}      
			Database::Query("DELETE FROM dealterms WHERE dealid='".$aspect->GetVar('crossdealid')."';");
			if ($aspect->GetVar('dealtype')!='L')
			{
	        	DataTable::Insert("dealterms","dealid,keyfield,key,rate,base,descrip",$aspect->GetVar('crossdealid'),"","",$aspect->GetVar('rate'),$aspect->GetVar('base'),"default");			
			}
			else
			{
				for ($i=0;$i<10;++$i)
				{
					$keyfield = ValidatedForm::GetGroupFieldFromPost("keyfield",$i);
					$key = ValidatedForm::GetGroupFieldFromPost("key",$i);
					if ($key===false) break;
					$rate = ValidatedForm::GetGroupFieldFromPost("rate",$i);
					$base = ValidatedForm::GetGroupFieldFromPost("base",$i);
					$descrip = ValidatedForm::GetGroupFieldFromPost("descrip",$i);
	        		DataTable::Insert("dealterms","dealid,keyfield,key,rate,base,descrip",$aspect->GetVar('crossdealid'),$keyfield,$key,$rate,$base,$descrip);
				}
			}
			DataTable::UpdateFromPost("deals",$aspect->GetVar('dealid'),"reserve,packagingdeductions,minpayout",$aspect);
			Database::Query("UPDATE deals SET packagingdeductions='".$aspect->GetVar('packagingdeductions')."',reserve='".$aspect->GetVar('reserve')."',minpayout='".Currency::DISPLAYTODATABASE($aspect->GetVar('minpayout'))."' WHERE crossdealid='".$aspect->GetVar('crossdealid')."';");

			break;
		}
		case "NEW DEAL":
		case "EDIT DEAL":
		{
		// -------- USE OF NEW FLEX FORM CODE -------- //
		//	print_r($_POST);
			if ($aspect->GetAction()=='NEW DEAL') HeaderBar('New Deal'); else HeaderBar('Edit Deal');
			$form = new FlexForm();
			$form->SetAspect($aspect);
			$form->SetTarget('deals');
			if ($aspect->GetAction()=='EDIT DEAL')
			{
				$form->SetHidden('dealid',getVarS('dealid'));
				$form->SetHidden('uid',getVarS('dealid'));
			}
			
			// first ascertain whether fresh form, refreshed form, or to be populated from DB
			$dealexists = $aspect->GetAction()=="EDIT DEAL";
			$refreshed = false;
			if (!empty($_POST['refreshedpost'])) $refreshed=true;
			
			$deal = new CDeal();
			if ($dealexists && !$refreshed)
				$deal->LoadFromDB(getVarS('dealid'));
			else
				$deal->LoadFromPOST($_POST);		
							
			$form->AddField(FieldFactory::CreateDealType("dealtype")->Description("Deal Type")->Set($deal->dealtype)->OnChangeRefresh());

			$form->AddField(FieldFactory::CreateSalesFilter("salesfilter")->Description("Sales Filter")->Set($deal->salesfilter));


			$field = FieldFactory::CreateBand("band")->Description("Band")->Set($deal->band)->OnChangeRefresh();
			//print "band ".$deal->band;
			if ($deal->dealtype=='S')
				$field->description = 'Remixer';
			$form->AddField($field);
			if ($deal->dealtype=='S')
			{	
				$field = FieldFactory::CreateProductListForRemixer("product",$deal->band)->Description("Product")->Set($deal->product)->OnChangeRefresh();
				$form->AddField($field);
				$form->AddField(FieldFactory::CreateTrackListForProductAndRemixer("track",$field->currentvalue,$deal->band)->Description("Track")->Set($deal->track));
			}
			else
			{
				$field = FieldFactory::CreateProductListForBand("product",$deal->band)->Description("Product")->Set($deal->product)->OnChangeRefresh();
				$currentproduct = $field->currentvalue;
				$form->AddField($field);
				$form->AddField(FieldFactory::CreateTrackListForProductAndBand("track",$currentproduct,$deal->band)->Description("Track")->AddEmptyOption("--All tracks--")->Set($deal->track));
			}
			$form->AddField(FieldFactory::CreateAdvance("advance")->Description("Advance")->Set($deal->advance));
			$form->AddField(FieldFactory::CreateDate("advancepaiddate")->Description("Date Advance Paid")->Set($deal->advancepaiddate));
			$form->AddField(FieldFactory::CreateDate("startdate")->Description("Deal Start Date")->Set($deal->startdate));
			$form->AddField(FieldFactory::CreateDate("enddate")->Description("Deal End Date")->Set($deal->enddate));

			// now optional ones
			if ($deal->dealtype=='L')
				$form->AddField(FieldFactory::CreateLicensees("licensee")->Description("Licensee")->Set($deal->licensee));
			else if ($deal->dealtype=='S')
				{
					// auto create crossdealid
					$cd = Database::QueryGetResults("SELECT crossdealid FROM deals WHERE band='$deal->band' AND dealtype='S' LIMIT 1;");
					if ($cd==false)
						$deal->crossdealid = $deal->uid;
					else
						$deal->crossdealid = $cd['crossdealid'];
				}
				else
					$form->AddField(FieldFactory::CreateCrossDeal("crossdealid",$deal->band)->Description("Cross Calculate With")->Set($deal->crossdealid)->OnChangeRefresh());
			
			
			// get the dealterm from the DB
			$deal->LoadDealTerms();
			$form->AddField(FieldFactory::CreateTextField("descrip")->Description("Dealset Description")->Size(48)->Set($deal->descrip));
			
			$form->AddField(FieldFactory::CreatePercentage("rate",$deal->rate)->Description("Deal Rate")->Set($deal->rate));
			$form->AddField(FieldFactory::CreatePercentage("reserve",$deal->reserve)->Description("Reserve")->Set($deal->reserve));
			$form->AddField(FieldFactory::CreatePercentage("packagingdeductions",$deal->packagingdeductions)->Description("Packaging Deductions")->Set($deal->packagingdeductions));
//			$form->AddField(FieldFactory::CreateCurrency("minpayout",$deal->minpayout)->Description("Minimum Payout")->Set($deal->minpayout));
			$form->AddField(FieldFactory::CreateBaseRate("base",$deal->base)->Description("Base Rate")->Set($deal->base));
			
			$form->AddAction(Action::Create("BACK"));
			$form->AddAction(Action::Create("SAVE"));	// validate before sending
			
/*
			if ($dealexists)
				$form->AddAction(Action::Create("SAVE"));	// validate before sending
			else
				$form->AddAction(Action::Create("SUBMIT NEW"));	// validate before sending
*/			
			//print_r($deal);

			$aspect->Attach($form);
			$aspect->Present();
		//	print_r($form);
			break;
		}
		case "SAVE":
		{
			// reverse the date order
			$_POST['advancepaiddate'] = reverse_date_order($_POST['advancepaiddate']);
			$_POST['startdate'] = reverse_date_order($_POST['startdate']);
			$_POST['enddate'] = reverse_date_order($_POST['enddate']);

	//		print_r($_POST);

			
			//print_r($_POST); print"<br>";
			$aspect->SetNextAction("VIEW");
			$deal = new CDeal();
			$deal->LoadFromPost($_POST);
		//	print_r($deal); print "<br><br>";
			$deal->SaveToDB($deal->uid);
	//		print_r($deal);
			break;
		}
		case "SUBMIT EXTENDED DEAL":
		case "SUBMIT NEW":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			if ($aspect->GetVar('startdate')=="")
			{
				$result = Database::QueryGetResult("SELECT releasedate FROM product WHERE uid='".$aspect->GetVar('product')."';");
				$aspect->SetVar('startdate',$result['releasedate']);
			}
			$errors =  Deal::Validate($_POST,false);
			if (empty($errors))
			{
				if ($aspect->GetVar('dealtype')=='L')
				{
					// This is a licence therefore pick a crossdeal from any other matching licence deals
					$result = Database::QueryGetResult("SELECT uid FROM deals WHERE licensee='".$aspect->GetVar('licensee')."' AND crossdealid=uid LIMIT 1;");

					$aspect->SetNextAction("EDIT TERMS");
					$uid = DataTable::InsertFromPost("deals","product,track,band,dealtype,licensee,advance,advancepaiddate,startdate,enddate",$aspect);
					if ($result===false)
						$_POST['crossdealid'] = $uid;
					else
						$_POST['crossdealid'] = $result['uid'];
					Database::Query("UPDATE deals SET crossdealid='".$_POST['crossdealid']."' WHERE uid='$uid';");
					$_POST['dealid'] = $uid;
				}
				else
				{
					$aspect->SetNextAction("EDIT TERMS");
					$uid = DataTable::InsertFromPost("deals","product,track,band,dealtype,licensee,advance,advancepaiddate,startdate,enddate,crossdealid",$aspect);
					$_POST['dealid'] = $uid;
					// if crossdeal 0 set to itself
					if ($_POST['crossdealid']==0)
						Database::Query("UPDATE deals SET crossdealid='$uid' WHERE uid='$uid' LIMIT 1;");

				}
	
			}
			else
			{
				$aspect->Error($errors);
			}
			break;
		}
		
		// Depreciated
		case "EXTEND DEAL":
		{
			$form = new ValidatedForm("deals","product,track,band,dealtype,licensee,rate,reserve,minpayout,advance,packagingdeductions,advancepaiddate,startdate,enddate,crossdealid");
			$form->SetValuesFromDB("deals",$aspect->GetVar('dealid'));
			$starttime = strtotime($form->GetValue('enddate'));
			$startdate = date( "Y-m-d", mktime(0,0,0,date('m',$starttime),date('d',$starttime)+1,date('Y',$starttime)));
			$form->SetValue('startdate',$startdate);
			$form->SetValue('enddate','');
			$form->AddAction("SUBMIT EXTENDED DEAL",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->PreserveVar('dealid');
			$aspect->Present();
			break;
		}
		// Depreciated
		case "RETRO DIGITAL DEALS":
		{
			$query = "";
			$digitalproducts = Database::QueryGetResults("SELECT * FROM product WHERE uid LIKE '%D';");
			if (!empty($digitalproducts))
			{
				foreach ($digitalproducts as $product)
				{
					print "Processing ".$product['uid']."<br>";
					$newuid = substr($product['uid'],0,strlen($product['uid'])-1);
					print " searching product ".$newuid." ... ";
					$deals = Database::QueryGetResults("SELECT * FROM deals WHERE product='".$newuid."';" );
					if (!empty($deals))
					{
						// Check if already done
						
						print "duplicating deals: ";
						foreach($deals as $deal)
						{
						
							$dupcheck = Database::QueryGetResult("SELECT COUNT(uid) FROM deals WHERE product='".$product['uid']."' AND band='".$deal['band']."';");
							if ($dupcheck['COUNT(uid)']>0)
							{
								print $deal['uid']." skipping,";
								continue;
							}
							print $deal['uid'].", ";
							unset($deal['uid']);
							unset($deal['advance']);		// don't duplicate advances
							unset($deal['advancepaiddate']);
							$deal['product'] = $product['uid'];
							DataTable::InsertFromArray("deals",$deal,true);
						}
						print "<br>";
					}
					else
					{
						print "no deals found<br>";
					}
				}
			}
			else
			{
				print "No digital products (cat no. end D) found<br>";
			}
			$form = new Form();
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}

		default:
		{
			$aspect->DefaultAction();
		}
	}	
}
$aspect->End();
?>	


