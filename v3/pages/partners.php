<?php
require_once('util.php' );
require_once('cForm.php');
require_once('cDataTable.php');
require_once('cList.php');
require_once('cAspect.php');
require_once('cDatabase.php');
require_once('cStock.php');
require_once('cValidatedForm.php');
//require_once('cStatement2.php');


//---- This class creates the main view ----//
require_once( 'cSimpleList.php' );

class PartnersList extends SimpleList {
		
	protected function actions()
	{
		$this->addAction("ADD PARTNER","Add Partner",SL_NOSELECTION);
		$this->addAction("EDIT PARTNER","Edit",SL_SINGLESELECTION);
		$this->addAction("REMOVE PARTNER","Delete",SL_MULTIPLESELECTION);
		$this->addAction("MANAGE STATEMENTS","Manage Statements",SL_SINGLESELECTION);
//		$this->addAction("ADD PHYSICAL SALES","Add Physical Sales",SL_SINGLESELECTION);
		$this->addAction("STOCK LEVELS","View Stock Levels",SL_SINGLESELECTION);

	}
	
	protected function populate()
	{
		$this->_header = array('COMPANY','CATEGORY');
		$rows = Database::QueryGetResults('SELECT uid,name,type FROM partners;');
		foreach($rows as $row)
		{
			$typelist = "";
			if ($row['type']!='')
			{
				for($i=0;$i<strlen($row['type']);++$i)
				{
					$type = $row['type'][$i];
					if ($typelist!="") $typelist.=" & ";
					if ($type=='D') $typelist .="Distributor";
					if ($type=='M') $typelist .="Manufacturer";
					if ($type=='L') $typelist .="Licensor";
					if ($type=='S') $typelist .="Shop";
					if ($type=='C') $typelist .="Licensee";
				}
			}
			$this->_rows[] = array('uid'=>$row['uid'],'name'=>$row['name'],'type'=>$typelist);
		
			// supply chain type
			
		}
		return true;
	}
	
	protected function containerpage()
	{
		return "partners";
	}

	public function PartnersList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}
}



Database::Init();

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);


$aspect = new Aspect;
$aspect->Start("partners","VIEW");

while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$productlist = new PartnersList('distributorid');
			$productlist->Present();
			$aspect->Present();
			break;
			
			$list = new TableList;
			$list->Begin("distributorid","partners");
			$list->AddView("all","partners","%name%","","name");
			$list->AddView("distributors","partners","%name%","type","name","Distributors","type LIKE '%D%'");
			$list->AddView("licensees","partners","%name%","type","name","Licensees","type LIKE '%C%'");
			$list->AddView("licensors","partners","%name%","type","name","Licensors","type LIKE '%L%'");
			$list->AddView("manufacturers","partners","%name%","type","name","Manufacturers","type LIKE '%M%'");
			$list->AddView("shops","partners","%name%","type","name","Shops","type LIKE '%S%'");
			$list->SetInfoBoxHandler("getdistributorinfo");
			$list->AddAction("ADD PARTNER",false);
			$list->AddAction("EDIT PARTNER",true);
			$list->AddAction("REMOVE PARTNER",true);
			$list->SetConfirm("REMOVE PARTNER","Are you sure?");
			$list->AddAction("MANAGE STATEMENTS",true);
			$list->AddAction("ADD PHYSICAL SALES",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_PARTNERS_REGISTERSALES);
//			$list->AddAction("ADD DOWNLOAD SALES",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_PARTNERS_REGISTERSALES);
			$list->AddAction("STOCK LEVELS",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_PARTNERS_STOCKLEVELS);
			$aspect->Attach($list);
			$aspect->Present();
			break;
       }	
	   case "REMOVE PARTNER":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			foreach (getVar('distributorid') as $did)
				DataTable::Delete("partners",$did);
			break;
		}
		case "STOCK LEVELS":
		{
			$stock = Stock::GetLevels(getVarS('distributorid'));
			$result = Database::Query("SELECT name FROM partners WHERE uid='".getVarS('distributorid')."';");
			$row = $result->GetNext();
			
			HeaderBar($row['name']." Stock Levels");
			print '<table class=spreadsheet>';
			print "<tr><th>PRODUCT</th><th>UNITS</th></tr>";
			foreach ($stock as $key => $value)
			{
				print "<tr><td>".$key."</td><td>".$stock[$key]." units</td></tr>";
			}
			print '</table>';
			$aspect->BackButton();
			$aspect->Present();
			break;
		}
		case "EDIT PARTNER":
		{
			HeaderBar("Edit Partner");
			$form = new ValidatedForm("partners","name,address1,address2,city,postcode,country,phone,email,type");
			$form->SetValuesFromDB("partners",getVarS('distributorid'));
			$form->AddCancel();
			$form->AddAction("SUBMIT",true,"","","SUBMIT EDIT");	// validate before sending
			$form->AddHidden('distributorid',getVarS('distributorid'));
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

      		// convert to string
     		 $_POST['type'] = implode("",$_POST['type']);
 			DataTable::UpdateFromPost("partners",getVarS('distributorid'),"name,address1,address2,city,postcode,country,phone,email,type",$aspect);
			break;
		}
		case "ADD PARTNER":
		{
			HeaderBar("Add New Partner");
			$form = new ValidatedForm("partners","name,address1,address2,city,postcode,country,phone,email,type");
			$form->AddCancel();
			$form->AddAction("SUBMIT",true,"","","SUBMIT NEW");	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

      		// convert to string
 		     $_POST['type'] = implode("",$_POST['type']);
			DataTable::InsertFromPost("partners","name,address1,address2,city,postcode,country,phone,email,type",$aspect);
			break;
		}
		case "ADD PHYSICAL SALES":
		{
			$result = Database::Query("SELECT type FROM partners WHERE name='".getVarS("distributorid")."';");
			$distributor = $result->GetNext();
			$product = Stock::GetLevels($_POST['distributorid']);
			if (empty($product))
			{
				print "This distributor/retailer has no stock registered at it";
				$aspect->BackButton();
			}
			else
			{
				$form = new ValidatedForm;
//				$form->AddHidden("distributorid",$_POST['distributorid']);
				$form->AddFixed("Distributor","distributorid","partners",getVarS('distributorid'));
				
				$form->AddField("Date","date","date",false);

				$option = "select:=,";
				foreach ($product as $key => $value)
				{
					$option = $option.$key."='".$key." (".$product[$key]." in stock)',"; 
				}
				$form->StartGroup(0,20,true);
					$form->AddField("Product","product",$option,"");
					$form->AddField_FromTable("sale","quantity");
                    $form->AddField_FromTable("sale","baseprice");
					$form->AddField_FromTable("sale","value");
				$form->EndGroup();
				$form->AddAction("SUBMIT SALES",true);
				$form->AddCancel();
				$aspect->Attach($form);
			}
			Database::FinishQuery($result);
			$aspect->Present();
			break;
		}
		case "SUBMIT SALES":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$date = getpostdate();
			for ($i=0;$i<20;++$i)
			{
				$product = ValidatedForm::GetGroupFieldFromPost("product",$i);
				if ($product==false) break;
				$quantity = ValidatedForm::GetGroupFieldFromPost("quantity",$i);
				$value = ValidatedForm::GetGroupFieldFromPost("value",$i);
				DataTable::Insert("sale",		// table
								  "product,distributor,quantity,value,date,saletype",		// fields
								  $product,$_POST["distributorid"],$quantity,$value,$date,'P' // values
								 );	
			}
			break;
		}
		case "MANAGE STATEMENTS":
		{
			$aspect->SetNextAction("VIEW");
			$result = Database::QueryGetResult("SELECT * FROM deals WHERE licensee='".getVarS('distributorid')."';");
			if ($result===false)
			{
				popupError(STRING_ERROR_NOT_LICENSEE); break;
			}
			$startdate=""; $enddate="";
			$statements = Database::QueryGetResults("SELECT * FROM artiststatements WHERE artist='".getVarS('distributorid')."' AND type='L' ORDER BY startdate;");
			if (!empty($statements))
			{
				print "<form method=post action=index.php><input type=hidden name=page value=partners>";
				print "<input type=hidden name=distributorid value='".$_POST['distributorid']."'>";

				print "<table width=100%><tr><td><b>Period</b></td><td><b>Amount Paid</b></td><td><b>Balance</b></td><td></td></tr>\n";
				foreach ($statements as $statement)
				{
//					print_r($statement);
					print "<tr><td>".$statement['startdate']." - ".$statement['enddate']."</td>\n";
					print "<td>".Currency::DATABASETODISPLAY($statement['amount'])."</td>\n";
					print "<td>".Currency::DATABASETODISPLAY($statement['reserve'])."</td>\n";
					print "<td><button type=submit name=submitvalue class=small value='REMOVESTATEMENT:".$statement['pdfhash']."'>REMOVE</button></td>\n";
					$outfilename = "statement_".$statement['pdfhash'].".pdf";
					print "<td><a href='download.php?file=".$outfilename."'>pdf link</a></td>\n";
					print "</tr>";
					
					$nextdate = strtotime($statement['enddate']);
					$startdateraw = mktime(0, 0, 0, date("m",$nextdate),  date("d",$nextdate)+1,  date("Y",$nextdate));
					$startdate  = date("Y-m-d",$startdateraw);
					$enddate  = date("Y-m-d",mktime(0, 0, 0, date("m",$startdateraw)+6,  date("d",$startdateraw),  date("Y",$startdateraw))-1);

				}
				print "</table><br>\n";
				print "</form>\n";	
			}
			
 			print "<b>Generate New Statement</b><br>";
			$form = new ValidatedForm("artiststatements","licensee,startdate,enddate","licensee");
			$form->SetValue("type","L");
			$form->SetValue("startdate",$startdate);
			$form->SetValue("enddate",$enddate);
			$form->SetValue("licensee",$_POST['distributorid']);
			$form->AddAction("CALCULATE STATEMENT",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CALCULATE STATEMENT":
		{
			$_POST['distributorid'] = $_POST['licensee'];
			$startdate = getVar('startdate');
			$enddate = getVar('enddate');
			$statement = new Statement2;
			$statement->Generate(1,getVarS("distributorid"),$startdate,$enddate);
			$statement->Display();

			$pdfhash = Statement2::MakeStatementHash(getVarS("distributorid"),"",$startdate,$enddate);
			$outfilename = Statement2::MakeStatementFilename2($pdfhash);
			if ($statement->IsComplete())
			{
				// Store this for later			
				$_SESSION['statement'] = $statement->GetTotals();
				$statement->SaveAsPDF($outfilename);
//				print "saving $outfilename<br>";
			}
			
			$form = new Form;
			$form->Begin("partners");
			$form->AddHidden("distributorid",getVarS("distributorid"));
			$form->AddHidden("startdate",$startdate);
			$form->AddHidden("enddate",$enddate);
			$form->AddHidden("pdfhash",$pdfhash);
			if ($statement->IsComplete())
			{
				$form->AddHidden("filename",$outfilename);
				$form->AddAction("SAVE STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
			}
			else
				print "<p>This statement is not complete and so can not be saved. It may have no data in it, or require entry of balance information</p>";
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SAVE STATEMENT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// Get session
			foreach ($_SESSION['statement'] as $result)
			{
//				print_r($result);
				$query = "INSERT INTO artiststatements SET artist='".getVarS('distributorid')."',type='L',crossdealid='".$result['id']."',amount='".$result['amountdue']."',reserve='".$result['carryover']."',startdate='".$_POST['startdate']."',enddate='".$_POST['enddate']."',pdfhash='".$_POST['pdfhash']."';";
//				print $query."<br>";
				Database::Query($query);
			}
			// don't need now
			unset($_SESSION['statement']);			
			$aspect->SetNextAction("MANAGE STATEMENTS");
			break;
		}
		case "REMOVESTATEMENT":
		{
			$pdfhash = $aspect->GetActionData();
			$query = "DELETE FROM artiststatements WHERE artist='".getVarS('distributorid')."' AND pdfhash='".$pdfhash."';";

			$aspect->SetNextAction("MANAGE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			Database::Query($query);
			unlink(Statement2::MakeStatementFilename2($pdfhash));
			break;
		}
	   default:
	   {
	   		$aspect->DefaultAction();
	   }
	}
	
}	

$aspect->End();

?>

