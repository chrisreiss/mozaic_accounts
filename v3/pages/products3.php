<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once('cValidatedForm.php');
require_once('cDeal.php');
require_once('cProduct.php');
require_once('cWorkSheet.php');
require_once('cFile.php');
require_once('cMetadataExporters.php');

function CompressTracks()
{
	$tracklist = "";
	for($i=1;$i<=20;++$i)
	{
		$track = "id".$i;
		if ($_POST[$track]!="0")
		{
			if ($tracklist!="") 
				$tracklist = $tracklist.",";
			
			$tracklist = $tracklist.$_POST[$track];
		}
	}
	return $tracklist;
}

Database::Init();

$aspect = new Aspect;
$aspect->Start("products","VIEW");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$list = new TableList;
			$list->Begin("productid","products");
			$list->AddView("product ordered by cat no","product","%uid% (%artist.bands:name% - %title%)","","uid");
			$list->AddView("product ordered by release date","product","%uid% (%artist.bands:name% - %title%)","","releasedate");
			$list->ReverseDirection(true);
			$list->AddView("product grouped by band","product","%title% (%uid%)","artist","title","%artist.bands:name%");
			$list->AddView("product grouped by media","product","%uid% (%artist.bands:name% - %title%)","format","releasedate","%format%");
			$list->AddAction("ADD PRODUCT",false);
			$list->AddAction("REMOVE PRODUCT",true);
			$list->SetConfirm("REMOVE PRODUCT","This removes the product completely from the system including all sales and stock movement history. Are you sure?");
			$list->SetInfoBoxHandler("getproductinfo");
			$list->AddAction("EDIT PRODUCT",true);
			$list->AddAction("ADD PRODUCT FORMAT",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_ADDPRODUCTFORMAT);
			$list->AddAction("ADD STOCK MOVEMENT",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_ADDSTOCKMOVEMENT);
			$list->AddAction("STOCK MOVEMENT HISTORY",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_STOCKMOVEMENTHISTORY);
			$list->AddAction("PRODUCT PROFIT ANALYSIS",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_PRODUCTPROFITANALYSIS);
			$list->AddAction("ADD SALE",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_ADDSALE);
			$list->AddAction("ADD LICENCE",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_ADDLICENCE);
			$list->AddAction("ADD EXPENSE",true, ACTION_TOOLTIP, STRING_TOOLTIP_BUTTON_PRODUCT_ADDEXPENSE);
			$list->AddAction("EXPORT METADATA",true, ACTION_TOOLTIP, "");
			$list->AddAction("EXPORT BEATPORT XML",true, ACTION_TOOLTIP, "");
			if ($_SESSION['userid']=='tom')
			{
				$list->AddAction("AUTO CREATE DEAL",true);
			}

			$aspect->Attach($list);
			$aspect->Present();
			break;
		}
		case "AUTO CREATE DEAL":
		{
			$aspect->SetNextAction("VIEW");
			print "This will automatically create deals using the default deal settings for the selected product. Please review the results before commiting.<br>";
			Deal::CreateDealsFromProduct($aspect->GetVar('productid'),false);
			
			$form = new Form();
			$form->AddAction("COMMIT AUTO DEAL");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();				
			break;
		}
		case "COMMIT AUTO DEAL":
		{
			$aspect->SetNextAction("VIEW");
			if (!empty($_SESSION['queries']))
			{
				foreach ($_SESSION['queries'] as $query)
				{
					Database::Query($query);
				}
			}
			else
			{
				$aspect->Error("No pending database transactions found");
			}
			unset ($_SESSION['queries']);
			break;
		}

		case "EXPORT BEATPORT XML":
		{
			$aspect->SetNextAction("VIEW");
			$exporter = new BeatportExport();
			$xml = $exporter->Process($aspect->GetVar('productid'));
			$result = str_ireplace('<','&lt;',$xml);
			$result = str_ireplace('>','&gt;',$result);
			$result = str_ireplace("\n","<br>",$result);
			$result = str_ireplace(" ","&nbsp;",$result);
			
			print "<table class=spreadsheet width=100%>\n";
			print '<tr><td>';
			print $result;
			print '</td></tr>';
			print '</table>';
			$filename = File::GetPath($aspect->GetVar('productid')."_Track_Info.xml");
			$fd = fopen($filename,"wb");
			fwrite($fd,$xml,strlen($xml));
			fclose($fd);
			print "<a href='download.php?file=".$aspect->GetVar('productid')."_Track_Info.xml"."'>download link</a>";
			$form = new Form();
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "EXPORT METADATA":
		{
			// label, catno, epalbumartist,albumeptitle,upc,releasedate,trackartist,tracktitle,version,remixer,position,format,irsc,trackduration,genre,subgenre,publisher
			$aspect->SetNextAction("VIEW");
			
			// releasedate, artist, trackname, publisher, tracklength, irsc, upc
			$product = Database::QueryGetResult("SELECT * FROM product WHERE uid='".$aspect->GetVar('productid')."';");
			$tracksproduct = Database::QueryGetResults("SELECT * FROM tracksproduct WHERE product='".$aspect->GetVar('productid')."' ORDER BY position;");	
			
			$worksheet = new Worksheet();
			$worksheet->AddHeaderRow("catno","productartist","producttitle","upc","releasedate","trackartist","tracktitle","position","irsc","trackduration","publisher");
			foreach($tracksproduct as $trackid)
			{
				$trackdata = Database::QueryGetResult("SELECT * FROM tracks WHERE uid='".$trackid['track']."'");
				$worksheet->AddRow($product['uid'],TableInfo::DisplayFromType('band',$product['artist']),$product['title'],$product['barcode'],$product['releasedate'],
									TableInfo::DisplayFromType('band',$trackdata['artist']),$trackdata['title'],$trackid['position']+1,$trackdata['uid'],$trackdata['tracklength'],$trackdata['publisher']);
			}
			$worksheet->DisplayAsTable();
			$filename = File::GetPath("catexporttemp.csv");
			$worksheet->SaveAsCSV($filename);
			print "<a href='download.php?file=catexporttemp.csv'>csv link</a>";
			$form = new Form();
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			
			break;
		}
		case "ADD PRODUCT FORMAT":
		{
			print "<b>Create a new format for the product ".$aspect->GetVar('productid').":</b><br>";
			$form = new ValidatedForm("product","uid,barcode,format");
			$form->AddField("Replicate deals","dealcopy","checkbox",true);
			$form->SetValue("dealcopy",true);
			$form->SetValue("uid",$aspect->GetVar('productid'));
			$form->AddHidden("olduid",$aspect->GetVar('productid'));
			$form->AddAction("SUBMIT NEW FORMAT",true);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW FORMAT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			if (DataTable::GetEntryFromID("product",$aspect->GetVar('uid'))!=false)
			{
				$aspect->Error("Duplicate Catalogue Number - Not Permitted");
				$aspect->SetNextAction("ADD PRODUCT FORMAT");
				$aspect->SetVar('productid',$aspect->GetVar('olduid'));
				break;
			}
			
			$samebarcode = Database::QueryGetResult("SELECT uid FROM product WHERE barcode='".$aspect->GetVar('barcode')."';");
			if (!empty($samebarcode))
			{
				$aspect->Error("Duplicate Barcode Number as Product '".$samebarcode['uid']."' - Not Permitted");
				$aspect->SetNextAction("ADD PRODUCT FORMAT");
				$aspect->SetVar('productid',$aspect->GetVar('olduid'));
				break;
			}

			$copyproduct = Database::QueryGetResult("SELECT * FROM product WHERE uid='".$aspect->GetVar('olduid')."';");
			$copyproduct['uid'] = $aspect->GetVar('uid');
			$copyproduct['barcode'] = $aspect->GetVar('barcode');
			$copyproduct['format'] = $aspect->GetVar('format');
			if (isset($copyproduct['accountid'])) unset($copyproduct['accountid']);
			
			DataTable::InsertFromArray("product",$copyproduct);
			// Also need to copy the tracks
			$tracks = Database::QueryGetResults("SELECT track,position FROM tracksproduct WHERE product='".$aspect->GetVar('olduid')."';");
			foreach($tracks as $track)
			{
				$track['product'] = $aspect->GetVar('uid');
				DataTable::InsertFromArray("tracksproduct",$track,true);
			}
						
			if ($aspect->GetVar('dealcopy'))
			{
				$deals = Database::QueryGetResults("SELECT * FROM deals WHERE product='".$aspect->GetVar('olduid')."';" );
				if (!empty($deals))
				{
					foreach($deals as $deal)
					{
						unset($deal['uid']);
						unset($deal['advance']);		// don't duplicate advances
						unset($deal['advancepaiddate']);
						$deal['product'] = $aspect->GetVar('uid');
						DataTable::InsertFromArray("deals",$deal,true);
					}
				}
				else
				{
					$aspect->Error("Warning: No deals were found for product ".$aspect->GetVar('olduid').", so none copied");
				}
			}
			break;
		}
		case "PRODUCT PROFIT ANALYSIS":
		{
			$productid = $aspect->GetVar('productid');
			// Add in products
			$result = Database::Query("SELECT * FROM `sale` WHERE product = '".$productid."' ORDER BY date;");
			for($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				$description = "Sale : ".TableInfo::Display("sale","distributor",$row['distributor']).", ".$row['quantity']." units";
				$items[] = array('date'=>$row['date'],'amount'=>Currency::GETRAWVALUE($row['value']),'description'=>$description);
			}
			Database::FinishQuery($result);
			// Add in expenses
			$result = Database::Query("SELECT * FROM expenses WHERE product = '".$productid."' ORDER BY invoicedate;");
			for($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				$description = "Expense : ".$row['description'];
				$items[] = array('date'=>$row['invoicedate'],'amount'=>(0-Currency::GETRAWVALUE($row['amount'])),'description'=>$description);
     		}
			Database::FinishQuery($result);
			// Add in licence advances
			$result = Database::Query("SELECT advancepaiddate,description,advance FROM licences WHERE licencetype='P' AND licenceduid = '".$productid."' UNION ALL SELECT advancepaiddate,description,advance FROM licences WHERE licencetype='T' AND licenceduid IN (SELECT track FROM tracksproduct WHERE product='".$productid."') ORDER BY advancepaiddate;");
			for($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				if ($row['advancepaiddate']!="0000-00-00")
				{	
					$description = "Advance Received : ".$row['description'];
					$items[] = array('date'=>$row['advancepaiddate'],'amount'=>Currency::GETRAWVALUE($row['advance']),'description'=>$description);
				}
				$result2 = Database::Query("SELECT * FROM licencestatements WHERE licenceuid = '".$row['uid']."' ORDER BY paiddate;");
				for($j=0;$j<$result2->GetNum();++$j)
				{
					$row2 = $result2->GetNext();
					if ($row2['paiddate']!="0000-00-00")
					{	
						$description = "Royalties Received : ".$row['description'];
						$items[] = array('date'=>$row2['paiddate'],'amount'=>Currency::GETRAWVALUE($row2['amount']),'description'=>$description);
					}
				}
				Database::FinishQuery($result2);	
			}
			Database::FinishQuery($result);

     		$result = Database::Query("SELECT band,advancepaiddate, advance FROM deals WHERE product='".$productid."';");
			for($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				if ($advance!="")
				{	
					$description = "Advance Paid to ".TableInfo::DisplayFromType("band",$row['band']);
					$items[] = array('date'=>$row['advancepaiddate'],'amount'=>(0-Currency::GETRAWVALUE($row['advance'])),'description'=>$description);
				}
			}
   			Database::FinishQuery($result);

			print "<form name=product method=post action='index.php'><input type=hidden name=page value=products>";
			print "<input type=hidden name=action value='PRODUCT PROFIT ANALYSIS'>";
			print "Cost analysis for ";
			EditField("productid","product",$_POST['productid'],"onchange='this.form.submit();'");
			print "</form>\n";

			$form = new Form;
			$form->Begin("products");

			if (isset($items))
			{
				sort($items);
			
				print "<table class=spreadsheet border=1 width=100%><tr><td><b>Date</b></td><td><b>Description</b></td><td><b>Value</b></td><td><b>Profit/Loss</b></td></tr>\n";
				$total = 0;
				foreach ($items as $item)
				{
					print "<tr>";
					print "<td>".displaydate($item['date'])."</td>";
					print "<td>".$item['description']."</td>";
					print "<td>".Currency::DISPLAY($item['amount'])."</td>";
					$total += $item['amount'];
					print "<td>".Currency::DISPLAY($total)."</td>";
					print "</tr>\n";
				}
				print "<tr><td></td><td></td><td></td><td><b>".Currency::DISPLAY($total)."</b></td></tr>\n";
				print "</table>\n";
			}
			else
			{
				print "<p>No data available for this product on the system</p>\n";
			}
			$form->AddAction("DONE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE PRODUCT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::Delete("product",$aspect->GetVar('productid'));
			Database::Query("DELETE FROM tracksproduct WHERE product='".$aspect->GetVar('productid')."';");
			Database::Query("DELETE FROM stock WHERE product='".$aspect->GetVar('productid')."';");
			Database::Query("DELETE FROM sale WHERE product='".$aspect->GetVar('productid')."';");
			Database::Query("DELETE FROM expenses WHERE product='".$aspect->GetVar('productid')."';");
			Database::Query("DELETE FROM deals WHERE product='".$aspect->GetVar('productid')."';");
			break;
		}
		case "ADD PRODUCT":
		case "EDIT PRODUCT":
		{
			$form = new ValidatedForm("product","uid,artist,title,barcode,format,releasedate,comments");

			$form->AddField('Disc 1 Subtitle',"subtitle1","text48",true);
      		$form->AddField("Disc 1 Tracks","tracks1","tracks",true);
		
			if(isset($_POST['refreshedpost']))
			{
				$numdisc = Product::GetNumDiscsFromFormat($aspect->GetVar('format'));
			}
			else
			{
				$res = Database::QueryGetResult("SELECT format FROM product WHERE uid='".$aspect->GetVar('productid')."' LIMIT 1;");
				$numdisc = Product::GetNumDiscsFromFormat($res['format']);
				
			}
			
			if ($numdisc>1)
			{
				$form->AddField("Disc 2 Subtitle","subtitle2","text48",true);
      			$form->AddField("Disc 2 Tracks","tracks2","tracks",true);
      		}
      		
      		if ($numdisc>2)
      		{
				$form->AddField("Disc 3 Subtitle","subtitle3","text48",true);
      			$form->AddField("Disc 3 Tracks","tracks3","tracks",true);
			}
			
			if ($numdisc>3)
			{
				$form->AddField("Disc 4 Subtitle","subtitle4","text48",true);
      			$form->AddField("Disc 4 Tracks","tracks4","tracks",true);
			}

			if (!isset($_POST['refreshedpost']) && isset($_POST['productid'])) 
			{
				$form->SetValuesFromDB("product",$aspect->GetVar('productid'));
				$result = Database::Query("SELECT track,position FROM tracksproduct WHERE product='".$aspect->GetVar('productid')."' ORDER BY position;");
	      		for ($i=0;$i<$result->GetNum();++$i)
				{
					$row = $result->GetNext();
					if ($row['position']<100)
					  $tracks1[] = $row['track'];
					else if ($row['position']>=100 && $row['position']<200)
					  $tracks2[] = $row['track'];
					else if ($row['position']>=200 && $row['position']<300)
					  $tracks3[] = $row['track'];
					else if ($row['position']>=300)
					  $tracks4[] = $row['track'];
				}
			}
			else
			{
				$tracks1 = $aspect->GetMultipleVar("tracks1*");
				$tracks2 = $aspect->GetMultipleVar("tracks2*");
				$tracks3 = $aspect->GetMultipleVar("tracks3*");
				$tracks4 = $aspect->GetMultipleVar("tracks4*");
			}
      		
      		if (!empty($tracks1)) $form->SetValue("tracks1",implode(",",$tracks1));
			if (!empty($tracks2)) $form->SetValue("tracks2",implode(",",$tracks2));
			if (!empty($tracks3)) $form->SetValue("tracks3",implode(",",$tracks3));
			if (!empty($tracks4)) $form->SetValue("tracks4",implode(",",$tracks4));

			
			$form->AddAction("SUBMIT EDIT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$form->SetFilter("artist","tracks");
			$form->SetFilter("format","*");
			$aspect->Attach($form);
			if (isset($_POST['productid'])) $aspect->PreserveVar('productid');
			$aspect->Present();
			break;
		}
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			if (!isset($_POST['productid']))
			{
				if (!DataTable::GetIDExist("product",$aspect->GetVar('uid')))
				{
					DataTable::InsertFromPost("product","uid,artist,title,subtitle1,subtitle2,subtitle3,subtitle4,barcode,comments,format,releasedate",$aspect);
					$_POST['productid'] = $_POST['uid'];
				}
				else
				{
					Error("That catologue number is already in use. Operation aborted","products");
						$aspect->Present();
						break;
				}
			
			}
			else
			{
				
				if ($aspect->GetVar('productid')!=$aspect->GetVar('uid'))
				{
					if (DataTable::GetIDExist("product",$aspect->GetVar('uid')))
					{
						Error("That catologue number is already in use. Operation aborted","products");
						$aspect->Present();
						break;
					}
					Database::Query("UPDATE sale SET product='".$aspect->GetVar("uid")."' WHERE product='".$aspect->GetVar("productid")."';");
					Database::Query("UPDATE expenses SET product='".$aspect->GetVar("uid")."' WHERE product='".$aspect->GetVar("productid")."';");
					Database::Query("UPDATE licences SET licenceduid='".$aspect->GetVar("uid")."' WHERE licencetype='P' AND licenceduid='".$aspect->GetVar("productid")."';");
				}
			}

			DataTable::UpdateFromPost("product",$aspect->GetVar('productid'),"uid,artist,title,subtitle1,subtitle2,subtitle3,subtitle4,barcode,comments,format,releasedate",$aspect);
			// Remove from tracksproduct and then reenter.
			Database::Query("DELETE FROM tracksproduct WHERE product='".$aspect->GetVar('uid')."';");
			
		//	die('hi');
	//	print_r($_POST);
      		$tracks = $aspect->GetMultipleVar('tracks1*');
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i);
			}
      		$tracks = $aspect->GetMultipleVar('tracks2*');
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i+100);
			}
      		$tracks = $aspect->GetMultipleVar('tracks3*');
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i+200);
			}
      		$tracks = $aspect->GetMultipleVar('tracks4*');
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i+300);
			}
	
			break;			
		}
		case "ADD PRODUCT":
		{
			$uidresult = Database::QueryGetResult("SELECT uid FROM product ORDER BY uid DESC LIMIT 1;");
			$uid = CreateNewCatNumber($uidresult['uid']);
			$form = new ValidatedForm("product","uid,artist,title,barcode,format,releasedate,comments");
//			$form->AddField("Use Standard Deal","createdeal","checkbox",true,"Automatically assigns a default deal for the product to the artist(s)");
//			$form->AddField("Deal","createdeal","createdeal",true,"For your convience you can create a new deal for this product now");
//			$form->SetValue("createdeal",0);

			$numdisc = Product::GetNumDiscsFromFormat($aspect->GetVar('format'));
			$form->AddField('Disc 1 Subtitle',"subtitle1","text48",true);
      		$form->AddField("Disc 1 Tracks","tracks1","tracks",true);
     		if (!empty($tracks1)) $form->SetValue("tracks1",implode(",",$tracks1));

			if ($numdisc>1)
			{
				$form->AddField("Disc 2 Subtitle","subtitle2","text48",true);
      			$form->AddField("Disc 2 Tracks","tracks2","tracks",true);
				if (!empty($tracks2)) $form->SetValue("tracks2",implode(",",$tracks2));
      		}
      		
      		if ($numdisc>2)
      		{
				$form->AddField("Disc 3 Subtitle","subtitle3","text48",true);
      			$form->AddField("Disc 3 Tracks","tracks3","tracks",true);
				if (!empty($tracks3)) $form->SetValue("tracks3",implode(",",$tracks3));
			}
			
			if ($numdisc>3)
			{
				$form->AddField("Disc 4 Subtitle","subtitle4","text48",true);
      			$form->AddField("Disc 4 Tracks","tracks4","tracks",true);
				if (!empty($tracks4)) $form->SetValue("tracks4",implode(",",$tracks4));
			}

			$form->SetValue("uid",$uid);
			$form->AddAction("SUBMIT NEW",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$form->SetFilter("artist","tracks");
			$form->SetFilter("format","*");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("product","uid,artist,title,subtitle1,subtitle2,subtitle3,subtitle4,barcode,comments,format,releasedate",$aspect);
			// for the moment lets keep the tracks duplicated
      $tracks = explode(",",$aspect->GetVar('tracks1'));
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i);
			}
      $tracks = explode(",",$aspect->GetVar('tracks2'));
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i+100);
			}
      $tracks = explode(",",$aspect->GetVar('tracks3'));
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i+200);
			}
      $tracks = explode(",",$aspect->GetVar('tracks4'));
			foreach ($tracks as $i=>$track)
			{
				if ($track!="") DataTable::Insert("tracksproduct","product,track,position",$aspect->GetVar('uid'),$track,$i+300);
			}
			break;
		}

		case "ADD EXPENSE":
		{
			$form = new ValidatedForm("expenses","product,invoicedate,description,invoice,amount,paiddate,type,comments");
			$form->SetValue("product",$aspect->GetVar('productid'));
			$form->AddAction("SUBMIT EXPENSE",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT EXPENSE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("expenses","product,invoicedate,paiddate,type,description,amount,comments",$aspect);
			break;
		}
		case "ADD SALE":
		{
			$form = new ValidatedForm("sale","product,distributor,quantity,value,date,saletype","product");
			$form->SetValue("product",$aspect->GetVar('productid'));
			$form->AddAction("SUBMIT SALE",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT SALE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("sale","product,distributor,quantity,value,date,saletype",$aspect);
			break;
		}
		case "ADD STOCK MOVEMENT":
		{
			$form = new ValidatedForm("stock","product,source,destination,quantity,date,description","product");
			$form->SetValue("product",$aspect->GetVar('productid'));
			$form->AddAction("SUBMIT STOCK MOVEMENT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT STOCK MOVEMENT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("stock","product,source,destination,quantity,date,description",$aspect);
			break;
		}
		case "ADD LICENCE":
		{
			$form = new ValidatedForm("licences","licenceduid,licencetype,licensor,description,advance,royaltyrate,packagingdeductions,reserveforreturns,datestatement,royaltyinterval","licenceduid,licencetype");
			$productid = $aspect->GetVar('productid');
			$form->SetValue("licencetype","P");
			$form->SetValue('licenceduid',$productid);
			$form->AddHidden('licencetype','P');
			$form->AddAction("SUBMIT LICENCE",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT LICENCE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::InsertFromPost("licences","licenceduid,licencetype,licensor,licencetype,description,advance,royaltyrate,packagingdeductions,reserveforreturns,datestatement,royaltyinterval",$aspect);
			break;
		}
		case "EDITSTOCK":
		{
			$form = new ValidatedForm("stock","product,source,destination,quantity,date,description","product");
			$form->SetValuesFromDB("stock",$aspect->GetActionData());
			$form->AddHidden("uid",$aspect->GetActionData());
			$form->AddAction("MODIFY STOCK MOVEMENT",true);	// validate before sending
			$form->AddAction("DELETE STOCK MOVEMENT",false);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->PreserveVar('productid');
			$aspect->Present();
			break;
		}
		case "MODIFY STOCK MOVEMENT":
		{
			$aspect->SetNextAction("STOCK MOVEMENT");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::UpdateFromPost("stock",$aspect->GetVar('uid'),"product,source,destination,quantity,date,description",$aspect);
			break;
		}
		case "DELETE STOCK MOVEMENT":
		{
			$aspect->SetNextAction("STOCK MOVEMENT");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::Delete("stock",$aspect->GetVar('uid'));
			break;
		}
		case "EDITSALE":
		{
			$form = new ValidatedForm("sale","product,track,distributor,quantity,value,date,saletype");
			$form->SetValuesFromDB("sale",$aspect->GetActionData());
			$form->AddHidden("uid",$aspect->GetActionData());
			$form->AddAction("MODIFY SALE",true);	// validate before sending
			$form->AddAction("DELETE SALE",true);	// validate before sending
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->PreserveVar('productid');
			$aspect->Present();
			break;
		}
		case "MODIFY SALE":
		{
			$aspect->SetNextAction("STOCK MOVEMENT");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::UpdateFromPost("sale",$aspect->GetVar('uid'),"product,track,distributor,quantity,value,date,saletype",$aspect);
			break;
		}
		case "DELETE SALE":
		{
			$aspect->SetNextAction("STOCK MOVEMENT");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::Delete("sale",$aspect->GetVar('uid'));
			break;
		}
		case "STOCK MOVEMENT HISTORY":
		{
			unset($date);
			print "<form name=product method=post action='index.php'><input type=hidden name=page value=products>";
			print "<input type=hidden name=action value='STOCK MOVEMENT HISTORY'>";
			print "Stock movements for ";//<b>".$_POST['productid']."</b></p>";
			EditField("productid","product",$_POST['productid'],"onchange='this.parentNode.submit();'");
			print "</form>\n";
			
			$j = 0;
			$query = "SELECT * FROM stock WHERE product='".$_POST['productid']."' ORDER BY date;";
			$result = Database::Query($query);
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				$salestock[] = array('source'=>$row['source'],'date'=>$row['date'],'distributor'=>$row['destination'],
									 'quantity'=>$row['quantity'],'value'=>$row['netamount'],'description'=>$row['description'],
									 'uid'=>$row['uid']);
				$distlist[$row['destination']] = 1;
				$distlist[$row['source']] = 1;
			}
			Database::FinishQuery($result);
			
			$query = "SELECT * FROM sale WHERE product='".$_POST['productid']."' AND saletype='P' ORDER BY distributor,date;";
			$result = Database::Query($query);
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				$salestock[] = array('source'=>"",'date'=>$row['date'],'distributor'=>$row['distributor'],
									 'quantity'=>$row['quantity'],'value'=>$row['value'],'description'=>"Sales",
									 'uid'=>$row['uid']);
				$distlist[$row['distributor']] = 1;
			}
			Database::FinishQuery($result);

			$form = new Form;
			$form->Begin("products");
			$form->AddHidden("productid",$_POST['productid']);

			$inserthtml = "";

			if (!empty($salestock))
			{
				foreach ($salestock as $key => $row) 
				{
					$distributor[$key]  = $row['distributor'];
					$date[$key] = $row['date'];
				}
			
				array_multisort($date, SORT_ASC, $salestock);
		
				// grab each distributor
				$totalsales = new Currency;
				$value = new Currency;
				$totalsold = 0;
				$totalstock = 0;
				$currentdistributor = "";
	
				$nameresult = Database::Query("SELECT uid,name,type FROM partners;");
				for ($i=0;$i<$nameresult->GetNum();++$i)
				{
					$row = $nameresult->GetNext();
					$distinfo[$row['uid']]['name'] = $row['name'];
					$distinfo[$row['uid']]['type'] = $row['type'];
				}
				Database::FinishQuery($nameresult);
							
							
				foreach ($distlist as $currentdistributor => $mycount)
				{
					if ($currentdistributor==0 || $distinfo[$currentdistributor]['type']=='V')
						continue;
						
					$stocklevel = 0;
					$inserthtml .= "<b>".$distinfo[$currentdistributor]['name']."</b><br>";
					$inserthtml .= "<table class=spreadsheet border=1 width=100%>\n";
					$inserthtml .= "<tr><td width=12%><b>Date</b></td><td width=12%><b>Quantity</b></td><td width=12%><b>Stock Level</b></td><td width=30%><b>Movement</b></td><td><b>Comments</b></td><td width=5%></td></tr>\n";
				
					foreach ($salestock as $key => $row)
					{
						if ($row['distributor']==$currentdistributor || $row['source']==$currentdistributor)
						{
							$description = $row['description'];
							if ($description=="Sales")
							{
								$color = "0000CC";
							}
							else if ($row['source']!=0 && $distinfo[$row['source']]['type']=="M")
							{
								$color = "008800";
								if ($row['source']==$currentdistributor)
									$totalstock += $row['quantity'];
							}
							else if ($row['source']==0)
							{
								$color = "000000";
								$totalstock += $row['quantity'];
							}
							else
							{
								$color = "000000";
							}
							
							if ($row['source']==$currentdistributor)
							{
								$stocklevel -= $row['quantity'];
								$movement = ">> ".$distinfo[$row['distributor']]['name'];
							}
							else
							{
								if ($row['description']=="Sales")
								{
									$stocklevel -= $row['quantity'];
									$value->SetFromDatabaseFormat($row['value']);
									$totalsales->Add($value);
									$totalsold += $row['quantity'];
									$movement = ">> Sold for ".($value->GetDisplayFormat());
	//								$description = "";
								}
								else
								{
									$stocklevel += $row['quantity'];
									if (empty($distinfo[$row['source']]['name']))
										$movement = "<i>Unknown</i>";
									else
										$movement = $distinfo[$row['source']]['name']." >>";
								}
							}
										
							$inserthtml .= "<tr>";
							$sc = "<span style='color:".$color.";'>";
							$ec = "</span>";
							
							// If manufactuer - display stock level as 0
//							print $row['source']."<br>";
							if ($distinfo[$currentdistributor]['type']=='M') 
							{
								$stocklevel=0;
							}
							
							$inserthtml .= "<td>".$sc.displaydate($row['date']).$ec."</td><td>".$sc.$row['quantity'].$ec."</td><td>".$sc.$stocklevel.$ec."</td><td>".$sc.$movement.$ec."</td><td>".$sc.$description.$ec."</td>";
							if ($row['description']=="Sales") 
							{
								$inserthtml .= "<td><button type=submit name=submitvalue class=small value='EDITSALE:".$row['uid']."'>EDIT</button></td>";
							}
							else
							{
								$inserthtml .= "<td><button type=submit name=submitvalue class=small value='EDITSTOCK:".$row['uid']."'>EDIT</button></td>";
							}
							$inserthtml .= "</tr>\n";
						}
					}
					$inserthtml .= "</table><br><br>\n";
				}
				
				$inserthtml .= "<table class=spreadsheet width=30%>";
				$inserthtml .= "<tr><td><b>Stock Manufactured</b></td><td>".$totalstock."</td></tr>";
				$inserthtml .= "<tr><td><b>Stock Sold Thru</b></td><td>".$totalsold."</td></tr>";
				$inserthtml .= "<tr><td><b>Stock Remaining</b></td><td>".($totalstock-$totalsold)."</td></tr>";
				$inserthtml .= "<tr><td><b>Value of Sales</b></td><td>".$totalsales->GetDisplayFormat()."</td></tr>";	
				$inserthtml .= "</table><br>";
			}
			else
			{
				$inserthtml .= "<p>No stock found on the system</p>";
			}
							
			$form->InsertHTML($inserthtml);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}	
}
$aspect->End();
?>	


