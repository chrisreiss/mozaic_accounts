<?php

require_once('util.php');
require_once('cForm.php');
require_once('cDataTable.php');
require_once('cList.php');
require_once('cAspect.php');
require_once('cDatabase.php');
require_once('cStatement.php');
require_once('cValidatedForm.php');
require_once('dbexport.php');
require_once('cMail.php');
require_once('cReports.php');
require_once('cCurrency.php');
require_once('strings.php');
require_once('cFile.php');

Database::Connect("mozaic");

if ($_SESSION['userid']!="tom")
	die("not authorised");


function cleanup($var){
	return Database::Quote($var);
}

//---- This class creates the main view ----//
require_once( 'cSimpleList.php' );

class AccountList extends SimpleList {
		
	protected function actions()
	{
		$this->addAction("ADD ACCOUNT","Add Account",SL_NOSELECTION);
		$this->addAction("SET ACTIVE","Set Active",SL_SINGLESELECTION);
		$this->addAction("EDIT ACCOUNT","Edit Account",SL_SINGLESELECTION);
//		$this->addAction("REMOVE ACCOUNT","Delete Band",SL_SINGLESELECTION);
		$this->addAction("MANAGE STATEMENTS","Account To",SL_SINGLESELECTION);
		$this->addAction("IMPORT ARTISTS","Import Artists",SL_NOSELECTION);
		$this->addAction("IMPORT CATALOGUE","Import Catalogue",SL_NOSELECTION);
		$this->addAction("IMPORT DEALS","Import Deals",SL_NOSELECTION);
		$this->addAction("IMPORT IDS","Import ID",SL_NOSELECTION);
	}
	
	protected function populate()
	{

		$this->_header = array('NAME','DATABASE');
		$accounts = Database::QueryGetResults('SELECT uid,name,`database` FROM accounts;');
		foreach($accounts as $account)
		{
			if ($account['uid']==$_SESSION['accountid'])
					$name=  $account['name']." (active)";
			else
				$name = $account['name'];

			$this->_rows[] = array('uid'=>$account['uid'],'name'=>$name,'database'=>$account['database']);
		}
		
		return true;
	}
	
	protected function containerpage()
	{
		return "admin";
	}

	public function AccountList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}
}



$aspect = new Aspect;
$aspect->Start("admin","MANAGE ACCOUNTS");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		case "MANAGE ACCOUNTS":
		{
						Database::Connect("mozaic");

			$accountlist = new AccountList('accountid');
			$accountlist->Present();		
			$aspect->Present();
			break;
		}
		
		case "ACTIVITY REFRESH":
		case "ACTIVITY":
		{
			$users = Database::QueryGetResults("SELECT displayname, lastlogin, lastactivity, lastlogout, accountid FROM mozaic.users ORDER BY lastlogin DESC;");
			print "<table width=100%>";
			print "<tr><td><b>Last Login</b></td><td><b>Display Name</b></td><td><b>Account</b></td><td><b>Status</b></td></tr>\n";
			foreach ($users as $user)
			{
				if ($user['lastlogin']=="0000-00-00 00:00:00")
				{
					$status = "INACTIVE";
				}
				else
				{
					$timeout = 30*60; 
					if ($user['lastlogout']!="0000-00-00 00:00:00" && strtotime($user['lastlogout'])>=strtotime($user['lastactivity']))
					{	
						$status="LOGGED OUT"; 
					}
					else if (strtotime($user['lastactivity'])+$timeout<time()) 
					{
						$status = "TIMED OUT";
					}
					else 
						$status="ACTIVE";
				}
				
				print "<tr><td>".$user['lastlogin']."</td><td>".$user['displayname']."</td><td>".$user['accountid']."</td><td>".$status."</td></tr>\n";
			}
			print "</table>";
			$form = new Form;
			$form->AddAction("ACTIVITY REFRESH");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SET ACTIVE":
		{
			Database::Query("UPDATE users SET accountid='".getVarS("accountid")."' WHERE userid='tom';");
			$_SESSION['accountid'] = getVarS("accountid");
			Database::Connect("mozaic");
			$accountdata = Database::QueryGetResult('SELECT `database`,`site`,`packages` FROM accounts WHERE uid=\''.$_SESSION['accountid'].'\';');
			$_SESSION['database'] = $accountdata['database'];
			$_SESSION['site'] = $accountdata['site'];
			$_SESSION['packages'] = explode(',',$accountdata['packages']);

			Database::Disconnect();
			
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			break;
		}
		case "ADD ACCOUNT":
		{
			$form = new ValidatedForm();
			$form->AddTable("accounts","name,superuser,startdate");
			$form->AddTable("users","password,displayname,email");
			$form->AddField("Trial Account","trial","checkbox","true");
			$form->AddField("Modules","packages","multiselect",true);
			$form->SetValue("packages","*licensing=Licensing,promo=Promo");
			$form->AddCancel("CANCEL");
			$form->AddAction("SUBMIT NEW ACCOUNT",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW ACCOUNT":
		{
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			$aspect->SetVar("userid",$aspect->GetVar('superuser')); // copy value from name to user id because we inserting to two tables
			if ($aspect->GetVar("trial"))
				$aspect->SetVar("status","T");
			else
				$aspect->SetVar("status","A");
				
			Database::Connect("mozaic");
			
			// create an empty address
//			$invoiceaddresid = DataTable::Insert("addresses","address1","Please fill out your address");	
//			$aspect->SetVar("invoiceaddress",$invoiceaddresid); 
			$aspect->SetVar('site','v2');
			$aspect->SetVar('packages',implode($_POST['packages'],','));
			$accountid = DataTable::InsertFromPost("accounts","name,superuser,status,packages,site",$aspect);
			Database::Query("UPDATE accounts SET database='".$accountid."' WHERE uid='".$accountid."';");
			$aspect->SetVar("accountid",$accountid); 
			DataTable::InsertFromPost("users","userid,password,displayname,email,accountid",$aspect);
			if (!file_exists(".//accounts//"))
			{
				mkdir ("..//accounts");
			}
			print "accountid = $accountid";
			mkdir("..//accounts//".$accountid);
			mkdir("..//accounts//".$accountid."//scripts",0755);
			
/*			// Send a welcome mail to them
			$mail = new Mail;
			$mail->SetMail("Welcome","This is your welcome message. Please take the time to fill out your invoice address from the home page edit profile option");
			MailManager::Send($mail,MAIL_TARGET_USER,$aspect->GetVar('superuser'));*/
			break;
		}
		case "EDIT ACCOUNT":
		{
			$form = new ValidatedForm();
			$form->AddTable("accounts","name,superuser,startdate,status");
			$form->AddHidden("accountid",$aspect->GetVar('accountid'));
			$form->AddCancel("CANCEL");
			$form->AddAction("UPDATE ACCOUNT",true);	// validate before sending
			$form->SetValuesFromDB("accounts",$aspect->GetVar('accountid'));
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "UPDATE ACCOUNT":
		{
			DataTable::UpdateFromPost("accounts",$aspect->GetVar('accountid'),"name,superuser,startdate,status",$aspect);
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			break;
		}
		case "REMOVE ACCOUNT":
		{
			$accountid = $aspect->GetVar('accountid');		

			// Delete all user mails and user accounts
			$users = Database::QueryGetResults("SELECT userid FROM users WHERE accountid='$accountid';");
			foreach ($users as $user)
				Database::Query("DELETE FROM mails WHERE distribution='user' AND destination='".$user['userid']."';"); 
			Database::Query("DELETE FROM mails WHERE distribution='account' AND destination='$accountid';");
			Database::Query("DELETE FROM users WHERE accountid='$accountid';"); 
			Database::Query("DELETE FROM accounts WHERE uid='$accountid';");
			// ALL GONE!!!

			$sql = 'DROP DATABASE my_db';
			$result = Database::Query("DROP DATABASE mozaic_".$accountid.";");
			if ($result==false)
				die("Could not drop database: ".mysql_error());
			
			$aspect->SetNextAction("MANAGE ACCOUNTS");
			break;
		}
		case "BACKUP DATABASE":
		{
			Database::Connect();
			$filename = "temp/backup_".date("Y-d-m").".sql";
			$db_backup = new db_backup(Database::GetLinkID(),$filename);
			print "<p>Save your database by right clicking <a href=\"$filename\">here</a> and save to your computer.</p>";
			Success("admin");
			$aspect->Present();
			break;
		}
		case "RESTORE DATABASE":
		{
			print "<p><b>Warning restoring will reset all data in the database. If in doubt backup first!</b><br>A restore may take some time, please be patient.</p>";
			print "<p>Please select the database sql file: ";
			$form = new Form;
			$form->Begin("admin");
			$form->AddField("Database File","sqlfile","file","");
			$form->AddHidden("MAX_FILE_SIZE","30000000");
			$form->AddAction("CONFIRM RESTORE");
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CONFIRM RESTORE":
		{
			Database::Connect();
			if (is_uploaded_file($_FILES['sqlfile']['tmp_name']))
			{
				print "Restoring from file ".$_FILES['sqlfile']['tmp_name']."<br>";
				$db_restore = new db_restore(Database::GetLinkID(),$_FILES['sqlfile']['tmp_name']);
				if ($db_restore->was_errors())
				{
					print "<p>Errors detected:<br>";
					print $db_restore->get_errors();
					print "</p>";
				}
				print "<p>Database Updated</p>";
				Success("admin");
				
			}
			else
			{
				Error("Cannot restore file ".$_FILES['sqlfile']['name'],"admin");
			}
			$aspect->Present();
			break;
		}
		case "IMPORT ARTISTS":
		{
			print "Format : BAND, ARTIST, SPLIT<br><br>";
			$form = new Form;
			$form->Begin("admin");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddField("Filename","importfile","file","");
			$form->AddCancel();
			$form->AddAction("PROCESS ARTISTS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "PROCESS ARTISTS":
		{
			$aspect->SetNextAction("VIEW");
			Database::Connect();

			$importfile = "./accounts/".$_SESSION['accountid']."/import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);

			if (!move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile))
			{
				$aspect->Error("Cannot move uploaded file ".$_FILES['importfile']['tmp_name']." to ".$importfile);
				break;
			}
			$worksheet = Worksheet::Load($importfile);
			$numrow = count($worksheet);
			for($i=2;$i<$numrow;++$i)
			{
				$artist = addslashes($worksheet[$i][2]);
				$band = addslashes($worksheet[$i][1]);
				$split = addslashes($worksheet[$i][3]);
				
				$result = Database::QueryGetResult("SELECT uid FROM artist WHERE realname='".$artist."';");	
				if ($result==false)
					$artist_uid = DataTable::Insert("artist","realname",$artist);
				else
					$artist_uid = $result['uid'];
					
				$result = Database::QueryGetResult("SELECT uid FROM bands WHERE name='".$band."';");	
				if ($result==false)
					$band_uid = DataTable::Insert("bands","name",$band);
				else
					$band_uid = $result['uid'];
						
				DataTable::Insert("bandartist","band,artist,split",$band_uid,$artist_uid,$split);
				print $i." : ".$band_uid." = ".$band." includes ".$artist_uid." = ".$artist." (split $split)<br>";
			}
			break;
		}
		case "IMPORT CATALOGUE":
		{
			print "Format : PRODUCT_NAME, PRODUCT_CODE, BAND_NAME, TRACK_POS, TRACK_NAME, REMIX_NAME, ISRC, BARCODE, RELEASE DATE, PUBLISHER, TRACKLENGTH<br><br>";
			$form = new Form;
			$form->Begin("admin");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddField("Filename","importfile","file","");
			$form->AddCancel();
			$form->AddAction("PROCESS CATALOGUE");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "PROCESS CATALOGUE":
		{
			Database::Connect();
			$aspect->SetNextAction("VIEW");
			$importfile = File::GetPath("import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1));
			//"./accounts/".$_SESSION['accountid']."/import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);
			print $_FILES['importfile']['name']." mm " . $_FILES['importfile']['tmp_name'] . ' to '. $importfile. '<br>';
			if (!move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile))
			{
				$aspect->Error("Cannot move uploaded file ".$_FILES['importfile']['tmp_name']." to ".$importfile);
				break;
			}

//			1 PRODUCT_NAME, 2 PRODUCT_CODE, 3 BAND_NAME, 4 TRACK_POS, 5 TRACK_NAME, 6 REMIX_NAME, 7 ISRC, 8 BARCODE, 9 RELEASE DATE, 
// 10 PUBLISHER, 11 TRACKLENGTH;



			Database::Query("TRUNCATE bands;");
			Database::Query("TRUNCATE artist;");
			Database::Query("TRUNCATE bandartist;");
			Database::Query("TRUNCATE product;");
			Database::Query("TRUNCATE tracks;");
			Database::Query("TRUNCATE tracksproduct;");
			Database::Query("TRUNCATE deals;");


			$worksheet = Worksheet::Load($importfile);
			$numrow = count($worksheet);
			$stamp = time();

			$product_number_last = "";
			$barcode_last = "";
            $track_pos_auto = 1;
			$band_name_last = "";
			
			for($i=2;$i<=$numrow;++$i)
			{
				print $i." : "; print_r($worksheet[$i]); print"<br>";

				// Validate

				if ($worksheet[$i][2]==""){
					print "Skipping line because no product number<br>";
					continue;
				}

				if ($worksheet[$i][3]==""){
					print "Skipping line because no band name<br>";
					continue;
				}

				if ($worksheet[$i][5]==""){
					print "Skipping line because no track name<br>";
					continue;
				}

				$product_name = cleanup($worksheet[$i][1]);				
                $product_number = cleanup($worksheet[$i][2]);
                if ($product_number!=$product_number_last)
				{
					$track_pos_auto=1;
				}
				else
				{
					$track_pos_auto += 1;

				}
				if ($product_number=="") 
				{
					$product_number=$product_number_last;
				}	
				$band_name = cleanup($worksheet[$i][3]);
				if ($band_name=="")
				{
					$band_name = $band_name_last;
				}
				$track_pos = cleanup($worksheet[$i][4]);
				if ($track_pos=="")
				{
					$track_pos = $track_pos_auto;
				}

				$track_name = cleanup($worksheet[$i][5]);
				$remix_name = cleanup($worksheet[$i][6]);
				$isrc = $worksheet[$i][7];
				$isrc = str_replace(" ","",$isrc);
				$isrc = str_replace("-","",$isrc);
				if ($isrc=="")
				{
					$isrc = $worksheet[$i][10];
			//		print "$i : No ISRC - skipped line<br>";
			//		continue;	// can't process this
				}
				$isrc = cleanup($isrc);

				$barcode = $worksheet[$i][8];
				$barcode = cleanup($barcode);
				$barcode = str_replace(" ","",$barcode);
				$barcode = str_replace("-","",$barcode);
				if ($barcode=="")
				{
					$barcode = $barcode_last;
				}

				$release_date = cleanup($worksheet[$i][9]);
				$publisher = cleanup($worksheet[$i][10]);
				$tracklength = cleanup($worksheet[$i][11]);
				
                $product_number_last = $product_number;
				$barcode_last = $barcode;
				$band_name_last = $band_name;
                
                // Check if band exists                


				$result = Database::QueryGetResult("SELECT uid FROM bands WHERE name=".$band_name.";");	
				if ($result===false)
				{
					Database::Query("INSERT INTO bands (name) VALUES ($band_name)");
					$band_uid = Database::LastInsertId();
					//$band_uid = DataTable::Insert("bands","name",$band_name);
					
					Database::Query("INSERT INTO artist (realname) VALUES ($band_name)");
					$artist_uid = Database::LastInsertId();
//					$artist_uid = DataTable::Insert("artist","realname",$band_name);

					Database::Query("INSERT INTO bandartist (band,artist,split) VALUES ($band_uid,$artist_uid,'100')");
//					DataTable::Insert("bandartist","band,artist,split",$band_uid,$artist_uid,100);
				}
				else
				{
					$band_uid = $result['uid'];
				}
				
                // Check that track exists - if not add it
				$result = Database::QueryGetResult("SELECT uid FROM tracks WHERE uid=".$isrc.";");
				if ($result==false) {
					//DataTable::Insert("tracks","uid,artist,title,version,publisher,tracklength,comments",$isrc,$band_uid,$track_name,$remix_name,$publisher,$tracklength,$stamp);
					$query = "INSERT INTO tracks (uid,artist,title,version,publisher,tracklength,comments) VALUES ($isrc,$band_uid,$track_name,$remix_name,$publisher,$tracklength,$stamp)";
					//print $query.'<br>';
					Database::Query($query);
				}
					
				
                // check that the product exists - if not add it
				$result = Database::QueryGetResult("SELECT uid FROM product WHERE uid=".$product_number.";");
				if ($result==false){
				//	DataTable::Insert("product","uid,artist,title,barcode,releasedate,comments",$product_number,$band_uid,$product_name,$barcode,$release_date,$stamp);
					Database::Query("INSERT INTO product (uid,artist,title,barcode,releasedate,comments) VALUES ($product_number,$band_uid,$product_name,$barcode,$release_date,$stamp)");
				}

                // add the track to the product

				Database::Query("INSERT INTO tracksproduct (track,product,position) VALUES ($isrc,$product_number,$track_pos)");
				
//				DataTable::Insert("tracksproduct","track,product,position",$isrc,$product_number,$track_pos);
				print "$i : $isrc, $product_number, $track_pos<br>";
				
			}
			break;
		}
		case "IMPORT DEALS":
		{
			print "Format : Deal Type, Band, Product, Track, Advance, Date Advance Paid, Deal Start Date, Deal End Date, Cross Calculate with, Deal Rate,	Reserve, Packaging Deductions, Minimum Payout<br><br>";
			$form = new Form;
			$form->Begin("admin");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddField("Filename","importfile","file","");
			$form->AddCancel();
			$form->AddAction("PROCESS DEALS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "PROCESS DEALS":
		{
			$aspect->SetNextAction("VIEW");
			Database::Connect();

			$importfile = "../accounts/".$_SESSION['accountid']."/import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);

			if (!move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile))
			{
				$aspect->Error("Cannot move uploaded file ".$_FILES['importfile']['tmp_name']." to ".$importfile);
				break;
			}
			$worksheet = Worksheet::Load($importfile);
			$numrow = count($worksheet);
			
			Database::Query("TRUNCATE deals;");
			Database::Query("TRUNCATE dealterms;");
			
			// 
			for($i=2;$i<$numrow;++$i)
			{
				if ($worksheet[$i][1]=="Artist Royalty Deal") $dealtype = "R";
				else if ($worksheet[$i][1]=="Artist Profit Split Deal") $dealtype = "P";
				else {
					print "Error row $i : Bad Deal Type '".$worksheet[$i][1]."'<br>"; continue;
				}		
				
				$band = $worksheet[$i][2];
				$product = $worksheet[$i][3];
				$track = $worksheet[$i][4];
				$advance = empty($worksheet[$i][5]) ? "" : Currency::DISPLAYTODATABASE($worksheet[$i][5]);
				$advancepaiddate = $worksheet[$i][6];
				$reserve = $worksheet[$i][11];
				$packagingdeductions = $worksheet[$i][12];
				$minpayout = Currency::DISPLAYTODATABASE($worksheet[$i][13]);

				$dealrate = $worksheet[$i][10];


				$bandid = Database::QueryGetResult("SELECT uid FROM bands WHERE name=".cleanup($band).";");
				if (empty($bandid)) {
					print "Error row $i : Band Not Found '$band'<br>";
					continue;			// skip the BAND NOT FOUND
				}
				
				$productid = Database::QueryGetResult("SELECT uid FROM product WHERE uid='".$product."';");


				
				$query = "SELECT * FROM deals WHERE product='".$productid['uid']."' AND band='".$bandid['uid']."';";
				$existing_deals = Database::QueryGetResults($query);
				if (empty($existing_deals))
				{
					
					
					$crossdealids = Database::QueryGetResults("SELECT DISTINCT crossdealid FROM deals WHERE band='".$bandid['uid']."' AND dealtype='$dealtype'");
					
					if (count($crossdealids)>1)
						die ('cross deal id error occurred');
					
					$query = "INSERT INTO deals (dealtype,reserve,packagingdeductions,advance,band,product,track,advancepaiddate,minpayout) VALUES ('".$dealtype."','".$reserve."','".$packagingdeductions."','".$advance."','".$bandid['uid']."','".$productid['uid']."','','".$advancepaiddate."','".$minpayout."');";
					Database::Query($query);
					$dealid = Database::LastInsertId();

					if (empty($crossdealids)) 
						$crossdealid = $dealid;
					else
						$crossdealid = $crossdealids[0]['crossdealid'];
					
					
					$query = "UPDATE deals SET crossdealid='".$crossdealid."' WHERE uid='".$dealid."';";
					Database::Query($query);
					
					// check if artist has deals
					$query = "INSERT INTO dealterms (dealid,rate,base) VALUES ('".$crossdealid."','".$dealrate."','salevalue');";
					Database::Query($query);

					print "OK row $i : Added deal $band, $product, $crossdealid<br>"; 
	
				}
			}
			break;
		}
		case "IMPORT IDS":
		{
			print "Format : Product Name, Cat No, Band Name, Track Name, Remix NAme, ISRC, UPC, Format, Track ID<br><br>";
			$form = new Form;
			$form->Begin("admin");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddField("Filename","importfile","file","");
			$form->AddCancel();
			$form->AddAction("PROCESS IDS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "PROCESS IDS":
		{
			$aspect->SetNextAction("VIEW");
			Database::Connect();
			$importfile = "../accounts/".$_SESSION['accountid']."/import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);

			if (!move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile))
			{
				$aspect->Error("Cannot move uploaded file ".$_FILES['importfile']['tmp_name']." to ".$importfile);
				break;
			}

			
			ini_set('auto_detect_line_endings',TRUE);
			// Scan each row
			$import_file = fopen($importfile,"r");
			$i=0;
			while($row = fgetcsv($import_file,1000,",")) {
				++$i;
				if ($i==1) continue;
				//print '<br>';
				// process each row
				$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product='".$row[1]."' ORDER BY position");
				
				if ($tracks==false) {
					$product = Database::QueryGetResult("SELECT uid FROM product WHERE uid='".$row[1]."'");
					if ($product==false)
						print '[Row '.$i.'] '.$row[1].' - No such product in database<br>';
					else 
						print '[Row '.$i.'] '.$row[1].' - Has no tracks in it<br>';
					continue;
				}
				
				$found = false;
				foreach($tracks as $track) {
					$trackinfo = Database::QueryGetResult("SELECT title,version,uid FROM tracks WHERE uid='".$track['track']."'");
					if ((strcasecmp($trackinfo['title'],$row[3])==0 && strcasecmp($trackinfo['version'],$row[4])==0) ||
						($trackinfo['version']=="" && strcasecmp($trackinfo['title'],$row[3])==0 && (strcasecmp($row[4],"Original Mix")==0) || $row[4]=="")) {
						print '[Row '.$i.'] '.$row[1].' - OK! found track &quot'.$trackinfo['title'].'&quot - &quot'.$trackinfo['version'].'&quot - it has ISRC of '.$trackinfo['uid'].' setting track ID to '.$row[8].'<br>';
						Database::Query("UPDATE tracks SET trackid='".$row[8]."' WHERE uid='".$trackinfo['uid']."' LIMIT 1;");
						$found = true;
						break;
					}
				}

				if (!$found) {
					print '[Row '.$i.'] '.$row[1].' - Cannot find track matching &quot'.$row[3].'&quot - &quot'.$row[4].'&quot in &quot'.$row[1].'&quot this product contains the following tracks:<br>';
					foreach($tracks as $track) {
						$trackinfo = Database::QueryGetResult("SELECT title,version,uid FROM tracks WHERE uid='".$track['track']."'");
						print '-----> '.$trackinfo['uid'].' : &quot'.$trackinfo['title'].'&quot - &quot'.$trackinfo['version'].'&quot<br>';
					} 
				}				
/*					$track = Database::QueryGetResult("SELECT title,version,uid FROM tracks WHERE title='".$row[3]."' AND version='".$row[4]."' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$row[1]."')");
					if ($track==false && $row[4]=='Original Mix')
					{
						$track = Database::QueryGetResult("SELECT title,version,uid FROM tracks WHERE title='".$row[3]."' AND version='' AND uid IN (SELECT track FROM tracksproduct WHERE product='".$row[1]."')");
					}
					
					if ($track==false)*/

			}
			break;
			
		}
		
		default:
		{
			$aspect->DefaultAction();
		}
	}
}
$aspect->End();
?>