<?php
// Page to manage the statements
// Show by period - drop down. In there display all artists and their status - balance, statement generated

require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cStatement.php');
require_once( 'cValidatedForm.php');
require_once( 'strings.php' );
require_once( 'cEmail.php' );


//---- This class creates the main view ----//
require_once( 'cSimpleList.php' );

class StatementList extends SimpleList {
		
	protected function actions()
	{
		$this->addaction("CALCULATE STATEMENT",'Create new statement');

	}
	
	protected function populate()
	{
		$this->_header = array('ARTIST','LAST ACCOUNTED','ACCOUNT DUE');
		$artists = Database::QueryGetResults('SELECT uid,realname FROM artist;');
		$todaysdate = time();
		foreach($artists as $artist)
		{
			$statements = Database::QueryGetResults("SELECT enddate FROM artiststatements WHERE artist='".$artist['uid']."' ORDER BY enddate DESC LIMIT 1");

			// calculate if statement is due
			$datenow = new DateTime("now");
			$statementdate = new DateTime($statements[0]['enddate']);
			$diff = $datenow->diff($statementdate);			
			if ($diff->m>6) $due='Y'; else $due='';
			
			$this->_rows[] = array('uid'=>$artist['uid'],'realname'=>$artist['realname'],'lastdate'=>$statements[0]['enddate'],'due'=>$due);
		}
		
		return true;
	}
	
	protected function containerpage()
	{
		return "statements";
	}

	public function StatementList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}
}


Database::Init();

$aspect = new Aspect;
$aspect->Start("artists","VIEW");
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		case "FINISHED":
		{
			$statementlist = new StatementList('productid');
			$statementlist->Present();
			$aspect->Present();
			break;
			
			$list = new TableList;
			$list->Begin("bandartistid","artists");
			$list->AddView("by band","bandartist","%artist.artist:realname%","band","artist","%band.bands:name%");
			$list->AddView("by artist","bandartist","%band.bands:name%","artist","band","%artist.artist:realname%");
			$unassigned = Database::QueryGetResults("SELECT uid,realname FROM artist WHERE uid NOT IN (SELECT DISTINCT artist FROM bandartist);");
			$unassignedignore="~";
			if ($unassigned!=false)
			{
				foreach ($unassigned as $artist)
				{
					// append "U" to front of ID to differentiate these from the assigned artists
					$unassigneduids[] = "U".$artist['uid'];
					$list->AddItem("!NO BAND ASSIGNED!","U".$artist['uid'],$artist['realname']);
				}
				$unassignedignore .= implode(",",$unassigneduids);	// negate these
			}
			$list->SetInfoBoxHandler("getartistinfo");
			$list->AddAction("MANAGE STATEMENTS",true,ACTION_ENABLELIST,$unassignedignore,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_BANDS_MANAGESTATEMENTS);
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		
		case "CALCULATE STATEMENT":
		{
			$bandlist = "";
			for ($i=0; $i<count($_POST['bands']);$i++) {
				if ($i>0) $bandlist .= ",";
				$bandlist .= $_POST['bands'][$i];
			}
			$startdate = $aspect->GetVar('startdate');
			$enddate = $aspect->GetVar('enddate');
			$statement = new Statement;
			$detaillevel = $aspect->GetVar('detaillevel');
			// slight hack you have to display comps in order to enter the right balances
			if ($aspect->GetVar('detaillevel')==0) $detaillevel = 1;

			// but when we do the pdf we will leave them out
			$statement->Generate($detaillevel,$aspect->GetVar("artist"),$bandlist,$startdate,$enddate,true);
			$statement->Display();
			$pdfhash = Statement::MakeStatementHash($aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
			$outfilename = Statement::MakeStatementFilename2($pdfhash);
			if ($statement->IsComplete())
			{
				// Store this for later			
				$_SESSION['results'] = $statement->GetResults();
				// If we've been doing this at wrong detail level	
				$statement->SaveAsPDF($outfilename);
			}
			
			$form = new Form;
			$form->Begin("artists");
			$form->AddHidden("artistid",$aspect->GetVar("artist"));
			$form->AddHidden("startdate",$startdate);
			$form->AddHidden("enddate",$enddate);
			$form->AddHidden("pdfhash",$pdfhash);
			if ($statement->IsComplete())
			{
				$email = Database::QueryGetValue("email","artist","uid='".$aspect->GetVar("artist")."'");
				$form->AddHidden("email",$email);
				$form->AddHidden("filename",$outfilename);
				if ($email!="")
				{	
					$form->AddAction("MAIL STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
				}
				else
				{
					print "<p>There is no email address registered for this artist. If you fill in this detail we can send it directly to them</p>";
				}
				$form->AddAction("SAVE STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
			}
			else
				print "<p>This statement is not complete and so can not be saved. It may have no data in it, or require entry of balance information</p>";
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}

		default:
		{
			$aspect->DefaultAction();
		}
	}
}
$aspect->End();		

?>