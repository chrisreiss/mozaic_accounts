<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cImport.php');
require_once('cImportFix.php');

require_once( 'cExport.php');
require_once( 'cFile.php' );

/*
if ($_SESSION['userid']!='tom')
{
  print "<p>We are currently performing some routine maintanence on this section of the website. Please try again later.</p>";
  exit();  
}*/

set_time_limit  ( 600  );

Database::Init();

// Do some housekeeping first
//if (!is_dir("../accounts/".$_SESSION['accountid'])) mkdir("../accounts/".$_SESSION['accountid']);
//if (!is_dir("../accounts/".$_SESSION['accountid']."/scripts")) mkdir("../accounts/".$_SESSION['accountid']."/scripts");


$aspect = new Aspect;
$aspect->Start("import","VIEW");

//print_r($_POST);
while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$list = new TableList;
			$list->Begin("importid","import");

			$list->AddItem("Built-in Exporters","#exportcat","Catalogue Export");
			$list->AddItem("Built-in Exporters","#exportlicence","Licenced Tracks Export");
			$importers = SalesImporter::Enumerate();
			foreach($importers as $importer)
			{
				$list->AddItem("Built-in Importers",'*'.$importer['id'],$importer['name']);
			}
			
			// Get common scripts
			$scripts = Import::GetScripts("public");
			if (!empty($scripts))
			{
				foreach ($scripts as $script)
				{	
					if ($script['display']!="MAMtemporaryScriptFilename")
					{
						$list->AddItem("Built-in Imports",$script['path'],$script['display']);
					}
				}
			}

			$editablescripts = "";
			$scripts = Import::GetScripts($_SESSION['accountid']);
			if (!empty($scripts))
			{
				foreach ($scripts as $script)
				{
					if ($script['display']!="MAMtemporaryScriptFilename")
					{
						$editablescripts .= $script['path'].",";
						$list->AddItem("User Imports",$script['path'],$script['display']);
					}
				}
			}

			$scripts = Export::GetScripts($_SESSION['accountid']);
			if (!empty($scripts))
			{
				foreach ($scripts as $script)
				{
					if ($script['display']!="MAMtemporaryScriptFilename")
					{
						$editablescripts .= $script['path'].",";
						$list->AddItem("User Exports",$script['path'],$script['display']);
					}
				}
			}
			
			$list->AddAction("NEW IMPORTER",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_NEWIMPORTER);
			$list->AddAction("NEW EXPORTER",false,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_NEWEXPORTER);
			$list->AddAction("EDIT",true,ACTION_ENABLELIST,$editablescripts,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_EDIT);
			$list->AddAction("REMOVE",true,ACTION_ENABLELIST,$editablescripts,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_REMOVE);
			$list->AddAction("RUN",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_INOUT_RUN);
			if ($_SESSION['userid']=="tom" && $_SESSION['accountname']=="Mozaic Music")
				$list->AddAction("RESET DEMO",false);
				
			$aspect->Attach($list);			
			$aspect->Present();
			break;
		}
		case "RESET DEMO":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			Database::Query("DELETE FROM sales WHERE saletype='V';");
			break;
		}
		case "NEW IMPORTER":
		{
			print "<p>Please select an example worksheet (CSV or EXCEL file) you would like this new importer to work on.<br>";
			print "We will examine it to extract the column headers from it:</p>";
			$form = new Form;
			$form->Begin("import");
			$form->AddField("Filename","importfile","file","");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddHidden("importid","Unnamed");
			$form->AddSelectionList("Type of data","table","sale=Sales,expenses=Expenses","sale");
			$form->AddCancel();
			$form->AddAction("EXAMINE WORKSHEET");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "NEW EXPORTER":
		{
			$form = new Form;
			$form->Begin("import");
			$form->AddHidden("exportid","New Exporter");
			$form->AddSelectionList("Type of data","table","sale=Sales,expenses=Expenses","sale");
			$form->AddCancel();
			$form->AddAction("SELECT OUTPUT DATA");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SELECT OUTPUT DATA":
		{
			$form = new Form;
			$form->Begin("import");
			$export = new Export;
			$export->SetScript($_POST['exportid']);
			$export->SetTable($_POST['table']);
			$export->PresentEditForm($form);
			$form->AddCancel();
			$form->AddAction("SAVE EXPORTER");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SAVE EXPORTER":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }
			
			$export = new Export;
			$export->LoadFromPost();
			// Delete old copy
			if ($export->GetScriptName()!=$_POST['exportid'])
			{
				Import::Remove($_POST['exportid']);
			}
			$export->Save($export->GetScriptName());
			break;
		}
		case "EXAMINE WORKSHEET":
		{
			if ($_FILES['importfile']['name']=="")
			{
				$aspect->SetNextAction("SELECT COLUMNS");
				$_POST['headerrow'] = 1;
				$_POST['importfile'] = "";
				break;
			}
			$importfile = "import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);
			$importfile = File::GetPath($importfile);
			if (file_exists($importfile)) 
			{
				unlink($importfile);
			}
			if (move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile)==false)
			{
				Error("Cannot upload file ".$_FILES['importfile']['tmp_name']." to $importfile","import");
				$aspect->Present();
				break;
			}
			$worksheet = Worksheet::Load($importfile);
			$form = new Form;
			$form->Begin("import");	
			$form->AddTitle("<b>Please select the row containing the column headers (displaying first 20 rows):</b>");
			$inserthtml = "<tr><td><table border=1>";
			$firstline = -1;
			for ($i=1;$i<=20;++$i)
			{
				if (!empty($worksheet[$i]))
				{
					$inserthtml .= "<tr><td><input type=radio name=headerrow value=".$i."";
					if ($firstline==-1)
					{
						$filledfields=0;
						foreach ($worksheet[$i] as $field)
						{
							if ($field!="") ++$filledfields;
						}
						$fieldcount = count($worksheet[$i]);
						if ($fieldcount!=0 && $filledfields==$fieldcount && $fieldcount>2)
						{
							$firstline = $i;
							$inserthtml .= " checked ";
						}
					}
					$inserthtml .= "></td><td>".implode("</td><td>",$worksheet[$i])."</td></tr>\n";
				}
			}
			$inserthtml .= "</table></td></tr>";
			$form->InsertHTML($inserthtml); // nasty hack
			$form->AddHidden("table",$_POST['table']);
			$form->AddHidden("importid",$_POST['importid']);
			$form->AddHidden("importfile",$importfile);
			$form->AddCancel();
			$form->AddAction("SELECT COLUMNS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SELECT COLUMNS":
		{
			$form = new Form;
			$form->Begin("import");
			$import = new Import;
			$import->SetScript($_POST['importid']);
			$import->SetTable($_POST['table']);
			$import->SetColumnRow($_POST['headerrow']);
			$import->PresentEditForm($form,$_POST['importfile']);
			$form->AddCancel();
			$form->AddAction("SAVE SCRIPT");
			$form->AddAction("RUN WITHOUT SAVING");
			$aspect->Attach($form);
			$aspect->PreserveVar('importid');
			$aspect->Present();
			break;
		}
		case "EDIT":
		{
			$form = new Form;
			$form->Begin("import");

			if (strpos($_POST['importid'],".exp",0)!==false)
			{	$script = new Export;
			}
			else
				$script = new Import;

			$script->Load($_POST['importid']);
			$script->PresentEditForm($form,"");
			$form->AddHidden("importid",$_POST['importid']);
			$form->AddCancel();
			$form->AddAction("SAVE SCRIPT");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

			Import::Remove($_POST['importid']);
			break;
		}
		case "RUN WITHOUT SAVING":
		{
			$import = new Import;
			$import->LoadFromPost();
			$import->SetScript("MAMtemporaryScriptFilename");
			$import->Save($import->GetScriptName());
			$_POST['importid'] = "MAMtemporaryScriptFilename";
			$aspect->SetNextAction("RUN");
			break;
		}
		case "SAVE SCRIPT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

			if (strpos($_POST['importid'],".exp",0)!==false)
				$script = new Export;
			else
				$script = new Import;

			$script->LoadFromPost();
			// Delete old copy
			if ($script->GetScriptName()!=$_POST['importid'])
			{
				$script->Remove($_POST['importid']);
			}
			$script->Save($script->GetScriptName());
			break;
		}
		case "RUN":
		{
			if ($_POST['importid'][0]=='#')
			{
				if ($_POST['importid']==="#exportcat")
				{
					$worksheet = new Worksheet;
					$worksheet->AddTitleRow("Catalogue");
					$worksheet->AddHeaderRow("Cat No","Format","Artist","Title","UPC Barcode","Release Date","Tracks...");
					
					$bandnames = Database::QueryGetResults("SELECT uid,name FROM bands;");
					$bandmap[0] = "Various Artists";
					foreach ($bandnames as $band)
					{
						$bandmap[$band['uid']] = $band['name'];
					}
					$catalogue = Database::QueryGetResults("SELECT uid,format,artist,title,barcode,releasedate FROM product ORDER BY uid;");
					foreach ($catalogue as $product)
					{
						$worksheet->AddCell($product['uid']);
						$worksheet->AddCell($product['format']);
						$worksheet->AddCell($bandmap[$product['artist']]);
						$worksheet->AddCell($product['title']);
						$worksheet->AddCell($product['barcode']);
						$worksheet->AddCell(displaydate($product['releasedate']));
						$tracks = Database::QueryGetResults("SELECT track FROM tracksproduct WHERE product='".$product['uid']."' ORDER BY position;");
						foreach ($tracks as $track)
						{
							$trackname = Database::QueryGetResult("SELECT title FROM tracks WHERE uid='".$track['track']."' LIMIT 1;");
							$worksheet->AddCell($trackname['title']);
						}
						$worksheet->GotoNextRow();
					}
				  $filename = "catalogue.csv";
				}
				else if ($_POST['importid']==="#exportlicence")
				{
					$filename = "licences.csv";
					$worksheet = new Worksheet;
					$worksheet->AddTitleRow("Track Licences");
					$worksheet->AddHeaderRow("Band","Track Name","Compilation","Licensee","Country","Address...");
					$licences = Database::QueryGetResults("SELECT licenceduid, licensor,description,advancepaiddate FROM licences WHERE licencetype='T' ORDER BY advancepaiddate;");
					if ($licences!==false)
					{
						foreach($licences as $licence)
						{
							$licensor = Database::QueryGetResult("SELECT * FROM creditor WHERE uid='".$licence['licensor']."' LIMIT 1;");
							$track = Database::QueryGetResult("SELECT artist, title FROM tracks WHERE uid='".$licence['licenceduid']."' LIMIT 1;");
							$artist = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$track['artist']."' LIMIT 1;");
							$address = "\"".$licensor['address1'].",".$licensor['address2'].",".$licensor['city'].",".$licensor['postcode']."\"";
							$worksheet->AddRow($artist['name'],$track['title'],$licence['description'],$licensor['name'],$licensor['country'],$address);
						}
					}
		        }

				$pathname = File::GetPath($filename);
				$worksheet->SaveAsCSV($pathname);
				print "<p>Click to save as text csv file <a href='download.php?file=".$filename."'>here</a></p>";
				print "<p>CSV stands for comma seperated values - they are text files that can be imported into a wide number of spreadsheets including MS Excel. Make sure the extensions is .csv</p>";
				$form = new Form;
				$form->Begin("import");
				$form->AddCancel("FINISHED");
				$aspect->Attach($form);
				$aspect->Present();
				break;
			}
			
			// new type of ImportFIX scripts
			if ($_POST['importid'][0]=='*')
			{
				$import = new SalesImporter;
				$form = new Form;
				$form->Begin("import");
				$form->AddField("Filename","importfile","file","");
				$form->AddHidden("importid",$_POST['importid']);
				
				// Get values that are missing
				$importerid = substr($_POST['importid'],1);	// remove the * at the front
				//die($importerid);
				$fields = $import->GetFieldsNeedingFillingByUser($importerid);
				$tableinfo = TableInfo::GetTable("sale");
				if (!empty($fields))
				{
					foreach ($fields as $field)
					{
						$form->AddField($tableinfo[$field]['description'],$field,$tableinfo[$field]['type'],"");
					}
				}
				$form->AddField("Currency Conversion","convert","float","1.0");
				$form->AddField("Batch Identifier","batchid","text16",substr($_POST['importid'],7).date("ymd",time()));
				$form->AddCancel();
				$form->AddAction("PROCESS WORKSHEET");
				$aspect->Attach($form);
			}
			else
			{
				if (strpos($_POST['importid'],".exp",0)!==false)
				{
					$form = new Form;
					$form->Begin("import");
					$export = new Export;
					$export->Load($_POST['importid']);
					$export->ProcessFile("tempexport.csv");
					print "<p>Export completed - right click <a href=\"tempexport.csv\">here</a> and save the sheet to your harddrive</p>";
					$form->AddAction("FINISHED");
					$aspect->Attach($form);
				}
				else
				{
					$import = new Import;
					$import->Load($_POST['importid']);
					$form = new Form;
					$form->Begin("import");
					$form->AddField("Filename","importfile","file","");
					$form->AddHidden("importid",$_POST['importid']);
					$import->PresentFixedForm($form);
					$form->AddCancel();
					$form->AddAction("PROCESS WORKSHEET");
					$aspect->Attach($form);
				}
			}
			$aspect->Present();
			break;
		}
		case "PROCESS WORKSHEET":
		case "REFRESH":
		{
		
			$form = new Form;
			$form->Begin("import");
			$form->AddCancel();

			$importfile = "import.".substr($_FILES['importfile']['name'],strpos($_FILES['importfile']['name'],".")+1);
			$importfile = File::GetPath($importfile);
			if (file_exists($importfile))
			{
				unlink($importfile);
			}
			if (move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile)==FALSE)
			{
				die("Unable to upload file. Please inform support.");
			}
			// Do processing
			if ($_POST['importid'][0]=='*')
			{
				$importerid = substr($_POST['importid'],1);	// remove the * at the front
				$import = new SalesImporter;
				
				$fields = $import->GetFieldsNeedingFillingByUser($importerid);
				foreach ($fields as $field)
				{
					$fixeddata[$field] = $_POST[$field];
					$form->AddHidden($field,$_POST[$field]);
				}
				$fixeddata['convert'] = $_POST['convert']; // special case
				$fixeddata['batchid'] = $_POST['batchid']; // special case
					
				$_SESSION['query'] = $import->LoadAndProcess($importerid,$importfile,$fixeddata);				
			}
			else
			{
				$import = new Import;
				$import->Load($_POST['importid']);
				$fixedvalue = $import->GetFixedValues();
				$_SESSION['query'] = $import->ProcessFile($importfile,$fixedvalue);
			}

			$form->AddHidden('importfile',$importfile);
			$form->AddHidden('importid',$importid);
			$form->AddAction("COMMIT VALID DATA");
	//		$form->AddAction("REFRESH");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "COMMIT VALID DATA":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

			set_time_limit(400);
			$queries = explode(";\n",$_SESSION['query']);
			foreach ($queries as $query)
			{
				if (!empty($query))
				{
					Database::Query($query.";");
				}
			}
			unset($_SESSION['query']);
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}

$aspect->End();
?>
