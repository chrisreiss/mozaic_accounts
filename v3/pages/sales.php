<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cListTable.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cImport.php');
require_once( 'cValidatedForm.php' );
require_once( 'cCurrency.php' );
require_once( 'cEditField.php' );
require_once('cSimpleList.php');

class SalesList extends SimpleList {
		
	var $totalquantity = 0;
	var $totalamount = 0;
		
	protected function actions()
	{
		$this->addaction("IMPORT SALES","Import Sales",SL_NOSELECTION);
		$this->addaction("ADD SALES", "Add Sales Manually",SL_NOSELECTION);
		$this->addaction("REMOVE SALES", STRING_ACTION_DELETE,SL_MULTIPLESELECTION);
		$this->addaction("EDIT SALE",STRING_ACTION_EDIT,SL_SINGLESELECTION);
		$this->addaction("ADVANCED SEARCH",'Advanced Search',SL_NOSELECTION);
		$this->addaction("MANAGE SALES BATCH",'Manage Sales Batch',SL_NOSELECTION);
		$this->addaction("NEW IMPORTER",'New Importer',SL_NOSELECTION);
	}
	
	protected function footer()
	{
		print "<span class=simplelistfooter>Total Quantity: ".$this->totalquantity."  Total Value: ".Currency::Display($this->totalamount)."<span>";
	}
	
	protected function populate($startid=0)
	{
		// Store in session
		if (empty($_SESSION['filterBatchId'])) $_SESSION['filterBatchId'] = 'all sales';
		if (empty($_SESSION['filterProduct'])) $_SESSION['filterProduct'] = 'all products';
	
		$_SESSION['filterBatchId'] = getVar('filterBatchId',$_SESSION['filterBatchId']);
		$_SESSION['filterProduct'] = getVar('filterProduct',$_SESSION['filterProduct']);

		// filters
//		$where = "WHERE 1 "; 
		$where="";
		if ($_SESSION['filterBatchId']!="all sales")    $where .= 'AND batchid=\''.$_SESSION['filterBatchId'].'\' ';
		if ($_SESSION['filterProduct']!='all products') $where .= 'AND product=\''.$_SESSION['filterProduct'].'\' ';
		
		$rows = Database::QueryGetResults("SELECT sale.uid,1 as row,date,product,track,partners.name,shop,quantity,value,territory FROM sale JOIN partners WHERE partners.uid=sale.distributor $where ORDER BY date DESC LIMIT ".$startid.",".SL_MAXITEMS.";");
	
		$this->_header = array('ROW','DATE','CAT#','TRACK','DISTRIBUTOR','SHOP','QUANTITY','VALUE','TERRITORY');
		$distributors = Database::QueryGetResults("SELECT uid,name FROM partners");
		
		$tracks = Database::QueryGetResults("SELECT uid,title,version FROM tracks");
		foreach($tracks as $track)
		{
			$tracklookup[$track['uid']] = formatTrackTitle($track['title'],$track['version']);
		}
		if (!empty($rows))
		{
			foreach ($rows as $row)
			{
				$this->totalquantity += $row['quantity'];
				$this->totalamount += Currency::GETRAWVALUE($row['value']);
				
				$row['row'] = $startid++;
//					PrintTable($row);	

				$row['value'] = Currency::DATABASETODISPLAY($row['value']);
				if (!empty($row['track'])) 
				{
					if (!array_key_exists($row['track'],$tracklookup))
						$row['track'] = '<i>'.$row['track'].'???</i>';
					else
						$row['track'] = $tracklookup[$row['track']];
				}
				$this->_rows[] = $row;
			}
		}

		return true;
	}
	
	protected function filters()
	{


//		print_r($_POST);
		$insertcode = 'onChange="document.mainform.submit();" class="filter"';
		
		
		// filter by batch -> band -> product -> title
		print 'Display '; 
		$batches = array('all sales'); 
		$batches = array_merge($batches,getSalesBatches());
		print formfieldSelectFromArray("filterBatchId",$batches,$_SESSION['filterBatchId'],$insertcode);
		
		$products = array(array('all products','all products'));
		if ($_SESSION['filterBatchId']!='all sales')
			$distinctproducts = Database::QueryGetResults("SELECT DISTINCT product FROM sale WHERE batchid='".$_SESSION['filterBatchId']."';");
		else
			$distinctproducts = Database::QueryGetResults("SELECT DISTINCT product FROM sale;");
		
		if (!empty($distinctproducts))
			foreach($distinctproducts as $d) {
				$products[] = array($d['product'],$d['product']);
				}
				
		print formfieldSelectFromArrayWithKey("filterProduct",$products,$_SESSION['filterProduct'],$insertcode);	
	}
	
	protected function containerpage()
	{
		return "sales";
	}

	public function SalesList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}

}



Database::Init();

$aspect = new Aspect;
$aspect->Start("sales","VIEW");

while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$saleslist = new SalesList('saleid');
			$saleslist->Present();
			$aspect->Present();
			break;
		}
		case "IMPORTS":
		{
			$form = new Form();

			$form->AddField("Batch Id","batchid","batchid","");
			$form->AddAction("IMPORT NEW SHEET",false);
			$form->AddAction("VIEW SALES SHEET",false);
			$form->AddAction("REMOVE SALES SHEET",false);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE SALES SHEET":
		{
			$aspect->SetNextAction("VIEW");
			if ($aspect->GetVar('batchid')!="")
			{
				Database::Query("DELETE FROM sale WHERE batchid='".$aspect->GetVar('batchid')."';");
			}
			break;
		}
		case "VIEW SALES SHEET":
		{
			$aspect->SetVar('filterbatchid',$aspect->GetVar('batchid'));
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "SCAN AGAIN":
		case "SCAN FOR SALES":
		case "ADVANCED SEARCH":
		{
			HeaderBar('Advanced Search');
			$form = new ValidatedForm();
			$form->AddField("Distributor/Shop","source","distributor",true);
			$form->AddField("Start Date","startdate","date",true);
			$form->AddField("End Date","enddate","date",true);
			$form->AddField("Band","band","band",true);
			$form->AddField("Product","product","product",true);
			$form->AddField("Band","track","track",true);
			$form->AddField("Sales Type","saletype","saletype",true);
			$form->AddField("Batch","batch","batchid",true);

			// If these don't exist then should just be null
			$form->SetValue("source",$aspect->GetVar("source"));
			$form->SetValue("startdate",$aspect->GetVar("startdate"));
			$form->SetValue("enddate",$aspect->GetVar("enddate"));
			$form->SetValue("band",$aspect->GetVar("band"));
			$form->SetValue("product",$aspect->GetVar("product"));
			$form->SetValue("track",$aspect->GetVar("track"));
			$form->SetValue("saletype",$aspect->GetVar("saletype"));
			$form->SetValue("batchid",$aspect->GetVar("batchid"));


			if ($aspect->IsVarSet("source"))
			{
				$query = "SELECT * FROM sale WHERE 1 ";
				if (getVarS("source")!="") $query .= "AND distributor='".getVarS("source")."' ";
				if (getVarS("startdate")!="") $query .= "AND date>='".getVarS("startdate")."' ";
				if (getVarS("enddate")!="") $query .= "AND date<='".getVarS("enddate")."' ";
				if (getVarS("band")!="") {  
					$query .= "AND (product IN (SELECT uid FROM product WHERE artist='".getVarS("band")."') OR track IN (SELECT uid FROM tracks WHERE artist='".getVarS("band")."')) ";
				}

				if (getVarS("product")!="") $query .= "AND product='".getVarS("product")."' ";
				if (getVarS("track")!="") $query .= "AND track='".getVarS("track")."' ";

				if (getVarS("saletype")!="") $query .= "AND saletype='".getVarS("saletype")."' ";
				if (getVarS("batchid")!="") $query .= "AND batchid='".getVarS("batchid")."' ";
				$query .="LIMIT 5000;";

				//print $query;


				$sales = Database::QueryGetResults($query);

				//$sales = Database::QueryGetResults("SELECT * FROM sale WHERE distributor='".$aspect->GetVar("source")."' AND date BETWEEN '".$aspect->GetVar("startdate")."' AND '".$aspect->GetVar("enddate")."';"); 
				//$distname = TableInfo::DisplayFromType("creditor",$aspect->GetVar('source'));

				$form->AddCancel();
				$form->AddAction("SCAN AGAIN",true);
				/*if (!empty($sales))
				{
					$form->AddAction("DELETE SALES",true);
				}*/
				$aspect->Attach($form);
				$aspect->Present();

				$total_quantity = 0;
				$total_value = 0;
				if (!empty($sales))
				{
					print "<table class=spreadsheet>\n";
					print "<tr><th>Date</th><th>Product</th><th>Track</th><th>Distributor</th><th>Quantity</th><th>Value</th><th>Type</th><th>Batch ID</th></tr>\n";
					foreach($sales as $sale)
					{
						print "<tr><td>".displaydate($sale['date'])."</td><td>".$sale['product']."</td><td>".$sale['track']."</td><td>".TableInfo::DisplayFromType("partner",$sale['distributor'])."</td><td>".$sale['quantity']."</td><td>".Currency::DATABASETODISPLAY($sale['value'])."</td><td>".TableInfo::DisplayFromType("saletype",$sale['saletype'])."</td><td>".$sale['batchid']."</td></tr>\n";
						$total_quantity += $sale['quantity'];
						$total_value += Currency::GETRAWVALUE($sale['value']);
					}
					print "</table>";
					print "Total Quantity = ".$total_quantity."  Total Value of Sales = ".Currency::DISPLAY($total_value);
				}
				else
				{
					print "<p style='color:black'>No results found</p>";
				}
			}
			else
			{
				$form->AddCancel();
				$form->AddAction("SCAN FOR SALES",true);
				$aspect->Attach($form);
				$aspect->Present();
			}
			break;
		}
/*		case "SCAN FOR SALES":
		{

			$form = new Form();
			$form->AddHidden("source",$aspect->GetVar("source"));
			$form->AddHidden("startdate",$aspect->GetVar("startdate"));
			$form->AddHidden("enddate",$aspect->GetVar("enddate"));
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}*/
		case "MANAGE SALES BATCH":
		{
			$form = new Form();

			$form->AddField("Batch Id","batchid","batchid","");
			$form->AddAction("VIEW BATCH SALES",false);
			$form->AddAction("REMOVE BATCH",false);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE BATCH":
		{
			$aspect->SetNextAction("VIEW");
			if ($aspect->GetVar('batchid')!="")
			{
				Database::Query("DELETE FROM sale WHERE batchid='".$aspect->GetVar('batchid')."';");
			}
			break;
		}
		case "VIEW BATCH SALES":
		{
			$aspect->SetVar('filterbatchid',$aspect->GetVar('batchid'));
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "DELETE SALES":
		{
			Database::Query("DELETE FROM sale WHERE distributor='".$aspect->GetVar("source")."' AND date BETWEEN '".$aspect->GetVar("startdate")."' AND '".$aspect->GetVar("enddate")."';"); 
			$aspect->SetNextAction("VIEW");
			break;
		}
		case "ADD SALES":
		{
			if (!isset($_POST['addcount'])) $_POST['addcount']=1;
			$form = new ValidatedForm("sale","product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey");
			HeaderBar("Add Sales ".$_POST['addcount']);
			
			if (isset($_POST['saleid2'])) $form->SetValue("product",$_POST['saleid2']);

			if ($_POST['addcount']>1) 
				$form->SetValuesFromPost($aspect,"distributor,saletype,date,termkey");
			$form->AddHidden('addcount',$_POST['addcount']);

			$form->SetFilter("product","track");
			$form->SetEventCode("track","onChange=\"ChangeSaleType(this)\"");
			$form->AddCancel("BACK");
			$form->AddAction("SAVE",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "EDIT SALE":
		{
			HeaderBar("Edit Sale");
		//	print_r($_POST);
			$saleid = $_POST['saleid'][0];
			$form = new ValidatedForm("sale","product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey,batchid");
			
						$form->SetValuesFromDB("sale",$saleid);
			$form->AddHidden("saleid",$saleid);
			$form->AddHidden("saleid2",$saleid);
			$form->AddCancel();
			$form->AddAction("SUBMIT",true,"","","SUBMIT SALE");	// validate before sending
			$form->SetFilter("product","track");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SAVE":
		{
			$aspect->SetNextAction("ADD SALES");
			// Demo safety check
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			
			DataTable::InsertFromPost("sale","product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey",$aspect);
			$_POST['addcount']+=1;
			break;
		}
		case "SUBMIT SALE":
		{
			$aspect->SetNextAction("VIEW");

			// Demo safety check
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			DataTable::UpdateFromPost("sale",$aspect->GetVar('saleid'),"product,track,distributor,quantity,value,baseprice,saletype,date,territory,shop,mechanicals,distfee,termkey,batchid",$aspect);
			break;
		}
		case "REMOVE SALES":
		{
			$aspect->SetNextAction("VIEW");
			// Demo safety check
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			foreach ($_POST['saleid'] as $value)
			{
				DataTable::Delete("sale",$value);
			}
			break;
		}
		case "IMPORT NEW SHEET":
		{

			HeaderBar('Import Sales');
			$aspect->SetNextAction("VIEW");
			$form = new Form();
			$form->AddField("Batch Name","batchid","text16","");
			$form->AddField("Spreadsheet File","importfile","file","");
			$salesimporters = Import::EnumerateImports();
			if (!empty($salesimporters))
			{
				$scriptlist = "";
				foreach ($salesimporters as $script)
				{
					$scriptlist .= $script.".imp=".$script.",";
				}
				$form->AddSelectionList("Format","format",$scriptlist,"");
				$form->AddAction("IMPORT",true);	// validate before sending
			}
			else
			{
				$form->AddFixed("Format","format","text16","No Scripts Available");
			}
			$form->AddAction("EDIT IMPORT TEMPLATES",false);
			$form->AddCancel();
			$aspect->Attach($form);
			$aspect->Present();

			break;
		}
		case "NEW IMPORTER":
		{
			HeaderBar('New Importer');
			print "<p>Please select an example worksheet (CSV or EXCEL file) you would like this new importer to work on.<br>";
			print "We will examine it to extract the column headers from it:</p>";
			$form = new Form;
			$form->Begin("import");
			$form->AddField("Example Sales Sheet","importfile","file","");
			$form->AddHidden("MAX_FILE_SIZE","50000000");
			$form->AddHidden("importid","Unnamed");
			$form->AddCancel();
			$form->AddAction("EXAMINE WORKSHEET");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "EXAMINE WORKSHEET":
		{
			HeaderBar('New Importer > Select Header');
			if ($_FILES['importfile']['name']=="")
			{
				$aspect->SetNextAction("SELECT COLUMNS");
				$_POST['headerrow'] = 1;
				$_POST['importfile'] = "";
				break;
			}
			$folderPath = $_SERVER['DOCUMENT_ROOT'] . '/accounts/' . $_SESSION['accountid'] . '/';
			$fileParts = pathinfo($_FILES['importfile']['name']);
			$importfile = $folderPath . 'import.csv';

			if (file_exists($importfile)) 
			{
				unlink($importfile);
			}
			if (move_uploaded_file($_FILES['importfile']['tmp_name'],$importfile)==false)
			{
				Error("Cannot upload file ".$_FILES['importfile']['tmp_name']." to $importfile","import");
				$aspect->Present();
				break;
			}
			$worksheet = Worksheet::Load($importfile);
			$form = new Form;
			$form->Begin("import");	
			$form->AddTitle("<b>Please select the row containing the column headers (displaying first 20 rows):</b>");
			$inserthtml = '<tr><td><table class="spreadsheet">';
			$firstline = -1;
			for ($i=1;$i<=20;++$i)
			{
				if (!empty($worksheet[$i]))
				{
					$inserthtml .= "<tr><td><input type=radio name=headerrow value=".$i."";
					if ($firstline==-1)
					{
						$filledfields=0;
						foreach ($worksheet[$i] as $field)
						{
							if ($field!="") ++$filledfields;
						}
						$fieldcount = count($worksheet[$i]);
						if ($fieldcount!=0 && $filledfields==$fieldcount && $fieldcount>2)
						{
							$firstline = $i;
							$inserthtml .= " checked ";
						}
					}
					$inserthtml .= "></td><td>".implode("</td><td>",$worksheet[$i])."</td></tr>\n";
				}
			}
			$inserthtml .= "</table></td></tr>";
			$form->InsertHTML($inserthtml); // nasty hack
			$form->AddHidden("importid",$_POST['importid']);
			$form->AddHidden("importfile",$importfile);
			$form->AddCancel();
			$form->AddAction("SELECT COLUMNS");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SELECT COLUMNS":
		{
			HeaderBar('New Importer > Assign Columns');
			$form = new Form;
			$form->Begin("import");
			$import = new Import;
			$import->SetScript($_POST['importid']);
			$import->SetTable('sale');
			$import->SetColumnRow($_POST['headerrow']);
			$import->PresentEditForm($form,$_POST['importfile']);
			$form->AddCancel();
			$form->AddAction("SAVE SCRIPT");
			$form->AddAction("RUN WITHOUT SAVING");
			$aspect->Attach($form);
			$aspect->PreserveVar('importid');
			$aspect->Present();
			break;
		}
		case "EDIT":
		{
			$form = new Form;
			$form->Begin("import");

			if (strpos($_POST['importid'],".exp",0)!==false)
			{	$script = new Export;
			}
			else
				$script = new Import;

			$script->Load($_POST['importid']);
			$script->PresentEditForm($form,"");
			$form->AddHidden("importid",$_POST['importid']);
			$form->AddCancel();
			$form->AddAction("SAVE SCRIPT");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "REMOVE":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

			Import::Remove($_POST['importid']);
			break;
		}
		case "RUN WITHOUT SAVING":
		{
			$import = new Import;
			$import->LoadFromPost();
			$import->SetScript("MAMtemporaryScriptFilename");
			$import->Save($import->GetScriptName());
			$_POST['importid'] = "MAMtemporaryScriptFilename";
			$aspect->SetNextAction("RUN");
			break;
		}
		case "SAVE SCRIPT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

			if (strpos($_POST['importid'],".exp",0)!==false)
				$script = new Export;
			else
				$script = new Import;

			$script->LoadFromPost();
			// Delete old copy
			if ($script->GetScriptName()!=$_POST['importid'])
			{
				$script->Remove($_POST['importid']);
			}
			$script->Save($script->GetScriptName());
			break;
		}
		case "IMPORT SALES":
		{			
			HeaderBar("Import Sales");
					// Built in importers
	/*		$importers = SalesImporter::Enumerate();
			foreach($importers as $importer)
				$option[] = array('id'=>'*'.$importer['id'],'name'=>$importer['name']);
	*/		
			// Get common scripts
			$importers = Import::GetScripts("public");
			if (!empty($importers))
				foreach ($importers as $importer)
						$option[] = array('id'=>$importer['path'],'name'=>$importer['display']);

			$editablescripts = "";
			$importers = Import::GetScripts($_SESSION['accountid']);
			if (!empty($importers))
				foreach ($importers as $importer)
						$option[] = array('id'=>$importer['path'],'name'=>$importer['display']);
						

			if (!isset($_POST['importid'])) $_POST['importid']=$option[0]['id'];

			$form = new Form;
			$form->Begin("import");
			$javascript = 'onChange="RepostForm(this,\''.$aspect->GetAction().'\')"';
			$form->AddCustom('Format','importid',formfieldSelectFromArrayWithKey("importid",$option,$_POST['importid'],$javascript));
			$form->AddField('CSV Upload File','importfile','file','');
			// new type of ImportFIX scripts
			if ($_POST['importid'][0]=='*')
			{
				$import = new SalesImporter;
				$importerid = substr($_POST['importid'],1);	// remove the * at the front
				$fields = $import->GetFieldsNeedingFillingByUser($importerid);
				$tableinfo = TableInfo::GetTable("sale");
				if (!empty($fields))
				{
					foreach ($fields as $field)
					{
						$form->AddField($tableinfo[$field]['description'],$field,$tableinfo[$field]['type'],"");
					}
				}
				$form->AddField("Batch Identifier","batchid","text16",substr($_POST['importid'],7,11).date("Y-m",time()));

			}
			else
			{
				$import = new Import;
				$import->Load($_POST['importid']);
				$import->PresentFixedForm($form);
			}
			
			$form->AddField("Currency Conversion","convert","float","1.0");
		//	$form->AddField("CSV delimiter","delimiter","text1",",");

//			$form->AddField("Batch Identifier","batchid","text16",substr($_POST['importid'],7).date("ymd",time()));
			$form->AddCancel();
			$form->AddAction("PROCESS WORKSHEET");
			$aspect->Attach($form);

			$aspect->Present();
			break;
		}
		case "PROCESS WORKSHEET":
		case "REFRESH":
		{
			HeaderBar('Import Sales Processed');

			$form = new Form;
			$form->Begin("import");
			$form->AddCancel();
			$folderPath = $_SERVER['DOCUMENT_ROOT'] . '/accounts/' . $_SESSION['accountid'] . '/';
			$fileParts = pathinfo($_FILES['importfile']['name']);
			
			if (file_exists($folderPath . 'import.csv'))
			{
				unlink($folderPath . 'import.csv');
			}

			$importfile = $folderPath . 'import.csv';

			if(move_uploaded_file($_FILES['importfile']['tmp_name'], $folderPath . 'import.csv') == FALSE){
				die ('There was an error moving ' . $fileParts['basename']);
			}

			/*$upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/images/";
			if (!file_exists($upload_dir) || !is_writable($upload_dir)) {
				die("Upload directory is not writable, or does not exist.");
			}
			
			if (move_uploaded_file($_FILES['importfile']['tmp_name'], __DIR__ . ' ../../accounts/' . $importfile)==FALSE)
			{
				echo '<pre>';
				print_r( $_FILES);
				echo '</pre>';
				die("Unable to move uploaded file '".$_FILES['importfile']['tmp_name']."' to '".$importfile."'. Please inform support.");
			}*/

			$delimiter = ",";
			// Do processing
			if ($_POST['importid'][0]=='*')
			{
				$importerid = substr($_POST['importid'],1);	// remove the * at the front
				$import = new SalesImporter;
				
				$fields = $import->GetFieldsNeedingFillingByUser($importerid);
				foreach ($fields as $field)
				{
					$fixeddata[$field] = $_POST[$field];
					$form->AddHidden($field,$_POST[$field]);
				}
				$fixeddata['convert'] = $_POST['convert']; // special case
				$fixeddata['batchid'] = $_POST['batchid']; // special case
				$delimiter = $_POST['delimiter'];
					
				$_SESSION['query'] = $import->LoadAndProcess($importerid,$importfile,$fixeddata,$delimiter);				
			}
			else
			{
				$import = new Import;
				$import->Load($_POST['importid']);
				$fixedvalue = $import->GetFixedValues();
				$_SESSION['query'] = $import->ProcessFile($importfile,$fixedvalue,$delimiter);
			}

			$form->AddHidden('importfile',$importfile);
	//		$form->AddHidden('importid',$importid);
			$form->AddAction("COMMIT VALID DATA");
	//		$form->AddAction("REFRESH");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "COMMIT VALID DATA":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO_DISABLED); break; }

			set_time_limit(400);
			$queries = explode(";\n",$_SESSION['query']);
			foreach ($queries as $query)
			{
				if (!empty($query))
				{
					Database::Query($query.";");
				}
			}
			unset($_SESSION['query']);
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}

$aspect->End();
?>