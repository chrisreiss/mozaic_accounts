<?php
require_once( 'util.php' );
require_once( 'cForm.php' );
require_once( 'cList.php' );
require_once( 'cAspect.php' );
require_once('cDatabase.php' );
require_once('cDataTable.php');
require_once('cValidatedForm.php');


//---- This class creates the main view ----//
require_once('cSimpleList.php');
class TracksList extends SimpleList {
		
	public function actions()
	{
		$this->addaction("ADD TRACKS","New",SL_NOSELECTION);
//		$this->addaction("IMPORT TRACKS","Import from File",SL_NOSELECTION);
		$this->addaction("EDIT TRACK", STRING_ACTION_EDIT,SL_SINGLESELECTION);
		$this->addaction("REMOVE TRACKS", STRING_ACTION_DELETE,SL_MULTIPLESELECTION);
	}
	
	protected function populate()
	{
		$where="1";
		$filter = getVar('filterBands','all bands');
		if ($filter!="all bands") $where = 'tracks.artist=\''.$filter.'\' ';


		$this->_header = array('ISRC','ARTIST','TITLE','REMIXER');
		$rows = Database::QueryGetResults("SELECT tracks.uid,bands.name,tracks.title,tracks.version,tracks.remixer FROM tracks JOIN bands WHERE bands.uid=tracks.artist AND ".$where.";");
		foreach ($rows as $row)
		{
			if ($row['version']=="") $trackname = $row['title'];
			else
			$trackname = $row['title'].' ('.$row['version'].')';
			$this->_rows[] = array('uid'=>$row['uid'],'band'=>$row['name'],'title'=>$trackname,'remixer'=>$row['remixer']);
		}
		return true;
	}
	
	protected function filters()
	{
		$insertcode = 'onChange="document.mainform.submit();" class="filter"';
		// filter by batch -> band -> product -> title
		print 'Display '; 
		$batches = array(array('','all bands')); $batches = array_merge($batches,getBands());
//		print_r($batches);
		print formfieldSelectFromArrayWithKey("filterBands",$batches,getVar('filterBands','all bands'),$insertcode);		
	}

	protected function containerpage()
	{
		return "tracks";
	}

	public function TracksList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = false;
	}

}

Database::Init();

$aspect = new Aspect;
$aspect->Start("tracks","VIEW");


while (!$aspect->IsComplete())
{
	switch($aspect->GetAction())
	{
		default:
		{
			$trackslist = new TracksList('trackuid');
			$trackslist->Present();
			$aspect->Present();
			break;
		}
		
		case "IMPORT TRACKS":
		{
			print '<span class="formtitle">Import Tracks</span>';

			print "Please import a file with the following format:";
			print '<table>';
			print '<tr>';
			print '<td>ISRC</td><td>TRACK TITLE</td><td>REMIXER</td>';
			print '</tr>';
			print '</table>';
			$aspect->Present();
			break;		
		}
		
		case "REMOVE TRACKS":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// tracksproduct table
			// trackuid, productid, position
			// Reorder the positions so that they are shifted down
			foreach ($_POST['trackuid'] as $trackuid)
			{
				$result = Database::Query("SELECT product,position FROM tracksproduct WHERE track='".$trackuid."';");
				// Update the product/track numbers
				if ($result!=false)
				{
					for ($i=0;$i<$result->GetNum();++$i)
					{
						$row = $result->GetNext();
						$query = "UPDATE tracksproduct SET position=position-1 WHERE product='".$row['product']."' AND position>".$row['position'].";";
						Database::Query($query);
						print $query."<br>";
					}
					Database::FinishQuery($result);
					
					Database::Query("DELETE FROM tracksproduct WHERE track='".$trackuid."';");
				}
				Database::Query("DELETE FROM tracks WHERE uid='".$trackuid."';");
			}
			
			break;
		}
		
		case "EDIT TRACK":
		{
			// Need to make sure that there are no references to this track
			HeaderBar('Edit Track');
			$form = new ValidatedForm("tracks","uid,trackid,artist,title,version,remixer,publisher,tracklength,comments");
			$form->SetValuesFromDB("tracks",$_POST['trackuid'][0]);
			$form->AddCancel();
			$form->AddAction("SUBMIT",true,"","","SUBMIT EDIT");	// validate before sending
			$form->AddHidden("trackuid",$_POST['trackuid'][0]);
			$aspect->Attach($form);
			$aspect->Present();
			$rows = Database::QueryGetResults("SELECT name FROM bands;");
			foreach ($rows as $row) $bands[]=$row['name'];
			print '<script language="javascript" type="text/javascript">'."\n";
			print "AutoComplete_Create('remixer', ['".implode("','",$bands)."'].sort(), 6);\n";
			print '</script>'."\n";

			break;
		}
		
		case "SUBMIT EDIT":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			if ($aspect->GetVar('trackuid')!=$aspect->GetVar('uid'))
			{
				if (DataTable::GetIDExist("tracks",$aspect->GetVar('uid')))
				{
					Error("That catologue number is already in use. Operation aborted","tracks");
					$aspect->Present();
					break;
				}
				Database::Query("UPDATE tracksproduct SET track='".$aspect->GetVar("uid")."' WHERE track='".$aspect->GetVar("trackuid")."';");
				Database::Query("UPDATE sale SET product='".$aspect->GetVar("uid")."' WHERE product='".$aspect->GetVar("trackuid")."';");
				Database::Query("UPDATE licences SET licenceduid='".$aspect->GetVar("uid")."' WHERE licencetype='T' AND licenceduid='".$aspect->GetVar("trackuid")."';");
			}
			
			DataTable::UpdateFromPost("tracks",$aspect->GetVar('trackuid'),"uid,trackid,artist,title,version,remixer,publisher,tracklength,comments",$aspect);
			break;
		}
		
		case "ADD TRACKS":
		{
			if (!isset($_POST['addcount'])) $_POST['addcount']=1;
			HeaderBar("Add New Track (".$_POST['addcount'].")");

	//		print "Track ".$_POST['addcount']."<hr>";
			if ($_POST['addcount']==1)
			{
				$uidresult = Database::QueryGetResult("SELECT uid FROM tracks ORDER BY uid DESC LIMIT 1;");
				$uid = CreateNewCatNumber($uidresult['uid']);
			}
			else
			{
				$uid = CreateNewCatNumber($_POST['uid']);
			}
			$form = new ValidatedForm("tracks","uid,trackid,artist,title,version,remixer,publisher,tracklength,comments");

			if ($_POST['addcount']>1) 
				$form->SetValuesFromPost($aspect,"artist,publisher");
			$form->AddHidden('addcount',$_POST['addcount']+1);
//			$form->SetValuesFromPost($aspect);
			$form->SetValue("uid",$uid);
			$form->AddCancel("BACK");
			$form->AddAction("SAVE AND CONTINUE",true,"","","SAVE");	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			
			$rows = Database::QueryGetResults("SELECT name FROM bands;");
			foreach ($rows as $row) $bands[]=$row['name'];
			print '<script language="javascript" type="text/javascript">'."\n";
			print "AutoComplete_Create('remixer', ['".implode("','",$bands)."'].sort(), 6);\n";
			print '</script>'."\n";

			break;
		}
		
		case "SAVE":
		{
			$aspect->SetNextAction("ADD TRACKS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$result = DataTable::InsertFromPost("tracks","uid,trackid,artist,title,version,remixer,publisher,tracklength,comments",$aspect);
			$aspect->SetVar('trackuid',$aspect->GetVar('uid'));
			break;
		}
	}
}

$aspect->End();

?>
