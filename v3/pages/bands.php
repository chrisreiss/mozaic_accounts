<?php
require_once( 'util.php');
require_once( 'cForm.php');
require_once( 'cDataTable.php');
require_once( 'cList.php');
require_once( 'cAspect.php');
require_once( 'cDatabase.php');
require_once( 'cStatement.php');
require_once( 'cFlexForm.php' );
require_once( 'cValidatedForm.php');
require_once( 'strings.php' );
require_once( 'cEmail.php' );
require_once( 'forms.php' );


//---- This class creates the main view ----//
require_once( 'cSimpleList.php' );

class BandList extends SimpleList {
		
	protected function actions() 
	{
		$this->addAction("ADD ARTIST","New Band Member",SL_NOSELECTION);
		$this->addAction("ADD BAND","New Band",SL_NOSELECTION);
		$this->addAction("EDIT BAND","Edit Band",SL_SINGLESELECTION);
		$this->addConfirmAction("REMOVE BAND","Delete Band",SL_MULTIPLESELECTION);
		$this->addAction("MANAGE STATEMENTS","Account To",SL_SINGLESELECTION);
//		$this->addAction("EDIT ARTIST","All Band Members",SL_NOSELECTION);

	}
	
	protected function populate()
	{
		$this->_header = array('BAND','MEMBERS','LAST ACCOUNTED TO');
		$bands = Database::QueryGetResults('SELECT uid,name FROM bands ORDER BY name;');
		$todaysdate = time();
		foreach($bands as $band)
		{
			$members = Database::QueryGetResults("SELECT bandartist.artist,artist.realname FROM bandartist JOIN artist WHERE artist.uid=bandartist.artist AND bandartist.band='".$band['uid']."';");
			$memberslist = "";
			if (!empty($members))
				foreach($members as $member)
				{
					if (!empty($memberslist)) $memberslist .=",";
					$memberslist .= $member['realname'];
				}
				
			$statements = Database::QueryGetResults("SELECT enddate FROM artiststatements WHERE artist='".$band['uid']."' ORDER BY enddate DESC LIMIT 1");
			$this->_rows[] = array('uid'=>$band['uid'],'bandname'=>$band['name'],'members'=>$memberslist,'lastdate'=>$statements[0]['enddate']);
		}
		
		return true;
	}
	
	protected function containerpage()
	{
		return "bands";
	}

	public function BandList($name)
	{
		$this->_listname = $name;
		$this->_key = 'uid';
		$this->_hidekey = true;
	}
}



Database::Init();
	//		print_r($_POST); print '<br>';

$aspect = new Aspect;
$aspect->Start("bands","VIEW");
while (!$aspect->IsComplete())
{
//	print $aspect->GetAction();
	switch($aspect->GetAction())
	{
		case "VIEW":
		{
			$productlist = new BandList('bandid');
			$productlist->Present();
			$aspect->Present();
			break;
		}

		case "QUICK ADD ARTISTBAND":
		{
			// Artist realname
			// Contact Details
			// Member of bands (automatic split) can edit later. Add a new one
			print '<h2>New Band</h2>';
//			print 'This quick form lets you create a producer artist (i.e. one member)<br>';
			$form = new ValidatedForm("artist","realname,email,address1,address2,city,postcode,country,phone");
//			$form->AddField_FromTable("bands","name");
//			$form->StartGroup(0,9,true);
				$form->AddField("Member of","bands","bandauto",true);
//			$form->EndGroup();

			$form->AddCancel();
			$form->AddAction("SUBMIT ARTISTBAND",true);	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT ARTISTBAND":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$artistuid = DataTable::InsertFromPost("artist","realname,email",$aspect);
			$banduid = DataTable::InsertFromPost("bands","name",$aspect);
			DataTable::Insert("bandartist","band,artist,split",$banduid,$artistuid,100);
			break;
		}
		case "REMOVE ARTIST":
		{
			$aspect->SetNextAction("VIEW");
							
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// We need to go from bandartist uid to artist uid
			$ids = getVar('bandid');
			foreach($ids as $id)
			{
				DataTable::DeleteMultiple("bandartist","artist='$id'");
				DataTable::Delete("artist",$artistid);
			}
			break;
		}
		case "REMOVE BAND":
		{
			$aspect->SetNextAction("VIEW");				
//			popup('Do you want to remove bandmembers in the system that will no longer be in any bands?');
//			break;
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			$ids = getVar('bandid');

			foreach($ids as $id)
			{
				DataTable::DeleteMultiple("bandartist","band='$id'");
				DataTable::Delete("bands",$id);
//				$bandid = DataTable::GetFieldFromTable("bandartist",$aspect->GetVar("bandartistid"),"band");
			}
			
			// Check if artists need to be deleted
			$artists = Database::QueryGetResults("SELECT uid FROM artist");
			foreach($artists as $artist)
			{
				$res = Database::QueryGetResult("SELECT band FROM bandartist WHERE artist=".$artist['uid']);
				if (empty($res))
					$memberstodelete[] = $artist['uid'];
				
			}
			
			if (isset($memberstodelete))
			{
				HeaderBar("Remove Band");
				form_Begin("");
				print 'There are artists that are no longer in any bands, do you wish to remove these artists entirely from the system?';
				print '<input type=hidden name=page value="bands">';
				print '<table><tr><td width=100>Delete</td><td>Artist</td></tr>';
				foreach($memberstodelete as $member)
				{
					print '<tr><td><input type=checkbox name=deleteartist[] value='.$member.' checked></td>';
					$realname = Database::QueryGetResult("SELECT realname FROM artist WHERE uid='".$member."'");
					print '<td>'.$realname['realname'].'</td></tr>';
				}
				print '<td>';
				print button_Submit('Remove',"REMOVE ARTISTS",$addcode="");
				print '</td><td></td>';
				form_End();
				
				$aspect->Present();
			}
			break;
		}
		case "ADD ARTIST":
		{
			HeaderBar('New Band Member');
			$form = new ValidatedForm("artist","realname,address1,address2,city,postcode,country,email,phone");
//			$form->SetTitle('New Band Member');
			$form->AddCancel("BACK");
			$form->AddAction("SUBMIT",true,"","","SUBMIT NEW ARTIST");	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW ARTIST":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			$_SESSION['addartist'] = DataTable::InsertFromPost("artist","realname,address1,address2,city,postcode,country,email,phone",$aspect);
			break;
		}
		case "ADD BAND":
		{
			HeaderBar('New Band');
			$form = new ValidatedForm();
//			$form->SetTitle('New Band');
			$form->AddField_FromTable("bands","name");
			$form->StartGroup(0,9,true);
				$form->AddField("Band Member","member","artist",true);
			$form->EndGroup();
			$form->AddCancel("BACK");
			$form->AddAction("SUBMIT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM,"SUBMIT NEW BAND");	// validate before sending
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT NEW BAND":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			$i = 0;
			// check if any band members
			if (ValidatedForm::GetGroupFieldFromPost("member",$i)==false)
			{
				$aspect->Error("Cannot add a band with no members");
				break;
			}

			DataTable::InsertFromPost("bands","name",$aspect);
			$bandid = DataTable::GetID("bands","name",$aspect->GetVar('name'));
			for ($i=0;$i<999;++$i)
			{
				$value = ValidatedForm::GetGroupFieldFromPost("member",$i);
				if ($value==false)
					break;
				$members[] = $value;
			}
			if ($i>0)
			{
				$split = 100 / $i;
				for ($j=0;$j<$i;++$j)
				{
					DataTable::Insert("bandartist","band,artist,split",$bandid,$members[$j],$split);
				}
			}
			break;
		}
		case "EDIT BAND":
		{
					HeaderBar('Edit Band');

			// Bit more complex from usual - because bandartist table references two other tables - band and artist!
			$form = new ValidatedForm();

			// Need to get the band from the bandartist entry the user clicked on
			//$bandid = DataTable::GetFieldFromTable("bandartist",$aspect->GetVar('bandartistid'),"band");
			$bandid = getVarS('bandid');
			$form->AddField_FromTable("bands","name");
			
			// Get the band name from band table
			$bandname = DataTable::GetFieldFromTable("bands",$bandid,"name");
			$form->SetValue("name",$bandname);
			
			// Then fill out the band members - declare the first then fill the values
			$form->StartGroup(0,9,true);
				$form->AddField("Split","split","percentage",true);
				$form->AddField("Band Member","member","artist",true);
			$form->EndGroup();
		
			// Fill in the band members from the database
			$result = Database::Query("SELECT artist,split FROM bandartist WHERE band='$bandid';");
			for ($i=0;$i<$result->GetNum();++$i)
			{
				$row = $result->GetNext();
				// Set group value will cause these to appear! (I hope)
				$form->SetGroupValue($i,"member",$row['artist']);
				$form->SetGroupValue($i,"split",$row['split']);
			}
			Database::FinishQuery($result);
			
			// usual stuff
			$form->AddCancel();
			$form->AddAction("SUBMIT",true,ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM,"SUBMIT EDIT BAND");	// validate before sending
			$form->AddHidden("bandartistid",$aspect->GetVar('artistid'));
			$form->AddHidden("bandid",$bandid);
			$form->AddCustomScript("if (!CheckSplit()) {alert('Artist splits do not add up to 100%'); return false;}");
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "SUBMIT EDIT BAND":
		{
			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }
			$bandid = $aspect->GetVar("bandid");

			// TODO - Check if remove artists are not in any bands.
			// get existing bands, compare to posted
			$currentband = Database::QueryGetResults("SELECT artist FROM bandartist WHERE band='".$bandid."'");
			if (!empty($currentband))
			{
				foreach($currentband as $member)
				{
					$found = false;
					for ($i=0;$i<999;++$i)
					{
						$value = ValidatedForm::GetGroupFieldFromPost("member",$i);
						if ($value==false) break;
						if ($member['artist']==$value) { $found=true; break; }
					}
					if (!$found) $removedmembers[]=$member['artist'];
				}
			}	
			
			DataTable::UpdateFromPost("bands",$bandid,"name",$aspect);
			// Wipe all band members from the database
			DataTable::DeleteMultiple("bandartist","band='$bandid'");
			for ($i=0;$i<999;++$i)
			{
				$value = ValidatedForm::GetGroupFieldFromPost("member",$i);
				if ($value==false)
					break;
				$split = ValidatedForm::GetGroupFieldFromPost("split",$i);
				DataTable::Insert("bandartist","band,artist,split",$bandid,$value,$split);
			}
			
			// Check if artists are in any bands
			if (isset($removedmembers))
			{
				foreach($removedmembers as $member)
				{
					$res = Database::QueryGetResult("SELECT COUNT(band) FROM bandartist WHERE artist='".$member."'");
					if (empty($res) || $res['COUNT(band)']==0) $memberstodelete[]=$member;
				}
				unset($removedmembers);
			}
						
			if (isset($memberstodelete))
			{
				form_Begin();
		//		print '<form method=post action=index.html>';
				print 'There are artists that are no longer in bands, do you wish to remove these artists entirely from the system?';
				print '<input type=hidden name=page value="artists">';
				print '<table><tr><td width=100>Delete</td><td>Artist</td></tr>';
				foreach($memberstodelete as $member)
				{
					print '<tr><td><input type=checkbox name=deleteartist[] value='.$member.' checked></td>';
					$realname = Database::QueryGetResult("SELECT realname FROM artist WHERE uid='".$member."'");
					print '<td>'.$realname['realname'].'</td></tr>';
				}
				print '<td>';
				print button_Submit('Remove these Artists',"REMOVE ARTISTS",$addcode="");
				print '</td><td></td>';
				form_End();
				$aspect->Present();
			}

			break;
		}
		case "REMOVE ARTISTS":
		{
			$aspect->SetNextAction("VIEW");
			if (isset($_POST['deleteartist']))
			{
				foreach($_POST['deleteartist'] as $artistid)
					DataTable::Delete("artist",$artistid);
			}
			break;
		}
		
		case "SUBMIT EDIT ARTIST":
		{
			$aspect->SetNextAction("VIEW");

			DataTable::UpdateFromPost("artist",getVar('artistid'),"realname,address1,address2,city,postcode,country,email,phone",$aspect);
		}
		case "EDIT ARTIST":
		{
			HeaderBar();

			// We need to go from bandartist uid to artist uid
			//$addcode = "onChange='changed=true; validateform.submitvalue.disabled=false;';";
			$addcode = "onChange='document.getElementById(\"SUBMIT EDIT ARTIST\").disabled=false;'";
			form_Begin("Edit Artist","artist");
			if (getVar('lastartist')!=getVar('oldname'))
			{
				// save the artist
			}
			$artistid = getVar('artistid',1);
			$artistdetails = Database::QueryGetResult("SELECT * FROM artist WHERE uid='".$artistid."'");
			form_Field("Artist",	input_ArtistDropdown("artistid",$artistid,"onChange='submitForm(this);'"));
			form_Field("Name",		input_Text("realname",$artistdetails['realname'],$addcode));
			form_Field("Email",		input_Text("email",$artistdetails['email'],$addcode));
			form_Field("Address",	input_Text("address1",$artistdetails['address1'],$addcode));
			form_Field("",			input_Text("address2",$artistdetails['address2'],$addcode));
			form_Field("City",		input_Text("city",$artistdetails['city'],$addcode));
			form_Field("Postcode",	input_Text("postcode",getVar("postcode"),$addcode));
			form_Field("Country",	input_Text("country",getVar("country"),$addcode));
			form_Field("Phone",		input_Text("phone",getVar("phone"),$addcode));
			print input_Hidden("page",getVar("page"));
			print input_Hidden("lastartist",getVar("oldname"));
			print input_Hidden("action",'EDIT ARTIST');
			form_Field("", button_Submit("Save","SUBMIT EDIT ARTIST","disabled").button_Submit("Cancel","VIEW").'</td></tr>');
			form_End();
			
			$aspect->Present();
			break;
		}
		case "REMOVESTATEMENT":
		{
			$pdfhash = $aspect->GetActionData();
			$query = "DELETE FROM artiststatements WHERE pdfhash='".$pdfhash."';";
//			print $query;
			$aspect->SetNextAction("MANAGE STATEMENTS");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			Database::Query($query);
			unlink(Statement::MakeStatementFilename2($pdfhash));
			break;
		}
		case "MANAGE STATEMENTS":
		{
			$bandid = getVarS('bandid');
			$bandname = Database::QueryGetResult("SELECT name FROM bands WHERE uid='".$bandid."'");
			
			HeaderBar($bandname['name']."'s Accounts");


			$statements = Database::QueryGetResults("SELECT * FROM artiststatements WHERE artist='".$bandid."' ORDER BY startdate,pdfhash");
			print "<form class='mform' method=post action=index.php><input type=hidden name=page value=bands>";
			print "<input type=hidden name=bandid value='".$bandid."'>";
//			print "<input type=hidden name=bandartistid value='".$aspect->GetVar('bandartistid')."'>";
			print "<table width=100% class='statements'>";
			if (date("m",time())>6)
			{
				$startdate  = date("Y-m-d",mktime(0, 0, 0, 1, 1, date("Y",time())));
				$enddate  = date("Y-m-d",mktime(0, 0, 0, 6, 30, date("Y",time())));
			}
			else
			{
				$startdate  = date("Y-m-d",mktime(0, 0, 0, 7, 1, date("Y",time())-1));
				$enddate  = date("Y-m-d",mktime(0, 0, 0, 12, 31, date("Y",time())-1));
			}

			$reserve = 0;
			if (!empty($statements))
			{

				print "<span class='formtitle'>Previous Statements</span><br>";
				print "<tr><th>Period</th><th>Band Member</th><th>Paid to Artist</th><th>Balance Carried</th><th>Advances to Recoup</th><th>Actions</th></tr>";
				$laststartdate = "";
				$lastenddate = "";
				$lastpdfhash = "";
				$total_amount = 0;
				$maxi = count($statements)-1;
				foreach ($statements as $i=>$statement)
				{
					if (($i!=0 && $laststatement['pdfhash']!=$statement['pdfhash'] )) 
					{
						print '<tr>';
						print '<td>';
						print date("d-m-Y",strtotime($laststatement['startdate']))." to ".date("d-m-Y",strtotime($laststatement['enddate']));
						print "</td><td>";
						if ($laststatement['bandmember']!=0) print TableInfo::DisplayFromType("artist",$laststatement['bandmember']);
						print "</td>";
						print "<td>".Currency::DISPLAY($total_amount)."</td><td>".Currency::DATABASETODISPLAY($laststatement['reserve'])."</td>";									print "<td>".Currency::DATABASETODISPLAY($laststatement['advancerecoup'])."</td>";					
						print "<td><button type=submit name=submitvalue class=clear value='REMOVESTATEMENT:".$laststatement['pdfhash']."'><img src='images/deleteiconwhite.png'></button>\n";
						
						$total_amount = 0;
						$outfilename = "statement_".$laststatement['pdfhash'].".pdf";
						print "<a href='download.php?file=".$outfilename."'><img src='images/downloadiconwhite.png'></a></a></td>\n";
						$laststartdate = $laststatement['startdate'];
						$lastenddate = $laststatement['enddate'];
						
						print '</tr>'; 
					}
					// if not a compilation - then tot it up
					if ($statement['crossdealid'][0]!='C') 
						$total_amount += Currency::GETRAWVALUE($statement['amount']);
					
					$laststatement = $statement;
					
					// Update our next statement dates
					$nextdate = strtotime($statement['enddate']);
					$startdateraw = mktime(0, 0, 0, date("m",$nextdate),  date("d",$nextdate)+1,  date("Y",$nextdate));
					$startdate  = date("Y-m-d",$startdateraw);
					$enddate  = date("Y-m-d",mktime(0, 0, 0, date("m",$startdateraw)+6,  date("d",$startdateraw),  date("Y",$startdateraw))-1);
					$reserve = $statement['reserve'];

				}
				
				// last time
				print "<tr>";
				print "<td>";
				print date("d-m-Y",strtotime($laststatement['startdate']))." to ".date("d-m-Y",strtotime($laststatement['enddate']));
				print "</td><td>";
				if ($laststatement['bandmember']!=0) print TableInfo::DisplayFromType("artist",$laststatement['bandmember']);
				print "</td><td>".Currency::DATABASETODISPLAY($total_amount)."</td><td>".Currency::DATABASETODISPLAY($laststatement['reserve'])."</td>";
			print "<td>".Currency::DATABASETODISPLAY($laststatement['advancerecoup'])."</td>";
				print "<td><button type=submit name=submitvalue class=clear value='REMOVESTATEMENT:".$laststatement['pdfhash']."'><img src='images/deleteiconwhite.png'></button>\n";
				$outfilename = "statement_".$laststatement['pdfhash'].".pdf";
				print "<a href='download.php?file=".$outfilename."'><img src='images/downloadiconwhite.png'></a></td>\n";
				$laststartdate = $laststatement['startdate'];
				$lastenddate = $laststatement['enddate'];
				
				print "</tr>"; 
				
				// Update our next statement dates
				$nextdate = strtotime($statement['enddate']);
				$startdateraw = mktime(0, 0, 0, date("m",$nextdate),  date("d",$nextdate)+1,  date("Y",$nextdate));
				$startdate  = date("Y-m-d",$startdateraw);
				$enddate  = date("Y-m-d",mktime(0, 0, 0, date("m",$startdateraw)+6,  date("d",$startdateraw),  date("Y",$startdateraw))-1);
				$reserve = $statement['reserve'];

				// end repeat
				
				print "</table><hr><br>";
			}
			else
			{
				$reserve = false;
				print "<span class='formtitle'>No Statement History</span><hr><br>";
			}

			print "</form>\n";

			$form = new FlexForm();
			$form->SetTitle("Create New Statement");
			$form->SetAspect($aspect);
			$form->SetTarget('bands');
			$results = Database::QueryGetResults("SELECT artist FROM bandartist WHERE band='".$bandid."'");
			foreach($results as $r) { $artists[$r['artist']] = Database::QueryGetValue("realname","artist","uid='".$r['artist']."'"); }
			if (count($results)>1)
				$form->AddField(FieldFactory::CreateDropdown("artist")->Description("Artist")->SetOptions($artists));
			else
				$form->AddField(FieldFactory::CreateHidden("artist",$results[0]['artist']));
				
			$form->AddField(FieldFactory::CreateCheckbox("treatasthirdpartycomp")->Description("Treat in-house compilations as third party")->Set(true));
			$form->AddField(FieldFactory::CreateCheckbox("include_individual_sales_in_comp")->Description("Individual sales in comps get calculated in comp profit/loss")->Set(false));
			$form->AddField(FieldFactory::CreateCheckbox("includecomps")->Description("Include compilation calculations in artist copy")->Set(true));
			$form->AddField(FieldFactory::CreateDate("startdate")->Description("Deal Start Date")->Set($startdate));
			$form->AddField(FieldFactory::CreateDate("enddate")->Description("Deal End Date")->Set($enddate));
			$form->AddField(FieldFactory::CreateHidden("band",$bandid));
			$form->AddAction(Action::Create("BACK"));
			$form->AddAction(Action::Create("CALCULATE STATEMENT"));
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "CALCULATE STATEMENT":
		{
			HeaderBar("Statement");
			$bandlist = $_POST['band'];
			$startdate = implode('-', array_reverse(explode('/', $aspect->GetVar('startdate'))));
			$enddate = implode('-', array_reverse(explode('/', $aspect->GetVar('enddate'))));
			// Generate the statement
//			$startdate = $aspect->GetVar('startdate');
//			$enddate = $aspect->GetVar('enddate');
			$statement = new Statement;

			$includecomps = $aspect->GetVar('includecomps');
			$statement->Generate(true,$aspect->GetVar("artist"),$bandlist,$startdate,$enddate,true,$aspect->GetVar('treatasthirdpartycomp'),$aspect->GetVar('include_individual_sales_in_comp'));
			$statement->Display();


			$pdfhash = Statement::MakeStatementHash($aspect->GetVar("artist"),$bandlist,$startdate,$enddate);
			//print $pdfhash." = ".$aspect->GetVar("artist").",".$bandlist.",".$startdate.",".$enddate."<br>";
			$outfilename = Statement::MakeStatementFilename2($pdfhash);
			if ($statement->IsComplete())
			{
				// Store this for later			
				$_SESSION['results'] = $statement->GetResults();
				// If we've been doing this at wrong detail level	
				if ($includecomps==false){
					$statement->Generate(false,$aspect->GetVar("artist"),$bandlist,$startdate,$enddate,true,$aspect->GetVar('treatasthirdpartycomp'),false); //$aspect->GetVar('include_individual_sales_in_comp'));
				}
				$statement->SaveAsPDF($outfilename);
//				print "<a href='$outfilename' title='right or alt click to download'>PDF file</a><br>";
			}
			
			$form = new Form;
			$form->Begin("bands");
			$form->AddHidden("artistid",$aspect->GetVar("artist"));
			$form->AddHidden("startdate",$startdate);
			$form->AddHidden("enddate",$enddate);
			$form->AddHidden("pdfhash",$pdfhash);
			$form->AddCancel();
			if ($statement->IsComplete())
			{
				$email = Database::QueryGetValue("email","artist","uid='".$aspect->GetVar("artist")."'");
				$form->AddHidden("email",$email);
				$form->AddHidden("filename",$outfilename);
				if ($email!="")
				{	
					$form->AddAction("MAIL STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
				}
				else
				{
					print "<p>There is no email address registered for this artist. If you fill in this detail we can send it directly to them</p>";
				}
				$form->AddAction("SAVE STATEMENT",ACTION_TOOLTIP,STRING_TOOLTIP_BUTTON_CONFIRM);
			}
			else
				print "<p>This statement is not complete and so can not be saved. It may have no data in it, or require entry of balance information</p>";
			$aspect->Attach($form);
			$aspect->Present();
			break;
		}
		case "MAIL STATEMENT":
		{
			$result = Database::QueryGetResult("SELECT name,invoiceaddress FROM accounts WHERE uid='".$_SESSION['accountid']."';");
			$address = DataTable::GetEntryFromID("addresses",$result['invoiceaddress']);
			$attachment[] = array("file"=>$aspect->GetVar("filename"),"content_type"=>"pdf");
			Email::Send($aspect->GetVar('email'), "Please find attached your royalty statement.", "Royalty Statement", "tom@discerningear.co.uk", $_SESSION['accountname'], $attachment);
		}
		case "SAVE STATEMENT":
		{

			$aspect->SetNextAction("VIEW");
			if (IsDemoAccount()) { $aspect->Error(STRING_DEMO); break; }

			// Get session
			foreach ($_SESSION['results'] as $key=>$result)
			{
				$query = "INSERT INTO artiststatements SET artist='".$result['band']."',bandmember='".getVarS('artistid')."',crossdealid='".$key."',amount='".$result['payable']."',reserve='".$result['balance']."',startdate='".$_POST['startdate']."',enddate='".$_POST['enddate']."',pdfhash='".$_POST['pdfhash']."',advancerecoup='".$result['advancerecoup']."';";
				Database::Query($query);
			}
			// don't need now
			unset($_SESSION['results']);			
			$aspect->SetNextAction("VIEW");
			break;
		}
		default:
		{
			$aspect->DefaultAction();
		}
	}
}
$aspect->End();		
?>


