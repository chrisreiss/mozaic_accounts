<?php	
	ini_set("include_path", "./:./classes:./thirdparty:./localise");
	ini_set('display_errors', 'On');
	error_reporting(E_ERROR);


	require_once("headerfooter.php");
	if (!Authorize::IsAuthorized())
	{
		if ($_SERVER['SERVER_NAME']!='local.mozaic')
			header('Location:https://cyphon.com/login.php?error=4');
		else
			header('Location:http://local.mozaic/login.php?error=4');
	}

	$firsttime = false;
	if (empty($_POST['page']))
	{
		if (empty($_GET['page']))
		{
			$page="bands";
			$firsttime = true;
		}
		else
			$page=$_GET['page'];
	}
	else
	{
		$page = $_POST['page'];
	}

	displayHeader();
	
	if (!empty($_SESSION))
	{

		//displayMenuBar();
		displayStartDynamic();
		print "	<!DYNAMIC CONTENT OF PAGE ".$page." BEGINS HERE>\n";
		
		$fullpage = 'pages/'.$page.".php";
		if (file_exists($fullpage))
		{
			include $fullpage;
		}
		else
		{
			$fullpage = 'pages/'.$page.".htm";
			if (file_exists($fullpage))
				include $fullpage;
			else
			{
				print "Cannot find requested page $page";
			}
		}
		displayEndDynamic();
	}
	else
	{
		include "present.php";
	}
//	displayFooter();
?>
