  <script type="text/javascript">
  $(document).ready(function(){
    $("#tabs").tabs({ fx: { opacity: 'toggle' } });
  });
  </script>
  
<div id="tabs"> 
  <ul> 
  <li><a href="#welcome">Welcome</a></li> 
  <li><a href="#sales">Sales</a></li> 
  <li><a href="#licensing">Licensing</a></li> 
  <li><a href="#accounting">Accounting</a></li> 
  <li><a href="#promos">Promos</a></li> 
  <li><a href="#more">More</a></li>
  <li><a href="#customers">Customers</a></li>
  <li><a href="#contact">Contact Us</a></li> 
</ul> 
 
<div id="welcome">
<table width=100% cellpadding="8"><tr><td valign=top>
<p>Mozaic Accounts is a web service that cuts the chore of running a record label, letting you concentrate on more important things - the music.</p>
<p>At the heart of our service is a powerful royalty accounting system that will calculate how much you should be paying your artists as well as producing detailed statements. No more spending hours staring at a spreadsheet.</p>
<p>Access the service anywhere in the world via a browser, our backed-up servers run 24/7 and keep your data safe and secure.</p>
<p>We charge a small monthly subscription and our flexible pricing structure allows you to chose the features you require that meets your budget.</p>
<p>The system was designed by record labels for record labels, its simple, fast and effective. We believe small is beautiful.</p>
</td><td><img width=300px src="images/records.jpg" border=1></td></tr></table>
</div>

<div id="sales">
<p>If you sell your music through a number of different retail channels or digital online sites then sorting through your sales statements can be tiresome especially for digital sales when each individual sale has its own entry.</p>
<p>Mozaic accounts provides a single place to collate all your sales data and then process and analyse them. Find out what territories and shops are performing best for you.</p>
<p>Our flexible import system can load sales sheets directly into your account from any number of pre-defined or custom formats. It will then process all the individual items and store the data in a central database.</p>
<p>We support any combination of physical, digital and streaming revenue income channels.</p>
</div>

<div id="licensing">
<table width=100% cellpadding="8"><tr><td valign=top>
<p>Our licensing module is an invaluable feature for labels that license their music to third-party compilations, territories or for labels that license in music from other providers.</p>
<p>Store the details of your licensing deals in our system and it will keep track of who owes you what.</p>
<p>Generate reminders to your licensees and then when your licensing income comes in this can be included as part of the artists royalty calculation.</p>
<p>Visa verse you can produce royalty statements for your licensors at a click of a button from your sales data.</p>
</td><td><img width=300px src="images/screenshot3crop.jpg" border=1></td></tr></table>
</div>

<div id="accounting">
<table width=100% cellpadding="8"><tr><td valign=top>
<p>The accounting system inside mozaic account allows you to set up all kinds of deals with your artists.</p>
<p>The system provides both net receipts/profit split and royalty based deals. It supports featured artist and compilation calculations accordingly.</p>
<p>Set up reserves, minimum payouts. Log advances paid, categorise and assign expenses.</p>
<p>Review previous artist statements, set your accounting period and then press go. Carry over balances and income will be analysed to produce an easy to read royalty statement that can be mailed directly to the artist as a pdf.</p>
</td><td><img width=300px src="images/screenshot1crop.jpg" border=1></td></tr></table>
</div>

<div id="promos">
<p>Our brand new promotional distribution system provides our customers a cost effective solution for getting their upcoming releases into the hands of DJ, press and taste makers.</p>
<p>Manage multiple mailing lists, set up a campaign quickly using the catalogue data already in the system. Just write a promo text and upload your promo tracks to our servers.</p>
<p>Your recipients will receive a customizable html email with a unique link to a feedback page where they can preview and download the tracks.</p>
<p>View the success of your campaign using our reporting tools and then gather DJ reactions into a press release automatically. It couldn't be simpler.</p>
</div>

<div id="more">
<table width=100% cellpadding="8"	><tr><td valign=top>
<p>Having your catalogue and sales data on the system means that we can provide all sorts of other useful functions.</p>
<p>Managing physical stock? We have stock tracking tools built in that comes for free, keep tabs on who has what.</p>
<p>Export product meta data to a number of different popular stores format. We hate doing anything twice so if we can find a way to automate something we will.</p>
<p>Gain a unique insight into your business using a number of different analytical and reporting functions.</p>
<p>Let us know what you would find useful. We always want to improve our service - mozaic is constantly evolving as the market changes.</p>
</td><td><img width=300px src="images/screenshot2crop.jpg" border=1></td></tr></table>
</div>

<div id="customers">
<table width=100% cellpadding="8"	><tr><td valign=top>
<p>Our customers include amoungst others some of the leading dance music labels in the industry such as:</p>
<ul><li>Freerange Records</li>
<li>Simple / Aus Music</li>
<li>Urbantorque</li>
<li>Rekids</li>
<li>Propa Talent</li>
<li>Paper Recordings</li></ul>
</td><td width=300px><p class='quote'><i>'Moziac is an easy to use and essential business tool for all record labels that want to minimize the headache of accounting. It allows you keep a tight record of costs/sales throughout the year which means you spend less time crunching numbers and more time on the creative aspects of releasing music.'</i><br>Will Saul/Simple Records</p>
</td></tr></table>

</div>


<div id="contact">
<p>Get in touch with us to find out more information about mozaic accounts or organise a free 30-day trial.</p>
<p>Sales: <a href="mailto:sales@moziacaccounts.com">sales@mozaicaccounts.com</a></p>
<p>Corporate: <a href="mailto:info@mozaicaccounts.com">info@mozaicaccounts.com</a></p>
<p>Mozaic Accounts Ltd. is a limited company and is registered in the UK.</p>
</div>

</div> 