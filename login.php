<?php
	session_start();
	session_unset();
	srand();
	$challenge = "";
	for ($i = 0; $i < 80; $i++) 
	{
    	$challenge .= dechex(rand(0, 15));
	}
	$_SESSION['challenge'] = $challenge;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
<title>Mozaic Accounts Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="v3/theme1.css" rel="stylesheet" type="text/css">
<SCRIPT type="text/javascript" SRC="md5.js"></SCRIPT>
</head>

<body>

<script type="text/javascript">
    function login() {
        var loginForm = document.getElementById("loginForm");
        if (loginForm.userid.value == "") {
            alert("Please enter your user name.");
            return false;
        }
        if (loginForm.password.value == "") {
            alert("Please enter your password.");
            return false;
        }
        try {
        var submitForm = document.getElementById("submitForm");
        submitForm.userid.value = loginForm.userid.value;
        submitForm.response.value = md5(loginForm.challenge.value+loginForm.password.value);
        } catch(ex) { alert(ex); }
        submitForm.submit();
        return true;
    }
</script>


<div id="container">


<div id='header'>
</div>
	
<div id='main'>
<table width=100% height=200><tr><td align=center>

<?php        
    if (isset($_GET['error']))
    {
        if ($_GET['error']==2) { print '<p style="color:red">Incorrect login details please try again<br></p>'; } //<a href="remind.php" style="color:red">Forgotten your password?</a></p>'; }
        if ($_GET['error']==1) { print '<p style="color:red">Internal error please inform support</p>'; }
        if ($_GET['error']==3) { print '<p style="color:red">Reminder sent please check your email</p>'; }
        if ($_GET['error']==4) { print '<p style="color:red">Sorry but you timed out. Please log back in</p>'; }
    }
?>


<form id="loginForm" method="post" action="processlogin.php">
			<table><tr>
			<td>Login</td><td>
			<input type="text" name="userid">
			</td></tr><tr><td>Password</td><td>
			<input type="password" name="password">
			<input type="hidden" name="challenge" value="<?php print $challenge; ?>">
			</td></tr></table>
			<input type="submit" class="submit" name="submit" value="LOGIN" onclick="login(); return false;"></form>
			
<form id="submitForm" action="processlogin.php" method="post">
            <div>
                <input type="hidden" name="userid"/>
                <input type="hidden" name="response"/>
            </div>
        </form>
     

	
</td></tr></table>
</div>	


</body>
</html>

