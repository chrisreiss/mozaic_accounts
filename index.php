<?php
	session_start();
	srand();
	$challenge = "";
	for ($i = 0; $i < 80; $i++) 
	{
    	$challenge .= dechex(rand(0, 15));
	}
	$_SESSION['challenge'] = $challenge;
?>
<html>
<head>
	<title>Mozaic Accounts</title>
	<META NAME="keywords" CONTENT="royalties,accounting,music,record label,application,statements,licencing,calculating">
	<META NAME="description" CONTENT="Affordable On-line Royalty Accounting Solutions for the Music Industry">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="mozaichome.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="ui.core.js"></script>
	<script type="text/javascript" src="ui.tabs.js"></script>
	<SCRIPT type="text/javascript" SRC="md5.js"></SCRIPT>

</head>

<body>
<script type="text/javascript">
    function login() {
        var loginform = document.getElementById("loginform");
        if (loginform.userid.value == "") {
            alert("Please enter your user name.");
            return false;
        }
        if (loginform.password.value == "") {
            alert("Please enter your password.");
            return false;
        }
        loginform.response.value = md5("<?php echo $challenge;?>"+loginform.password.value);
        console.log("respone = " + loginform.response.value);
        loginform.password.value = null;		// clear the password so its not posted
        loginform.submit();
        return true;
    }
</script>
<div id="wrapper">


<div id="container">
	<div id="logoholder">
		<div id="logo">
		    <div class="square" id="square1"></div>
		    <div class="square" id="square2"></div>
		    <div class="square" id="square3"></div>
		    <div class="square" id="square4"></div>
		    <div class="square" id="square5"></div>
		    <div class="square" id="square6"></div>
		    <div class="square" id="square7"></div>
		    <div class="square" id="square8"></div>
		    <div class="square" id="square9"></div>
		    <div class="square" id="square10"></div>
		    <div class="square" id="square11"></div>
		    <div class="square" id="square12"></div>
		    <div class="square" id="square13"></div>
		    <div class="square" id="square14"></div>
		    <div class="square" id="square15"></div>
		    <div class="square" id="square16"></div>
		    <div class="square" id="square17"></div>
		    <div class="square" id="square18"></div>
		    <div class="square" id="square19"></div>
		</div>
		<div id="logotext">Mozaic</div>
	</div>

	<div id="copy">
		<p><span style="font-weight: bold;">Mozaic Accounts</span> is a simple web service that cuts the chore of running a record label, letting you concentrate on the important stuff - the music.</p>
		<p>For a small monthly fee you get access to a simple but powerful royalty accounting system designed for both physical and digital distribution that lets you:</p>
		<ul>
			<li>Upload sales sheets directly from your distributor or retailer</li>
			<li>Set up royalty share, net receipt or remix split deals</li>
			<li>Keep track of licensing agreements and expenses</li>
			<li>One click export of artist royalty statements in PDF format</li>
		</ul>
		<p>Interested in a free trial? Then <a style="color:black" href="mailto:support@mozaicaccounts.co.uk">get in touch</a></p>
	</div>
	
	<div id="login">
		<form name="loginform" id="loginform" action="processlogin.php" method="post">
			<label>username</label><input type="text" name="userid"/>
			<label>password</label><input type="password" name="password"/>
			<input type="hidden" name="response"/>
			<input class="button" type="submit" onclick="login(); return false;" value="Login">
		</form>
		<div id="pulldown">member login</div>
	</div>
</div>

</div> <!--wrapper-->

<script type="text/javascript">
	(function ($, window, document) {
		$(function() {
			$("#pulldown").bind("click", function(event) { 
				$("#login").animate({top: "0"},500);
			});
		});
	}(window.jQuery, window, document));
</script>
</body>
</html>
