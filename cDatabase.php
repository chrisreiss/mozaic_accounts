<?php

// change these for the different hosts
if ($_SERVER['SERVER_NAME'] == "mozaicaccounts.co.uk" 
	|| $_SERVER['SERVER_NAME'] == "www.mozaicaccounts.co.uk" 
	|| $_SERVER['SERVER_NAME'] == "cyphon.com")
{
	define("HOST","localhost");
	define("DATABASE","mozaic");
	define("USER","website");
	define("PASSWORD",">B8fTI2FJ$17T&%IXL,a");
}
/*else if ($_SERVER['SERVER_NAME'] == "www.mozaicaccounts.com" || $_SERVER['SERVER_NAME'] == "mozaicaccounts.com")
{
	define("HOST","localhost");
	define("DATABASE","szirtest_mozaic");
	define("USER","szirtest_www");
	define("PASSWORD","Nx5g7(8w19.D");
}*/
else
{
	define("HOST","localhost");
	define("DATABASE","mozaic");
	define("USER","homestead");
	define("PASSWORD","secret");
}

define("ADMINUSER","mozaic");
define("ADMINPASSWORD","PK2rL{d4bbiKDv");


class DatabaseResult
{
	var $m_result;
	
	function DatabaseResult($result)
	{
		$this->m_result = $result;
	}
	
	function GetNum()
	{
		if ($this->m_result==false)
		{
			return 0;
		}
		
		return $this->m_result->rowCount();
	}
	
	function GetNext()
	{
		return $this->m_result->fetch(PDO::FETCH_ASSOC);
	}

	function GetNextRow()
	{
		return $this->m_result->fetch(PDO::FETCH_ASSOC);
	}
	
	function Get($index)
	{
		return $this->m_result->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_ABS, $index);
	}
};



$db = null;
$db_dbSelected = false;
$db_results = false;
$db_numopenqueries = 0;


class Database
{
	
	static function Init()
	{
		global $db_numopenqueries;
		Database::Connect();
		$db_numopenqueries = 0;
	}
	

	// Wraps a database connection request
	static function Connect()
	{
		$args = func_get_args();
		if (empty($args[0])) 
			$database = "mozaic";
		else
			$database = $args[0];
		
		global $db;
		global $db_dbSelected;

		try {
			$db = new PDO('mysql:host=localhost;dbname='.$database.';charset=utf8',USER,PASSWORD);
		} catch (PDOException $ex){
			die("An error occurred :".$ex->getMessage());
		}
	}

	static function CheckConnect(){
		global $db;
		if ($db==null){
			Database::Connect();
		}
	}

	static function QueryParam($query)
	{
		//$query = func_get_arg(0);
		for ($i=1;$i<func_num_args();++$i)
		{
			$count = 0;
			$varl = "'".(string)func_get_arg($i)."'";
			$query = str_replace("%".strval($i),$varl,$query,$count);
			if ($count==0) break;
		}
	//	print $query."<br>";
		return Database::QueryGetResults($query);
	}

	// This is not slash safe!
	static function Query($query)
	{
		global $db;
		Database::CheckConnect();
		// check there is only one semi colon - to avoid hacking
		if (substr_count($query,";")>1)
		{
			$query = substr($query,0,strpos($query,";"));
			Trace(1,"Removed multiple query");
		}

		$result = $db->query($query);
		if ($result==false)
		{
			Trace(1,$query);
			return new DatabaseResult(false);
		}
		return (new DatabaseResult($result));
	}
	
	// Return the first value
	static function QueryGetValue($field,$table,$condition)
	{
		$result = Database::QueryGetResult("SELECT $field FROM $table WHERE ".$condition);
		return $result[$field];
	}
		// This is not slash safe!
	static function QueryGetResults($query)
	{
		global $db;
		Database::CheckConnect();
		// check there is only one semi colon - to avoid hacking
		if (substr_count($query,";")>1)
		{
			$query = substr($query,0,strpos($query,";"));
			Trace(1,"Removed multiple query");
		}
//		print "QUERY : ".$query."<br>";
		$result = $db->query($query);
		if ($result==false)
		{
			Trace(1,$query);
			return false;
		}
		
		//return $result->fetchAll(PDO::FETCH_ASSOC);

		$numresult = $result->rowCount(); 

		for ($i=0;$i<$numresult;++$i)
		{
			$results[] = $result->fetch(PDO::FETCH_ASSOC);
		}
		//mysql_free_result($result);
		if ($numresult!=false)
			return ($results);
	}

	static function QueryGetSingleResults($query)
	{
		$results = Database::QueryGetResults($query);
		if (!empty($results))
		{
			foreach($results as $row)
			{
				$arr[] = $row[key($row)];
			}
			return $arr;
		}
	}
	
	static function QueryGetResult($query)
	{
		$results = Database::QueryGetResults($query);
		if ($results==false)
			return false;
		return $results[0];
	}

	
	static function FindUID($table,$searchcol,$searchvalue)
	{
		$result = Database::Query("SELECT uid FROM ".$table." WHERE ".$searchcol." = '".$searchvalue."';");
		if ($result->GetNum()==0)
		{
			return false;
		}
		$row = $result->GetNext();
		Database::FinishQuery($result);
		return $row['uid'];
	}
	
	static function GetLinkID()
	{
		global $db_linkID;
		// connect if not connected
		if (!isset($db_linkID) || $db_linkID==false)
		{
			return false;
		}
		return $db_linkID;
	}
	
	static function FinishQuery(&$result)
	{
		mysql_free_result($result->m_result);
		unset ($result);
	}
	
	static function Validate($string)
	{
		global $db;

		if ($string=="") return $string;
		
        if(get_magic_quotes_gpc()) 
		{
            $string = stripslashes($string);
  		}
		
//		$string = mysql_real_escape_string($string);	
		return $string;
	}
	
	static function LastInsertId(){
		return $db->lastInsertId();
	}

	static function Disconnect()
	{
		global $db;
		$db = null;
	}
	
}
?>
